Module Resolution
    Public Const DISP_CHANGE_SUCCESSFUL As Short = 0
    Public Const DISP_CHANGE_RESTART As Short = 1
    Public Const DISP_CHANGE_FAILED As Short = -1
    Public Const CDS_UPDATEREGISTRY As Short = 1
    Public Const DM_BITSPERPEL As Integer = &H40000
    Public Const DM_PELSHEIGHT As Integer = &H100000
    Public Const DM_PELSWIDTH As Integer = &H80000
    Const BITSPIXEL As Short = 12

    Public Structure DevMode
        <VBFixedString(32), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=32)> Public dmDeviceName As String
        Dim dmSpecVersion As Short
        Dim dmDriverVersion As Short
        Dim dmSize As Short
        Dim dmDriverExtra As Short
        Dim dmFields As Integer
        Dim dmOrientation As Short
        Dim dmPaperSize As Short
        Dim dmPaperLength As Short
        Dim dmPaperWidth As Short
        Dim dmScale As Short
        Dim dmCopies As Short
        Dim dmDefaultSource As Short
        Dim dmPrintQuality As Short
        Dim dmColor As Short
        Dim dmDuplex As Short
        Dim dmYResolution As Short
        Dim dmTTOption As Short
        Dim dmCollate As Short
        <VBFixedString(32), System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.ByValTStr, SizeConst:=32)> Public dmFormName As String
        Dim dmLogPixels As Short
        Dim dmBitsPerPel As Integer
        Dim dmPelsWidth As Integer
        Dim dmPelsHeight As Integer
        Dim dmDisplayFlags As Integer
        Dim dmDisplayFrequency As Integer
        Dim dmICMMethod As Integer ' Windows 95 only
        Dim dmICMIntent As Integer ' Windows 95 only
        Dim dmMediaType As Integer ' Windows 95 only
        Dim dmDitherType As Integer ' Windows 95 only
        Dim dmICCManufacturer As Integer ' Windows 95 only
        Dim dmICCModel As Integer ' Windows 95 only
        Dim dmPanningWidth As Integer ' Windows 95 only
        Dim dmPanningHeight As Integer ' Windows 95 only
    End Structure

    'UPGRADE_WARNING: Structure DevMode may require marshalling attributes to be passed as an argument in this Declare statement. Click for more: �ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword = "vbup1050"�
    Public Declare Function ChangeDisplaySettings Lib "user32" Alias "ChangeDisplaySettingsA" (ByRef lpDevMode As DevMode, ByVal dwFlags As Integer) As Integer


    Public Declare Function GetDeviceCaps Lib "gdi32" (ByVal hdc As Integer, ByVal nIndex As Integer) As Integer

    'UPGRADE_ISSUE: Declaring a parameter �As Any� is not supported. Click for more: �ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword = "vbup1016"�
    Public Declare Function CreateDC Lib "gdi32" Alias "CreateDCA" (ByVal lpDriverName As String, ByVal lpDeviceName As String, ByVal lpOutput As String, ByVal lpInitData As String) As Integer



    Public ScreenWidth As Object
    Public ScreenHeight As Short
    Public X As Object

    Public Y As Short
    Public intOldBITSPIXEL As Integer
    Dim I As Short

    Public Sub ChangeIt(ByRef lPelsWidth As Integer, ByRef lPelsHeight As Integer, ByRef lBitsPerPel As Integer)

        Dim utDevMode As DevMode
        Dim iRes As Integer
        Dim sMsg As String

        With utDevMode
            .dmSize = Len(utDevMode)
            .dmPelsWidth = lPelsWidth
            .dmPelsHeight = lPelsHeight
            .dmBitsPerPel = lBitsPerPel
            .dmFields = DM_BITSPERPEL Or DM_PELSHEIGHT Or DM_PELSWIDTH
        End With

        iRes = ChangeDisplaySettings(utDevMode, CDS_UPDATEREGISTRY)


        Select Case iRes
            Case Is = DISP_CHANGE_SUCCESSFUL
                'sMsg = "Display setting has been changed successfully."
            Case Is = DISP_CHANGE_RESTART
                sMsg = "You have to restart your computer in order to carry out the new setting."
            Case Is = DISP_CHANGE_FAILED
                sMsg = "Sorry� failed to change the display setting."
        End Select

        If sMsg <> vbNullString Then MsgBox(sMsg, MsgBoxStyle.DefaultButton1, "Display")

    End Sub
End Module
