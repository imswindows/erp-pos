﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SaudiMarketPOS
{
    public partial class MainMenu : Form
    {
        public MainMenu()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        

        private void btnReports_Click(object sender, EventArgs e)
        {

        }

        private void MainMenu_Load(object sender, EventArgs e)
        {
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            try
            {
                lblCashirName.Text = Program.fristName.ToString() + " " + Program.lastName.ToString();
                if (Program.roleID == 3)
                {
                    btnChangePass.Enabled = false;
                    btnOptions.Enabled = false;
                    btnReports.Enabled = false;
                    btnCloseShift.Enabled = false;
                }
                if (Program.roleID == 4)
                {
                    btnOptions.Enabled = false;
                    btnReports.Enabled = false;
                    btnCloseShift.Enabled = false;
                }
                if (Program.roleID == 2)
                {
                    btnOptions.Enabled = false;
                    btnReports.Enabled = false;
                    btnChangePass.Enabled = false;
                }
            }
            catch(Exception ex)
            {}
            
        }
       
        private void btnOptions_Click(object sender, EventArgs e)
        {
            //frmOptions options = new frmOptions(this);
            //this.Hide();
            //options.ShowDialog(this);
        }

       

        private void btnCloseShift_Click(object sender, EventArgs e)
        {
            CloseShifts cl = new CloseShifts();
            cl.ShowDialog();
        }

        private void btnChangePass_Click(object sender, EventArgs e)
        {
            ChangePassword chnPass = new ChangePassword();
            chnPass.ShowDialog();
        }

        private void btnBarcodePrinting_Click(object sender, EventArgs e)
        {
            BarcodeAuto frm = new BarcodeAuto();
            frm.ShowDialog();
        }

        private void MainMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void btnOldShifts_Click(object sender, EventArgs e)
        {
            OldShifts frm = new OldShifts();
            frm.ShowDialog();
        }

        private void deletedOrders_Click(object sender, EventArgs e)
        {
            DeletedOrders frm = new DeletedOrders();
            frm.ShowDialog();
        }
    }
}
