﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SaudiMarketPOS.Properties;
namespace SaudiMarketPOS
{
    public partial class ReturnTransaction : Form
    {
        public ReturnTransaction()
        {
            InitializeComponent();
        }

        private void ReturnTransaction_Load(object sender, EventArgs e)
        {
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pictureBox1.Image = Image.FromFile(apppath + "Icon.ico");
        }
    }
}
