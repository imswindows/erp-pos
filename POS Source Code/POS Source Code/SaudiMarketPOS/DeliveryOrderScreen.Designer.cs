﻿namespace SaudiMarketPOS
{
    partial class DeliveryOrderScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pbImsLogo = new System.Windows.Forms.PictureBox();
            this.pbXreca = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblCashir = new System.Windows.Forms.Label();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbCustomerName = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbDeliveryEmp = new System.Windows.Forms.ComboBox();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvDeliveryOrder = new System.Windows.Forms.DataGridView();
            this.label9 = new System.Windows.Forms.Label();
            this.txtInvTotal = new System.Windows.Forms.TextBox();
            this.txtDiscountPercent = new System.Windows.Forms.TextBox();
            this.txtCashDiscount = new System.Windows.Forms.TextBox();
            this.chkCredit = new System.Windows.Forms.CheckBox();
            this.txtCredit = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDueAmount = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPaid = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtChange = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCreditTransactionNum = new System.Windows.Forms.TextBox();
            this.chkCreditCard = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtItemBarcode = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label19 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.lblWebDiscounts = new System.Windows.Forms.Label();
            this.deliveryFees = new System.Windows.Forms.TextBox();
            this.txtWebDiscounts = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.btnCancelInv = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.dgvWebOrders = new System.Windows.Forms.DataGridView();
            this.UncollectedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OrderNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedCustomerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedCustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedCustomerData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedPaymentMethodID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedPaymentMethod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedNotes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedWebObj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tWebOrders = new System.Windows.Forms.Timer(this.components);
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXreca)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliveryOrder)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWebOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel6.Controls.Add(this.pbImsLogo);
            this.panel6.Controls.Add(this.pbXreca);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Location = new System.Drawing.Point(197, 3);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(736, 58);
            this.panel6.TabIndex = 24;
            // 
            // pbImsLogo
            // 
            this.pbImsLogo.ErrorImage = null;
            this.pbImsLogo.Location = new System.Drawing.Point(15, 9);
            this.pbImsLogo.Name = "pbImsLogo";
            this.pbImsLogo.Size = new System.Drawing.Size(90, 37);
            this.pbImsLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImsLogo.TabIndex = 18;
            this.pbImsLogo.TabStop = false;
            // 
            // pbXreca
            // 
            this.pbXreca.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbXreca.ErrorImage = null;
            this.pbXreca.Location = new System.Drawing.Point(605, 14);
            this.pbXreca.Name = "pbXreca";
            this.pbXreca.Size = new System.Drawing.Size(120, 27);
            this.pbXreca.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbXreca.TabIndex = 17;
            this.pbXreca.TabStop = false;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(301, 12);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 29);
            this.label4.TabIndex = 13;
            this.label4.Text = "أوامر التوصيل";
            // 
            // lblCashir
            // 
            this.lblCashir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCashir.AutoSize = true;
            this.lblCashir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lblCashir.ForeColor = System.Drawing.Color.White;
            this.lblCashir.Location = new System.Drawing.Point(543, 3);
            this.lblCashir.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCashir.Name = "lblCashir";
            this.lblCashir.Size = new System.Drawing.Size(41, 13);
            this.lblCashir.TabIndex = 5;
            this.lblCashir.Text = "label3";
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lblDateTime.ForeColor = System.Drawing.Color.White;
            this.lblDateTime.Location = new System.Drawing.Point(5, 3);
            this.lblDateTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDateTime.Size = new System.Drawing.Size(41, 13);
            this.lblDateTime.TabIndex = 2;
            this.lblDateTime.Text = "label3";
            this.lblDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel2.Controls.Add(this.cmbCustomerName);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.txtCustomerID);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(361, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(375, 45);
            this.panel2.TabIndex = 27;
            // 
            // cmbCustomerName
            // 
            this.cmbCustomerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbCustomerName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCustomerName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCustomerName.DisplayMember = "FirstName";
            this.cmbCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.cmbCustomerName.FormattingEnabled = true;
            this.cmbCustomerName.Location = new System.Drawing.Point(2, 10);
            this.cmbCustomerName.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCustomerName.Name = "cmbCustomerName";
            this.cmbCustomerName.Size = new System.Drawing.Size(158, 21);
            this.cmbCustomerName.TabIndex = 1;
            this.cmbCustomerName.ValueMember = "ID";
            this.cmbCustomerName.SelectedIndexChanged += new System.EventHandler(this.cmbCustomerName_SelectedIndexChanged);
            this.cmbCustomerName.Leave += new System.EventHandler(this.cmbCustomerName_Leave);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(161, 13);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "اسم العميل";
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.txtCustomerID.Location = new System.Drawing.Point(223, 10);
            this.txtCustomerID.Margin = new System.Windows.Forms.Padding(2);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new System.Drawing.Size(86, 20);
            this.txtCustomerID.TabIndex = 0;
            this.txtCustomerID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCustomerID.Click += new System.EventHandler(this.selectTxt);
            this.txtCustomerID.TextChanged += new System.EventHandler(this.txtCustomerID_TextChanged);
            this.txtCustomerID.Enter += new System.EventHandler(this.selectTxt);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(309, 13);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "رقم ح العميل";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.cmbDeliveryEmp);
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(357, 45);
            this.panel3.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(2, 11);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 17);
            this.label8.TabIndex = 29;
            this.label8.Text = "[ F4 ]";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(278, 13);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 28;
            this.label1.Text = "مندوب التوصيل";
            // 
            // cmbDeliveryEmp
            // 
            this.cmbDeliveryEmp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDeliveryEmp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDeliveryEmp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDeliveryEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.cmbDeliveryEmp.FormattingEnabled = true;
            this.cmbDeliveryEmp.Location = new System.Drawing.Point(45, 10);
            this.cmbDeliveryEmp.Margin = new System.Windows.Forms.Padding(2);
            this.cmbDeliveryEmp.Name = "cmbDeliveryEmp";
            this.cmbDeliveryEmp.Size = new System.Drawing.Size(229, 21);
            this.cmbDeliveryEmp.TabIndex = 2;
            this.cmbDeliveryEmp.Leave += new System.EventHandler(this.cmbDeliveryEmp_Leave);
            // 
            // Total
            // 
            this.Total.FillWeight = 61.17462F;
            this.Total.HeaderText = "الاجمالي";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // ItemQty
            // 
            this.ItemQty.FillWeight = 58.28725F;
            this.ItemQty.HeaderText = "الكمية";
            this.ItemQty.Name = "ItemQty";
            // 
            // ItemPrice
            // 
            this.ItemPrice.FillWeight = 54.95871F;
            this.ItemPrice.HeaderText = "السعر";
            this.ItemPrice.Name = "ItemPrice";
            this.ItemPrice.ReadOnly = true;
            // 
            // ItemUnit
            // 
            this.ItemUnit.FillWeight = 51.12155F;
            this.ItemUnit.HeaderText = "الوحدة";
            this.ItemUnit.Name = "ItemUnit";
            this.ItemUnit.ReadOnly = true;
            // 
            // ItemName
            // 
            this.ItemName.FillWeight = 241.91F;
            this.ItemName.HeaderText = "اسم الصنف";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // ItemBarcode
            // 
            this.ItemBarcode.FillWeight = 132.5479F;
            this.ItemBarcode.HeaderText = "رقم الصنف";
            this.ItemBarcode.Name = "ItemBarcode";
            // 
            // ItemID
            // 
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.Visible = false;
            // 
            // dgvDeliveryOrder
            // 
            this.dgvDeliveryOrder.AllowUserToAddRows = false;
            this.dgvDeliveryOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDeliveryOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvDeliveryOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvDeliveryOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvDeliveryOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeliveryOrder.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemID,
            this.ItemBarcode,
            this.ItemName,
            this.ItemUnit,
            this.ItemPrice,
            this.ItemQty,
            this.Total});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvDeliveryOrder.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvDeliveryOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDeliveryOrder.EnableHeadersVisualStyles = false;
            this.dgvDeliveryOrder.Location = new System.Drawing.Point(0, 0);
            this.dgvDeliveryOrder.Margin = new System.Windows.Forms.Padding(2);
            this.dgvDeliveryOrder.MultiSelect = false;
            this.dgvDeliveryOrder.Name = "dgvDeliveryOrder";
            this.dgvDeliveryOrder.RowHeadersVisible = false;
            this.dgvDeliveryOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDeliveryOrder.Size = new System.Drawing.Size(735, 457);
            this.dgvDeliveryOrder.TabIndex = 3;
            this.dgvDeliveryOrder.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDeliveryOrder_CellEndEdit);
            this.dgvDeliveryOrder.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDeliveryOrder_RowEnter);
            this.dgvDeliveryOrder.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvDeliveryOrder_RowsAdded);
            this.dgvDeliveryOrder.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvDeliveryOrder_UserDeletedRow_1);
            this.dgvDeliveryOrder.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvDeliveryOrder_UserDeletingRow);
            this.dgvDeliveryOrder.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvDeliveryOrder_KeyDown);
            this.dgvDeliveryOrder.Validated += new System.EventHandler(this.dgvDeliveryOrder_Validated);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label9.Location = new System.Drawing.Point(63, 5);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 20);
            this.label9.TabIndex = 4;
            this.label9.Text = "الاجمالي";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // txtInvTotal
            // 
            this.txtInvTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtInvTotal.Location = new System.Drawing.Point(15, 32);
            this.txtInvTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtInvTotal.Name = "txtInvTotal";
            this.txtInvTotal.ReadOnly = true;
            this.txtInvTotal.Size = new System.Drawing.Size(155, 26);
            this.txtInvTotal.TabIndex = 5;
            this.txtInvTotal.Text = "0,00";
            this.txtInvTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtInvTotal.Click += new System.EventHandler(this.selectTxt);
            this.txtInvTotal.Enter += new System.EventHandler(this.selectTxt);
            // 
            // txtDiscountPercent
            // 
            this.txtDiscountPercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiscountPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.txtDiscountPercent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtDiscountPercent.Location = new System.Drawing.Point(133, 25);
            this.txtDiscountPercent.Margin = new System.Windows.Forms.Padding(2);
            this.txtDiscountPercent.Name = "txtDiscountPercent";
            this.txtDiscountPercent.Size = new System.Drawing.Size(118, 35);
            this.txtDiscountPercent.TabIndex = 6;
            this.txtDiscountPercent.Text = "0,00";
            this.txtDiscountPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDiscountPercent.Click += new System.EventHandler(this.selectTxt);
            this.txtDiscountPercent.Enter += new System.EventHandler(this.selectTxt);
            this.txtDiscountPercent.Leave += new System.EventHandler(this.txtDiscountPercent_Leave);
            // 
            // txtCashDiscount
            // 
            this.txtCashDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCashDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.txtCashDiscount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCashDiscount.Location = new System.Drawing.Point(11, 25);
            this.txtCashDiscount.Margin = new System.Windows.Forms.Padding(2);
            this.txtCashDiscount.Name = "txtCashDiscount";
            this.txtCashDiscount.Size = new System.Drawing.Size(118, 35);
            this.txtCashDiscount.TabIndex = 7;
            this.txtCashDiscount.Text = "0,00";
            this.txtCashDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCashDiscount.Leave += new System.EventHandler(this.txtCashDiscount_Leave);
            // 
            // chkCredit
            // 
            this.chkCredit.AutoSize = true;
            this.chkCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.chkCredit.Location = new System.Drawing.Point(194, 169);
            this.chkCredit.Margin = new System.Windows.Forms.Padding(2);
            this.chkCredit.Name = "chkCredit";
            this.chkCredit.Size = new System.Drawing.Size(51, 24);
            this.chkCredit.TabIndex = 8;
            this.chkCredit.Text = "اجل";
            this.chkCredit.UseVisualStyleBackColor = true;
            this.chkCredit.CheckedChanged += new System.EventHandler(this.chkCredit_CheckedChanged);
            // 
            // txtCredit
            // 
            this.txtCredit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCredit.Enabled = false;
            this.txtCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.txtCredit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCredit.Location = new System.Drawing.Point(11, 159);
            this.txtCredit.Margin = new System.Windows.Forms.Padding(2);
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.Size = new System.Drawing.Size(153, 35);
            this.txtCredit.TabIndex = 9;
            this.txtCredit.Text = "0,00";
            this.txtCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel4.Controls.Add(this.label14);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.label10);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.lblCashir);
            this.panel4.Controls.Add(this.lblDateTime);
            this.panel4.Location = new System.Drawing.Point(197, 622);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(736, 24);
            this.panel4.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label14.Location = new System.Drawing.Point(145, 3);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 13);
            this.label14.TabIndex = 52;
            this.label14.Text = "التوقيت";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::SaudiMarketPOS.Properties.Resources.time;
            this.pictureBox3.Location = new System.Drawing.Point(187, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // label10
            // 
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(204, -3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 29);
            this.label10.TabIndex = 51;
            this.label10.Text = "|";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::SaudiMarketPOS.Properties.Resources.Cahsier;
            this.pictureBox2.Location = new System.Drawing.Point(719, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label3.Location = new System.Drawing.Point(677, 3);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "الكاشير";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label5.Location = new System.Drawing.Point(61, 186);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "ملاحظات";
            // 
            // txtNotes
            // 
            this.txtNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtNotes.Location = new System.Drawing.Point(15, 209);
            this.txtNotes.Multiline = true;
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(155, 46);
            this.txtNotes.TabIndex = 6;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label13.Location = new System.Drawing.Point(15, 61);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(157, 31);
            this.label13.TabIndex = 4;
            this.label13.Text = "المطلوب سداده";
            // 
            // txtDueAmount
            // 
            this.txtDueAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.txtDueAmount.Location = new System.Drawing.Point(15, 95);
            this.txtDueAmount.Margin = new System.Windows.Forms.Padding(2);
            this.txtDueAmount.Name = "txtDueAmount";
            this.txtDueAmount.ReadOnly = true;
            this.txtDueAmount.Size = new System.Drawing.Size(155, 38);
            this.txtDueAmount.TabIndex = 10;
            this.txtDueAmount.Text = "0,00";
            this.txtDueAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDueAmount.TextChanged += new System.EventHandler(this.txtDueAmount_TextChanged_1);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label12.Location = new System.Drawing.Point(41, 324);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 20);
            this.label12.TabIndex = 6;
            this.label12.Text = "المدفوع  (F5)";
            this.label12.Visible = false;
            // 
            // txtPaid
            // 
            this.txtPaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtPaid.Location = new System.Drawing.Point(15, 348);
            this.txtPaid.Margin = new System.Windows.Forms.Padding(2);
            this.txtPaid.Name = "txtPaid";
            this.txtPaid.Size = new System.Drawing.Size(155, 26);
            this.txtPaid.TabIndex = 11;
            this.txtPaid.Text = "0,00";
            this.txtPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPaid.Visible = false;
            this.txtPaid.TextChanged += new System.EventHandler(this.txtPaid_TextChanged);
            this.txtPaid.Leave += new System.EventHandler(this.txtPaid_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label11.Location = new System.Drawing.Point(61, 390);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 20);
            this.label11.TabIndex = 8;
            this.label11.Text = "المتبقى";
            this.label11.Visible = false;
            // 
            // txtChange
            // 
            this.txtChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtChange.Location = new System.Drawing.Point(15, 417);
            this.txtChange.Margin = new System.Windows.Forms.Padding(2);
            this.txtChange.Name = "txtChange";
            this.txtChange.ReadOnly = true;
            this.txtChange.Size = new System.Drawing.Size(155, 26);
            this.txtChange.TabIndex = 12;
            this.txtChange.Text = "0,00";
            this.txtChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtChange.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(168, 118);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "رقم العمليه";
            // 
            // txtCreditTransactionNum
            // 
            this.txtCreditTransactionNum.Enabled = false;
            this.txtCreditTransactionNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.txtCreditTransactionNum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCreditTransactionNum.Location = new System.Drawing.Point(11, 107);
            this.txtCreditTransactionNum.Margin = new System.Windows.Forms.Padding(2);
            this.txtCreditTransactionNum.Name = "txtCreditTransactionNum";
            this.txtCreditTransactionNum.Size = new System.Drawing.Size(153, 35);
            this.txtCreditTransactionNum.TabIndex = 14;
            this.txtCreditTransactionNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCreditTransactionNum.TextChanged += new System.EventHandler(this.txtCreditTransactionNum_TextChanged);
            // 
            // chkCreditCard
            // 
            this.chkCreditCard.AutoSize = true;
            this.chkCreditCard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.chkCreditCard.Location = new System.Drawing.Point(62, 77);
            this.chkCreditCard.Margin = new System.Windows.Forms.Padding(2);
            this.chkCreditCard.Name = "chkCreditCard";
            this.chkCreditCard.Size = new System.Drawing.Size(176, 24);
            this.chkCreditCard.TabIndex = 13;
            this.chkCreditCard.Text = "دفع بواسطة بطاقة ائتمان";
            this.chkCreditCard.UseVisualStyleBackColor = true;
            this.chkCreditCard.CheckedChanged += new System.EventHandler(this.chkCreditCard_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Silver;
            this.panel1.Controls.Add(this.txtCreditTransactionNum);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.txtCredit);
            this.panel1.Controls.Add(this.chkCredit);
            this.panel1.Controls.Add(this.txtCashDiscount);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtDiscountPercent);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.chkCreditCard);
            this.panel1.Location = new System.Drawing.Point(938, 185);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(259, 214);
            this.panel1.TabIndex = 31;
            this.panel1.Visible = false;
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Silver;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(179, 1);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(24, 20);
            this.label20.TabIndex = 6;
            this.label20.Text = "%";
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Silver;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(45, 1);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(37, 20);
            this.label18.TabIndex = 8;
            this.label18.Text = "نقدي";
            // 
            // txtItemBarcode
            // 
            this.txtItemBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtItemBarcode.BackColor = System.Drawing.Color.DimGray;
            this.txtItemBarcode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtItemBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.txtItemBarcode.ForeColor = System.Drawing.Color.White;
            this.txtItemBarcode.Location = new System.Drawing.Point(744, 592);
            this.txtItemBarcode.Margin = new System.Windows.Forms.Padding(2);
            this.txtItemBarcode.Name = "txtItemBarcode";
            this.txtItemBarcode.Size = new System.Drawing.Size(189, 17);
            this.txtItemBarcode.TabIndex = 4;
            this.txtItemBarcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtItemBarcode.TextChanged += new System.EventHandler(this.txtItemBarcode_TextChanged);
            this.txtItemBarcode.Enter += new System.EventHandler(this.selectTxt);
            this.txtItemBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemBarcode_KeyDown);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(197, 595);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(97, 13);
            this.label19.TabIndex = 39;
            this.label19.Text = "F9 لتعديل الكمية";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.label22);
            this.panel5.Controls.Add(this.lblWebDiscounts);
            this.panel5.Controls.Add(this.deliveryFees);
            this.panel5.Controls.Add(this.txtWebDiscounts);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.txtNotes);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.txtDueAmount);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.txtChange);
            this.panel5.Controls.Add(this.txtInvTotal);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.txtPaid);
            this.panel5.Location = new System.Drawing.Point(4, 3);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(189, 459);
            this.panel5.TabIndex = 40;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label22.Location = new System.Drawing.Point(100, 152);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(70, 20);
            this.label22.TabIndex = 13;
            this.label22.Text = "خ. توصيل";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // lblWebDiscounts
            // 
            this.lblWebDiscounts.AutoSize = true;
            this.lblWebDiscounts.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.lblWebDiscounts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.lblWebDiscounts.Location = new System.Drawing.Point(58, 263);
            this.lblWebDiscounts.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblWebDiscounts.Name = "lblWebDiscounts";
            this.lblWebDiscounts.Size = new System.Drawing.Size(73, 20);
            this.lblWebDiscounts.TabIndex = 13;
            this.lblWebDiscounts.Text = "الخصومات";
            this.lblWebDiscounts.Visible = false;
            // 
            // deliveryFees
            // 
            this.deliveryFees.Enabled = false;
            this.deliveryFees.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.deliveryFees.Location = new System.Drawing.Point(16, 149);
            this.deliveryFees.Margin = new System.Windows.Forms.Padding(2);
            this.deliveryFees.Name = "deliveryFees";
            this.deliveryFees.Size = new System.Drawing.Size(74, 26);
            this.deliveryFees.TabIndex = 14;
            this.deliveryFees.Text = "0,00";
            this.deliveryFees.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtWebDiscounts
            // 
            this.txtWebDiscounts.Enabled = false;
            this.txtWebDiscounts.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtWebDiscounts.Location = new System.Drawing.Point(15, 287);
            this.txtWebDiscounts.Margin = new System.Windows.Forms.Padding(2);
            this.txtWebDiscounts.Name = "txtWebDiscounts";
            this.txtWebDiscounts.Size = new System.Drawing.Size(155, 26);
            this.txtWebDiscounts.TabIndex = 14;
            this.txtWebDiscounts.Text = "0,00";
            this.txtWebDiscounts.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtWebDiscounts.Visible = false;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.Controls.Add(this.panel3);
            this.panel7.Controls.Add(this.panel2);
            this.panel7.Location = new System.Drawing.Point(197, 65);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(736, 45);
            this.panel7.TabIndex = 47;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label16.Location = new System.Drawing.Point(809, 112);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(147, 13);
            this.label16.TabIndex = 48;
            this.label16.Text = "[ F7 ]  لعرض قائمة العملاء";
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.dgvDeliveryOrder);
            this.panel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel8.Location = new System.Drawing.Point(197, 130);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(735, 457);
            this.panel8.TabIndex = 50;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(515, 595);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(286, 13);
            this.label21.TabIndex = 51;
            this.label21.Text = "أدخل رقم الصنف أو F6 لعرض قائمة الاصناف و البحث";
            // 
            // btnCancelInv
            // 
            this.btnCancelInv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelInv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnCancelInv.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.cancel;
            this.btnCancelInv.FlatAppearance.BorderSize = 0;
            this.btnCancelInv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelInv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.btnCancelInv.ForeColor = System.Drawing.Color.White;
            this.btnCancelInv.Location = new System.Drawing.Point(938, 94);
            this.btnCancelInv.Margin = new System.Windows.Forms.Padding(2);
            this.btnCancelInv.Name = "btnCancelInv";
            this.btnCancelInv.Size = new System.Drawing.Size(259, 87);
            this.btnCancelInv.TabIndex = 15;
            this.btnCancelInv.UseVisualStyleBackColor = false;
            this.btnCancelInv.Click += new System.EventHandler(this.btnCancelInv_Click);
            this.btnCancelInv.MouseEnter += new System.EventHandler(this.btnCancelInv_MouseEnter);
            this.btnCancelInv.MouseLeave += new System.EventHandler(this.btnCancelInv_MouseLeave);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.btnClose.BackgroundImage = global::SaudiMarketPOS.Properties.Resources._out;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Location = new System.Drawing.Point(938, 559);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(259, 87);
            this.btnClose.TabIndex = 17;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseEnter += new System.EventHandler(this.btnClose_MouseEnter);
            this.btnClose.MouseLeave += new System.EventHandler(this.btnClose_MouseLeave);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.btnPrint.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.print_green;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Location = new System.Drawing.Point(938, 3);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(259, 87);
            this.btnPrint.TabIndex = 16;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            this.btnPrint.MouseEnter += new System.EventHandler(this.btnPrint_MouseEnter);
            this.btnPrint.MouseLeave += new System.EventHandler(this.btnPrint_MouseLeave);
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel9.Controls.Add(this.dgvWebOrders);
            this.panel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel9.Location = new System.Drawing.Point(4, 492);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(189, 154);
            this.panel9.TabIndex = 52;
            // 
            // dgvWebOrders
            // 
            this.dgvWebOrders.AllowUserToAddRows = false;
            this.dgvWebOrders.AllowUserToDeleteRows = false;
            this.dgvWebOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvWebOrders.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvWebOrders.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvWebOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWebOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UncollectedID,
            this.OrderNumber,
            this.UncollectedCustomerID,
            this.UncollectedCustomerName,
            this.UncollectedCustomerData,
            this.UncollectedTotal,
            this.UncollectedPaymentMethodID,
            this.UncollectedPaymentMethod,
            this.UncollectedNotes,
            this.UncollectedWebObj});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvWebOrders.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvWebOrders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWebOrders.EnableHeadersVisualStyles = false;
            this.dgvWebOrders.Location = new System.Drawing.Point(0, 0);
            this.dgvWebOrders.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvWebOrders.MultiSelect = false;
            this.dgvWebOrders.Name = "dgvWebOrders";
            this.dgvWebOrders.ReadOnly = true;
            this.dgvWebOrders.RowHeadersVisible = false;
            this.dgvWebOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvWebOrders.Size = new System.Drawing.Size(189, 154);
            this.dgvWebOrders.TabIndex = 1;
            this.dgvWebOrders.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvWebOrders_CellDoubleClick);
            // 
            // UncollectedID
            // 
            this.UncollectedID.DataPropertyName = "ID";
            this.UncollectedID.HeaderText = "UncollectedID";
            this.UncollectedID.Name = "UncollectedID";
            this.UncollectedID.ReadOnly = true;
            this.UncollectedID.Visible = false;
            // 
            // OrderNumber
            // 
            this.OrderNumber.DataPropertyName = "Notes";
            this.OrderNumber.HeaderText = "رقم الطلب";
            this.OrderNumber.Name = "OrderNumber";
            this.OrderNumber.ReadOnly = true;
            // 
            // UncollectedCustomerID
            // 
            this.UncollectedCustomerID.DataPropertyName = "CustomerID";
            this.UncollectedCustomerID.HeaderText = "CustomerID";
            this.UncollectedCustomerID.Name = "UncollectedCustomerID";
            this.UncollectedCustomerID.ReadOnly = true;
            this.UncollectedCustomerID.Visible = false;
            // 
            // UncollectedCustomerName
            // 
            this.UncollectedCustomerName.DataPropertyName = "CustomerName";
            this.UncollectedCustomerName.HeaderText = "اسم العميل";
            this.UncollectedCustomerName.Name = "UncollectedCustomerName";
            this.UncollectedCustomerName.ReadOnly = true;
            this.UncollectedCustomerName.Visible = false;
            // 
            // UncollectedCustomerData
            // 
            this.UncollectedCustomerData.DataPropertyName = "CustomerData";
            this.UncollectedCustomerData.HeaderText = "بيانات العميل";
            this.UncollectedCustomerData.Name = "UncollectedCustomerData";
            this.UncollectedCustomerData.ReadOnly = true;
            this.UncollectedCustomerData.Visible = false;
            // 
            // UncollectedTotal
            // 
            this.UncollectedTotal.DataPropertyName = "Total";
            this.UncollectedTotal.HeaderText = "الاجمالي";
            this.UncollectedTotal.Name = "UncollectedTotal";
            this.UncollectedTotal.ReadOnly = true;
            // 
            // UncollectedPaymentMethodID
            // 
            this.UncollectedPaymentMethodID.DataPropertyName = "PaymentMethod";
            this.UncollectedPaymentMethodID.HeaderText = "PaymentMethodID";
            this.UncollectedPaymentMethodID.Name = "UncollectedPaymentMethodID";
            this.UncollectedPaymentMethodID.ReadOnly = true;
            this.UncollectedPaymentMethodID.Visible = false;
            // 
            // UncollectedPaymentMethod
            // 
            this.UncollectedPaymentMethod.DataPropertyName = "strPaymentMethod";
            this.UncollectedPaymentMethod.HeaderText = "طريقة الدفع";
            this.UncollectedPaymentMethod.Name = "UncollectedPaymentMethod";
            this.UncollectedPaymentMethod.ReadOnly = true;
            this.UncollectedPaymentMethod.Visible = false;
            // 
            // UncollectedNotes
            // 
            this.UncollectedNotes.DataPropertyName = "Notes";
            this.UncollectedNotes.HeaderText = "ملاحظات";
            this.UncollectedNotes.Name = "UncollectedNotes";
            this.UncollectedNotes.ReadOnly = true;
            this.UncollectedNotes.Visible = false;
            // 
            // UncollectedWebObj
            // 
            this.UncollectedWebObj.DataPropertyName = "WebObj";
            this.UncollectedWebObj.HeaderText = "WebObj";
            this.UncollectedWebObj.Name = "UncollectedWebObj";
            this.UncollectedWebObj.ReadOnly = true;
            this.UncollectedWebObj.Visible = false;
            // 
            // tWebOrders
            // 
            this.tWebOrders.Enabled = true;
            this.tWebOrders.Interval = 10000;
            this.tWebOrders.Tick += new System.EventHandler(this.tWebOrders_Tick);
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(100, 469);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(89, 17);
            this.label15.TabIndex = 53;
            this.label15.Text = "طلبات الانترنت";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label17.Location = new System.Drawing.Point(197, 112);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(183, 13);
            this.label17.TabIndex = 54;
            this.label17.Text = "[ Alt + E ]  للدفع عن طريق الفيزا";
            // 
            // DeliveryOrderScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 650);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.btnCancelInv);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.txtItemBarcode);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel8);
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "DeliveryOrderScreen";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Text = "شاشة اوامر التوصيل";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeliveryOrderScreen_FormClosing);
            this.Load += new System.EventHandler(this.DeliveryOrderScreen_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ctl_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.DeliveryOrderScreen_KeyPress);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbXreca)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeliveryOrder)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWebOrders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblCashir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox cmbCustomerName;
        public System.Windows.Forms.TextBox txtCustomerID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemID;
        private System.Windows.Forms.DataGridView dgvDeliveryOrder;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtInvTotal;
        private System.Windows.Forms.TextBox txtDiscountPercent;
        private System.Windows.Forms.TextBox txtCashDiscount;
        private System.Windows.Forms.CheckBox chkCredit;
        private System.Windows.Forms.TextBox txtCredit;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDueAmount;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPaid;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtChange;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCreditTransactionNum;
        private System.Windows.Forms.CheckBox chkCreditCard;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnCancelInv;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.TextBox txtItemBarcode;
        private System.Windows.Forms.ComboBox cmbDeliveryEmp;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pbXreca;
        private System.Windows.Forms.PictureBox pbImsLogo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblWebDiscounts;
        private System.Windows.Forms.TextBox txtWebDiscounts;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Timer tWebOrders;
        private System.Windows.Forms.DataGridView dgvWebOrders;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedID;
        private System.Windows.Forms.DataGridViewTextBoxColumn OrderNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedCustomerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedCustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedCustomerData;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedPaymentMethodID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedPaymentMethod;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedNotes;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedWebObj;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox deliveryFees;
    }
}