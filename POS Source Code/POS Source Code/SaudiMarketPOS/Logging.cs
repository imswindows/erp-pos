﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SaudiMarketPOS
{
    public class Logging
    {
        public static string LogFilePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\log\\";

        public static void DoLogging(string strMsg, string strMsgID, string strSource, bool isHours = true, string FileName = "")
        {
            string strLogFilePath = LogFilePath;

            if (isHours)
                strLogFilePath = strLogFilePath + FileName + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + ".txt";

            else
                strLogFilePath = strLogFilePath + FileName + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + ".txt";
            try
            {
                StreamWriter objStreamWriter;
                objStreamWriter = File.AppendText(strLogFilePath);
                objStreamWriter.WriteLine(DateTime.Now.ToString(@"dd/MM/yyyy hh:mm:ss tt") + "\t" + "\t" + strMsgID + "\t" + strMsg + " \t " + strSource);
                objStreamWriter.Flush();
                objStreamWriter.Close();
            }
            catch
            {
            }
            finally
            {
                //AppsettingsReader = null;
            }
        }
    }
}
