﻿namespace SaudiMarketPOS
{
    partial class CreditCardTransaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreditCardTransaction));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbImsLogo = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblCashir = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtInvNumber = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtVisaAmount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCreditTransNumber = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCashAmount = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pbImsLogo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.lblCashir);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblDateTime);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(690, 73);
            this.panel1.TabIndex = 8;
            // 
            // pbImsLogo
            // 
            this.pbImsLogo.ErrorImage = null;
            this.pbImsLogo.Location = new System.Drawing.Point(11, 11);
            this.pbImsLogo.Name = "pbImsLogo";
            this.pbImsLogo.Size = new System.Drawing.Size(90, 37);
            this.pbImsLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImsLogo.TabIndex = 15;
            this.pbImsLogo.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("STC Bold", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(206, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(333, 36);
            this.label2.TabIndex = 13;
            this.label2.Text = "تسجيل حركات بطاقات الائتمان";
            // 
            // lblCashir
            // 
            this.lblCashir.AutoSize = true;
            this.lblCashir.ForeColor = System.Drawing.Color.White;
            this.lblCashir.Location = new System.Drawing.Point(563, 48);
            this.lblCashir.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCashir.Name = "lblCashir";
            this.lblCashir.Size = new System.Drawing.Size(40, 18);
            this.lblCashir.TabIndex = 5;
            this.lblCashir.Text = "label3";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label1.Location = new System.Drawing.Point(644, 48);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "الكاشير";
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.ForeColor = System.Drawing.Color.White;
            this.lblDateTime.Location = new System.Drawing.Point(106, 48);
            this.lblDateTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDateTime.Size = new System.Drawing.Size(40, 18);
            this.lblDateTime.TabIndex = 2;
            this.lblDateTime.Text = "label3";
            this.lblDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label4.Location = new System.Drawing.Point(258, 48);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 18);
            this.label4.TabIndex = 3;
            this.label4.Text = "التاريخ";
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.txtCashAmount);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.txtNotes);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.txtInvNumber);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.txtVisaAmount);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.txtCreditTransNumber);
            this.panel3.Location = new System.Drawing.Point(56, 91);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(578, 101);
            this.panel3.TabIndex = 0;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(437, 104);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 18);
            this.label8.TabIndex = 14;
            this.label8.Text = "ادخل رقم البون";
            this.label8.Visible = false;
            // 
            // txtNotes
            // 
            this.txtNotes.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtNotes.Location = new System.Drawing.Point(80, 68);
            this.txtNotes.Margin = new System.Windows.Forms.Padding(2);
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(342, 29);
            this.txtNotes.TabIndex = 3;
            this.txtNotes.Enter += new System.EventHandler(this.selectTxt);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(468, 71);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 18);
            this.label5.TabIndex = 12;
            this.label5.Text = "ملاحظات";
            // 
            // txtInvNumber
            // 
            this.txtInvNumber.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtInvNumber.Location = new System.Drawing.Point(80, 101);
            this.txtInvNumber.Margin = new System.Windows.Forms.Padding(2);
            this.txtInvNumber.Name = "txtInvNumber";
            this.txtInvNumber.Size = new System.Drawing.Size(342, 29);
            this.txtInvNumber.TabIndex = 0;
            this.txtInvNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtInvNumber.Visible = false;
            this.txtInvNumber.TextChanged += new System.EventHandler(this.txtInvNumber_TextChanged);
            this.txtInvNumber.Leave += new System.EventHandler(this.txtInvNumber_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(482, 5);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 18);
            this.label3.TabIndex = 11;
            this.label3.Text = "المبلغ";
            // 
            // txtVisaAmount
            // 
            this.txtVisaAmount.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtVisaAmount.Location = new System.Drawing.Point(273, 2);
            this.txtVisaAmount.Margin = new System.Windows.Forms.Padding(2);
            this.txtVisaAmount.Name = "txtVisaAmount";
            this.txtVisaAmount.Size = new System.Drawing.Size(149, 29);
            this.txtVisaAmount.TabIndex = 0;
            this.txtVisaAmount.Text = "0.00";
            this.txtVisaAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtVisaAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            this.txtVisaAmount.Enter += new System.EventHandler(this.selectTxt);
            this.txtVisaAmount.Leave += new System.EventHandler(this.txtAmount_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(426, 38);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 18);
            this.label6.TabIndex = 9;
            this.label6.Text = "رقم حركة البطاقة";
            // 
            // txtCreditTransNumber
            // 
            this.txtCreditTransNumber.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtCreditTransNumber.Location = new System.Drawing.Point(80, 35);
            this.txtCreditTransNumber.Margin = new System.Windows.Forms.Padding(2);
            this.txtCreditTransNumber.Name = "txtCreditTransNumber";
            this.txtCreditTransNumber.Size = new System.Drawing.Size(342, 29);
            this.txtCreditTransNumber.TabIndex = 2;
            this.txtCreditTransNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCreditTransNumber.TextChanged += new System.EventHandler(this.txtCreditTransNumber_TextChanged);
            this.txtCreditTransNumber.Enter += new System.EventHandler(this.selectTxt);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(471, 203);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(163, 55);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "تسجيل العملية";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(56, 203);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(163, 55);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "إغلاق (ESC)";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(426, 5);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 18);
            this.label7.TabIndex = 15;
            this.label7.Text = "فيزا";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(236, 5);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(36, 18);
            this.label9.TabIndex = 17;
            this.label9.Text = "نقدي";
            // 
            // txtCashAmount
            // 
            this.txtCashAmount.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.txtCashAmount.Location = new System.Drawing.Point(80, 2);
            this.txtCashAmount.Margin = new System.Windows.Forms.Padding(2);
            this.txtCashAmount.Name = "txtCashAmount";
            this.txtCashAmount.Size = new System.Drawing.Size(152, 29);
            this.txtCashAmount.TabIndex = 1;
            this.txtCashAmount.Text = "0.00";
            this.txtCashAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCashAmount.Enter += new System.EventHandler(this.selectTxt);
            // 
            // CreditCardTransaction
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 269);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreditCardTransaction";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تسجيل حركات بطاقات الائتمان";
            this.Activated += new System.EventHandler(this.CreditCardTransaction_Activated);
            this.Load += new System.EventHandler(this.CreditCardTransaction_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblCashir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtVisaAmount;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtCreditTransNumber;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtInvNumber;
        private System.Windows.Forms.PictureBox pbImsLogo;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.TextBox txtCashAmount;
        private System.Windows.Forms.Label label7;
    }
}