﻿namespace SaudiMarketPOS
{
    partial class DeliveryOrderStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeliveryOrderStatus));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbImsLogo = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabDeliveryStatus = new System.Windows.Forms.TabControl();
            this.tabUncollectedOrders = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.dgvUnclollectedOrder = new System.Windows.Forms.DataGridView();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnGetOrders = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpOrderDate = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tabCollectedOrders = new System.Windows.Forms.TabPage();
            this.dgvCollectedOrders = new System.Windows.Forms.DataGridView();
            this.CollectedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CollectedEmpName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CollectedDueAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnGetCollected = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpCollectedDate = new System.Windows.Forms.DateTimePicker();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnExit = new System.Windows.Forms.Button();
            this.tabCancelledOrders = new System.Windows.Forms.TabPage();
            this.dgvCancelledOrders = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpCancelledDate = new System.Windows.Forms.DateTimePicker();
            this.btnGetCancelled = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).BeginInit();
            this.tabDeliveryStatus.SuspendLayout();
            this.tabUncollectedOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUnclollectedOrder)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabCollectedOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCollectedOrders)).BeginInit();
            this.tabCancelledOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCancelledOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel1.Controls.Add(this.pbImsLogo);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(886, 58);
            this.panel1.TabIndex = 13;
            // 
            // pbImsLogo
            // 
            this.pbImsLogo.ErrorImage = null;
            this.pbImsLogo.Location = new System.Drawing.Point(12, 12);
            this.pbImsLogo.Name = "pbImsLogo";
            this.pbImsLogo.Size = new System.Drawing.Size(90, 37);
            this.pbImsLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImsLogo.TabIndex = 15;
            this.pbImsLogo.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("STC Bold", 18F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(345, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(338, 32);
            this.label1.TabIndex = 0;
            this.label1.Text = "متابعة حالة التحصيل لأوامر التوصيل";
            // 
            // tabDeliveryStatus
            // 
            this.tabDeliveryStatus.Controls.Add(this.tabUncollectedOrders);
            this.tabDeliveryStatus.Controls.Add(this.tabCollectedOrders);
            this.tabDeliveryStatus.Controls.Add(this.tabCancelledOrders);
            this.tabDeliveryStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabDeliveryStatus.Font = new System.Drawing.Font("STC Bold", 10F, System.Drawing.FontStyle.Bold);
            this.tabDeliveryStatus.ImageList = this.imageList1;
            this.tabDeliveryStatus.Location = new System.Drawing.Point(0, 58);
            this.tabDeliveryStatus.Margin = new System.Windows.Forms.Padding(2);
            this.tabDeliveryStatus.Name = "tabDeliveryStatus";
            this.tabDeliveryStatus.RightToLeftLayout = true;
            this.tabDeliveryStatus.SelectedIndex = 0;
            this.tabDeliveryStatus.Size = new System.Drawing.Size(886, 413);
            this.tabDeliveryStatus.TabIndex = 14;
            // 
            // tabUncollectedOrders
            // 
            this.tabUncollectedOrders.BackColor = System.Drawing.Color.Silver;
            this.tabUncollectedOrders.Controls.Add(this.label2);
            this.tabUncollectedOrders.Controls.Add(this.dgvUnclollectedOrder);
            this.tabUncollectedOrders.Controls.Add(this.btnSave);
            this.tabUncollectedOrders.Controls.Add(this.btnGetOrders);
            this.tabUncollectedOrders.Controls.Add(this.label3);
            this.tabUncollectedOrders.Controls.Add(this.dtpOrderDate);
            this.tabUncollectedOrders.Controls.Add(this.groupBox1);
            this.tabUncollectedOrders.ImageIndex = 0;
            this.tabUncollectedOrders.Location = new System.Drawing.Point(4, 27);
            this.tabUncollectedOrders.Margin = new System.Windows.Forms.Padding(2);
            this.tabUncollectedOrders.Name = "tabUncollectedOrders";
            this.tabUncollectedOrders.Padding = new System.Windows.Forms.Padding(2);
            this.tabUncollectedOrders.Size = new System.Drawing.Size(878, 382);
            this.tabUncollectedOrders.TabIndex = 0;
            this.tabUncollectedOrders.Text = "اوامر تحت التحصيل";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label2.Location = new System.Drawing.Point(367, 348);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(161, 19);
            this.label2.TabIndex = 59;
            this.label2.Text = "اضغط [ F9 ] لتغير حالة الأمر";
            // 
            // dgvUnclollectedOrder
            // 
            this.dgvUnclollectedOrder.AllowUserToAddRows = false;
            this.dgvUnclollectedOrder.AllowUserToDeleteRows = false;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvUnclollectedOrder.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvUnclollectedOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.dgvUnclollectedOrder.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvUnclollectedOrder.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvUnclollectedOrder.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle8.Font = new System.Drawing.Font("STC Bold", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.Desktop;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUnclollectedOrder.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvUnclollectedOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("STC Bold", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvUnclollectedOrder.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgvUnclollectedOrder.EnableHeadersVisualStyles = false;
            this.dgvUnclollectedOrder.Location = new System.Drawing.Point(4, 46);
            this.dgvUnclollectedOrder.Margin = new System.Windows.Forms.Padding(2);
            this.dgvUnclollectedOrder.MultiSelect = false;
            this.dgvUnclollectedOrder.Name = "dgvUnclollectedOrder";
            this.dgvUnclollectedOrder.RowHeadersVisible = false;
            this.dgvUnclollectedOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUnclollectedOrder.Size = new System.Drawing.Size(867, 290);
            this.dgvUnclollectedOrder.TabIndex = 4;
            this.dgvUnclollectedOrder.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvUnclollectedOrder_CellBeginEdit);
            this.dgvUnclollectedOrder.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUnclollectedOrder_CellClick);
            this.dgvUnclollectedOrder.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUnclollectedOrder_CellContentClick);
            this.dgvUnclollectedOrder.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvUnclollectedOrder_CellDoubleClick);
            this.dgvUnclollectedOrder.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvUnclollectedOrder_CellValidating);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnSave.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.shortcut_حفظ;
            this.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(741, 340);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(130, 38);
            this.btnSave.TabIndex = 1;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            this.btnSave.MouseEnter += new System.EventHandler(this.btnSave_MouseEnter);
            this.btnSave.MouseLeave += new System.EventHandler(this.btnSave_MouseLeave);
            // 
            // btnGetOrders
            // 
            this.btnGetOrders.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnGetOrders.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnGetOrders.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.display_shortcut;
            this.btnGetOrders.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGetOrders.FlatAppearance.BorderSize = 0;
            this.btnGetOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetOrders.Location = new System.Drawing.Point(273, 4);
            this.btnGetOrders.Margin = new System.Windows.Forms.Padding(2);
            this.btnGetOrders.Name = "btnGetOrders";
            this.btnGetOrders.Size = new System.Drawing.Size(130, 38);
            this.btnGetOrders.TabIndex = 0;
            this.btnGetOrders.UseVisualStyleBackColor = false;
            this.btnGetOrders.Click += new System.EventHandler(this.btnGetOrders_Click);
            this.btnGetOrders.MouseEnter += new System.EventHandler(this.btnGetOrders_MouseEnter);
            this.btnGetOrders.MouseLeave += new System.EventHandler(this.btnGetOrders_MouseLeave);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label3.Location = new System.Drawing.Point(596, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 19);
            this.label3.TabIndex = 49;
            this.label3.Text = "التاريخ";
            // 
            // dtpOrderDate
            // 
            this.dtpOrderDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpOrderDate.Font = new System.Drawing.Font("Simplified Arabic", 12F);
            this.dtpOrderDate.Location = new System.Drawing.Point(407, 6);
            this.dtpOrderDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtpOrderDate.Name = "dtpOrderDate";
            this.dtpOrderDate.Size = new System.Drawing.Size(185, 34);
            this.dtpOrderDate.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(4, -1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 42);
            this.groupBox1.TabIndex = 63;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "طرق الدفع";
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label7.Location = new System.Drawing.Point(10, 19);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 19);
            this.label7.TabIndex = 62;
            this.label7.Text = "2: اونلاين";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label5.Location = new System.Drawing.Point(196, 19);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 19);
            this.label5.TabIndex = 60;
            this.label5.Text = "0: نقدي";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label6.Location = new System.Drawing.Point(113, 19);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 19);
            this.label6.TabIndex = 61;
            this.label6.Text = "1: فيزا";
            // 
            // tabCollectedOrders
            // 
            this.tabCollectedOrders.BackColor = System.Drawing.Color.Silver;
            this.tabCollectedOrders.Controls.Add(this.dgvCollectedOrders);
            this.tabCollectedOrders.Controls.Add(this.btnGetCollected);
            this.tabCollectedOrders.Controls.Add(this.label4);
            this.tabCollectedOrders.Controls.Add(this.dtpCollectedDate);
            this.tabCollectedOrders.ImageIndex = 1;
            this.tabCollectedOrders.Location = new System.Drawing.Point(4, 27);
            this.tabCollectedOrders.Margin = new System.Windows.Forms.Padding(2);
            this.tabCollectedOrders.Name = "tabCollectedOrders";
            this.tabCollectedOrders.Padding = new System.Windows.Forms.Padding(2);
            this.tabCollectedOrders.Size = new System.Drawing.Size(878, 382);
            this.tabCollectedOrders.TabIndex = 1;
            this.tabCollectedOrders.Text = "اوامر تم تحصيلها";
            // 
            // dgvCollectedOrders
            // 
            this.dgvCollectedOrders.AllowUserToAddRows = false;
            this.dgvCollectedOrders.AllowUserToDeleteRows = false;
            this.dgvCollectedOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.dgvCollectedOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCollectedOrders.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("STC Bold", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCollectedOrders.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgvCollectedOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCollectedOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CollectedID,
            this.CollectedEmpName,
            this.CollectedDueAmount});
            this.dgvCollectedOrders.EnableHeadersVisualStyles = false;
            this.dgvCollectedOrders.Location = new System.Drawing.Point(4, 46);
            this.dgvCollectedOrders.Margin = new System.Windows.Forms.Padding(2);
            this.dgvCollectedOrders.Name = "dgvCollectedOrders";
            this.dgvCollectedOrders.RowHeadersVisible = false;
            this.dgvCollectedOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCollectedOrders.Size = new System.Drawing.Size(867, 290);
            this.dgvCollectedOrders.TabIndex = 59;
            // 
            // CollectedID
            // 
            this.CollectedID.DataPropertyName = "ID";
            this.CollectedID.HeaderText = "رقم البون";
            this.CollectedID.Name = "CollectedID";
            this.CollectedID.ReadOnly = true;
            // 
            // CollectedEmpName
            // 
            this.CollectedEmpName.DataPropertyName = "EmpName";
            this.CollectedEmpName.HeaderText = "مندوب التوصيل";
            this.CollectedEmpName.Name = "CollectedEmpName";
            this.CollectedEmpName.ReadOnly = true;
            // 
            // CollectedDueAmount
            // 
            this.CollectedDueAmount.DataPropertyName = "DueAmount";
            this.CollectedDueAmount.HeaderText = "المطلوب سداده";
            this.CollectedDueAmount.Name = "CollectedDueAmount";
            this.CollectedDueAmount.ReadOnly = true;
            // 
            // btnGetCollected
            // 
            this.btnGetCollected.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnGetCollected.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnGetCollected.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.display1;
            this.btnGetCollected.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGetCollected.FlatAppearance.BorderSize = 0;
            this.btnGetCollected.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetCollected.Location = new System.Drawing.Point(273, 4);
            this.btnGetCollected.Margin = new System.Windows.Forms.Padding(2);
            this.btnGetCollected.Name = "btnGetCollected";
            this.btnGetCollected.Size = new System.Drawing.Size(130, 38);
            this.btnGetCollected.TabIndex = 54;
            this.btnGetCollected.UseVisualStyleBackColor = false;
            this.btnGetCollected.Click += new System.EventHandler(this.btnGetCollected_Click);
            this.btnGetCollected.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.btnGetCollected.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label4.Location = new System.Drawing.Point(596, 14);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 19);
            this.label4.TabIndex = 53;
            this.label4.Text = "التاريخ";
            // 
            // dtpCollectedDate
            // 
            this.dtpCollectedDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpCollectedDate.Font = new System.Drawing.Font("Simplified Arabic", 12F);
            this.dtpCollectedDate.Location = new System.Drawing.Point(407, 6);
            this.dtpCollectedDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtpCollectedDate.Name = "dtpCollectedDate";
            this.dtpCollectedDate.Size = new System.Drawing.Size(185, 34);
            this.dtpCollectedDate.TabIndex = 51;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "close.gif");
            this.imageList1.Images.SetKeyName(1, "open.jpg");
            this.imageList1.Images.SetKeyName(2, "Search.png");
            this.imageList1.Images.SetKeyName(3, "delivery.png");
            this.imageList1.Images.SetKeyName(4, "delivery-icon.png");
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.btnExit.BackgroundImage = global::SaudiMarketPOS.Properties.Resources._out;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Location = new System.Drawing.Point(8, 425);
            this.btnExit.Margin = new System.Windows.Forms.Padding(2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(130, 38);
            this.btnExit.TabIndex = 2;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseEnter += new System.EventHandler(this.btnExit_MouseEnter);
            this.btnExit.MouseLeave += new System.EventHandler(this.btnExit_MouseLeave);
            // 
            // tabCancelledOrders
            // 
            this.tabCancelledOrders.BackColor = System.Drawing.Color.Silver;
            this.tabCancelledOrders.Controls.Add(this.dgvCancelledOrders);
            this.tabCancelledOrders.Controls.Add(this.btnGetCancelled);
            this.tabCancelledOrders.Controls.Add(this.label8);
            this.tabCancelledOrders.Controls.Add(this.dtpCancelledDate);
            this.tabCancelledOrders.Location = new System.Drawing.Point(4, 27);
            this.tabCancelledOrders.Name = "tabCancelledOrders";
            this.tabCancelledOrders.Size = new System.Drawing.Size(878, 382);
            this.tabCancelledOrders.TabIndex = 2;
            this.tabCancelledOrders.Text = "اوامر ملغاة";
            // 
            // dgvCancelledOrders
            // 
            this.dgvCancelledOrders.AllowUserToAddRows = false;
            this.dgvCancelledOrders.AllowUserToDeleteRows = false;
            this.dgvCancelledOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.dgvCancelledOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCancelledOrders.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("STC Bold", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCancelledOrders.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvCancelledOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCancelledOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.dgvCancelledOrders.EnableHeadersVisualStyles = false;
            this.dgvCancelledOrders.Location = new System.Drawing.Point(4, 46);
            this.dgvCancelledOrders.Margin = new System.Windows.Forms.Padding(2);
            this.dgvCancelledOrders.Name = "dgvCancelledOrders";
            this.dgvCancelledOrders.RowHeadersVisible = false;
            this.dgvCancelledOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCancelledOrders.Size = new System.Drawing.Size(867, 290);
            this.dgvCancelledOrders.TabIndex = 63;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ID";
            this.dataGridViewTextBoxColumn1.HeaderText = "رقم البون";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "EmpName";
            this.dataGridViewTextBoxColumn2.HeaderText = "مندوب التوصيل";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "DueAmount";
            this.dataGridViewTextBoxColumn3.HeaderText = "المطلوب سداده";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label8.Location = new System.Drawing.Point(596, 14);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 19);
            this.label8.TabIndex = 61;
            this.label8.Text = "التاريخ";
            // 
            // dtpCancelledDate
            // 
            this.dtpCancelledDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dtpCancelledDate.Font = new System.Drawing.Font("Simplified Arabic", 12F);
            this.dtpCancelledDate.Location = new System.Drawing.Point(407, 6);
            this.dtpCancelledDate.Margin = new System.Windows.Forms.Padding(2);
            this.dtpCancelledDate.Name = "dtpCancelledDate";
            this.dtpCancelledDate.Size = new System.Drawing.Size(185, 34);
            this.dtpCancelledDate.TabIndex = 60;
            // 
            // btnGetCancelled
            // 
            this.btnGetCancelled.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnGetCancelled.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnGetCancelled.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.display1;
            this.btnGetCancelled.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGetCancelled.FlatAppearance.BorderSize = 0;
            this.btnGetCancelled.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGetCancelled.Location = new System.Drawing.Point(273, 4);
            this.btnGetCancelled.Margin = new System.Windows.Forms.Padding(2);
            this.btnGetCancelled.Name = "btnGetCancelled";
            this.btnGetCancelled.Size = new System.Drawing.Size(130, 38);
            this.btnGetCancelled.TabIndex = 62;
            this.btnGetCancelled.UseVisualStyleBackColor = false;
            this.btnGetCancelled.Click += new System.EventHandler(this.btnGetCancelled_Click);
            this.btnGetCancelled.MouseEnter += new System.EventHandler(this.btnGetCancelled_MouseEnter);
            this.btnGetCancelled.MouseLeave += new System.EventHandler(this.btnGetCancelled_MouseLeave);
            // 
            // DeliveryOrderStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 471);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.tabDeliveryStatus);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DeliveryOrderStatus";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "متابعة حالة التحصيل لأوامر التوصيل";
            this.Load += new System.EventHandler(this.DeliveryOrderStatus_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).EndInit();
            this.tabDeliveryStatus.ResumeLayout(false);
            this.tabUncollectedOrders.ResumeLayout(false);
            this.tabUncollectedOrders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUnclollectedOrder)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabCollectedOrders.ResumeLayout(false);
            this.tabCollectedOrders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCollectedOrders)).EndInit();
            this.tabCancelledOrders.ResumeLayout(false);
            this.tabCancelledOrders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCancelledOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabDeliveryStatus;
        private System.Windows.Forms.TabPage tabUncollectedOrders;
        private System.Windows.Forms.TabPage tabCollectedOrders;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnGetOrders;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpOrderDate;
        private System.Windows.Forms.Button btnGetCollected;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpCollectedDate;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DataGridView dgvUnclollectedOrder;
        private System.Windows.Forms.DataGridView dgvCollectedOrders;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pbImsLogo;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CollectedID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CollectedEmpName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CollectedDueAmount;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabCancelledOrders;
        private System.Windows.Forms.DataGridView dgvCancelledOrders;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpCancelledDate;
        private System.Windows.Forms.Button btnGetCancelled;
    }
}