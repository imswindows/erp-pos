﻿namespace SaudiMarketPOS
{
    partial class WebDeliveryOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.dgvWebOrders = new System.Windows.Forms.DataGridView();
            this.UncollectedID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedCustomerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedCustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedCustomerData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedPaymentMethodID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedPaymentMethod = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedNotes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UncollectedWebObj = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWebOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSelect
            // 
            this.btnSelect.Location = new System.Drawing.Point(698, 415);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(75, 42);
            this.btnSelect.TabIndex = 1;
            this.btnSelect.Text = "اختيار";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Location = new System.Drawing.Point(12, 415);
            this.btnExit.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 42);
            this.btnExit.TabIndex = 2;
            this.btnExit.Text = "خروج";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // dgvWebOrders
            // 
            this.dgvWebOrders.AllowUserToAddRows = false;
            this.dgvWebOrders.AllowUserToDeleteRows = false;
            this.dgvWebOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvWebOrders.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvWebOrders.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvWebOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWebOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UncollectedID,
            this.UncollectedCustomerID,
            this.UncollectedCustomerName,
            this.UncollectedCustomerData,
            this.UncollectedTotal,
            this.UncollectedPaymentMethodID,
            this.UncollectedPaymentMethod,
            this.UncollectedNotes,
            this.UncollectedWebObj});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvWebOrders.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvWebOrders.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvWebOrders.EnableHeadersVisualStyles = false;
            this.dgvWebOrders.Location = new System.Drawing.Point(0, 0);
            this.dgvWebOrders.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.dgvWebOrders.MultiSelect = false;
            this.dgvWebOrders.Name = "dgvWebOrders";
            this.dgvWebOrders.ReadOnly = true;
            this.dgvWebOrders.RowHeadersVisible = false;
            this.dgvWebOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvWebOrders.Size = new System.Drawing.Size(785, 407);
            this.dgvWebOrders.TabIndex = 0;
            this.dgvWebOrders.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvWebOrders_CellDoubleClick);
            // 
            // UncollectedID
            // 
            this.UncollectedID.DataPropertyName = "ID";
            this.UncollectedID.HeaderText = "ID";
            this.UncollectedID.Name = "UncollectedID";
            this.UncollectedID.ReadOnly = true;
            this.UncollectedID.Visible = false;
            // 
            // UncollectedCustomerID
            // 
            this.UncollectedCustomerID.DataPropertyName = "CustomerID";
            this.UncollectedCustomerID.HeaderText = "CustomerID";
            this.UncollectedCustomerID.Name = "UncollectedCustomerID";
            this.UncollectedCustomerID.ReadOnly = true;
            this.UncollectedCustomerID.Visible = false;
            // 
            // UncollectedCustomerName
            // 
            this.UncollectedCustomerName.DataPropertyName = "CustomerName";
            this.UncollectedCustomerName.HeaderText = "اسم العميل";
            this.UncollectedCustomerName.Name = "UncollectedCustomerName";
            this.UncollectedCustomerName.ReadOnly = true;
            // 
            // UncollectedCustomerData
            // 
            this.UncollectedCustomerData.DataPropertyName = "CustomerData";
            this.UncollectedCustomerData.HeaderText = "بيانات العميل";
            this.UncollectedCustomerData.Name = "UncollectedCustomerData";
            this.UncollectedCustomerData.ReadOnly = true;
            // 
            // UncollectedTotal
            // 
            this.UncollectedTotal.DataPropertyName = "Total";
            this.UncollectedTotal.HeaderText = "الاجمالي";
            this.UncollectedTotal.Name = "UncollectedTotal";
            this.UncollectedTotal.ReadOnly = true;
            // 
            // UncollectedPaymentMethodID
            // 
            this.UncollectedPaymentMethodID.DataPropertyName = "PaymentMethod";
            this.UncollectedPaymentMethodID.HeaderText = "PaymentMethodID";
            this.UncollectedPaymentMethodID.Name = "UncollectedPaymentMethodID";
            this.UncollectedPaymentMethodID.ReadOnly = true;
            this.UncollectedPaymentMethodID.Visible = false;
            // 
            // UncollectedPaymentMethod
            // 
            this.UncollectedPaymentMethod.DataPropertyName = "strPaymentMethod";
            this.UncollectedPaymentMethod.HeaderText = "طريقة الدفع";
            this.UncollectedPaymentMethod.Name = "UncollectedPaymentMethod";
            this.UncollectedPaymentMethod.ReadOnly = true;
            // 
            // UncollectedNotes
            // 
            this.UncollectedNotes.DataPropertyName = "Notes";
            this.UncollectedNotes.HeaderText = "ملاحظات";
            this.UncollectedNotes.Name = "UncollectedNotes";
            this.UncollectedNotes.ReadOnly = true;
            // 
            // UncollectedWebObj
            // 
            this.UncollectedWebObj.DataPropertyName = "WebObj";
            this.UncollectedWebObj.HeaderText = "WebObj";
            this.UncollectedWebObj.Name = "UncollectedWebObj";
            this.UncollectedWebObj.ReadOnly = true;
            this.UncollectedWebObj.Visible = false;
            // 
            // WebDeliveryOrders
            // 
            this.AcceptButton = this.btnSelect;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnExit;
            this.ClientSize = new System.Drawing.Size(785, 470);
            this.ControlBox = false;
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.dgvWebOrders);
            this.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "WebDeliveryOrders";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "طلبات الانترنت";
            this.Load += new System.EventHandler(this.WebDeliveryOrders_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWebOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.DataGridView dgvWebOrders;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedCustomerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedCustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedCustomerData;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedPaymentMethodID;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedPaymentMethod;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedNotes;
        private System.Windows.Forms.DataGridViewTextBoxColumn UncollectedWebObj;
    }
}