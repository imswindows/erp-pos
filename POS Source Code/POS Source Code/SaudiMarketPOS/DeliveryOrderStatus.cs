﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SaudiMarketPOS.Properties;

namespace SaudiMarketPOS
{
    public partial class DeliveryOrderStatus : Form
    {
        public DeliveryOrderStatus()
        {
            InitializeComponent();
        }
        bool collectedValue;
        SqlConnection con = new SqlConnection();
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void SetKeyDownEvent(Control ctlr)
        {
            ctlr.KeyDown += ctl_KeyDown;
            if (ctlr.HasChildren)
                foreach (Control c in ctlr.Controls)
                    this.SetKeyDownEvent(c);
        }
        void ctl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F8)
            {
                this.btnSave_Click(this.btnSave, new EventArgs());
            }
            if (e.KeyCode == Keys.F9)
            {
                if (dgvUnclollectedOrder.Rows.Count == 0) return;
                dgvUnclollectedOrder.Focus();
                //dgvUnclollectedOrder.CurrentCell = dgvUnclollectedOrder.Rows[0].Cells["Collected"];
                try
                {
                    DataGridViewRow dgvr = dgvUnclollectedOrder.SelectedRows[0];
                    bool IsCollected = false;
                    try
                    {
                        IsCollected = bool.Parse(dgvr.Cells["Collected"].Value.ToString());
                    }
                    catch
                    {
                        
                    }
                    dgvr.Cells["Collected"].Value = !IsCollected;
                    dgvUnclollectedOrder.ClearSelection();
                    dgvUnclollectedOrder.EndEdit();
                }
                catch
                {
                    
                }
            }
            if (e.KeyCode == Keys.F5)
            {
                this.btnGetOrders_Click(this.btnGetOrders, new EventArgs());
            }
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            if (e.KeyCode == Keys.D && e.Modifiers == Keys.Alt)
            {
                dgvUnclollectedOrder .ClearSelection();
                dgvUnclollectedOrder.CurrentCell = dgvUnclollectedOrder.Rows[dgvUnclollectedOrder.Rows.Count - 1].Cells["Collected"];
                dgvUnclollectedOrder.Rows[dgvUnclollectedOrder.Rows.Count - 1].Selected = true;
                dgvUnclollectedOrder.Focus();
            }
            if (e.KeyCode == Keys.Space)
            {
                if (this.ActiveControl == dgvUnclollectedOrder)
                {
                    if (dgvUnclollectedOrder.Rows.Count != 0)
                    {
                        if (collectedValue == true)
                        {
                            dgvUnclollectedOrder.CurrentRow.Cells["Collected"].Value = false;
                        }
                        if (collectedValue == false)
                        {
                            dgvUnclollectedOrder.CurrentRow.Cells["Collected"].Value = true;
                        }
                    }
                }
                
            }
        }
        private void DeliveryOrderStatus_Load(object sender, EventArgs e)
        {
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            try
            {
                dtpOrderDate.Value = DateTime.Now;
                con.ConnectionString = Program.conString;
                //con.ConnectionString = Settings.Default.ConnectionString;
                //Fill Employee Combobox
                //SqlCommand cmdFillEmpName = new SqlCommand();
                //cmdFillEmpName.CommandType = CommandType.StoredProcedure;
                //cmdFillEmpName.CommandText = "uspSelectEmpName";
                //cmdFillEmpName.Connection = con;
                //DataTable dtEmpName = new DataTable();
                //if (con.State == ConnectionState.Closed) con.Open();
                //dtEmpName.Load(cmdFillEmpName.ExecuteReader());
                //cmbDeliveryEmp.DataSource = dtEmpName;
                //cmbDeliveryEmp.ValueMember = "ID";
                //cmbDeliveryEmp.DisplayMember = "Name";
                //generateUncollectedOrdersDGV();
                //getCollectedOrders();
                this.SetKeyDownEvent(this);
            }
            catch (Exception ex)
            { }
            
        }
        private void getCollectedOrders()
        {
            DateTime DateFrom = DateTime.Parse(dtpCollectedDate.Value.ToString("dd/MM/yyyy 00:00:00"));
            DateTime DateTo = DateFrom.AddDays(1);
            SqlCommand collectedOrderCmad = new SqlCommand();
            collectedOrderCmad.Connection = con;
            collectedOrderCmad.CommandType = CommandType.StoredProcedure;
            collectedOrderCmad.CommandText = "uspSelectCollectedOrders";
            collectedOrderCmad.Parameters.AddWithValue("@DateFrom", DateFrom);
            collectedOrderCmad.Parameters.AddWithValue("@DateTo", DateTo);

            if (con.State == ConnectionState.Closed) con.Open();
            DataTable collectedOrderDT = new DataTable();
            collectedOrderDT.Load(collectedOrderCmad.ExecuteReader());
            dgvCollectedOrders.AutoGenerateColumns = false;
            dgvCollectedOrders.DataSource = collectedOrderDT;
        }

        private void GetCancelledOrders()
        {
            DateTime DateFrom = DateTime.Parse(dtpCancelledDate.Value.ToString("dd/MM/yyyy 00:00:00"));
            DateTime DateTo = DateFrom.AddDays(1);
            SqlCommand cancelledOrderCmad = new SqlCommand();
            cancelledOrderCmad.Connection = con;
            cancelledOrderCmad.CommandType = CommandType.StoredProcedure;
            cancelledOrderCmad.CommandText = "uspSelectCancelledOrders";
            cancelledOrderCmad.Parameters.AddWithValue("@DateFrom", DateFrom);
            cancelledOrderCmad.Parameters.AddWithValue("@DateTo", DateTo);

            if (con.State == ConnectionState.Closed) con.Open();
            DataTable cancelledOrderDT = new DataTable();
            cancelledOrderDT.Load(cancelledOrderCmad.ExecuteReader());
            dgvCancelledOrders.AutoGenerateColumns = false;
            dgvCancelledOrders.DataSource = cancelledOrderDT;
        }

        private void btnGetOrders_Click(object sender, EventArgs e)
        {
            try
            {
                dgvUnclollectedOrder.Columns.Clear();
                generateUncollectedOrdersDGV();
                getUnclollectedOrder();
                if (dgvUnclollectedOrder.Rows.Count == 0) return;
                dgvUnclollectedOrder.Focus();
                //dgvUnclollectedOrder.ClearSelection();
                dgvUnclollectedOrder.CurrentCell = dgvUnclollectedOrder.Rows[0].Cells["Collected"];
                roundNumbers();
            }
            catch (Exception ex)
            { }
        }
        private void getUnclollectedOrder()
        {
            SqlCommand cmdUncollctedOrders = new SqlCommand();
            cmdUncollctedOrders.Connection = con;
            cmdUncollctedOrders.CommandType = CommandType.StoredProcedure;
            cmdUncollctedOrders.CommandText = "uspSelectUncollctedOrders";
            cmdUncollctedOrders.Parameters.AddWithValue("@userID", Program.empID);
            DateTime orderDate;
            orderDate = dtpOrderDate.Value;
            DateTime orDate = Convert.ToDateTime(dtpOrderDate.Value.ToString("dd/MM/yyyy 23:59:59"));
            DateTime endDay = orDate.AddDays(1);
            cmdUncollctedOrders.Parameters.AddWithValue("@TransactionDate", orDate);
            cmdUncollctedOrders.Parameters.AddWithValue("@EndTime", endDay);

            if (con.State == ConnectionState.Closed) con.Open();
            DataTable uncollectedDT = new DataTable();
            uncollectedDT.Load(cmdUncollctedOrders.ExecuteReader());
            uncollectedDT.Columns["PaymentMethod"].ReadOnly = false;
            dgvUnclollectedOrder.AutoGenerateColumns = false;
            BindingSource bs = new BindingSource();
            bs.DataSource = uncollectedDT;
            dgvUnclollectedOrder.DataSource = bs;
            if (con.State == ConnectionState.Open) con.Close();

            foreach(DataGridViewRow dgvr in dgvUnclollectedOrder.Rows)
            {
                dgvr.Cells["Collected"].Value = false;
            }
        }
        private void generateUncollectedOrdersDGV()
        {
            dgvUnclollectedOrder.Columns.Clear();
            dgvUnclollectedOrder.Columns.Add("DeliveryEmloyeeID", "DeliveryEmloyeeID");
            dgvUnclollectedOrder.Columns.Add("UserID", "UserID");
            
            dgvUnclollectedOrder.Columns.Add("ID", "رقم البون");
            dgvUnclollectedOrder.Columns.Add("DeliveryEmloyeeName", "مندوب التوصيل");
            dgvUnclollectedOrder.Columns.Add("UserName", "الكاشير");
            dgvUnclollectedOrder.Columns.Add("Paid", "المدفوع");
            dgvUnclollectedOrder.Columns.Add("DueAmount", "المطلوب تحصيله");
            dgvUnclollectedOrder.Columns.Add("PaymentMethod", "طريقة الدفع");
            dgvUnclollectedOrder.Columns.Add("CreditCTransNo", "CreditCTransNo");
            dgvUnclollectedOrder.Columns.Add("CreditAmount", "CreditAmount");

            DataGridViewCheckBoxColumn orderStatusCol = new DataGridViewCheckBoxColumn();
            orderStatusCol.HeaderText="حالة التحصيل";
            orderStatusCol.Name = "Collected";
            orderStatusCol.DataPropertyName = "Collected";
            orderStatusCol.Width = 40;
            dgvUnclollectedOrder.Columns.Add(orderStatusCol);

            DataGridViewButtonColumn ordercancelCol = new DataGridViewButtonColumn();
            ordercancelCol.HeaderText = "إلغاء";
            ordercancelCol.Name = "Cancel";
            ordercancelCol.Width = 40;
            dgvUnclollectedOrder.Columns.Add(ordercancelCol);

            dgvUnclollectedOrder.Columns["DeliveryEmloyeeID"].DataPropertyName = "DeliveryEmloyeeID";
            dgvUnclollectedOrder.Columns["UserID"].DataPropertyName = "UserID";
            dgvUnclollectedOrder.Columns["ID"].DataPropertyName = "ID";
            dgvUnclollectedOrder.Columns["DeliveryEmloyeeName"].DataPropertyName = "EmpName";
            dgvUnclollectedOrder.Columns["UserName"].DataPropertyName = "UserName";
            dgvUnclollectedOrder.Columns["Paid"].DataPropertyName = "Paid";
            dgvUnclollectedOrder.Columns["DueAmount"].DataPropertyName = "DueAmount";
            dgvUnclollectedOrder.Columns["PaymentMethod"].DataPropertyName = "PaymentMethod";
            dgvUnclollectedOrder.Columns["PaymentMethod"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            //dgvUnclollectedOrder.Columns["PaymentMethod"].HeaderText = "طريقة الدفع";
            //dgvUnclollectedOrder.Columns["Collected"].DataPropertyName = "Collected";

            dgvUnclollectedOrder.Columns["DeliveryEmloyeeID"].Visible = false;
            dgvUnclollectedOrder.Columns["UserID"].Visible = false;

            dgvUnclollectedOrder.Columns["ID"].ReadOnly = true;
            dgvUnclollectedOrder.Columns["DeliveryEmloyeeName"].ReadOnly = true;
            dgvUnclollectedOrder.Columns["UserName"].ReadOnly = true;
            dgvUnclollectedOrder.Columns["DueAmount"].ReadOnly = true;
            dgvUnclollectedOrder.Columns["Paid"].ReadOnly = true;
            dgvUnclollectedOrder.Columns["CreditCTransNo"].Visible = false;
            dgvUnclollectedOrder.Columns["CreditAmount"].Visible = false;
            //dgvUnclollectedOrder.Columns["PaymentMethod"].ReadOnly = false;


            dgvUnclollectedOrder.Columns["ID"].Width = 100;
            dgvUnclollectedOrder.Columns["DeliveryEmloyeeName"].Width = 150;
            dgvUnclollectedOrder.Columns["DeliveryEmloyeeName"].FillWeight = 200;
            dgvUnclollectedOrder.Columns["UserName"].Width = 150;
            dgvUnclollectedOrder.Columns["DueAmount"].Width = 120;
            dgvUnclollectedOrder.Columns["Paid"].Width = 100;
            dgvUnclollectedOrder.Columns["PaymentMethod"].Width = 60;
            dgvUnclollectedOrder.AllowUserToAddRows = false;
            //dgvUnclollectedOrder.Columns["Collected"].Width = 100;
        }
        private void roundNumbers()
        {
            
            foreach (DataGridViewRow totDr in dgvUnclollectedOrder.Rows)
            {
                
                if (totDr.Cells["Paid"].Value != null && totDr.Cells["Paid"].Value.ToString() != "")
                {
                    decimal d = Math.Round(decimal.Parse(totDr.Cells["DueAmount"].Value.ToString()),2);
                    decimal p = Math.Round(decimal.Parse(totDr.Cells["Paid"].Value.ToString()),2);

                    totDr.Cells["DueAmount"].Value = d.ToString();
                    totDr.Cells["Paid"].Value = p.ToString();
                }      
            } 
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvUnclollectedOrder.Rows.Count == 0)
                {
                    return;
                }
                else
                {
                    foreach (DataGridViewRow dr in dgvUnclollectedOrder.Rows)
                    {
                        bool IsCollected = bool.Parse(dr.Cells["Collected"].Value.ToString());
                        if (IsCollected)
                        {
                            int PaymentMethod = int.Parse(dr.Cells["PaymentMethod"].Value.ToString());
                            SqlCommand updateUncollectedCmd = new SqlCommand();
                            updateUncollectedCmd.Connection = con;
                            updateUncollectedCmd.CommandType = CommandType.StoredProcedure;
                            updateUncollectedCmd.CommandText = "uspTransactionTableUpdCollected";
                            updateUncollectedCmd.Parameters.AddWithValue("@ID", dr.Cells["ID"].Value);
                            updateUncollectedCmd.Parameters.AddWithValue("@InvStatus", dr.Cells["Collected"].Value);
                            updateUncollectedCmd.Parameters.AddWithValue("@UserID", Program.empID);
                            updateUncollectedCmd.Parameters.AddWithValue("@PaymentMethod", dr.Cells["PaymentMethod"].Value);
                            updateUncollectedCmd.Parameters.AddWithValue("@CreditCTransNo", dr.Cells["CreditCTransNo"].Value);
                            if (con.State == ConnectionState.Closed) con.Open();
                            updateUncollectedCmd.ExecuteNonQuery();
                            if (PaymentMethod == 1)
                            {
                                SqlCommand updateCreditAmountCmd = new SqlCommand();
                                updateCreditAmountCmd.Connection = con;
                                updateCreditAmountCmd.CommandType = CommandType.Text;
                                updateCreditAmountCmd.CommandText = "UPDATE [TransactionTableH_Main] SET CreditAmount = @CreditAmount WHERE ID = @ID; UPDATE [PurchaseOrderH] SET CreditAmount = @CreditAmount WHERE ID = @ID;";
                                updateCreditAmountCmd.Parameters.AddWithValue("@ID", dr.Cells["ID"].Value);
                                updateCreditAmountCmd.Parameters.AddWithValue("@CreditAmount", dr.Cells["CreditAmount"].Value);
                                if (con.State == ConnectionState.Closed) con.Open();
                                updateCreditAmountCmd.ExecuteNonQuery();
                                if (con.State == ConnectionState.Open) con.Close();
                            }
                        }
                    }
                    dgvUnclollectedOrder.DataSource = new DataTable();
                    generateUncollectedOrdersDGV();
                    getUnclollectedOrder();
                    if (dgvUnclollectedOrder.Rows.Count == 0) return;
                    dgvUnclollectedOrder.Focus();
                    //dgvUnclollectedOrder.ClearSelection();
                    dgvUnclollectedOrder.CurrentCell = dgvUnclollectedOrder.Rows[0].Cells["Collected"];
                }
            }
            catch (Exception ex)
            { }
        }



        private void dgvUnclollectedOrder_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            //if (dgvUnclollectedOrder.Columns[e.ColumnIndex].Name == "Collected")
            //{
            //    string chkCollected;
            //    try
            //    {
            //        chkCollected = dgvUnclollectedOrder.CurrentRow.Cells["Collected"].Value.ToString();
            //        collectedValue = bool.Parse(chkCollected);
            //        int PaymentMethod = (int)dgvUnclollectedOrder.CurrentRow.Cells["PaymentMethod"].Value;
            //        if (PaymentMethod == 1)
            //        {
            //            CreditCardTransaction cct = new CreditCardTransaction();
            //            decimal Value = decimal.Parse(dgvUnclollectedOrder.CurrentRow.Cells["DueAmount"].Value.ToString());
            //            cct.Value = Value;
            //            cct.ShowDialog();
            //            if (cct.Result == DialogResult.OK)
            //            {
            //                dgvUnclollectedOrder.CurrentRow.Cells["CreditCTransNo"].Value = cct.TransNo;
            //            }
            //            else
            //            {
            //                dgvUnclollectedOrder.CurrentRow.Cells["CreditCTransNo"].Value = "";
            //            }
            //        }
            //    }
            //    catch
            //    {
            //        collectedValue = false;
            //    }
            //}
        }

        private void btnExit_MouseEnter(object sender, EventArgs e)
        {
            btnExit.BackgroundImage = Properties.Resources.out_gray;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.BackgroundImage = Properties.Resources._out;
        }

        private void btnSave_MouseEnter(object sender, EventArgs e)
        {
            btnSave.BackgroundImage = Properties.Resources.gray_shortcut_حفظ;
        }

        private void btnSave_MouseLeave(object sender, EventArgs e)
        {
            btnSave.BackgroundImage = Properties.Resources.shortcut_حفظ;
        }

        private void btnGetOrders_MouseEnter(object sender, EventArgs e)
        {
            btnGetOrders.BackgroundImage = Properties.Resources.display_shortcut_gray;
        }

        private void btnGetOrders_MouseLeave(object sender, EventArgs e)
        {
            btnGetOrders.BackgroundImage = Properties.Resources.display_shortcut;
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            btnGetCollected.BackgroundImage = Properties.Resources.display;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            btnGetCollected.BackgroundImage = Properties.Resources.display1;
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            //button3.BackgroundImage = Properties.Resources.display;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            //button3.BackgroundImage = Properties.Resources.display1;
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            //button4.BackgroundImage = Properties.Resources._1حفظ;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            //button4.BackgroundImage = Properties.Resources.حفظ;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            //button2.BackgroundImage = Properties.Resources.display;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            //button2.BackgroundImage = Properties.Resources.display1;
        }

        private void button7_MouseEnter(object sender, EventArgs e)
        {
            //button7.BackgroundImage = Properties.Resources._1حفظ;
        }

        private void button7_MouseLeave(object sender, EventArgs e)
        {
            //button7.BackgroundImage = Properties.Resources.حفظ;
        }

        private void btnGetCollected_Click(object sender, EventArgs e)
        {
            getCollectedOrders();
        }

        private void dgvUnclollectedOrder_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.RowIndex >= 0)
            //{
            //    try
            //    {
            //        if (e.ColumnIndex == dgvUnclollectedOrder.Columns["PaymentMethod"].Index)
            //        {
            //            int Payment = (int)dgvUnclollectedOrder.Rows[e.RowIndex].Cells["PaymentMethod"].Value;
            //            if (Payment != 2)
            //            {
            //                if(Payment == 1)
            //                {
            //                    Payment = 0;
            //                }
            //                else
            //                {
            //                    Payment = 1;
            //                }
            //            }
            //            dgvUnclollectedOrder.Rows[e.RowIndex].Cells["PaymentMethod"].Value = Payment;
            //        }
            //    }
            //    catch { }
            //}
        }

        private void dgvUnclollectedOrder_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvUnclollectedOrder.Columns["PaymentMethod"].Index)
                {
                    int Oldpaymentmethod = (int)dgvUnclollectedOrder.Rows[e.RowIndex].Cells["PaymentMethod"].Value;
                    int paymentmethod = 0;
                    if (int.TryParse(e.FormattedValue.ToString(), out paymentmethod))
                    {
                        if (Oldpaymentmethod != 2)
                        {
                            if (paymentmethod > 1 || paymentmethod < 0)
                            {
                                MessageBox.Show("ادخل قيمة صحيحة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                                e.Cancel = true;
                            }
                        }
                        else
                        {
                            if(Oldpaymentmethod != paymentmethod)
                            {
                                MessageBox.Show("لا يمكن تغيير حالة الطلب", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                                e.Cancel = true;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("ادخل قيمة صحيحة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                        e.Cancel = true;
                    }
                }
            }
        }

        //private void enableCell(DataGridViewCell dc, bool enabled)
        //{
        //    //toggle read-only state
        //    dc.ReadOnly = !enabled;
        //    if (enabled)
        //    {
        //        //restore cell style to the default value
        //        dc.Style.BackColor = dc.OwningColumn.DefaultCellStyle.BackColor;
        //        dc.Style.ForeColor = dc.OwningColumn.DefaultCellStyle.ForeColor;
        //    }
        //    else
        //    {
        //        //gray out the cell
        //        dc.Style.BackColor = Color.LightGray;
        //        dc.Style.ForeColor = Color.DarkGray;
        //    }
        //}

        private void dgvUnclollectedOrder_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvUnclollectedOrder.Columns["Cancel"].Index)
                {
                    AdminEnableDiscount adedis = new AdminEnableDiscount();
                    adedis.ShowDialog();
                    if (Program.enableDis == true)
                    {
                        int CancelOrderId = (int)dgvUnclollectedOrder.Rows[e.RowIndex].Cells["ID"].Value;
                        SqlCommand updateCanceledCmd = new SqlCommand();
                        updateCanceledCmd.Connection = con;
                        updateCanceledCmd.CommandType = CommandType.Text;
                        updateCanceledCmd.CommandText = "UPDATE [dbo].[TransactionTableH_Main] SET Canceled = 1, UserID = @UserID, CashingTime = @CashingTime WHERE ID = @ID";
                        updateCanceledCmd.Parameters.AddWithValue("@ID", CancelOrderId);
                        updateCanceledCmd.Parameters.AddWithValue("@UserID", adedis.userid);
                        updateCanceledCmd.Parameters.AddWithValue("@CashingTime", DateTime.Now);
                        if (con.State == ConnectionState.Closed) con.Open();
                        updateCanceledCmd.ExecuteNonQuery();
                        dgvUnclollectedOrder.DataSource = new DataTable();
                        generateUncollectedOrdersDGV();
                        getUnclollectedOrder();
                        if (dgvUnclollectedOrder.Rows.Count == 0) return;
                        dgvUnclollectedOrder.Focus();
                        dgvUnclollectedOrder.CurrentCell = dgvUnclollectedOrder.Rows[0].Cells["Collected"];
                        Program.enableDis = false;
                    }
                }
            }
        }

        private void dgvUnclollectedOrder_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvUnclollectedOrder.Columns["Collected"].Index)
            {
                string chkCollected;
                try
                {
                    DataGridViewCheckBoxCell cbcell = dgvUnclollectedOrder.Rows[e.RowIndex].Cells["Collected"] as DataGridViewCheckBoxCell;
                    if(cbcell.Value == null)
                    {
                        cbcell.Value = true;
                    }
                    else if(bool.Parse(cbcell.Value.ToString()) == false)
                    {
                        cbcell.Value = true;
                    }
                    else
                    {
                        cbcell.Value = false;
                    }
                    collectedValue = (null != cbcell && null != cbcell.Value && true == (bool)cbcell.Value);
                    if (true == collectedValue)
                    {
                        int PaymentMethod = (int)dgvUnclollectedOrder.CurrentRow.Cells["PaymentMethod"].Value;
                        if (PaymentMethod == 1)
                        {
                            CreditCardTransaction cct = new CreditCardTransaction();
                            decimal Value = decimal.Parse(dgvUnclollectedOrder.CurrentRow.Cells["DueAmount"].Value.ToString());
                            cct.Value = Value;
                            cct.ShowDialog();
                            if (cct.Result == DialogResult.OK)
                            {
                                dgvUnclollectedOrder.CurrentRow.Cells["CreditCTransNo"].Value = cct.TransNo;
                                dgvUnclollectedOrder.CurrentRow.Cells["CreditAmount"].Value = cct.VisaValue;
                            }
                            else
                            {
                                dgvUnclollectedOrder.CurrentRow.Cells["CreditCTransNo"].Value = "";
                                dgvUnclollectedOrder.CurrentRow.Cells["CreditAmount"].Value = 0;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    collectedValue = false;
                }
            }
        }

        private void btnGetCancelled_MouseEnter(object sender, EventArgs e)
        {
            btnGetCancelled.BackgroundImage = Properties.Resources.display;
        }

        private void btnGetCancelled_MouseLeave(object sender, EventArgs e)
        {
            btnGetCancelled.BackgroundImage = Properties.Resources.display1;
        }

        private void btnGetCancelled_Click(object sender, EventArgs e)
        {
            GetCancelledOrders();
        }
    }
}
