﻿namespace SaudiMarketPOS
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.btnCloseShift = new System.Windows.Forms.Button();
            this.btnReports = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbImsLogo = new System.Windows.Forms.PictureBox();
            this.lblCashirName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOptions = new System.Windows.Forms.Button();
            this.btnChangePass = new System.Windows.Forms.Button();
            this.btnBarcodePrinting = new System.Windows.Forms.Button();
            this.btnOldShifts = new System.Windows.Forms.Button();
            this.deletedOrders = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCloseShift
            // 
            this.btnCloseShift.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCloseShift.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnCloseShift.FlatAppearance.BorderSize = 0;
            this.btnCloseShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseShift.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnCloseShift.ForeColor = System.Drawing.Color.White;
            this.btnCloseShift.Location = new System.Drawing.Point(299, 197);
            this.btnCloseShift.Name = "btnCloseShift";
            this.btnCloseShift.Size = new System.Drawing.Size(167, 62);
            this.btnCloseShift.TabIndex = 0;
            this.btnCloseShift.Text = "اغلاق الورديات";
            this.btnCloseShift.UseVisualStyleBackColor = false;
            this.btnCloseShift.Click += new System.EventHandler(this.btnCloseShift_Click);
            // 
            // btnReports
            // 
            this.btnReports.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnReports.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnReports.FlatAppearance.BorderSize = 0;
            this.btnReports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReports.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnReports.ForeColor = System.Drawing.Color.White;
            this.btnReports.Location = new System.Drawing.Point(482, 197);
            this.btnReports.Name = "btnReports";
            this.btnReports.Size = new System.Drawing.Size(167, 62);
            this.btnReports.TabIndex = 2;
            this.btnReports.Text = "التقارير";
            this.btnReports.UseVisualStyleBackColor = false;
            this.btnReports.Visible = false;
            this.btnReports.Click += new System.EventHandler(this.btnReports_Click);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnExit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnExit.ForeColor = System.Drawing.Color.White;
            this.btnExit.Location = new System.Drawing.Point(299, 375);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(167, 62);
            this.btnExit.TabIndex = 4;
            this.btnExit.Text = "الخروج من النظام";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel1.Controls.Add(this.pbImsLogo);
            this.panel1.Controls.Add(this.lblCashirName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(764, 89);
            this.panel1.TabIndex = 12;
            // 
            // pbImsLogo
            // 
            this.pbImsLogo.ErrorImage = null;
            this.pbImsLogo.Location = new System.Drawing.Point(12, 23);
            this.pbImsLogo.Name = "pbImsLogo";
            this.pbImsLogo.Size = new System.Drawing.Size(90, 37);
            this.pbImsLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImsLogo.TabIndex = 16;
            this.pbImsLogo.TabStop = false;
            // 
            // lblCashirName
            // 
            this.lblCashirName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCashirName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCashirName.ForeColor = System.Drawing.Color.White;
            this.lblCashirName.Location = new System.Drawing.Point(501, 15);
            this.lblCashirName.Name = "lblCashirName";
            this.lblCashirName.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCashirName.Size = new System.Drawing.Size(182, 24);
            this.lblCashirName.TabIndex = 15;
            this.lblCashirName.Text = "label3";
            this.lblCashirName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(683, 15);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 14;
            this.label2.Text = "اهلا بك يا";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(275, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "القائمة الرئيسية للنظام";
            // 
            // btnOptions
            // 
            this.btnOptions.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOptions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnOptions.FlatAppearance.BorderSize = 0;
            this.btnOptions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnOptions.ForeColor = System.Drawing.Color.White;
            this.btnOptions.Location = new System.Drawing.Point(111, 197);
            this.btnOptions.Name = "btnOptions";
            this.btnOptions.Size = new System.Drawing.Size(167, 62);
            this.btnOptions.TabIndex = 3;
            this.btnOptions.Text = "إعد&دات";
            this.btnOptions.UseVisualStyleBackColor = false;
            this.btnOptions.Visible = false;
            this.btnOptions.Click += new System.EventHandler(this.btnOptions_Click);
            // 
            // btnChangePass
            // 
            this.btnChangePass.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnChangePass.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnChangePass.FlatAppearance.BorderSize = 0;
            this.btnChangePass.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangePass.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnChangePass.ForeColor = System.Drawing.Color.White;
            this.btnChangePass.Location = new System.Drawing.Point(299, 290);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(167, 62);
            this.btnChangePass.TabIndex = 1;
            this.btnChangePass.Text = "تغيير بيانات دخول المستخدمين";
            this.btnChangePass.UseVisualStyleBackColor = false;
            this.btnChangePass.Click += new System.EventHandler(this.btnChangePass_Click);
            // 
            // btnBarcodePrinting
            // 
            this.btnBarcodePrinting.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnBarcodePrinting.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnBarcodePrinting.FlatAppearance.BorderSize = 0;
            this.btnBarcodePrinting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBarcodePrinting.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnBarcodePrinting.ForeColor = System.Drawing.Color.White;
            this.btnBarcodePrinting.Location = new System.Drawing.Point(482, 290);
            this.btnBarcodePrinting.Name = "btnBarcodePrinting";
            this.btnBarcodePrinting.Size = new System.Drawing.Size(167, 62);
            this.btnBarcodePrinting.TabIndex = 13;
            this.btnBarcodePrinting.Text = "طباعة الباركود";
            this.btnBarcodePrinting.UseVisualStyleBackColor = false;
            this.btnBarcodePrinting.Click += new System.EventHandler(this.btnBarcodePrinting_Click);
            // 
            // btnOldShifts
            // 
            this.btnOldShifts.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnOldShifts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnOldShifts.FlatAppearance.BorderSize = 0;
            this.btnOldShifts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOldShifts.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btnOldShifts.ForeColor = System.Drawing.Color.White;
            this.btnOldShifts.Location = new System.Drawing.Point(111, 290);
            this.btnOldShifts.Name = "btnOldShifts";
            this.btnOldShifts.Size = new System.Drawing.Size(167, 62);
            this.btnOldShifts.TabIndex = 14;
            this.btnOldShifts.Text = "ورديات سابقة";
            this.btnOldShifts.UseVisualStyleBackColor = false;
            this.btnOldShifts.Click += new System.EventHandler(this.btnOldShifts_Click);
            // 
            // deletedOrders
            // 
            this.deletedOrders.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.deletedOrders.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.deletedOrders.FlatAppearance.BorderSize = 0;
            this.deletedOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.deletedOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.deletedOrders.ForeColor = System.Drawing.Color.White;
            this.deletedOrders.Location = new System.Drawing.Point(482, 375);
            this.deletedOrders.Name = "deletedOrders";
            this.deletedOrders.Size = new System.Drawing.Size(167, 62);
            this.deletedOrders.TabIndex = 15;
            this.deletedOrders.Text = "أوامر إلغاء";
            this.deletedOrders.UseVisualStyleBackColor = false;
            this.deletedOrders.Click += new System.EventHandler(this.deletedOrders_Click);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(764, 528);
            this.Controls.Add(this.deletedOrders);
            this.Controls.Add(this.btnOldShifts);
            this.Controls.Add(this.btnBarcodePrinting);
            this.Controls.Add(this.btnOptions);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnChangePass);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnReports);
            this.Controls.Add(this.btnCloseShift);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainMenu";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "القائمة الرئيسية";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainMenu_FormClosing);
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCloseShift;
        private System.Windows.Forms.Button btnReports;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCashirName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnOptions;
        private System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.PictureBox pbImsLogo;
        private System.Windows.Forms.Button btnBarcodePrinting;
        private System.Windows.Forms.Button btnOldShifts;
        private System.Windows.Forms.Button deletedOrders;
    }
}