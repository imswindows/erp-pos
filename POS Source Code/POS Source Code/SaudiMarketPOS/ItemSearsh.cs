﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using SaudiMarketPOS.Properties;
namespace SaudiMarketPOS
{
    public partial class ItemSearsh : Form
    {

        private Form parentForm;

        public ItemSearsh()
        {
            InitializeComponent();
        }

        public ItemSearsh(Form frmParent) : this()
        {
            this.parentForm = frmParent;
        }
       
        private void SetKeyDownEvent(Control ctlr)
        {
            ctlr.KeyDown += ctl_KeyDown;
            if (ctlr.HasChildren)
                foreach (Control c in ctlr.Controls)
                    this.SetKeyDownEvent(c);
        }
        void ctl_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.F6)
            //{
            //    ItemSearsh itms = new ItemSearsh(this);
            //    itms.ShowDialog(this);
            //    this.dgvTransactions.Focus();
            //}
            //if (e.KeyCode == Keys.F7)
            //{
            //    CustomerSearsh custSearsh = new CustomerSearsh(this);
            //    custSearsh.ShowDialog(this);
            //    this.txtItemBarcode.Focus();
            //}
            if (e.KeyCode == Keys.F5)
            {
                this.btnAddItemToInv_Click(this.btnAddItemToInv, new EventArgs());
            }
            else if (e.KeyCode == Keys.Escape)
            {
                this.btnClose_Click(this.btnClose, new EventArgs());
            }
            else if(e.KeyCode == Keys.Down)
            {
                NavigateDown();
            }
            else if(e.KeyCode == Keys.Up)
            {
                NavigateUp();
            }
        }

        private void NavigateUp()
        {
            int RowIndex = 0;
            try
            {
                RowIndex = dgvItemsSearsh.SelectedRows[0].Index;
            }
            catch
            {
                RowIndex = 0;
            }
            if (RowIndex == 0)
            {
                dgvItemsSearsh.Rows[dgvItemsSearsh.Rows.Count - 1].Selected = true;
            }
            else
            {
                dgvItemsSearsh.Rows[RowIndex - 1].Selected = true;
            }
            dgvItemsSearsh.FirstDisplayedScrollingRowIndex = dgvItemsSearsh.SelectedRows[0].Index;
        }

        private void NavigateDown()
        {
            int RowIndex = 0;
            try
            {
                RowIndex = dgvItemsSearsh.SelectedRows[0].Index;
            }
            catch
            {
                RowIndex = 0;
            }
            if (RowIndex == dgvItemsSearsh.Rows.Count - 1)
            {
                dgvItemsSearsh.Rows[0].Selected = true;
            }
            else
            {
                dgvItemsSearsh.Rows[RowIndex + 1].Selected = true;
            }
            dgvItemsSearsh.FirstDisplayedScrollingRowIndex = dgvItemsSearsh.SelectedRows[0].Index;
        }
        
        SqlConnection con = new SqlConnection();
        DataTable itemDT = new DataTable();
        private void ItemSearsh_Load(object sender, EventArgs e)
        {
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            try
            {
                //con.ConnectionString = Settings.Default.ConnectionString;
                con.ConnectionString = Program.conString;
                this.SetKeyDownEvent(this);

                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "selectItems";
                cmd.Connection = con;

                if (con.State == ConnectionState.Closed) con.Open();
                itemDT.Load(cmd.ExecuteReader());
                dgvItemsSearsh.AutoGenerateColumns = false;
                dgvItemsSearsh.DataSource = itemDT;
                if (con.State == ConnectionState.Open) con.Close();
                itemIDTextBox.Focus();
                dgvItemsSearsh.Columns["ItemBarcode"].Width = 200;
                dgvItemsSearsh.Columns["ItemName"].Width = 300;
                dgvItemsSearsh.Columns["SellPrice"].Width = 100;
            }
            catch (Exception ex)
            { }

        }
        private void itemIDTextBox_TextChanged(object sender, EventArgs e)
        {
            if (itemIDTextBox.Text != "")
            {
                itemNameTextBox.Text = "";
                try
                {
                    SqlCommand cmdFind = new SqlCommand();
                    cmdFind.CommandType = CommandType.StoredProcedure;
                    cmdFind.CommandText = "FindItemsByCode";
                    cmdFind.Parameters.AddWithValue("@Code", itemIDTextBox.Text);
                    cmdFind.Connection = con;
                    DataTable dtFind = new DataTable();
                    if (con.State == ConnectionState.Closed) con.Open();
                    dtFind.Load(cmdFind.ExecuteReader());
                    dgvItemsSearsh.AutoGenerateColumns = false;
                    dgvItemsSearsh.DataSource = dtFind;
                    if (con.State == ConnectionState.Open) con.Close();
                }
                catch (Exception ex)
                { }
            }
        }
        private void itemNameTextBox_TextChanged(object sender, EventArgs e)
        {
            if (itemNameTextBox.Text != "")
            {
                itemIDTextBox.Text = "";
                try
                {
                    SqlCommand cmdFind = new SqlCommand();
                    cmdFind.CommandType = CommandType.StoredProcedure;
                    cmdFind.CommandText = "FindItemsByName";
                    cmdFind.Parameters.AddWithValue("@Name", itemNameTextBox.Text);
                    cmdFind.Connection = con;
                    DataTable dtFind = new DataTable();
                    if (con.State == ConnectionState.Closed) con.Open();
                    dtFind.Load(cmdFind.ExecuteReader());
                    dgvItemsSearsh.AutoGenerateColumns = false;
                    dgvItemsSearsh.DataSource = dtFind;
                    if (con.State == ConnectionState.Open) con.Close();
                }
                catch (Exception ex)
                { }
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            //this.Dispose(); 
        }
        //private void dgvItemsSearsh_Leave(object sender, EventArgs e)
        //{
        //    Program.selectedProduct = dgvItemsSearsh.CurrentRow.Cells["ItemBarcode"].Value.ToString();
        //}
        private void btnAddItemToInv_Click(object sender, EventArgs e)
        {
            try
            {
                //this.parentForm.dgvTransactions.Rows.Add();
                //int rowIdx = this.parentForm.dgvTransactions.Rows.Count - 2;
                if (dgvItemsSearsh.SelectedRows[0].Cells["ItemBarcode"].Value.ToString() != string.Empty)
                {
                    //this.parentForm.dgvTransactions.Rows[rowIdx].Cells["ItemBarcode"].Value = dgvItemsSearsh.CurrentRow.Cells["ItemBarcode"].Value.ToString().Trim();// Program.selectedProductInsert.ToString();
                    //this.parentForm.dgvTransactions_CellEndEdit(this.parentForm.dgvTransactions, new DataGridViewCellEventArgs(this.parentForm.dgvTransactions.Rows[rowIdx].Cells["ItemBarcode"].ColumnIndex, this.parentForm.dgvTransactions.Rows[rowIdx].Cells["ItemBarcode"].RowIndex));
                    this.Close();
                    if (this.parentForm is TransactionScreen)
                    {
                        ((TransactionScreen)this.parentForm).txtItemBarcode.Text = dgvItemsSearsh.SelectedRows[0].Cells["ItemBarcode"].Value.ToString().Trim();
                        ((TransactionScreen)this.parentForm).txtItemBarcode.Focus();
                    }
                    else if (this.parentForm is DeliveryOrderScreen)
                    {
                        ((DeliveryOrderScreen)this.parentForm).txtItemBarcode.Text = dgvItemsSearsh.SelectedRows[0].Cells["ItemBarcode"].Value.ToString().Trim();
                        ((DeliveryOrderScreen)this.parentForm).txtItemBarcode.Focus();
                    }
                }
            }
            catch (Exception ex)
            { }
            //this.Dispose();
        }
        //private void itemIDTextBox_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.Down)
        //    {
        //        dgvItemsSearsh.Focus();
        //    }
        //    if (e.KeyData == Keys.F5)
        //    {
        //        dgvItemsSearsh.Focus();
        //        //dgvItemsSearsh_Leave(sender, new EventArgs());
        //        btnAddItemToInv_Click(sender, new EventArgs());
        //    }
        //}
        //private void itemNameTextBox_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.Down)
        //    {
        //        dgvItemsSearsh.Focus();
        //    }
        //    if (e.KeyData == Keys.F5)
        //    {
        //        dgvItemsSearsh.Focus();
        //        //dgvItemsSearsh_Leave(sender, new EventArgs());
        //        btnAddItemToInv_Click(sender, new EventArgs());
        //    }
        //}
        //private void dgvItemsSearsh_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.F5)
        //    {
        //        //dgvItemsSearsh_Leave(sender, new EventArgs());
        //        btnAddItemToInv_Click(sender, new EventArgs());
        //    }
        //}
    }
}
