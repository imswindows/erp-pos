﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SaudiMarketPOS
{
    public partial class BarcodeAuto : Form
    {
        List<ItemDataLog> SelectedLst = new List<ItemDataLog>();
        List<ItemDataLog> ItemLst = new List<ItemDataLog>();
        SqlConnection con = new SqlConnection();
        public BarcodeAuto()
        {
            InitializeComponent();
        }

        private class ItemDataLog
        {
            public string Barcode { get; set; }
            public string Name { get; set; }
            public decimal Price { get; set; }
            public decimal OldPrice { get; set; }
            public DateTime ChangeDate { get; set; }
        }

        private void Filter()
        {
            try
            {
                ItemLst.Clear();
                dgvPriceLog.DataSource = null;
                using (SqlConnection conn = new SqlConnection(Program.conString))
                {
                    string SQL = @"SELECT Item.ID, Item.Name, Item.Description, Item.SellPrice, Item.OnSale, Item.SalePrice, ItemPriceLog.ChangeDate FROM Item 
                               INNER JOIN [dbo].[ItemPriceLog] ON [dbo].[ItemPriceLog].ItemId = Item.ID WHERE ItemPriceLog.ChangeDate >= @DateFrom AND 
                               ItemPriceLog.ChangeDate <= @DateTo
                               ORDER BY ItemPriceLog.ChangeDate DESC";
                    SqlCommand cmd = new SqlCommand(SQL, conn);
                    cmd.Parameters.AddWithValue("@DateFrom", DateTime.Parse(dtpDateFrom.Value.ToString("dd/MM/yyyy 00:00:00")));
                    cmd.Parameters.AddWithValue("@DateTo", DateTime.Parse(dtpDateTo.Value.ToString("dd/MM/yyyy 23:59:59")));
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            ItemDataLog ItemObj = new ItemDataLog();
                            ItemObj.Barcode = rdr["ID"].ToString();
                            ItemObj.Name = rdr["Name"].ToString();
                            ItemObj.Price = decimal.Parse(rdr["SellPrice"].ToString());
                            ItemObj.ChangeDate = DateTime.Parse(rdr["ChangeDate"].ToString());
                            bool IsPromotion = false;
                            try { IsPromotion = bool.Parse(rdr["OnSale"].ToString()); } catch { }
                            if (IsPromotion)
                            {
                                ItemObj.OldPrice = decimal.Parse(rdr["SalePrice"].ToString());
                            }
                            ItemLst.Add(ItemObj);
                        }
                    }
                }
                dgvPriceLog.AutoGenerateColumns = false;
                dgvPriceLog.DataSource = ItemLst;
                dgvPriceLog.MultiSelect = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BarcodeAuto_Load(object sender, EventArgs e)
        {
            dtpDateFrom.Value = DateTime.Now.AddDays(-2);
            dtpDateTo.Value = DateTime.Now;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            SelectedLst.Clear();
            foreach (DataGridViewRow dgvr in dgvPriceLog.SelectedRows)
            {
                string Barcode = dgvr.Cells["ItemId"].Value.ToString();
                ItemDataLog ItemObj = ItemLst.Find(c => c.Barcode == Barcode);
                SelectedLst.Add(ItemObj);
            }
            BarcodePrinting frm = new BarcodePrinting();
            var allSelectedBarcodes = SelectedLst.Select(bar => new BarcodePrinting.ItemData() {
                Barcode = bar.Barcode,
                Name = bar.Name,
                Price = bar.Price,
                OldPrice = bar.OldPrice
            }).ToList();
            frm.SelectedLst.AddRange(allSelectedBarcodes);
            frm.ShowDialog();

        }

        private void dtpDateFrom_ValueChanged(object sender, EventArgs e)
        {
            dtpDateTo.MinDate = dtpDateFrom.Value;
            Filter();
        }

        private void dtpDateTo_ValueChanged(object sender, EventArgs e)
        {
            dtpDateFrom.MaxDate = dtpDateTo.Value;
            Filter();
        }
    }
}
