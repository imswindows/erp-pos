﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
//using gma.System.Windows;
using System.Threading;
using System.Collections;
using SaudiMarketPOS.Properties;
using ReceiptCreator;
using CustomerLineDisplay;
using System.Drawing.Printing;
using System.Reflection;

//using Utilities;
//using gma.System.Windows;

namespace SaudiMarketPOS
{
    public partial class TransactionScreen : Form
    {
        string CreditCTransNo = "";
        LineDisplay LD = new LineDisplay("Line Display");
        decimal CashValue = 0;
        decimal VisaValue = 0;
        int deletedTrans = 0;
        private string GetPaymentMethod(int ID)
        {
            switch(ID)
            {
                default:
                case 0:
                    return "Cash On Delivery";
                case 1:
                    return "Credit Card";
                case 2:
                    return "Online Paid";
                case 3:
                    return "Cash Payment";
                case 4:
                    return "Multi Payments";
            }
        }

        SqlConnection con = new SqlConnection();
        
        string itemID = string.Empty;
        string itemBarcode = string.Empty;
        string itemName = string.Empty;
        string itemUnit = string.Empty;
        string itemPrice = string.Empty;
        string custPhone = string.Empty;
        string custAddress = string.Empty;
        string paymentMethod = string.Empty;
        decimal itemQty;
        decimal itemTotal;
        decimal cashDiscount;
        decimal discountPercent;
        decimal invTotal;
        decimal paidCash;
        decimal dueAmount;
        decimal change;
        decimal creditAmount;
        int invID;
        int hID = 0;

        public TransactionScreen()
        {
            InitializeComponent();
        }
        //private void SetKeyDownEvent(Control ctlr)
        //{
        //    ctlr.KeyDown += ctl_KeyDown;
        //    if (ctlr.HasChildren)
        //        foreach (Control c in ctlr.Controls)
        //            this.SetKeyDownEvent(c);
        //}
        void ctl_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    txtItemBarcode.Focus();
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (dgvTransactionsss.Rows.Count <= 0)
                    {
                        GetLastTransaction();
                        Program.enableDis = false;
                        if (dgvTransactionsss.Rows.Count > 0)
                        {
                            printTransaction(true);
                            clearData();
                            clearTempTransactions();
                            DisableDiscount();
                            //txtTransactionID.Text = (hID + 1).ToString();
                            EnableForm();
                        }
                    }
                }
                else if (e.KeyCode == Keys.F3)
                {
                    txtItemBarcode.Focus();
                    selectTxt(txtItemBarcode, e);
                }
                else if (e.KeyCode == Keys.F4)
                {
                    chkDeliveryEmp.Focus();
                }
                else if (e.KeyCode == Keys.F5)
                {
                    this.txtPaid.Select();
                }
                else if (e.KeyCode == Keys.F6)
                {
                    ItemSearsh itms = new ItemSearsh(this);
                    itms.ShowDialog(this);
                    //this.dgvTransactionsss.Focus();
                    this.txtItemBarcode.Focus();
                }
                else if (e.KeyCode == Keys.F7)
                {
                    CustomerSearsh custSearsh = new CustomerSearsh(this);
                    custSearsh.ShowDialog(this);
                    //fillCustCmb();
                    this.txtItemBarcode.Focus();
                }
                else if (e.KeyCode == Keys.F8)
                {
                    calculateChange();
                    this.btnPrint_Click(this.btnPrint, new EventArgs());
                }
                else if (e.KeyCode == Keys.F9)
                {
                    AdminLoginQty adlog = new AdminLoginQty();
                    adlog.ShowDialog();
                    if (Program.checkRole == 1 || Program.checkRole == 2)
                    {
                        if (dgvTransactionsss.Rows.Count == 0) return;
                        dgvTransactionsss.ClearSelection();
                        dgvTransactionsss.Columns["ItemQty"].ReadOnly = false;
                        dgvTransactionsss.CurrentCell = dgvTransactionsss.Rows[dgvTransactionsss.Rows.Count - 1].Cells["ItemQty"];
                        dgvTransactionsss.BeginEdit(true);
                        txtItemBarcode.TabStop = false;
                        dgvTransactionsss.Focus();
                    }
                }
                else if (e.KeyCode == Keys.Delete)
                {
                    if (this.ActiveControl == dgvTransactionsss)
                    {
                        AdminDelItem adlog = new AdminDelItem();
                        adlog.ShowDialog();
                    }
                    //Program.delTransactionRow = true;
                    deleteItem();
                }
                else if (e.KeyCode == Keys.R && e.Modifiers == Keys.Alt)
                {
                    this.btnReturns_Click(this.btnPrint, new EventArgs());
                }
                else if (e.KeyCode == Keys.D && e.Modifiers == Keys.Alt)
                {
                    dgvTransactionsss.ClearSelection();
                    dgvTransactionsss.CurrentCell = dgvTransactionsss.Rows[dgvTransactionsss.Rows.Count - 1].Cells["ItemQty"];
                    dgvTransactionsss.Rows[dgvTransactionsss.Rows.Count - 1].Selected = true;
                    dgvTransactionsss.Focus();
                }
                else if (e.KeyCode == Keys.T && e.Modifiers == Keys.Alt)
                {
                    this.eliveryStatus_Click(this.btnDeliveryStatus, new EventArgs());
                }
                else if (e.KeyCode == Keys.E && e.Modifiers == Keys.Alt)
                {
                    this.btnCreditCard_Click(this.btnCreditCard, new EventArgs());
                }
            }
            catch (Exception ex)
            { }
        }

        bool IsEdit = false;
        private void DisableForm()
        {
            IsEdit = true;
            txtCustomerID.Enabled = false;
            cmbCustomerName.Enabled = false;
            txtCustomerID.Visible = false;
            cmbCustomerName.Visible = false;
            txtItemBarcode.Enabled = false;
            dgvTransactionsss.ReadOnly = true;
            dgvTransactionsss.AllowUserToDeleteRows = false;
        }

        private void EnableForm()
        {
            IsEdit = false;
            txtCustomerID.Enabled = true;
            cmbCustomerName.Enabled = true;
            txtCustomerID.Visible = true;
            cmbCustomerName.Visible = true;
            txtItemBarcode.Enabled = true;
            dgvTransactionsss.ReadOnly = false;
            dgvTransactionsss.AllowUserToDeleteRows = true;
        }

        private class LastOrderDetails
        {
            public string ItemId;
            public decimal SalePrice;
            public decimal QTY;
            public decimal Total;
        }

        private void GetLastTransaction()
        {
            ///// Get Last Order Details ///////////////////////
            SqlCommand LastOrderCmd = new SqlCommand();
            LastOrderCmd.Connection = con;
            LastOrderCmd.CommandType = CommandType.Text;
            LastOrderCmd.CommandText = "SELECT * FROM TransactionTable WHERE ID = @ID";
            LastOrderCmd.Parameters.AddWithValue("@ID", hID);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readOrder = LastOrderCmd.ExecuteReader();
            while (readOrder.Read())
            {
                clearData();
                txtInvTotal.Text = readOrder["Total"].ToString();
                txtDueAmount.Text = readOrder["DueAmount"].ToString();
                //txtNotes.Text = readOrder["Notes"].ToString();
                try { paymentMethod = WebOrders.GetPaymentMethod((int)readOrder["PaymentMethod"]); } catch { paymentMethod = WebOrders.GetPaymentMethod(0); }
                //customerData = readOrder["CustomerData"].ToString();
                DisableForm();
            }
            readOrder.Close();
            ////////////////////////////////////////////////////
            /////////////// Get Order Details //////////////////
            SqlCommand LastDetialsCmd = new SqlCommand();
            LastDetialsCmd.Connection = con;
            LastDetialsCmd.CommandType = CommandType.Text;
            LastDetialsCmd.CommandText = "SELECT * FROM TansactionDetail WHERE TransactionID = @ID";
            LastDetialsCmd.Parameters.AddWithValue("@ID", hID);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readDetails = LastDetialsCmd.ExecuteReader();
            List<LastOrderDetails> DetailsLst = new List<LastOrderDetails>();
            while (readDetails.Read())
            {
                LastOrderDetails Obj = new LastOrderDetails();
                Obj.ItemId = readDetails["ItemID"].ToString();
                Obj.SalePrice = decimal.Parse(readDetails["SalePrice"].ToString());
                Obj.QTY = decimal.Parse(readDetails["Quantity"].ToString());
                Obj.Total = Obj.SalePrice * Obj.QTY;
                DetailsLst.Add(Obj);
            }
            readDetails.Close();
            foreach (LastOrderDetails DetailObj in DetailsLst)
            {
                SqlCommand ItemCmd = new SqlCommand();
                ItemCmd.Connection = con;
                ItemCmd.CommandType = CommandType.Text;
                ItemCmd.CommandText = "SELECT * FROM Item WHERE ID = @ID";
                ItemCmd.Parameters.AddWithValue("@ID", DetailObj.ItemId);
                if (con.State == ConnectionState.Closed) con.Open();
                SqlDataReader readItem = ItemCmd.ExecuteReader();
                string Name = "";
                string Barcode = "";
                while (readItem.Read())
                {
                    Name = readItem["Name"].ToString();
                    Barcode = readItem["Barcode"].ToString();
                }
                readItem.Close();
                if (DetailObj.SalePrice >= 0)
                {
                    dgvTransactionsss.Rows.Add(DetailObj.ItemId, Barcode, Name, "", DetailObj.SalePrice, DetailObj.QTY, DetailObj.Total);
                }
            }
        }

        public void deleteItem()
        {
            if (Program.delTransactionRow == true)
            {
                //if (MessageBox.Show("هل انت متأكد", "تأكيد", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.RightAlign) == DialogResult.No)
                //{
                //    Program.delTransactionRow = false;
                //    return;
                //}
                //else
                //{

                //    MessageBox.Show(dgvTransactionsss.CurrentRow.Index.ToString());
                //    this.dgvTransactionsss.Rows.RemoveAt(this.dgvTransactionsss.CurrentRow.Index);
                //    CalcudalteDueAmount();
                //    calculateChange();
                //    calculateInvoiceTotal();
                //    Program.delTransactionRow = false;
                //}
            }
        }
        private void TransactionScreen_Load(object sender, EventArgs e)
        {
            LD.InitDisp();
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            foreach (DataGridViewColumn dgvc in dgvTransactionsss.Columns)
            {
                dgvc.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            pbXreca.Image = Image.FromFile(apppath + "xreca-logo.png");
            try
            {
                //this.SetKeyDownEvent(this);
                //acHook = new UserActivityHook();
                //acHook.KeyDown += acHook_KeyDown;
                //acHook.Start();
                //acHook = new UserActivityHook();
                //acHook.KeyDown += acHook_KeyDown;
                //acHook.Start();
                //ParameterizedThreadStart pts = new ParameterizedThreadStart(acHook_KeyDown);
                //Thread hookThread = new Thread(new ThreadStart(threadHandler));
                // hookThread.Start();
                //hookThread.Join();
                //gkh.HookedKeys.Add(Keys.F6);
                //gkh.HookedKeys.Add(Keys.F7);
                //gkh.KeyDown += gkh_KeyDown;
                Program.delTransactionRow = false;
                clearData();
                
                //con.ConnectionString = Settings.Default.ConnectionString;
                con.ConnectionString = Program.conString;
                lblDateTime.Text = DateTime.Now.ToString();

                lblCashir.Text = Program.fristName.ToString() + " " + Program.lastName.ToString();

                //Fill Customer Combobox
                fillCustCmb();

                //Fill Employee Combobox
                SqlCommand cmdFillEmpName = new SqlCommand();
                cmdFillEmpName.CommandType = CommandType.StoredProcedure;
                cmdFillEmpName.CommandText = "uspSelectDeliveryEmpName";
                cmdFillEmpName.Connection = con;
                DataTable dtEmpName = new DataTable();
                if (con.State == ConnectionState.Closed) con.Open();
                dtEmpName.Load(cmdFillEmpName.ExecuteReader());
                cmbDeliveryEmp.DataSource = dtEmpName;
                cmbDeliveryEmp.ValueMember = "ID";
                cmbDeliveryEmp.DisplayMember = "EmpName";
                txtCustomerID.Text = "1";

                if (con.State == ConnectionState.Open) con.Close();
                try
                {
                    getMaxInvID();
                }
                catch
                {
                    invID = 1;
                }
                txtTransactionID.Text = invID.ToString();
                this.txtSafeNumber.Text = Program.safeNumber.ToString();
                this.txtMachineName.Text = Program.machineName.ToString();
                //this.txtSafeNumber.Text = Settings.Default.SafeNumber.ToString();
                //if (Program.roleID != 1 || Program.roleID != 2)
                //{
                //    txtDiscountPercent.Enabled = false;
                //    txtCashDiscount.Enabled = false;
                //}
                dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemName"].Width = 235;
                dgvTransactionsss.Columns["ItemUnit"].Width = 70;
                //dgvTransactionsss.AllowUserToDeleteRows = false;
                cmbDeliveryEmp.Text = "";

                dgvTransactionsss.Columns["ItemID"].DataPropertyName = "ItemID";
                dgvTransactionsss.Columns["ItemBarcode"].DataPropertyName = "ItemBarcode";
                dgvTransactionsss.Columns["ItemName"].DataPropertyName = "ItemName";
                dgvTransactionsss.Columns["ItemUnit"].DataPropertyName = "ItemUnit";
                dgvTransactionsss.Columns["ItemPrice"].DataPropertyName = "ItemPrice";
                dgvTransactionsss.Columns["ItemQty"].DataPropertyName = "ItemQty";
                dgvTransactionsss.Columns["Total"].DataPropertyName = "ItemTotal";

                SqlCommand getTempItemCmd = new SqlCommand();
                getTempItemCmd.Connection = con;
                getTempItemCmd.CommandType = CommandType.StoredProcedure;
                getTempItemCmd.CommandText = "uspTempTransactionSelect";
                getTempItemCmd.Parameters.AddWithValue("UserID", Program.empID);
                if (con.State == ConnectionState.Closed) con.Open();
                SqlDataReader readTemp = getTempItemCmd.ExecuteReader();
                if (readTemp.HasRows == false)
                {
                    readTemp.Close();
                    return;
                }
                else
                {
                    while (readTemp.Read())
                    {
                        string tempItemID = readTemp[0].ToString();
                        string tempItemBarcode = readTemp[1].ToString();
                        string tempItemName = readTemp[2].ToString();
                        string tempItemUnit = readTemp[3].ToString();
                        string tempItemPrice = readTemp[4].ToString();
                        string tempItemQty =readTemp[5].ToString();
                        string tempItemTotal = readTemp[6].ToString();
                        dgvTransactionsss.Rows.Add(tempItemID, tempItemBarcode, tempItemName, tempItemUnit, Math.Round(decimal.Parse(tempItemPrice), 3).ToString(), Math.Round(decimal.Parse(tempItemQty), 2).ToString(), Math.Round(decimal.Parse(tempItemTotal), 2).ToString());

                    }
                    readTemp.Close();
                    calculateInvoiceTotal();
                    CalcudalteDueAmount();
                }
                if (con.State == ConnectionState.Open) con.Close();
            }
            catch (Exception ex)
            { }
        }

        private void fillCustCmb()
        {
            SqlCommand cmdFillCustName = new SqlCommand();
            cmdFillCustName.CommandType = CommandType.StoredProcedure;
            cmdFillCustName.CommandText = "uspSelectCustomer";
            cmdFillCustName.Connection = con;
            if (con.State == ConnectionState.Closed) con.Open();

            DataTable dtCustName = new DataTable();
            dtCustName.Load(cmdFillCustName.ExecuteReader());
            cmbCustomerName.DataSource = dtCustName;
            cmbCustomerName.ValueMember = "ID";
            cmbCustomerName.DisplayMember = "Name";
        }

        //globalKeyboardHook gkh = new globalKeyboardHook();

        //private void dgvTransactions_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.F6)
        //    {
        //        ItemSearsh itms = new ItemSearsh(this);
        //        itms.ShowDialog();
        //    }
        //}

        private void calculateInvoiceTotal()
        {
            invTotal = 0;

            foreach (DataGridViewRow totDr in dgvTransactionsss.Rows)
            {
                // حساب الاجمالى
                if (totDr.Cells[6].Value != null && totDr.Cells[6].Value.ToString() != "")
                {
                    invTotal += Math.Round(Convert.ToDecimal(totDr.Cells[6].Value), 2);
                }
            }
            txtInvTotal.Text = invTotal.ToString();
            LD.MoveCursorToLine(2);
            LD.WriteText("Total");
            LD.MoveCursor(14, 2);
            LD.WriteText(txtInvTotal.Text);
        }
        private void CalcudalteDueAmount()
        {
            decimal totalDiscount = 0.0m;
            cashDiscount =Math.Round( decimal.Parse(txtCashDiscount.Text.ToString()),2);
            totalDiscount =Math.Round( cashDiscount,2);
            discountPercent = Math.Round(decimal.Parse(txtDiscountPercent.Text.Trim()),2);
            if (discountPercent != 0)
            {
                discountPercent =Math.Round( discountPercent / 100.0m,2);
                decimal calcDiscountPercent =Math.Round( invTotal * discountPercent,2);
                totalDiscount +=Math.Round( calcDiscountPercent,2);
            }
            txtDueAmount.Text = Math.Round( invTotal - totalDiscount,2).ToString();
        }
        private void calculateChange()
        {
            if (paidCash == 0) return;
            change =Math.Round( paidCash - dueAmount,2);
            txtChange.Text = change.ToString();

            txtPaid.Text = Math.Round(dueAmount + change, 2).ToString();
            
        }
        private void selectTxt(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).SelectionStart = 0;
                ((TextBox)sender).SelectionLength = ((TextBox)sender).Text.Length;
            }
            catch { }

        }
        private void txtCashDiscount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtCashDiscount.Text.Trim() != "" && int.Parse(txtCashDiscount.Text) != 0)
                {
                    txtDiscountPercent.Text = "0";
                    cashDiscount = Math.Round(decimal.Parse(txtCashDiscount.Text.ToString()), 2);
                }
            }
            catch
            {
                MessageBox.Show("يجب ادخال ارقام فقط");
                txtCashDiscount.Text = "0";
                txtCashDiscount.Focus();
                selectTxt(this, e);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDateTime.Text = DateTime.Now.ToString();
        }
        private void clearData()
        {
            dgvTransactionsss.Rows.Clear();
            txtInvTotal.Text = "0,00";
            txtDiscountPercent.Text = "0,00";
            txtCashDiscount.Text = "0,00";
            //txtCredit.Text = "0,00";
            txtDueAmount.Text = "0,00";
            txtPaid.Text = "0,00";
            txtChange.Text = "0,00";
            //txtCreditTransactionNum.Text = "";
            cmbDeliveryEmp.Text = "";
            itemQty = 0;
            itemTotal = 0;
            cashDiscount = 0;
            discountPercent = 0;
            invTotal = 0;
            paidCash = 0;
            dueAmount = 0;
            change = 0;
            chkCredit.Checked = false;
            //chkCreditCard.Checked = false;
            chkDeliveryEmp.Checked = false;
            //txtCreditTransactionNum.Enabled = false;
            //txtCredit.Enabled = false;
            txtItemBarcode.Focus();
            Program.checkRole = 0;
            Program.enableDis = false ;
            txtCustomerID.Text = "1";
            txtDiscountPercent.Enabled = false;
            txtCashDiscount.Enabled = false;
            CreditCTransNo = "";
            chkCredit.Checked = false;
            paymentMethod = "";
            txtItemBarcode.Enabled = true;
            dgvTransactionsss.Enabled = true;
            VisaValue = 0;
            CashValue = 0;
            deletedTrans = 0;
        }
        private void btnCancelInv_Click(object sender, EventArgs e)
        {
            clearData();
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            this.KeyDown -= ctl_KeyDown;
            btnPrint.Click -= btnPrint_Click;
            try
            {
                if (dueAmount <=0 ||  paidCash + creditAmount < dueAmount )
                {
                    MessageBox.Show("برجاء مراجعة قيمة المبالغ", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.KeyDown += ctl_KeyDown;
                    btnPrint.Click += btnPrint_Click;
                    txtPaid.Focus();
                    return;
                }
                if (cmbCustomerName.Text.Trim() == "")
                {
                    MessageBox.Show("يجب تحديد اسم العميل", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    cmbCustomerName.Focus();
                    this.KeyDown += ctl_KeyDown;
                    btnPrint.Click += btnPrint_Click;
                    return;
                }
                if (chkDeliveryEmp.Checked == true)
                {
                    if (cmbDeliveryEmp.Text.Trim() == "")
                    {
                        MessageBox.Show("يجب تحديد مندوب التوصيل", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cmbDeliveryEmp.Focus();
                        this.KeyDown += ctl_KeyDown;
                        btnPrint.Click += btnPrint_Click;
                        return;
                    }
                }
                LD.InitDisp();
                LD.MoveCursorToLine(1);
                LD.WriteText("Paid   " + txtPaid.Text);
                LD.MoveCursorToLine(2);
                LD.WriteText("Change " + txtChange.Text);
                if (MessageBox.Show(string.Format("المستحق :  {1}{0}{0}المدفـوع :  {2}{0}{0}المـتبقى :  {3}{0}{0}", Environment.NewLine, txtDueAmount.Text.ToString(), txtPaid.Text.ToString(), txtChange.Text.ToString()), "تأكيد", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.RightAlign) == DialogResult.No)
                {
                    this.KeyDown += ctl_KeyDown;
                    btnPrint.Click += btnPrint_Click;
                    return;
                }
                else
                {
                    try
                    {
                        SqlCommand cmdCount = new SqlCommand();
                        cmdCount.Connection = con;
                        cmdCount.CommandType = CommandType.StoredProcedure;
                        cmdCount.CommandText = "uspTransactionCount";
                        var transBalance = 0;
                        if (con.State == ConnectionState.Closed) con.Open();
                        var rdr = cmdCount.ExecuteReader();
                        while (rdr.Read())
                        {
                            int.TryParse(rdr["CountTransaction"].ToString(), out transBalance);
                        }
                        if (con.State == ConnectionState.Open) con.Close();
                        var newId = "";
                        if (transBalance <= 0)
                            newId = "" + Program.branch + "" + Program.machine + "" + transBalance;
                        else
                            newId = (transBalance + 1).ToString();

                        Logging.DoLogging("*****Inserting new Transaction*****", "Transaction Id:" + newId, "User:" + Program.empID + " " + Program.fristName + " " + Program.lastName, false, "Transaction");

                        SqlCommand insertTransactionCmd = new SqlCommand();

                        insertTransactionCmd.Connection = con;
                        insertTransactionCmd.CommandType = CommandType.StoredProcedure;
                        insertTransactionCmd.CommandText = "uspTransactionInsert";
                        insertTransactionCmd.Parameters.AddWithValue("@ID", newId);
                        insertTransactionCmd.Parameters.AddWithValue("@CustomerID", cmbCustomerName.SelectedValue);
                        insertTransactionCmd.Parameters.AddWithValue("@Total", txtInvTotal.Text.Trim());
                        insertTransactionCmd.Parameters.AddWithValue("@UserID", Program.empID);
                        insertTransactionCmd.Parameters.AddWithValue("@DiscountPercent", decimal.Parse(txtDiscountPercent.Text.Trim()));
                        insertTransactionCmd.Parameters.AddWithValue("@CashDiscount", txtCashDiscount.Text.Trim());
                        insertTransactionCmd.Parameters.AddWithValue("@CreditAmount", VisaValue);
                        insertTransactionCmd.Parameters.AddWithValue("@DueAmount", txtDueAmount.Text.Trim());
                        insertTransactionCmd.Parameters.AddWithValue("@Paid", txtPaid.Text.Trim());
                        insertTransactionCmd.Parameters.AddWithValue("@Change", txtChange.Text.Trim());
                        insertTransactionCmd.Parameters.AddWithValue("@Notes", txtCustomerID.Text.Trim());
                        if (chkCredit.Checked == true)
                        {
                            insertTransactionCmd.Parameters.AddWithValue("@IsOnCredit", 1);
                        }
                        else
                        {
                            insertTransactionCmd.Parameters.AddWithValue("@IsOnCredit", 0);
                        }
                        insertTransactionCmd.Parameters.AddWithValue("@CreatedTime", DateTime.Now);
                        insertTransactionCmd.Parameters.AddWithValue("@SafeNumber", int.Parse(txtSafeNumber.Text.Trim().ToString()));
                        if (cmbDeliveryEmp.Enabled == true)
                        {
                            insertTransactionCmd.Parameters.AddWithValue("@collected", 0);
                            insertTransactionCmd.Parameters.AddWithValue("@IsDeliveryOrder", 1);
                            insertTransactionCmd.Parameters.AddWithValue("@DeliveryEmloyeeID", cmbDeliveryEmp.SelectedValue);
                        }
                        else
                        {
                            insertTransactionCmd.Parameters.AddWithValue("@collected", 0);
                            insertTransactionCmd.Parameters.AddWithValue("@IsDeliveryOrder", 0);
                            insertTransactionCmd.Parameters.AddWithValue("@DeliveryEmloyeeID", DBNull.Value);
                        }
                        insertTransactionCmd.Parameters.AddWithValue("@CreditCTransNo", CreditCTransNo);

                        if (con.State == ConnectionState.Closed) con.Open();
                        SqlDataReader ssd = insertTransactionCmd.ExecuteReader();
                        ssd.Close();
                        hID = int.Parse(newId);
                        if (con.State == ConnectionState.Open) con.Close();
                        if(deletedTrans > 0)
                        {
                            SqlCommand updateDeletedTransaction = new SqlCommand();

                            updateDeletedTransaction.Connection = con;
                            updateDeletedTransaction.CommandType = CommandType.StoredProcedure;
                            updateDeletedTransaction.CommandText = "uspDeletedTransactionTableUpdate";
                            updateDeletedTransaction.Parameters.AddWithValue("@ID", deletedTrans);
                            updateDeletedTransaction.Parameters.AddWithValue("@TransactionId", hID);
                            if (con.State == ConnectionState.Closed) con.Open();
                            updateDeletedTransaction.ExecuteReader();
                            if (con.State == ConnectionState.Open) con.Close();
                        }
                        Logging.DoLogging("*****Transaction header inserted successfully*****", "Transaction Id:" + newId, "User:" + Program.empID + " " + Program.fristName + " " + Program.lastName, false, "Transaction");
                        Logging.DoLogging("*****Inserting its details*****", "Transaction Id:" + newId, "User:" + Program.empID + " " + Program.fristName + " " + Program.lastName, false, "Transaction");
                        SqlCommand insertDetailCmd = new SqlCommand();
                        Logging.DoLogging("*****Inserting its details*****", "Details Count:" + dgvTransactionsss.Rows.Count, "User:" + Program.empID + " " + Program.fristName + " " + Program.lastName, false, "Transaction");
                        foreach (DataGridViewRow dr in dgvTransactionsss.Rows)
                        {
                            if (dr.Cells["Total"].Value != null & dr.Cells["ItemQty"].Value != null)
                            {
                                insertDetailCmd.Parameters.Clear();
                                insertDetailCmd.CommandType = CommandType.StoredProcedure;
                                insertDetailCmd.Connection = con;
                                insertDetailCmd.CommandText = "uspTransactionDetailInsert";
                                insertDetailCmd.Parameters.AddWithValue("@Cost", dr.Cells["Total"].Value);
                                insertDetailCmd.Parameters.AddWithValue("@SalePrice", dr.Cells["ItemPrice"].Value);
                                insertDetailCmd.Parameters.AddWithValue("@TransactionID", hID);
                                insertDetailCmd.Parameters.AddWithValue("@ItemID", dr.Cells["ItemID"].Value);
                                insertDetailCmd.Parameters.AddWithValue("@Quantity", dr.Cells["ItemQty"].Value);

                                if (con.State == ConnectionState.Closed) con.Open();
                                insertDetailCmd.ExecuteNonQuery();
                                if (con.State == ConnectionState.Open) con.Close();
                            }

                        }
                        if (!cmbDeliveryEmp.Enabled)
                        {
                            SqlCommand updateCreditAmountCmd = new SqlCommand();
                            updateCreditAmountCmd.Connection = con;
                            updateCreditAmountCmd.CommandType = CommandType.Text;
                            updateCreditAmountCmd.CommandText = "UPDATE [TransactionTableH_Main] SET Collected = 1 WHERE ID = @ID;";
                            updateCreditAmountCmd.Parameters.AddWithValue("@ID", newId);
                            if (con.State == ConnectionState.Closed) con.Open();
                            updateCreditAmountCmd.ExecuteNonQuery();
                            if (con.State == ConnectionState.Open) con.Close();
                        }

                    }
                    catch (Exception ex)
                    {
                        Logging.DoLogging("*****Exception happend*****", "400 Exception: " + ex.Message, "User:" + Program.empID + " " + Program.fristName + " " + Program.lastName, false, "Transaction");
                        Logging.DoLogging("*****Exception happend*****", "400 StackTrace: " + ex.StackTrace, "User:" + Program.empID + " " + Program.fristName + " " + Program.lastName, false, "Transaction");
                        if (con.State == ConnectionState.Open) con.Close();
                        this.KeyDown += ctl_KeyDown;
                        btnPrint.Click += btnPrint_Click;
                        return;
                    }

                    if (chkDeliveryEmp.Checked == false)
                    {
                        printTransaction();
                    }
                    if (chkDeliveryEmp.Checked == true)
                    {
                        printTransactionWithDelivery();
                    }
                    LD.InitDisp();
                    LD.WriteText("Thanks for shopping");
                    LD.MoveCursorToLine(2);
                    LD.WriteText("     with IMART     ");
                    clearData();
                    clearTempTransactions();
                    DisableDiscount();
                    txtTransactionID.Text = (hID + 1).ToString();
                    this.KeyDown += ctl_KeyDown;
                    btnPrint.Click += btnPrint_Click;
                }
            }
            catch (Exception ex)
            {
                this.KeyDown += ctl_KeyDown;
                btnPrint.Click += btnPrint_Click;
                con.Close();
            }
        }
        private void clearTempTransactions()
        {
            SqlCommand clearTempCmd = new SqlCommand();
            clearTempCmd.Connection = con;
            clearTempCmd.CommandType = CommandType.StoredProcedure;
            clearTempCmd.CommandText = "uspTempTransactionDelete";
            clearTempCmd.Parameters.AddWithValue("@UserID", Program.empID);
            if (con.State == ConnectionState.Closed) con.Open();
            clearTempCmd.ExecuteNonQuery();
            if (con.State == ConnectionState.Open) con.Close();
        }

        private void DeleteItemTempTransactions(string itemId, string itembarcode)
        {
            SqlCommand clearTempCmd = new SqlCommand();
            clearTempCmd.Connection = con;
            clearTempCmd.CommandType = CommandType.StoredProcedure;
            clearTempCmd.CommandText = "uspTempTransactionDeleteByItem";
            clearTempCmd.Parameters.AddWithValue("@UserID", Program.empID);
            clearTempCmd.Parameters.AddWithValue("@ItemID", itemId);
            clearTempCmd.Parameters.AddWithValue("@ItemBarcode", itembarcode);
            if (con.State == ConnectionState.Closed) con.Open();
            clearTempCmd.ExecuteNonQuery();
            if (con.State == ConnectionState.Open) con.Close();
        }
        //UserActivityHook acHook;
        //void threadHandler(object e)
        //{
        //    KeyEventArgs p = (KeyEventArgs)e;
        //    if (p.KeyData == Keys.F6)
        //    {
        //        ItemSearsh itms = new ItemSearsh(this);
        //        itms.ShowDialog();
        //    }
        //    if (p.KeyData == Keys.F7)
        //    {
        //        CustomerSearsh custSearsh = new CustomerSearsh(this);
        //        custSearsh.ShowDialog();
        //    }
        //}
        //void gkh_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.F6)
        //    {
        //        ItemSearsh itms = new ItemSearsh(this);
        //        itms.ShowDialog(this);
        //        this.dgvTransactions.Focus();
        //        e.Handled = true;
        //    }
        //    if (e.KeyData == Keys.F7)
        //    {
        //        CustomerSearsh custSearsh = new CustomerSearsh(this);
        //        custSearsh.ShowDialog(this);
        //        e.Handled = true;
        //    }
        //    //e.Handled = true;
        //}

        //void acHook_KeyDown(object sender, KeyEventArgs e)
        //{
        //    //Thread th = new Thread(new ParameterizedThreadStart(threadHandler));
        //    //th.Start(e);
        //    //th.Join();
        //    if (e.KeyData == Keys.F6)
        //    {
        //        e.Handled = true;
        //        ItemSearsh itms = new ItemSearsh(this);
        //        itms.ShowDialog(this);
        //        this.dgvTransactions.Focus();
        //    }
        //    if (e.KeyData == Keys.F7)
        //    {
        //        e.Handled = true;
        //        CustomerSearsh custSearsh = new CustomerSearsh(this);
        //        custSearsh.ShowDialog(this);
        //    }
        //}

        //void acHook_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.F6)
        //    {
        //        ItemSearsh itms = new ItemSearsh(this);
        //        itms.ShowDialog();
        //    }
        //    if (e.KeyData == Keys.F7)
        //    {
        //        CustomerSearsh custSearsh = new CustomerSearsh(this);
        //        custSearsh.ShowDialog();
        //    }
        //}
        private void getMaxInvID()
        {
            SqlCommand getInvIDCmd = new SqlCommand();
            getInvIDCmd.Connection = con;
            getInvIDCmd.CommandType = CommandType.StoredProcedure;
            getInvIDCmd.CommandText = "uspGetMaxTransactionID";

            if (con.State == ConnectionState.Closed) con.Open();
            object iid = getInvIDCmd.ExecuteScalar();
            invID = int.Parse((iid).ToString()) + 1;
            if (con.State == ConnectionState.Open) con.Close();
        }
        private void txtCashDiscount_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtCashDiscount.Text == "")
                {
                    txtCashDiscount.Text = "0,00";
                }
                cashDiscount =Math.Round( decimal.Parse(txtCashDiscount.Text.Trim()),2);
                txtCashDiscount.Text = cashDiscount.ToString();
                if (cashDiscount < 0 || cashDiscount > invTotal)
                {
                    MessageBox.Show("قيمة غير مناسبة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCashDiscount.Text = "0,00";
                    txtCashDiscount.Focus();
                    selectTxt(this, e);
                }
                else
                {
                    CalcudalteDueAmount();
                }
                calculateChange();
            }
            catch (Exception ex)
            { }

        }
        private void txtPaid_TextChanged(object sender, EventArgs e)
        {
            if (txtPaid.Text == "") return;
            try
            {
                paidCash =Math.Round( decimal.Parse(txtPaid.Text),2);
            }
            catch
            {
                MessageBox.Show("ادخل ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPaid.Focus();
                txtPaid.Clear();
                selectTxt(this, e);
            }

        }
        private void txtPaid_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtPaid.Text == "")
                {
                    txtPaid.Text = "0.00";
                    //MessageBox.Show("لا يمكن ترك القيمة فارغة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //txtPaid.Focus();
                    selectTxt(this, e);
                    return;
                }

                if (paidCash < dueAmount)
                {
                    if (paidCash == 0)
                    {
                        return;
                    }
                    else
                    {
                        txtPaid.Text = "0.00";
                        MessageBox.Show("المبلغ غير صحيح", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //txtPaid.Clear();
                        //txtPaid.Focus();
                    }
                }
                else
                {
                    calculateChange();
                }
            }
            catch (Exception ex)
            { }

        }
        private void txtDiscountPercent_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtDiscountPercent.Text != "" && int.Parse(txtDiscountPercent.Text) != 0)
                {
                    txtCashDiscount.Text = "0";
                    decimal.Parse(txtDiscountPercent.Text.Trim());
                }
            }
            catch
            {
                MessageBox.Show("ادخل ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDiscountPercent.Text = "0";
                txtDiscountPercent.Focus();
                selectTxt(this, e);
            }
        }

        private void txtDiscountPercent_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtDiscountPercent.Text.Trim() == "")
                {
                    txtDiscountPercent.Text = "0,00";
                }
                discountPercent =Math.Round( decimal.Parse(txtDiscountPercent.Text.Trim()),2);
                txtDiscountPercent.Text = discountPercent.ToString();
                if (discountPercent == 0)
                { return; }

                else if (discountPercent < 0 || discountPercent > 100)
                {
                    MessageBox.Show("قيمة غير مناسبة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDiscountPercent.Text = "0,00";
                    txtDiscountPercent.Focus();
                    selectTxt(this, e);
                    return;
                }
                else
                {
                    CalcudalteDueAmount();
                }
                calculateChange();
            }
            catch (Exception ex)
            { }
        }
        private void txtCustomerID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //SqlCommand custIdCmd = new SqlCommand();
                //custIdCmd.Connection = con;
                //custIdCmd.CommandType = CommandType.StoredProcedure;
                //custIdCmd.CommandText = "uspGetCustIdByAccNum";
                //custIdCmd.Parameters.AddWithValue("@AccountNumber", txtCustomerID.Text.Trim());
                //if (con.State == ConnectionState.Closed) con.Open();

                //SqlDataReader readCustID = null;
                //readCustID = custIdCmd.ExecuteReader();
                //int sv = -1;
                //while (readCustID.Read())
                //{
                //    sv = int.Parse(readCustID[0].ToString());
                //}
                //readCustID.Close();
                //if (con.State == ConnectionState.Open) con.Close();
                //cmbCustomerName.SelectedValue = sv;
                cmbCustomerName.SelectedValue = int.Parse(txtCustomerID.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show("لا يوجد عميل بهذا الرقم", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void txtCustomerID_Enter(object sender, EventArgs e)
        {
            selectTxt(sender, e);
        }
        private void cmbCustomerName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (cmbCustomerName.Text == "")
                {
                    txtItemBarcode.Focus();
                    return;
                }
                else
                {
                    DataTable tbSP = ((DataTable)cmbCustomerName.DataSource).Copy();
                    tbSP.DefaultView.RowFilter = "Name='" + cmbCustomerName.Text + "'";
                    if (tbSP.DefaultView.Count == 0)
                    {
                        MessageBox.Show("اسم العميل غير صحيح", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cmbCustomerName.Text = "";
                        cmbCustomerName.Focus();
                    }
                    tbSP.DefaultView.RowFilter = "";
                }
            }
            catch { }
        }
        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //SqlCommand accountNumCmd = new SqlCommand();
                //accountNumCmd.Connection = con;
                //accountNumCmd.CommandType = CommandType.StoredProcedure;
                //accountNumCmd.CommandText = "uspGetCustAccountNum";
                //accountNumCmd.Parameters.AddWithValue("@ID", cmbCustomerName.SelectedValue);
                //if (con.State == ConnectionState.Closed) con.Open();
                //SqlDataReader readAccountNum = accountNumCmd.ExecuteReader();
                //string sID = null;
                //while (readAccountNum.Read())
                //{
                //    sID = readAccountNum[0].ToString();
                //}
                //readAccountNum.Close();
                //if (con.State == ConnectionState.Open) con.Close();
                //txtCustomerID.Text = sID;
                txtCustomerID.Text = cmbCustomerName.SelectedValue.ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show("لا يوجد عميل بهذاالرقم", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void TransactionScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
            //MainMenu mn = new MainMenu();
            //mn.Show();
        }
        public void txtItemBarcode_TextChanged(object sender, EventArgs e)
        {
            if (txtItemBarcode.Text.Length > 16)
            {
                txtItemBarcode.Text = "";
                MessageBox.Show("خطأ في الادخال", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                txtItemBarcode.Focus();
                return;
            }
        }
        private void chkCredit_CheckedChanged(object sender, EventArgs e)
        {
            
        }
        private void chkCreditCard_CheckedChanged(object sender, EventArgs e)
        {
            
        }
        private void txtDueAmount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                dueAmount =Math.Round( decimal.Parse(txtDueAmount.Text),2);
            }
            catch (Exception ex)
            { }
        }

        private void dgvTransactionsss_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            dgvTransactionsss.Columns["ItemPrice"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemUnit"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemName"].ReadOnly = true;
            dgvTransactionsss.Columns["Total"].ReadOnly = true;
            if (Program.supervisorID > 0)
            {
                //if (dgvTransactions.Columns[e.ColumnIndex].Name == "ItemQty")
                if (dgvTransactionsss.Rows[e.RowIndex].Cells["ItemQty"].Value == null)
                {
                    MessageBox.Show("لا يمكن ترك القيمة فارغة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    dgvTransactionsss.Rows[e.RowIndex].Cells["ItemQty"].Value = 1;
                    dgvTransactionsss.BeginEdit(true);
                    return;
                }
                else
                {

                    try
                    {
                        decimal itemUnit = decimal.Parse(dgvTransactionsss.Rows[e.RowIndex].Cells["ItemQty"].Value.ToString());
                        if (itemUnit < itemQty)
                        {
                            MessageBox.Show("ادخل قيمة صحيحة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                            dgvTransactionsss.Rows[e.RowIndex].Cells["ItemQty"].Value = itemQty;
                            dgvTransactionsss.BeginEdit(true);
                            return;

                        }
                        else
                        {
                            decimal itemPrice = decimal.Parse(dgvTransactionsss.Rows[e.RowIndex].Cells["ItemPrice"].Value.ToString());
                            decimal itemTotal = Math.Round(itemUnit * itemPrice, 2);
                            dgvTransactionsss.Rows[e.RowIndex].Cells["Total"].Value = itemTotal.ToString();
                            calculateInvoiceTotal();
                            CalcudalteDueAmount();
                            calculateChange();
                            //dgvTransactions.Rows[e.RowIndex].Cells["ItemQty"].Selected = true;
                            dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
                            this.txtItemBarcode.Focus();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("يجب ادخال ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                        dgvTransactionsss.Rows[e.RowIndex].Cells["ItemQty"].Value = 1;
                        dgvTransactionsss.BeginEdit(true);
                        dgvTransactionsss.Columns["ItemPrice"].ReadOnly = true;
                        dgvTransactionsss.Columns["ItemUnit"].ReadOnly = true;
                        dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
                        dgvTransactionsss.Columns["ItemName"].ReadOnly = true;
                        dgvTransactionsss.Columns["Total"].ReadOnly = true;
                        dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
                        return;
                    }
                    dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
                    txtPaid.Text = "0.00";
                    txtChange.Text = "0.00";
                }
            }


            //try
            //{
            //    for (int i = 0; i <= dgvTransactions.Rows.Count - 1; i++)
            //        if (dgvTransactions.Rows[i].Cells[1].Value != null)
            //            if (dgvTransactions.Rows[i].Cells[1].Value.ToString().Length == 0)
            //                dgvTransactions.Rows.RemoveAt(i--);
            //    //GridComparer comparer = new GridComparer();
            //dgvTransactions.Sort(comparer);
            //}
            //catch { }
            //this.dgvTransactions.Rows.Add();
        }

        private void dgvTransactionsss_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgvTransactionsss.Columns["ItemPrice"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemUnit"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemName"].ReadOnly = true;
            dgvTransactionsss.Columns["Total"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
            itemTotal = 0;
        }

        //private void dgvTransactionsss_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        //{
        //    dgvTransactionsss.ClearSelection();
        //    dgvTransactionsss.CurrentCell = dgvTransactionsss.Rows[e.RowIndex].Cells["ItemQty"];
        //    dgvTransactionsss.BeginEdit(true);
        //}

        private void dgvTransactionsss_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            dgvTransactionsss.Columns["ItemPrice"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemUnit"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemName"].ReadOnly = true;
            dgvTransactionsss.Columns["Total"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
            if (dgvTransactionsss.RowCount <= 0)
                deletedTrans = 0;
            try
            {
                calculateInvoiceTotal();
                CalcudalteDueAmount();
                calculateChange();
                Program.delTransactionRow = false;
            }
            catch (Exception ex)
            { }
        }


        private void chkDeliveryEmp_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (chkDeliveryEmp.Checked == true)
                {
                    cmbDeliveryEmp.Enabled = true;
                    cmbDeliveryEmp.SelectedIndex = 0;
                    cmbDeliveryEmp.Focus();
                }

                if (chkDeliveryEmp.Checked == false)
                {
                    cmbDeliveryEmp.Enabled = false;
                    cmbDeliveryEmp.Text = "";
                }
            }
            catch (Exception ex)
            { }
        }

        private void cmbDeliveryEmp_Leave(object sender, EventArgs e)
        {
            try
            {
                if (cmbDeliveryEmp.Text=="")
                {
                    return;
                }
                else
                {
                    DataTable tbDP = ((DataTable)cmbDeliveryEmp.DataSource).Copy();
                    tbDP.DefaultView.RowFilter = "EmpName='" + cmbDeliveryEmp.Text + "'";
                    if (tbDP.DefaultView.Count == 0)
                    {
                        MessageBox.Show("اسم المندوب غير صحيح", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cmbDeliveryEmp.Text = "";
                        cmbDeliveryEmp.Focus();
                    }
                    tbDP.DefaultView.RowFilter = "";

                }
            }
            catch { }
        }

        private void dgvTransactionsss_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            dgvTransactionsss.Columns["ItemPrice"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemUnit"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemName"].ReadOnly = true;
            dgvTransactionsss.Columns["Total"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
            txtItemBarcode.Focus();
            txtPaid.Text = "0.00";
            txtChange.Text = "0.00";
        }
        private void printTransaction(bool copy = false)
        {
            TransactionReceipt tr = new TransactionReceipt();
            tr.transactionDate = DateTime.Now;
            tr.transactionTime = DateTime.Now;
            tr.TransactionNo = hID;
            tr.safeNo = int.Parse(txtSafeNumber.Text);
            tr.cashirName = lblCashir.Text;
            tr.deliveryEmpName = cmbDeliveryEmp.Text;
            tr.custName = cmbCustomerName.Text;
            tr.paymentMethod = paymentMethod;
            tr.GridViewRow = dgvTransactionsss;
            tr.copy = copy;
            //foreach (DataGridViewRow dr in dgvTransactionsss.Rows)
            //{
            //    tr.itemTot= float.Parse(dr.Cells["Total"].Value.ToString());
            //    tr.qty=float.Parse(dr.Cells["ItemQty"].Value.ToString());
            //    tr.price=float.Parse(dr.Cells["ItemPrice"].Value.ToString());
            //    tr.itemName=dr.Cells["ItemName"].Value.ToString();
            //}
            tr.transactionTot =Math.Round( decimal.Parse(txtInvTotal.Text),2);
            tr.discount = Math.Round(decimal.Parse(txtInvTotal.Text) - decimal.Parse(txtDueAmount.Text),2);
            tr.dueAmount = Math.Round(decimal.Parse(txtDueAmount.Text),2);
            tr.paid = Math.Round(dueAmount + change, 2);
            tr.change = Math.Round(decimal.Parse(txtChange.Text), 2);
            tr.seLogo = (Bitmap)Image.FromFile("s1.png"); 
            int rowsCount = dgvTransactionsss.RowCount * 40;
            tr.print(rowsCount+ 820);

        }

        private void printTransactionWithDelivery(bool copy = false)
        {
            SqlCommand phoneCmd = new SqlCommand();
            phoneCmd.Connection = con;
            phoneCmd.CommandType = CommandType.StoredProcedure;
            phoneCmd.CommandText = "GetCustPhoneNumber";
            phoneCmd.Parameters.AddWithValue("@ID", cmbCustomerName.SelectedValue);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readPhone =  phoneCmd.ExecuteReader();
            while (readPhone.Read())
            {
                custPhone = readPhone[0].ToString();
                custAddress = readPhone[1].ToString();
            }
            readPhone.Close();
   
            TransactionReceipt trWd = new TransactionReceipt();
            trWd.transactionDate = DateTime.Now;
            trWd.transactionTime = DateTime.Now;
            trWd.TransactionNo = hID;
            trWd.safeNo = int.Parse(txtSafeNumber.Text);
            trWd.cashirName = lblCashir.Text;
            trWd.deliveryEmpName = cmbDeliveryEmp.Text;
            trWd.custName = cmbCustomerName.Text;
            trWd.custPhone = custPhone;
            trWd.custAdress = custAddress;
            trWd.paymentMethod = paymentMethod;
            trWd.GridViewRow = dgvTransactionsss;
            trWd.copy = copy;
            //foreach (DataGridViewRow dr in dgvTransactionsss.Rows)
            //{
            //    tr.itemTot= float.Parse(dr.Cells["Total"].Value.ToString());
            //    tr.qty=float.Parse(dr.Cells["ItemQty"].Value.ToString());
            //    tr.price=float.Parse(dr.Cells["ItemPrice"].Value.ToString());
            //    tr.itemName=dr.Cells["ItemName"].Value.ToString();
            //}
            trWd.transactionTot = Math.Round(decimal.Parse(txtInvTotal.Text),2);
            trWd.discount = Math.Round(decimal.Parse(txtDiscountPercent.Text),2);
            trWd.dueAmount =Math.Round( decimal.Parse(txtDueAmount.Text),2);
            trWd.paid =Math.Round( decimal.Parse(txtPaid.Text),2);
            trWd.change = Math.Round(decimal.Parse(txtChange.Text), 2);
            int rowsCount = dgvTransactionsss.RowCount * 40;
            trWd.printTranWiD(rowsCount + 820);
        }
        private void btnReturns_Click(object sender, EventArgs e)
        {
            AdminLogin ad = new AdminLogin();
            ad.ShowDialog();
        }

        private void eliveryStatus_Click(object sender, EventArgs e)
        {
            DeliveryOrderStatus delOrdSc = new DeliveryOrderStatus();
            delOrdSc.ShowDialog();
        }

        private void chkDeliveryEmp_Enter(object sender, EventArgs e)
        {
            ((CheckBox)sender).BackColor = Color.Red;
        }

        private void chkDeliveryEmp_Leave(object sender, EventArgs e)
        {
            ((CheckBox)sender).BackColor = Color.Transparent;
        }

        private void txtCredit_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void txtItemBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                SqlDataReader readItem;
                if (e.KeyCode == Keys.Enter)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "uspGetItemsByBarcode";
                    if (con.State == ConnectionState.Closed) con.Open();
                    if (int.Parse(txtItemBarcode.Text.Trim().Substring(0, 2)) == 23 & txtItemBarcode.Text.Length >= 13)
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Barcode", txtItemBarcode.Text.Trim().Substring(2, 5));
                        itemQty = Math.Round(decimal.Parse(txtItemBarcode.Text.Trim().Substring(7, 5)) / 1000, 3);
                        if (itemQty <= 0)
                        {
                            itemQty = 1;
                        }

                        readItem = cmd.ExecuteReader();
                        if (readItem.HasRows == false)
                        {
                            readItem.Close();
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@Barcode", txtItemBarcode.Text);
                            itemQty = 1;
                            readItem = cmd.ExecuteReader();
                            if (readItem.HasRows == false)
                            {
                                readItem.Close();
                                cmd.Parameters.Clear();
                                txtItemBarcode.Clear();
                                ItemNotFound itmNFo = new ItemNotFound();
                                itmNFo.ShowDialog(this);
                                txtItemBarcode.Clear();
                                this.txtItemBarcode.Focus();
                                return;
                            }

                        }

                    }
                    else
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Barcode", txtItemBarcode.Text);
                        itemQty = 1;
                        readItem = cmd.ExecuteReader();
                    }
                    if (readItem.HasRows == false)
                    {
                        readItem.Close();
                        cmd.Parameters.Clear();
                        txtItemBarcode.Clear();
                        ItemNotFound itmNFo = new ItemNotFound();
                        itmNFo.ShowDialog(this);
                        txtItemBarcode.Clear();
                        txtItemBarcode.Focus();
                        return;
                    }
                    while (readItem.Read())
                    {
                        itemID = readItem[0].ToString();
                        itemBarcode = readItem[1].ToString();
                        itemName = readItem[2].ToString();
                        itemUnit = readItem[3].ToString();
                        itemPrice = Math.Round(decimal.Parse(readItem[4].ToString()), 2).ToString();
                    }
                    readItem.Close();
                    if (con.State == ConnectionState.Open) con.Close();
                    itemTotal = Math.Round(itemQty * decimal.Parse(itemPrice), 2);

                    LD.InitDisp();
                    LD.MoveCursorToLine(1);
                    if (itemName.Length > 12)
                    {
                        LD.WriteText(itemName.Substring(0, 11) + "  " + itemTotal);
                    }
                    else
                    {
                        LD.WriteText(itemName);
                        LD.MoveCursor(16, 1);
                        LD.WriteText(itemTotal.ToString());
                    }

                    bool IsExist = false;
                    DataGridViewRow Existdgvr = null;
                    foreach (DataGridViewRow dgvr in dgvTransactionsss.Rows)
                    {
                        if (dgvr.Cells["ItemID"].Value.ToString() == itemID)
                        {
                            IsExist = true;
                            Existdgvr = dgvr;
                        }
                    }
                    if (!IsExist)
                    {
                        dgvTransactionsss.Rows.Add(itemID, itemBarcode, itemName, itemUnit, itemPrice, itemQty, itemTotal);
                    }
                    else
                    {
                        decimal ExistQTY = decimal.Parse(Existdgvr.Cells["ItemQty"].Value.ToString());
                        decimal NewQTY = itemQty + ExistQTY;
                        Existdgvr.Cells["ItemQty"].Value = NewQTY;
                        Existdgvr.Cells["Total"].Value = NewQTY * decimal.Parse(itemPrice);
                    }

                    SqlCommand tempItemCmd = new SqlCommand();
                    tempItemCmd.Connection = con;
                    tempItemCmd.CommandType = CommandType.StoredProcedure;
                    tempItemCmd.CommandText = "uspTempTransactionInsert";
                    tempItemCmd.Parameters.AddWithValue("@UserID", Program.empID);
                    tempItemCmd.Parameters.AddWithValue("@ItemID", itemID);
                    tempItemCmd.Parameters.AddWithValue("@ItemBarcode", itemBarcode);
                    tempItemCmd.Parameters.AddWithValue("@ItemName", itemName);
                    tempItemCmd.Parameters.AddWithValue("@ItemUnit", itemUnit);
                    tempItemCmd.Parameters.AddWithValue("@ItemPrice", itemPrice);
                    tempItemCmd.Parameters.AddWithValue("@ItemQty", itemQty);
                    tempItemCmd.Parameters.AddWithValue("@ItemTotal", Math.Round(itemQty * decimal.Parse(itemPrice), 2));
                    if (con.State == ConnectionState.Closed) con.Open();
                    tempItemCmd.ExecuteNonQuery();
                    if (con.State == ConnectionState.Open) con.Close();
                    
                    txtItemBarcode.Clear();
                    calculateInvoiceTotal();
                    CalcudalteDueAmount();
                    dgvTransactionsss.FirstDisplayedScrollingRowIndex = dgvTransactionsss.Rows.Count - 1;
                }
            }
            catch (Exception ex)
            { }
        }

        private void dgvTransactionsss_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            dgvTransactionsss.Columns["ItemPrice"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemUnit"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemName"].ReadOnly = true;
            dgvTransactionsss.Columns["Total"].ReadOnly = true;
            //dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
            //bool testNumbers;
            //if (dgvTransactionsss.Columns[e.ColumnIndex].Name == "ItemQty")
            //{
            //    decimal num = 0;

            //    testNumbers = decimal.TryParse(e.FormattedValue.ToString(), out num);
            //    if (testNumbers == false && e.FormattedValue.ToString() != string.Empty)
            //    {
            //        MessageBox.Show("يجب ادخال ارقام فقط", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        e.Cancel = true;
            //        dgvTransactionsss.Rows[e.RowIndex].Cells["ItemQty"].Value = "";
            //        dgvTransactionsss.BeginEdit(true);
            //        return;
            //    }

            //}

        }

        private void dgvTransactionsss_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (Program.delTransactionRow == false)
                {
                    e.Cancel = true;
                }
                if (Program.delTransactionRow == true)
                {
                    if (MessageBox.Show("هل انت متأكد", "تأكيد", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.RightAlign) == DialogResult.No)
                    {
                        Program.delTransactionRow = false;
                        txtItemBarcode.Focus();
                        e.Cancel = true;
                    }
                    else
                    {
                        if(deletedTrans <= 0)
                        {
                            SqlCommand insertTransactionCmd = new SqlCommand();

                            insertTransactionCmd.Connection = con;
                            insertTransactionCmd.CommandType = CommandType.StoredProcedure;
                            insertTransactionCmd.CommandText = "uspDeletedTransactionTableInsert";
                            insertTransactionCmd.Parameters.AddWithValue("@CustomerID", cmbCustomerName.SelectedValue);
                            insertTransactionCmd.Parameters.AddWithValue("@Total", txtInvTotal.Text.Trim());
                            insertTransactionCmd.Parameters.AddWithValue("@UserID", Program.empID);
                            insertTransactionCmd.Parameters.AddWithValue("@DiscountPercent", decimal.Parse(txtDiscountPercent.Text.Trim()));
                            insertTransactionCmd.Parameters.AddWithValue("@CashDiscount", txtCashDiscount.Text.Trim());
                            insertTransactionCmd.Parameters.AddWithValue("@CreditAmount", VisaValue);
                            insertTransactionCmd.Parameters.AddWithValue("@DueAmount", txtDueAmount.Text.Trim());
                            insertTransactionCmd.Parameters.AddWithValue("@Paid", txtPaid.Text.Trim());
                            insertTransactionCmd.Parameters.AddWithValue("@Change", txtChange.Text.Trim());
                            insertTransactionCmd.Parameters.AddWithValue("@Notes", txtCustomerID.Text.Trim());
                            if (chkCredit.Checked == true)
                            {
                                insertTransactionCmd.Parameters.AddWithValue("@IsOnCredit", 1);
                            }
                            else
                            {
                                insertTransactionCmd.Parameters.AddWithValue("@IsOnCredit", 0);
                            }
                            insertTransactionCmd.Parameters.AddWithValue("@CreatedTime", DateTime.Now);
                            insertTransactionCmd.Parameters.AddWithValue("@SafeNumber", int.Parse(txtSafeNumber.Text.Trim().ToString()));
                            if (cmbDeliveryEmp.Enabled == true)
                            {
                                insertTransactionCmd.Parameters.AddWithValue("@collected", 0);
                                insertTransactionCmd.Parameters.AddWithValue("@IsDeliveryOrder", 1);
                                insertTransactionCmd.Parameters.AddWithValue("@DeliveryEmloyeeID", cmbDeliveryEmp.SelectedValue);
                            }
                            else
                            {
                                insertTransactionCmd.Parameters.AddWithValue("@collected", 0);
                                insertTransactionCmd.Parameters.AddWithValue("@IsDeliveryOrder", 0);
                                insertTransactionCmd.Parameters.AddWithValue("@DeliveryEmloyeeID", DBNull.Value);
                            }
                            insertTransactionCmd.Parameters.AddWithValue("@CreditCTransNo", CreditCTransNo);

                            if (con.State == ConnectionState.Closed) con.Open();
                            int.TryParse(insertTransactionCmd.ExecuteScalar().ToString(), out deletedTrans);
                            if (con.State == ConnectionState.Open) con.Close();
                        }
                        //MessageBox.Show(dgvTransactionsss.CurrentRow.Cells[""].Value    .Index.ToString());
                        //this.dgvTransactionsss.Rows.RemoveAt(this.dgvTransactionsss.CurrentRow.Index);
                        SqlCommand insertDetailCmd = new SqlCommand();
                        insertDetailCmd.Parameters.Clear();
                        insertDetailCmd.CommandType = CommandType.StoredProcedure;
                        insertDetailCmd.Connection = con;
                        insertDetailCmd.CommandText = "uspDeletedTransactionDetailInsert";
                        insertDetailCmd.Parameters.AddWithValue("@Cost", dgvTransactionsss.CurrentRow.Cells["Total"].Value);
                        insertDetailCmd.Parameters.AddWithValue("@SalePrice", dgvTransactionsss.CurrentRow.Cells["ItemPrice"].Value);
                        insertDetailCmd.Parameters.AddWithValue("@TransactionID", deletedTrans);
                        insertDetailCmd.Parameters.AddWithValue("@ItemID", dgvTransactionsss.CurrentRow.Cells["ItemID"].Value.ToString());
                        insertDetailCmd.Parameters.AddWithValue("@Quantity", dgvTransactionsss.CurrentRow.Cells["ItemQty"].Value);
                        insertDetailCmd.Parameters.AddWithValue("@ItemBarCode", dgvTransactionsss.CurrentRow.Cells["ItemBarcode"].Value);
                        insertDetailCmd.Parameters.AddWithValue("@ItemName", dgvTransactionsss.CurrentRow.Cells["ItemName"].Value);
                        insertDetailCmd.Parameters.AddWithValue("@ItemUnit", dgvTransactionsss.CurrentRow.Cells["ItemUnit"].Value);
                        insertDetailCmd.Parameters.AddWithValue("@ItemPrice", dgvTransactionsss.CurrentRow.Cells["ItemPrice"].Value);
                        insertDetailCmd.Parameters.AddWithValue("@ItemQty", dgvTransactionsss.CurrentRow.Cells["ItemQty"].Value);
                        insertDetailCmd.Parameters.AddWithValue("@ItemTotal", dgvTransactionsss.CurrentRow.Cells["Total"].Value);

                        if (con.State == ConnectionState.Closed) con.Open();
                        insertDetailCmd.ExecuteNonQuery();
                        if (con.State == ConnectionState.Open) con.Close();

                        SqlCommand updateDeleteCmd = new SqlCommand();
                        updateDeleteCmd.Parameters.Clear();
                        updateDeleteCmd.CommandType = CommandType.StoredProcedure;
                        updateDeleteCmd.Connection = con;
                        updateDeleteCmd.CommandText = "uspDeletedTransactionTableUpdateTime";
                        updateDeleteCmd.Parameters.AddWithValue("@ID", deletedTrans);
                        
                        if (con.State == ConnectionState.Closed) con.Open();
                        updateDeleteCmd.ExecuteNonQuery();
                        if (con.State == ConnectionState.Open) con.Close();

                        DeleteItemTempTransactions(dgvTransactionsss.CurrentRow.Cells["ItemID"].Value.ToString(), dgvTransactionsss.CurrentRow.Cells["ItemBarcode"].Value.ToString());
                        CalcudalteDueAmount();
                        calculateChange();
                        calculateInvoiceTotal();
                        Program.delTransactionRow = false;
                        txtItemBarcode.Focus();
                    }
                }
                dgvTransactionsss.Columns["ItemPrice"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemUnit"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemName"].ReadOnly = true;
                dgvTransactionsss.Columns["Total"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
            }
            catch (Exception ex)
            {
                dgvTransactionsss.Columns["ItemPrice"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemUnit"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemName"].ReadOnly = true;
                dgvTransactionsss.Columns["Total"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
            }
            
        }

        private void dgvTransactionsss_Enter(object sender, EventArgs e)
        {
            dgvTransactionsss.Columns["ItemPrice"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemUnit"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemName"].ReadOnly = true;
            dgvTransactionsss.Columns["Total"].ReadOnly = true;
            dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
            //dgvTransactionsss.ClearSelection();
            //dgvTransactionsss.Rows[dgvTransactionsss.Rows.Count - 1].Selected = true;
        }

        private void dgvTransactionsss_Leave(object sender, EventArgs e)
        {
            try
            {
                dgvTransactionsss.ClearSelection();
                dgvTransactionsss.Columns["ItemPrice"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemUnit"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemBarcode"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemName"].ReadOnly = true;
                dgvTransactionsss.Columns["Total"].ReadOnly = true;
                dgvTransactionsss.Columns["ItemQty"].ReadOnly = true;
            }
            catch (Exception ex)
            { }
        }

        private void chkCredit_Enter(object sender, EventArgs e)
        {
            ((CheckBox)sender).BackColor = Color.Red;
        }

        private void chkCredit_Leave(object sender, EventArgs e)
        {
            ((CheckBox)sender).BackColor = Color.Transparent;
        }

        private void btnCreditCard_Click(object sender, EventArgs e)
        {
            CreditCardTransaction cct = new CreditCardTransaction();
            cct.Value = decimal.Parse(txtDueAmount.Text);
            cct.ShowDialog();
            if (cct.Result == DialogResult.OK)
            {
                txtItemBarcode.Enabled = false;
                dgvTransactionsss.Enabled = false;
                CreditCTransNo = cct.TransNo;
                chkCredit.Checked = true;
                if (cct.CashValue > 0)
                {
                    paymentMethod = GetPaymentMethod(4);
                    CashValue = cct.CashValue;
                    VisaValue = cct.VisaValue;
                }
                else
                {
                    paymentMethod = GetPaymentMethod(1);
                    VisaValue = cct.VisaValue;
                }
                txtPaid.Text = (VisaValue + CashValue).ToString();
                txtPaid.Focus();
                txtPaid.SelectAll();
            }
            else
            {
                CreditCTransNo = "";
                chkCredit.Checked = false;
                paymentMethod = GetPaymentMethod(0);
            }
        }

        private void txtEnableDiscount_Enter(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Red;
        }

        private void txtEnableDiscount_Leave(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Violet;
        }
        public void enableDiscount()
        {
            txtDiscountPercent.Enabled = true;
            txtCashDiscount.Enabled = true;
            txtEnableDiscount.Enabled = false;
            txtEnableDiscount.BackgroundImage = Properties.Resources.sales_gray;
        }
        private void DisableDiscount()
        {
            txtDiscountPercent.Enabled = false;
            txtCashDiscount.Enabled = false;
            txtEnableDiscount.Enabled = true;
            txtEnableDiscount.BackgroundImage = Properties.Resources.sales;
        }
        private void txtEnableDiscount_Click(object sender, EventArgs e)
        {
            AdminEnableDiscount adedis = new AdminEnableDiscount();
            adedis.ShowDialog();
            if (Program.enableDis == true)
            {
                enableDiscount();
            }

        }

        private void btnPrint_MouseEnter(object sender, EventArgs e)
        {
            btnPrint.BackgroundImage = Properties.Resources.print_gray;
        }

        private void btnPrint_MouseLeave(object sender, EventArgs e)
        {
            btnPrint.BackgroundImage = Properties.Resources.print_green;
        }

        private void btnCreditCard_MouseEnter(object sender, EventArgs e)
        {
            btnCreditCard.BackgroundImage = Properties.Resources.Credit_card_gray;
        }

        private void btnCreditCard_MouseLeave(object sender, EventArgs e)
        {
            btnCreditCard.BackgroundImage = Properties.Resources.Credit_card;
        }

        private void btnDeliveryStatus_MouseEnter(object sender, EventArgs e)
        {
            btnDeliveryStatus.BackgroundImage = Properties.Resources.Delivery_gray;
        }

        private void btnDeliveryStatus_MouseLeave(object sender, EventArgs e)
        {
            btnDeliveryStatus.BackgroundImage = Properties.Resources.Delivery;
        }

        private void btnReturns_MouseEnter(object sender, EventArgs e)
        {
            btnReturns.BackgroundImage = Properties.Resources.Returns_gray;
        }

        private void btnReturns_MouseLeave(object sender, EventArgs e)
        {
            btnReturns.BackgroundImage = Properties.Resources.Returns;
        }

        private void txtEnableDiscount_MouseEnter(object sender, EventArgs e)
        {
            txtEnableDiscount.BackgroundImage = Properties.Resources.sales_gray;
        }

        private void txtEnableDiscount_MouseLeave(object sender, EventArgs e)
        {
            txtEnableDiscount.BackgroundImage = Properties.Resources.sales;
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.BackgroundImage = Properties.Resources.out_gray;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackgroundImage = Properties.Resources._out;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void txtSafeNumber_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCashDiscount_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                DisableDiscount();
            }
        }

        private void txtDiscountPercent_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DisableDiscount();
            }
        }

        private void TransactionScreen_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 48 && e.KeyChar <= 57)
            {
                if (!txtItemBarcode.Focused && !txtPaid.Focused && !txtCustomerID.Focused)
                {
                    //txtItemBarcode.Focus();
                    //txtItemBarcode.AppendText(e.KeyChar.ToString());
                }
            }
        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}