﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DAL;


namespace SaudiMarketPOS
{
    public partial class AdminLogin : Form
    {
        public AdminLogin()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection();
        private void AdminLogin_Load(object sender, EventArgs e)
        {
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            this.SetKeyDownEvent(this);
        }
        private void selectTxt(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).SelectionStart = 0;
                ((TextBox)sender).SelectionLength = ((TextBox)sender).Text.Length;
            }
            catch { }

        }
        private void SetKeyDownEvent(Control ctlr)
        {
            ctlr.KeyDown += ctl_KeyDown;
            if (ctlr.HasChildren)
                foreach (Control c in ctlr.Controls)
                    this.SetKeyDownEvent(c);
        }
        void ctl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Users UserObj = new Users();
            UserObj.con = con;
            UserObj.con.ConnectionString = Program.conString;
            UserObj.UserName = txtUserName.Text.Trim();
            UserObj.uPassword = txtPassword.Text.Trim();
            UserObj.Login();
            if (UserObj.ID == 0)
            {
                MessageBox.Show("اسم مستخدم او كلمة مرور خاطئة");
                return;
            }
            Program.supervisorID = UserObj.ID;
            Program.checkRole = (int)UserObj.RoleID;
            if (Program.checkRole == 1 || Program.checkRole == 2)
            {
                //Program.delTransactionRow = true;
                //TransactionScreen tr = new TransactionScreen();
                //tr.deleteItem();
                //this.Close();
                ReturnTransactionWindow retWin = new ReturnTransactionWindow();
                retWin.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("ليس لديك صلاحية", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                return;
            }
           
        }

        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                btnLogin_Click(sender, new EventArgs());
            }
        }
    }
}
