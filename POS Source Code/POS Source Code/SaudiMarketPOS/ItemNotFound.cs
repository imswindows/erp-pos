﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SaudiMarketPOS
{
    public partial class ItemNotFound : Form
    {
        private bool altf4pressd = false;

        public ItemNotFound()
        {
            InitializeComponent();
        }

        private void ItemNotFound_Load(object sender, EventArgs e)
        {
            this.SetKeyDownEvent(this);
        }
         private void SetKeyDownEvent(Control ctlr)
        {
            ctlr.KeyDown += ctl_KeyDown;
            if (ctlr.HasChildren)
                foreach (Control c in ctlr.Controls)
                    this.SetKeyDownEvent(c);
        }
         void ctl_KeyDown(object sender, KeyEventArgs e)
         {
             if (e.KeyCode == Keys.Escape)
             {
                 this.Close();
             }
         }

         private void ItemNotFound_FormClosing(object sender, FormClosingEventArgs e)
         {
             if (altf4pressd == true)
             {
                e.Cancel = true;
                altf4pressd = false;
             }
                     
         }
         
         private void ItemNotFound_KeyDown(object sender, KeyEventArgs e)
         {
             if (e.KeyCode == Keys.F4 && e.Modifiers == Keys.Alt)
             {
                 altf4pressd = true;
             }
         }

    }
}
