﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Barcode_Printer
{
    class BarcodePrinter
    {
        string PrinterName = "";



        public string LogoPath { get; set; }
        public string ItemBarcode { get; set; }
        public string ItemName { get; set; }
        public string ItemPrice { get; set; }
        public string OldPrice { get; set; }

        public BarcodePrinter(string printername)
        {
            PrinterName = printername;
        }

        void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            Font printFont = new Font("IDAutomationHC39M Free Version", 10);
            Font printFont1 = new Font("Times New Roman", 11, FontStyle.Bold);
            Font printFont2 = new Font("Times New Roman", 10, FontStyle.Strikeout | FontStyle.Bold);

            SolidBrush br = new SolidBrush(Color.Black);

            Image logo = Image.FromFile(LogoPath);
            ev.Graphics.DrawString("*" + ItemBarcode + "*", printFont, br, 5, 15);
            //ev.Graphics.DrawString("*" + ItemBarcode + "*", printFont, br, new Rectangle(0, 25, 250, 40));
            ev.Graphics.DrawImage(logo, new Rectangle(160, 75, 70, 70));
            ev.Graphics.DrawString(ItemName, printFont1, br, new Rectangle(10, 85, 160, 40));
            if (decimal.Parse(OldPrice) > 0)
            {
                ev.Graphics.DrawString(OldPrice + " L.E.", printFont2, br, 10, 135);
            }
            ev.Graphics.DrawString(ItemPrice + " L.E.", printFont1, br, 80, 135);
        }

        private void PrintImage()
        {
            Font printFont = new Font("IDAutomationHC39M Free Version", (float)10.5);
            Font printFont1 = new Font("Times New Roman", 9);
            Font printFont2 = new Font("Times New Roman", 8, FontStyle.Strikeout);

            string imageFilePath = AppDomain.CurrentDomain.BaseDirectory + "print.bmp";
            string imageFilePath2 = AppDomain.CurrentDomain.BaseDirectory + "print2.bmp";
            Bitmap bitmap = (Bitmap)Image.FromFile(imageFilePath);

            SolidBrush br = new SolidBrush(Color.Black);
            Image logo = Image.FromFile(LogoPath);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.DrawString("*" + ItemBarcode + "*", printFont, br, 5, 10);
                //ev.Graphics.DrawString("*" + ItemBarcode + "*", printFont, br, new Rectangle(0, 25, 250, 40));
                graphics.DrawImage(logo, new Rectangle(160, 60, 70, 70));
                graphics.DrawString(ItemName, printFont1, br, new Rectangle(10, 80, 120, 40));
                if (decimal.Parse(OldPrice) > 0)
                {
                    graphics.DrawString(OldPrice + " L.E.", printFont2, br, 10, 130);
                }
                graphics.DrawString(ItemPrice + " L.E.", printFont1, br, 80, 130);
            }
            var i2 = new Bitmap(bitmap);
            i2.Save(imageFilePath2, ImageFormat.Bmp);
        }

        public void BeginPrinting()
        {
            try
            {
                PrintDocument pd = new PrintDocument();
                pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
                // Set the printer name. 
                //pd.PrinterSettings.PrinterName = "\\NS5\hpoffice
                pd.PrinterSettings.PrinterName = PrinterName;
                pd.Print();
                //PrintImage();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
