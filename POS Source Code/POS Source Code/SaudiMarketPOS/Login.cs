﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using SaudiMarketPOS.Properties;

namespace SaudiMarketPOS
{
    public partial class Login : Form
    {
        SqlConnection con = new SqlConnection();

        public Login()
        {
            InitializeComponent();
        }
        //public string conString;

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        int getSafeNumber = -1;
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                
                //con.ConnectionString = Program.conString;
                SqlCommand cmd = new SqlCommand();
                cmd.Parameters.AddWithValue("@UserName", txtUserName.Text.Trim());
                cmd.Parameters.AddWithValue("@uPassword", txtPassword.Text.Trim());
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "usp_UsrLogin";
                cmd.Connection = con;

                if (con.State == ConnectionState.Closed) con.Open();

                DataTable dt = new DataTable();
                dt.Load(cmd.ExecuteReader());

                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("اسم مستخدم او كلمة مرور خاطئة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    if (con.State == ConnectionState.Open) con.Close();
                    return;
                }

                SqlDataReader dr;
                dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    Program.empID = int.Parse(dr[0].ToString());
                    Program.fristName = (dr[1].ToString());
                    Program.lastName = (dr[2].ToString());
                    Program.roleID = int.Parse(dr[3].ToString());
                }
                if (con.State == ConnectionState.Open) con.Close();
                dr.Close();
                if (Program.roleID == 3)
                {

                    SqlCommand com = new SqlCommand();
                    com.CommandType = CommandType.StoredProcedure;
                    com.Connection = con;
                    com.CommandText = "uspCheckUserHasOpendShift";
                    com.Parameters.AddWithValue("@userID", Program.empID);
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlDataReader readStatus = com.ExecuteReader();
                    if (readStatus.HasRows == false)
                    {
                        OpenBalance op = new OpenBalance();
                        op.Show();
                        this.Hide();
                        getSafeNumber = -1;
                    }
                    else
                    {
                        while (readStatus.Read())
                        {
                            getSafeNumber = int.Parse(readStatus[5].ToString());
                        }
                        
                    }
                    if (getSafeNumber == Program.safeNumber)
                    {
                        TransactionScreen trs = new TransactionScreen();
                        trs.Show();
                        this.Hide();
                    }
                    else 
                    {
                        if (getSafeNumber != -1)
                        {
                            MessageBox.Show("لا يمكنك الدخول من هذه الخزنة حيث انه تم تسجيل دخولك على خزنة اخرى", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                        }
                        
                    }
                    readStatus.Close();
                }
                if (Program.roleID == 4)
                {
                    DeliveryOrderScreen dosc = new DeliveryOrderScreen();
                    dosc.Show();
                    this.Hide();
                }
                if (Program.roleID == 2 || Program.roleID == 1)
                {
                    MainMenu mmn = new MainMenu();
                    mmn.Show();
                    this.Hide();
                }

            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("the server was not found or was not accessible") || ex.Message.ToLower().Contains("cannot open database"))
                {
                    MessageBox.Show("The server was not found or was not accessible -- فشل في الاتصال بالسيرفر");
                    if (con.State == ConnectionState.Open) con.Close();
                }
            }
        }

        private void txtUserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                btnLogin_Click(sender, new EventArgs());
            }
        }

        private void Login_Load(object sender, EventArgs e)

        {
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            con.ConnectionString = Program.conString;
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            pbXreca.Image = Image.FromFile(apppath + "xreca-logo.png");
            //pbClientLogo.Image = Image.FromFile(apppath + "CLogo.png");
            //this.ValidateConStr();
            //Program. = "Data Source=192.168.1.1;Initial Catalog=SeoudiPOS2013P1;User ID=sa;Password=@Hmed12345;connection timeout =120";
            //con.ConnectionString = Program.conString;
            //this.ValidateConStr();
            pLogin.BackColor = Color.FromArgb(200, Color.FromArgb(146, 25, 31));
            btnLogin.BackColor = Color.FromArgb(200, Color.FromArgb(146, 25, 31));
            btnExit.BackColor = Color.FromArgb(200, Color.FromArgb(146, 25, 31));

        }
        private void selectTxt(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).SelectionStart = 0;
                ((TextBox)sender).SelectionLength = ((TextBox)sender).Text.Length;
            }
            catch { }

        }
        private void ValidateConStr()
        {
            try
            {
                // I added this line to solve the connection problem(Ahmed Emad)
                //con.ConnectionString = Settings.Default.ConnectionString;
                con.ConnectionString = Program.conString;
                if (con.State == ConnectionState.Closed)
                    con.Open();
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("the server was not found or was not accessible"))
                {
                    MessageBox.Show("عنوان الإتصال بالسيرفر غير صحيح -- The server was not found or was not accessible");
                    //frmOptions options = new frmOptions(this);
                    //options.txtSafeNumber.Visible = false;
                    //options.label1.Visible = false;
                    //options.ShowDialog(this);
                    if (con.State == ConnectionState.Open)
                        con.Close();
                }
            }
        }
    }
}
