﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SaudiMarketPOS.Properties;

namespace SaudiMarketPOS
{
    public partial class OpenBalance : Form
    {
        public OpenBalance()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection();

        private void OpenBalance_Load(object sender, EventArgs e)
        {
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            lblCashirName.Text = Program.fristName.ToString() + " " + Program.lastName.ToString();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Decimal blns;
            try
            {
                blns = decimal.Parse(txtOpenBalance.Text.Trim());
                if (blns < 0)
                {
                    MessageBox.Show("مبلغ خاطئ","خطأ",MessageBoxButtons.OK,MessageBoxIcon.Error,MessageBoxDefaultButton.Button1,MessageBoxOptions.RightAlign);
                }
                //con.ConnectionString = Settings.Default.ConnectionString;
                con.ConnectionString = Program.conString;
                SqlCommand cmdCount = new SqlCommand();
                cmdCount.Connection = con;
                cmdCount.CommandType = CommandType.StoredProcedure;
                cmdCount.CommandText = "uspOpeningBalanceCount";
                var countBalance = 0;
                if (con.State == ConnectionState.Closed) con.Open();
                var rdr = cmdCount.ExecuteReader();
                while (rdr.Read())
                {
                    countBalance = int.Parse(rdr["CountBalance"].ToString());
                }
                if (con.State == ConnectionState.Open) con.Close();
                var newId = "" + Program.branch + "" + Program.machine + "" + countBalance;

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "uspOpenningBalanceInsert";
                cmd.Parameters.AddWithValue("@ID",int.Parse(newId));
                cmd.Parameters.AddWithValue("@userID",Program.empID);
                cmd.Parameters.AddWithValue("@OpenningBalance",txtOpenBalance.Text.Trim());
                //cmd.Parameters.AddWithValue("@SafeNumberID", Settings.Default.SafeNumber);
                cmd.Parameters.AddWithValue("@SafeNumberID", Program.safeNumber);
                DateTime dt = DateTime.Now;
                cmd.Parameters.AddWithValue("@CreatedTime",dt);
                if (con.State == ConnectionState.Closed) con.Open();
                cmd.ExecuteNonQuery();
                if(con.State==ConnectionState.Open) con.Close();

                TransactionScreen trs = new TransactionScreen();
                this.Hide();
                trs.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show("القمية غير صحيحة");
                txtOpenBalance.Clear();
                txtOpenBalance.Focus();
                return;
            }

            
        }

        private void txtOpenBalance_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                btnOK_Click(sender, e);
            }
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            Application.Exit();
            //this.Close();
            //MainMenu mn = new MainMenu();
            //mn.Show();
        }
    }
}
