﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace CustomerLineDisplay
{
    public class LineDisplay
    {
        public LineDisplay(string printername)
        {
            PrinterName = printername;
        }

        string PrinterName = "";

        const char HT = (char)9;
        const char BS = (char)8;
        const char US = (char)31;
        const char LF = (char)10;
        const char CR = (char)13;
        const char HOM = (char)11;
        const char B = (char)66;
        const char CAN = (char)24;
        const char CLR = (char)12;
        const char AT = (char)64;
        const char ESC = (char)27;
        const char E = (char)69;
        const char C = (char)67;
        const char MD2 = (char)2;
        const char MD3 = (char)3;

        ///<summary>
        ///Move cursor to specified direction.
        ///</summary>
        ///<param name="Direction"> 0:Right, 1:Left, 2:Up, 3:Down, 4:Right Most, 5:Left Most, 6:Home, 7:Bottom </param>
        public void MoveCursor(int Direction)
        {
            switch (Direction)
            {
                case 0:
                    SendStringToPrinter(PrinterName, HT.ToString());
                    break;
                case 1:
                    SendStringToPrinter(PrinterName, BS.ToString());
                    break;
                case 2:
                    SendStringToPrinter(PrinterName, US.ToString() + LF.ToString());
                    break;
                case 3:
                    SendStringToPrinter(PrinterName, LF.ToString());
                    break;
                case 4:
                    SendStringToPrinter(PrinterName, US.ToString() + CR.ToString());
                    break;
                case 5:
                    SendStringToPrinter(PrinterName, CR.ToString());
                    break;
                case 6:
                    SendStringToPrinter(PrinterName, HOM.ToString());
                    break;
                case 7:
                    SendStringToPrinter(PrinterName, US.ToString() + B.ToString());
                    break;
            }
        }

        /// <summary>
        /// Move cursor to specified position.
        /// </summary>
        /// <param name="X"> 1 to 20 </param>
        /// <param name="Y"> 1, 2</param>
        public void MoveCursor(int X, int Y)
        {
            SendStringToPrinter(PrinterName, US.ToString() + (char)36 + (char)X + (char)Y);
        }

        /// <summary>
        /// Clears line where cursor located.
        /// </summary>
        public void ClearCursorLine()
        {
            SendStringToPrinter(PrinterName, CAN.ToString());
        }

        /// <summary>
        /// Clears display screen.
        /// </summary>
        public void ClearDisp()
        {
            SendStringToPrinter(PrinterName, CLR.ToString());
        }

        /// <summary>
        /// Blink display screen.
        /// </summary>
        /// <param name="N">Blinking Speed 0 to 255(Slower) </param>
        public void BlinkDisp(int N)
        {
            SendStringToPrinter(PrinterName, US.ToString() + E.ToString() + ((char)N));
        }

        /// <summary>
        /// Initializes the display
        /// </summary>
        public void InitDisp()
        {
            SendStringToPrinter(PrinterName, ESC.ToString() + AT.ToString());
        }

        /// <summary>
        /// Write something on the display
        /// </summary>
        /// <param name="str"> Text to be printed </param>
        public void WriteText(string str)
        {
            SendStringToPrinter(PrinterName, str);
        }

        /// <summary>
        /// Set cursor on/off.
        /// </summary>
        /// <param name="value"> false = off, true = on </param>
        public void SetCursorOnOff(bool value)
        {
            switch(value)
            {
                case false:
                    SendStringToPrinter(PrinterName, US.ToString() + C.ToString() + (char)0);
                    break;
                case true:
                    SendStringToPrinter(PrinterName, US.ToString() + C.ToString() + (char)1);
                    break;
            }
        }

        /// <summary>
        /// Move cursor to first position in specified line
        /// </summary>
        /// <param name="LineNo"> Line number to move to </param>
        public void MoveCursorToLine(int LineNo)
        {
            SendStringToPrinter(PrinterName, US.ToString() + (char)36 + (char)1 + (char)LineNo);
        }

        /// <summary>
        /// Set Vertical Scroll Mode
        /// </summary>
        public void SetVerticalScroll()
        {
            SendStringToPrinter(PrinterName, US.ToString() + MD2.ToString());
        }

        /// <summary>
        /// Set Horizontal Scroll Mode
        /// </summary>
        public void SetHorizontalScroll()
        {
            SendStringToPrinter(PrinterName, US.ToString() + MD3.ToString());
        }

        public void SelfTest()
        {
            SendStringToPrinter(PrinterName, US.ToString() + AT.ToString());
        }

        // Structure and API declarions:
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
        public class DOCINFOA
        {
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;
            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;
        }
        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter, out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA", SetLastError = true, CharSet = CharSet.Ansi, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, Int32 level, [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

        [DllImport("winspool.Drv", EntryPoint = "EndDocPrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "StartPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "EndPagePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "WritePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, Int32 dwCount, out Int32 dwWritten);

        public static bool SendStringToPrinter(string szPrinterName, string szString)
        {
            IntPtr pBytes;
            Int32 dwCount;

            // How many characters are in the string?
            // Fix from Nicholas Piasecki:
            // dwCount = szString.Length;
            dwCount = (szString.Length + 1) * Marshal.SystemMaxDBCSCharSize;

            // Assume that the printer is expecting ANSI text, and then convert
            // the string to ANSI text.
            pBytes = Marshal.StringToCoTaskMemAnsi(szString);
            // Send the converted ANSI string to the printer.
            SendBytesToPrinter(szPrinterName, pBytes, dwCount);
            Marshal.FreeCoTaskMem(pBytes);
            return true;
        }

        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, Int32 dwCount)
        {
            Int32 dwError = 0, dwWritten = 0;
            IntPtr hPrinter = new IntPtr(0);
            DOCINFOA di = new DOCINFOA();
            bool bSuccess = false; // Assume failure unless you specifically succeed.
            di.pDocName = "My C#.NET RAW Document";
            di.pDataType = "RAW";

            // Open the printer.
            if (OpenPrinter(szPrinterName.Normalize(), out hPrinter, IntPtr.Zero))
            {
                // Start a document.
                if (StartDocPrinter(hPrinter, 1, di))
                {
                    // Start a page.
                    if (StartPagePrinter(hPrinter))
                    {
                        // Write your bytes.
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, out dwWritten);
                        EndPagePrinter(hPrinter);
                    }
                    EndDocPrinter(hPrinter);
                }
                ClosePrinter(hPrinter);
            }
            // If you did not succeed, GetLastError may give more information
            // about why not.
            if (bSuccess == false)
            {
                dwError = Marshal.GetLastWin32Error();
            }
            return bSuccess;
        }
    }
}
