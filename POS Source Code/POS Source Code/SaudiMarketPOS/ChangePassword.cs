﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using DAL;

namespace SaudiMarketPOS
{
    public partial class ChangePassword : Form
    {
        public ChangePassword()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection();
        string currntUs;
        string currntPass;
        bool userNameIsTrue;
        bool passwordIsTrue;
        private void ChangePassword_Load(object sender, EventArgs e)
        {
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            try
            {
                //con.ConnectionString = Settings.Default.ConnectionString;

                //Fill Employee Combobox
                //SqlCommand cmdFillEmpName = new SqlCommand();
                //cmdFillEmpName.CommandType = CommandType.StoredProcedure;
                //cmdFillEmpName.CommandText = "uspSelectEmpName";
                //cmdFillEmpName.Connection = con;
                //DataTable dtEmpName = new DataTable();
                //if (con.State == ConnectionState.Closed) con.Open();
                //dtEmpName.Load(cmdFillEmpName.ExecuteReader());
                Users UserObj = new Users();
                UserObj.con = con;
                UserObj.con.ConnectionString = Program.conString;
                DataTable dtEmpName = new DataTable();
                dtEmpName = UserObj.SelectEmployeeNames();
                cmbEmpName.DataSource = dtEmpName;
                cmbEmpName.ValueMember = "ID";
                cmbEmpName.DisplayMember = "Name";
            }
            catch (Exception ex)
            { }
        }
        private void clearData()
        {
            txtCurrentUs.Text = "";
            txtCurrentPass.Text = "";
            txtConfirmPass.Text = "";
            txtConfirmUser.Text = "";
            txtPassword.Text = "";
            txtUserName.Text = "";
            cmbEmpName.SelectedIndex = 0;
        }
        private void selectTxt(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).SelectionStart = 0;
                ((TextBox)sender).SelectionLength = ((TextBox)sender).Text.Length;
            }
            catch { }

        }
        

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Users UserObj = new Users();
                UserObj.con = con;
                string[] StrArray = new string[2];
                StrArray = UserObj.CheckCurrentPassword((int)cmbEmpName.SelectedValue);

                if (StrArray[0] != txtCurrentUs.Text.Trim() || StrArray[1] != txtCurrentPass.Text)
                {
                    MessageBox.Show("اسم المستخدم او كلمة المرور الحالية خاطئة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    txtCurrentUs.Focus();
                    return;
                }
                else
                {
                    Regex chkUserName = new Regex("[A-Za-z][A-Za-z0-9._]{5,14}");

                    if (chkUserName.IsMatch(txtUserName.Text.Trim()))
                    {
                        if (!char.IsDigit(txtUserName.Text[0]))
                        {
                            userNameIsTrue = true;
                            if (txtUserName.Text.Trim() == "")
                            {
                                MessageBox.Show("بجب استكمال جميع البيانات", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                                txtUserName.Focus();
                                return;
                            }
                            if (txtUserName.Text.Trim() != txtConfirmUser.Text.Trim())
                            {
                                MessageBox.Show("تأكيد اسم المستخدم لا يطابق اسم المستخدم الجديد", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                                txtUserName.Focus();
                                return;
                            }
                        }
                        else
                        {
                            userNameIsTrue = false;
                            MessageBox.Show("اتبع التعلميات الموضحه لإسم المستخدم", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                            txtUserName.Focus();
                            return;
                        }
                    }
                    else
                    {
                        userNameIsTrue = false;
                        MessageBox.Show("اتبع التعلميات الموضحه لإسم المستخدم", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                        txtUserName.Focus();
                        return;
                    }

                    Regex chkPassword = new Regex("[A-Za-z0-9]{6,15}");

                    if (chkPassword.IsMatch(txtPassword.Text.Trim()))
                    {
                        passwordIsTrue = true;
                        if (txtPassword.Text.Trim() == "")
                        {
                            MessageBox.Show("بجب استكمال جميع البيانات", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                            txtPassword.Focus();
                            return;
                        }
                        if (txtPassword.Text.Trim() != txtConfirmPass.Text.Trim())
                        {
                            MessageBox.Show("تأكيد كلمة المرور لا يطابق كلمة المرور الجديده", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                            txtPassword.Focus();
                            return;
                        }
                    }
                    else
                    {
                        passwordIsTrue = false;
                        MessageBox.Show("اتبع التعلميات الموضحه لكلمة المرور", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                        txtPassword.FindForm();
                        return;
                    }

                    if (userNameIsTrue == true && passwordIsTrue == true)
                    {
                        //SqlCommand passChangeCmd = new SqlCommand();
                        //passChangeCmd.Connection = con;
                        //passChangeCmd.CommandType = CommandType.StoredProcedure;
                        //passChangeCmd.CommandText = "uspUsersChangePassword";
                        //passChangeCmd.Parameters.AddWithValue("@UserID", cmbEmpName.SelectedValue);
                        //passChangeCmd.Parameters.AddWithValue("@UserName", txtConfirmUser.Text);
                        //passChangeCmd.Parameters.AddWithValue("@uPassword", txtConfirmPass.Text);
                        //if (con.State == ConnectionState.Closed) con.Open();
                        //passChangeCmd.ExecuteNonQuery();
                        //if (con.State == ConnectionState.Open) con.Close();
                        Users UpdateObj = new Users();
                        UpdateObj.con = con;
                        UpdateObj.ChangePassword((int)cmbEmpName.SelectedValue, txtConfirmUser.Text, txtConfirmPass.Text);
                        clearData();
                        MessageBox.Show("تم تغير بيانات الدخول بنجاح", "تاكيد", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        private void cmbEmpName_Leave(object sender, EventArgs e)
        {
            try
            {

                DataTable tbDP = ((DataTable)cmbEmpName.DataSource).Copy();
                tbDP.DefaultView.RowFilter = "Name='" + cmbEmpName.Text + "'";
                    if (tbDP.DefaultView.Count == 0)
                    {
                        MessageBox.Show("اسم الموظف غير صحيح غير صحيح", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cmbEmpName.Focus();
                    }
                    tbDP.DefaultView.RowFilter = "";
               
            }
            catch { }
        }

        
    }
}
