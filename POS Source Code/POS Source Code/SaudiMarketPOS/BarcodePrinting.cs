﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Barcode_Printer;

namespace SaudiMarketPOS
{
    public partial class BarcodePrinting : Form
    {
        public List<ItemData> SelectedLst = new List<ItemData>();
        List<ItemData> ItemLst = new List<ItemData>();
        SqlConnection con = new SqlConnection();

        public BarcodePrinting()
        {
            InitializeComponent();
        }

        public class ItemData
        {
            public string Barcode { get; set; }
            public string Name { get; set; }
            public decimal Price { get; set; }
            public decimal OldPrice { get; set; }
        }

        private void BarcodePrinting_Load(object sender, EventArgs e)
        {
            con.ConnectionString = Program.conString;
            foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                cbPrinters.Items.Add(printer);
            }
            cbPrinters.SelectedIndex = 0;
            ItemLst.Clear();
            using (SqlConnection conn = new SqlConnection(Program.conString))
            {
                string SQL = "SELECT [ID], [Name], [Description], [SellPrice], [SalePrice], [OnSale] FROM Item";
                SqlCommand cmd = new SqlCommand(SQL, conn);
                conn.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        ItemData ItemObj = new ItemData();
                        ItemObj.Barcode = rdr["ID"].ToString();
                        ItemObj.Name = rdr["Name"].ToString();
                        ItemObj.Price = decimal.Parse(rdr["SellPrice"].ToString());
                        bool IsPromotion = false;
                        try { IsPromotion = bool.Parse(rdr["OnSale"].ToString()); } catch { }
                        if (IsPromotion)
                        {
                            ItemObj.OldPrice = decimal.Parse(rdr["SalePrice"].ToString());
                        }
                        ItemLst.Add(ItemObj);
                    }
                }
            }
            cbBarcodes.DataSource = ItemLst;
            cbBarcodes.DisplayMember = "Barcode";
            cbBarcodes.ValueMember = "Barcode";

            cbNames.DataSource = ItemLst;
            cbNames.DisplayMember = "Name";
            cbNames.ValueMember = "Barcode";

            if (SelectedLst.Count > 0)
            {
                foreach (ItemData DataObj in SelectedLst)
                {
                    string[] DataRow = { DataObj.Barcode, DataObj.Name, "1" };
                    dgvItemBarcodes.Rows.Add(DataRow);
                }
                btnBeginPrinting.Focus();
            }
            cbBarcodes.SelectedIndex = -1;
            cbNames.SelectedIndex = -1;
        }

        private void btnAddToList_Click(object sender, EventArgs e)
        {
            bool IsExist = false;
            foreach (DataGridViewRow dgvr in dgvItemBarcodes.Rows)
            {
                if (dgvr.Cells["ItemId"].Value.ToString() == cbBarcodes.Text)
                {
                    IsExist = true;
                }
            }
            if (!IsExist)
            {
                string[] DataRow = { cbBarcodes.Text, cbNames.Text, nudPrintsNo.Value.ToString() };
                dgvItemBarcodes.Rows.Add(DataRow);
            }
            else
            {
                MessageBox.Show("هذا الصنف مضاف من قبل");
            }
        }

        private void btnBeginPrinting_Click(object sender, EventArgs e)
        {
            try
            {
                int Count = 0;
                foreach (DataGridViewRow dgvr in dgvItemBarcodes.SelectedRows)
                {
                    int PrintNo = int.Parse(dgvr.Cells["PrintNo"].Value.ToString());
                    for (int i = 0; i < PrintNo; i++)
                    {
                        BarcodePrinter print = new BarcodePrinter(cbPrinters.Text);
                        string ItemId = dgvr.Cells["ItemId"].Value.ToString();
                        ItemData ItemObj = ItemLst.Find(c => c.Barcode == ItemId);
                        print.ItemBarcode = ItemObj.Barcode;
                        print.ItemName = ItemObj.Name;
                        print.ItemPrice = ItemObj.Price.ToString("#.00");
                        print.OldPrice = ItemObj.OldPrice.ToString("#.00");
                        print.LogoPath = AppDomain.CurrentDomain.BaseDirectory + "s1.png";
                        print.BeginPrinting();
                        Count++;
                    }
                }
                if (MessageBox.Show("تم طباعة عدد " + Count.ToString() + " باركود. هل تريد الغلق؟", "Success", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.Yes)
                {
                    Close();
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void dgvItemBarcodes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvItemBarcodes.Columns["DeleteRow"].Index)
                {
                    dgvItemBarcodes.Rows.RemoveAt(e.RowIndex);
                }
            }
        }

        private void nudPrintsNo_Enter(object sender, EventArgs e)
        {
            nudPrintsNo.Select(0, nudPrintsNo.Value.ToString().Length);
        }
    }
}
