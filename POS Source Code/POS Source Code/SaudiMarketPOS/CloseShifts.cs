﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SaudiMarketPOS.Properties;
using ReceiptCreator;
using DAL;

namespace SaudiMarketPOS
{
    public partial class CloseShifts : Form
    {
        public CloseShifts()
        {
            InitializeComponent();
        }
        int shiftID;
        DateTime strating;
        DateTime ending;
        decimal openingBalance;

        decimal totalSales;
        string chkTotSales;

        decimal totDueAmount;
        string chkTotDueAmount;

        decimal totCredit;
        string chkCredit;

        decimal totalUncollected;
        string chkUncollcted;

        decimal totalRetrun;
        string chkReturn;

        decimal totalCreditCard;
        string chkCreditCard;

        decimal totalOnlinePaid;
        string chkTotalOnline;

        decimal discountValue;

        decimal safeBalance;
        decimal allShiftBalance;

        decimal netSales;
        decimal nextOpeningBalance;
        decimal netCashPaid;
        DateTime toCheckDelivery;
        int safeNumber;
        SqlConnection con = new SqlConnection();
        private void CloseShifts_Load(object sender, EventArgs e)
        {
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            try
            {
                //con.ConnectionString = Settings.Default.ConnectionString;
                //Fill Employee Combobox
                //SqlCommand cmdFillEmpName = new SqlCommand();
                //cmdFillEmpName.CommandType = CommandType.StoredProcedure;
                //cmdFillEmpName.CommandText = "uspSelectCashirName";
                //cmdFillEmpName.Connection = con;
                //DataTable dtEmpName = new DataTable();
                //if (con.State == ConnectionState.Closed) con.Open();
                //dtEmpName.Load(cmdFillEmpName.ExecuteReader());
                con.ConnectionString = Program.conString;
                Users UserObj = new Users();
                UserObj.con = con;
                DataTable dtEmpName = new DataTable();
                dtEmpName = UserObj.SelectCashierNames();
                cmbCashierName.DataSource = dtEmpName;
                cmbCashierName.ValueMember = "ID";
                cmbCashierName.DisplayMember = "Name";
                
            }
            catch(Exception ex)
            { }
        }

        private void cmbCashierName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.ActiveControl != btnClose)
                {
                    DataTable tbDP = ((DataTable)cmbCashierName.DataSource).Copy();
                    tbDP.DefaultView.RowFilter = "Name='" + cmbCashierName.Text + "'";
                    if (tbDP.DefaultView.Count == 0)
                    {
                        MessageBox.Show("اسم الكاشير غير صحيح", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cmbCashierName.Focus();
                    }
                    tbDP.DefaultView.RowFilter = "";
                }
            }
            catch { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }
        private void selectTxt(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).SelectionStart = 0;
                ((TextBox)sender).SelectionLength = ((TextBox)sender).Text.Length;
            }
            catch { }

        }
        private void btnCloseShift_Click(object sender, EventArgs e)
        {
            try
            {
                //SqlCommand com = new SqlCommand();
                //com.CommandType = CommandType.StoredProcedure;
                //com.Connection = con;
                //com.CommandText = "uspCheckUserHasOpendShift";
                //com.Parameters.AddWithValue("@userID", cmbCashierName.SelectedValue);
                //if (con.State == ConnectionState.Closed) con.Open();
                //SqlDataReader readStatus = com.ExecuteReader();
                OpenningBalance BalanceObj = new OpenningBalance();
                BalanceObj.con = con;
                bool IsOpen = BalanceObj.CheckOpenShift((int)cmbCashierName.SelectedValue);

                if (!IsOpen)
                {
                    MessageBox.Show("لا توجد ورديات مفتوحة لهذا الكاشير");
                    return;
                }
                else
                {
                    if (txtNextOpeningBalance.Text.Trim() == "")
                    {
                        MessageBox.Show("لا يمكن ترك قيمة استلام الوردية التالية فارغة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                        txtNextOpeningBalance.Focus();
                        return;
                    }
                    nextOpeningBalance = Math.Round(decimal.Parse(txtNextOpeningBalance.Text.Trim()), 2);
                    //Get Closed Shift ID
                    SqlCommand getShiftIDCmd = new SqlCommand();
                    getShiftIDCmd.Connection = con;
                    getShiftIDCmd.CommandType = CommandType.StoredProcedure;
                    getShiftIDCmd.CommandText = "uspGetClosedShiftID";
                    getShiftIDCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
                    SqlDataReader readShiftID = getShiftIDCmd.ExecuteReader();

                    while (readShiftID.Read())
                    {
                        shiftID = int.Parse(readShiftID[0].ToString());
                        safeNumber = int.Parse(readShiftID[1].ToString());
                        toCheckDelivery = Convert.ToDateTime(readShiftID[2].ToString());
                    }

                    readShiftID.Close();
                    //Check user has uncollected deliv orders

                    SqlCommand unCollectedCmd = new SqlCommand();
                    unCollectedCmd.Connection = con;
                    unCollectedCmd.CommandType = CommandType.StoredProcedure;
                    unCollectedCmd.CommandText = "uspCheckUserHasUnCollectedDO";
                    unCollectedCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
                    unCollectedCmd.Parameters.AddWithValue("@OpeningTime", toCheckDelivery);
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlDataReader readUnColOrders = unCollectedCmd.ExecuteReader();

                    if (readUnColOrders.HasRows == true)
                    {
                        if (MessageBox.Show("توجد بعض اوامر التوصيل غير محصلة في وردية هذا الكاشير هل ترغب في الاستمرار؟؟", "تحذير", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.RightAlign) == DialogResult.No)
                        {
                            readUnColOrders.Close();
                            return;

                        }
                        else
                        {
                            readUnColOrders.Close();
                            closeTheShift();
                        }
                    }
                    else
                    {
                        readUnColOrders.Close();
                        closeTheShift();
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        private void calculateNetSales()
        {
            netSales = totalSales - totalRetrun - discountValue;
        }
        private void calculateSafeBalance()
        {
            allShiftBalance = openingBalance + totalSales;
            safeBalance = openingBalance + netSales - totCredit - totalCreditCard - totalUncollected - totalOnlinePaid;
            Math.Round(safeBalance, 2);
        }
        private void calculateNetCashPaid()
        {
            netCashPaid = safeBalance - nextOpeningBalance;
        }
        private void closeTheShift()
        {
            SqlCommand cmdCloseShift = new SqlCommand();
            cmdCloseShift.Connection = con;
            cmdCloseShift.CommandType = CommandType.StoredProcedure;
            cmdCloseShift.CommandText = "uspClosingCashirShift";
            cmdCloseShift.Parameters.Clear();
            cmdCloseShift.Parameters.AddWithValue("@userID", cmbCashierName.SelectedValue);
            cmdCloseShift.Parameters.AddWithValue("@ClosingTime", DateTime.Now);
            cmdCloseShift.Parameters.AddWithValue("@Closed", 1);
            if (con.State == ConnectionState.Closed) con.Open();
            cmdCloseShift.ExecuteNonQuery();

            if (con.State == ConnectionState.Open) con.Close();

            //Get startTime and Endtime for the shifts and staring balance 
            SqlCommand timeRangeCmd = new SqlCommand();
            timeRangeCmd.Connection = con;
            timeRangeCmd.CommandType = CommandType.StoredProcedure;
            timeRangeCmd.CommandText = "uspGetShiftRange";
            timeRangeCmd.Parameters.AddWithValue("@ID", shiftID);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readSiftData = timeRangeCmd.ExecuteReader();
            while (readSiftData.Read())
            {
                openingBalance = Math.Round(decimal.Parse(readSiftData[0].ToString()), 2);
                strating = Convert.ToDateTime(readSiftData[1].ToString());
                ending = Convert.ToDateTime(readSiftData[2].ToString());
            }
            readSiftData.Close();
            //Get total sales during the shift
            SqlCommand totalSalesCmd = new SqlCommand();
            totalSalesCmd.Connection = con;
            totalSalesCmd.CommandType = CommandType.StoredProcedure;
            totalSalesCmd.CommandText = "uspGetTotalSalesDuringShift";
            totalSalesCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
            totalSalesCmd.Parameters.AddWithValue("@StartingShift", strating);
            totalSalesCmd.Parameters.AddWithValue("@EndingShift", ending);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readTotalSales = totalSalesCmd.ExecuteReader();
            while (readTotalSales.Read())
            {
                chkTotSales = readTotalSales[0].ToString();
            }
            readTotalSales.Close();
            if (con.State == ConnectionState.Open) con.Close();

            if (chkTotSales == string.Empty)
            {
                totalSales = 0;
                Math.Round(totalSales, 2);
            }
            else
            {
                totalSales = Math.Round(decimal.Parse(chkTotSales.ToString()), 2);
            }

            //Get total due amount during the shift to calculate the discounts
            SqlCommand totDueAmountCmd = new SqlCommand();
            totDueAmountCmd.Connection = con;
            totDueAmountCmd.CommandType = CommandType.StoredProcedure;
            totDueAmountCmd.CommandText = "uspGetTotalDueAmountDuringShift";
            totDueAmountCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
            totDueAmountCmd.Parameters.AddWithValue("@StartingShift", strating);
            totDueAmountCmd.Parameters.AddWithValue("@EndingShift", ending);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readTotDueAmount = totDueAmountCmd.ExecuteReader();
            while (readTotDueAmount.Read())
            {
                chkTotDueAmount = readTotDueAmount[0].ToString();
                //totDueAmount = Math.Round(decimal.Parse(readTotDueAmount[0].ToString()), 2);
            }
            readTotDueAmount.Close();
            if (con.State == ConnectionState.Open) con.Close();

            if (chkTotDueAmount == string.Empty)
            {
                totDueAmount = 0;
                Math.Round(totDueAmount, 2);
            }
            else
            {
                totDueAmount = Math.Round(decimal.Parse(chkTotDueAmount.ToString()), 2);
            }
            //Get the credit invoces total
            SqlCommand totCreditAmountCmd = new SqlCommand();
            totCreditAmountCmd.Connection = con;
            totCreditAmountCmd.CommandType = CommandType.StoredProcedure;
            totCreditAmountCmd.CommandText = "uspGetCreditTotalDuringShift";
            totCreditAmountCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
            totCreditAmountCmd.Parameters.AddWithValue("@StartingShift", strating);
            totCreditAmountCmd.Parameters.AddWithValue("@EndingShift", ending);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readCreditTotal = totCreditAmountCmd.ExecuteReader();

            while (readCreditTotal.Read())
            {
                chkCredit = readCreditTotal[0].ToString();
            }
            readCreditTotal.Close();
            if (con.State == ConnectionState.Open) con.Close();

            if (chkCredit == string.Empty)
            {
                totCredit = 0;
                Math.Round(totCredit, 2);
            }
            else
            {
                totCredit = Math.Round(decimal.Parse(chkCredit.ToString()), 2);
            }

            //Get the uncollected Delivery Orders Total
            SqlCommand uncollectedCmd = new SqlCommand();
            uncollectedCmd.Connection = con;
            uncollectedCmd.CommandType = CommandType.StoredProcedure;
            uncollectedCmd.CommandText = "uspGetunCollectedDOTotalDuringShift";
            uncollectedCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
            uncollectedCmd.Parameters.AddWithValue("@StartingShift", strating);
            uncollectedCmd.Parameters.AddWithValue("@EndingShift", ending);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readUncollectedTot = uncollectedCmd.ExecuteReader();

            while (readUncollectedTot.Read())
            {
                chkUncollcted = readUncollectedTot[0].ToString();
            }
            readUncollectedTot.Close();
            if (con.State == ConnectionState.Open) con.Close();

            if (chkUncollcted == string.Empty)
            {
                totalUncollected = 0;
                Math.Round(totalUncollected, 2);
            }
            else
            {
                totalUncollected = Math.Round(decimal.Parse(chkUncollcted.ToString()), 2);
            }
            //Get the return total
            SqlCommand returnCmd = new SqlCommand();
            returnCmd.Connection = con;
            returnCmd.CommandType = CommandType.StoredProcedure;
            returnCmd.CommandText = "uspGetReturnTotalDuringShift";
            returnCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
            returnCmd.Parameters.AddWithValue("@StartingShift", strating);
            returnCmd.Parameters.AddWithValue("@EndingShift", ending);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readReturnTotal = returnCmd.ExecuteReader();

            while (readReturnTotal.Read())
            {
                chkReturn = readReturnTotal[0].ToString();
            }
            readReturnTotal.Close();
            if (con.State == ConnectionState.Open) con.Close();

            if (chkReturn == string.Empty)
            {
                totalRetrun = 0;
                Math.Round(totalRetrun, 2);
            }
            else
            {
                totalRetrun = Math.Round(decimal.Parse(chkReturn.ToString()), 2);
            }
            //Get the collected Credit Card During the shift
            SqlCommand creditCardCmd = new SqlCommand();
            creditCardCmd.Connection = con;
            creditCardCmd.CommandType = CommandType.StoredProcedure;
            creditCardCmd.CommandText = "uspGetCollectedCreditCDuringShift";
            creditCardCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
            creditCardCmd.Parameters.AddWithValue("@StartingShift", strating);
            creditCardCmd.Parameters.AddWithValue("@EndingShift", ending);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readCreditCard = creditCardCmd.ExecuteReader();

            while (readCreditCard.Read())
            {
                chkCreditCard = readCreditCard[0].ToString();
            }
            readCreditCard.Close();
            if (con.State == ConnectionState.Open) con.Close();

            if (chkCreditCard == string.Empty)
            {
                totalCreditCard = 0;
                Math.Round(totalCreditCard, 2);
            }
            else
            {
                totalCreditCard = Math.Round(decimal.Parse(chkCreditCard.ToString()), 2);
            }
            //Get Total Online Paid
            SqlCommand onlinePaidCmd = new SqlCommand();
            onlinePaidCmd.Connection = con;
            onlinePaidCmd.CommandType = CommandType.StoredProcedure;
            onlinePaidCmd.CommandText = "uspGetPaidOnLineTotalDuringShift";
            onlinePaidCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
            onlinePaidCmd.Parameters.AddWithValue("@StartingShift", strating);
            onlinePaidCmd.Parameters.AddWithValue("@EndingShift", ending);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readOnlinePaid = onlinePaidCmd.ExecuteReader();
            while(readOnlinePaid.Read())
            {
                chkTotalOnline = readOnlinePaid[0].ToString();
            }
            readOnlinePaid.Close();
            if (con.State == ConnectionState.Open) con.Close();
            if (chkTotalOnline == string.Empty)
            {
                totalOnlinePaid = 0;
                Math.Round(totalOnlinePaid, 2);
            }
            else
            {
                totalOnlinePaid = Math.Round(decimal.Parse(chkTotalOnline.ToString()), 2);
            }
            ///////////////////////////////

            //Calculate the discount value
            discountValue = totalSales - totDueAmount;
            Math.Round(discountValue, 2);

            calculateNetSales();
            calculateSafeBalance();
            calculateNetCashPaid();
            printShiftDAta();

  
        }
        private void printShiftDAta()
        {
            TransactionReceipt sh = new TransactionReceipt();
            sh.cashirName = this.cmbCashierName.Text;
            sh.safeNo = safeNumber;
            sh.shiftStart = this.strating.ToString();
            sh.shiftEnd = this.ending.ToString();
            sh.openingBalance = this.openingBalance;
            sh.totalShiftSales = this.totalSales;
            sh.totalShiftBalance = this.allShiftBalance;
            sh.totalReturns = this.totalRetrun;
            sh.totalDiscount = this.discountValue;
            sh.netSales = this.netSales;
            sh.totalCredit = this.totCredit;
            sh.totalCreditCard = this.totalCreditCard;
            sh.totalUncollectedDO = this.totalUncollected;
            sh.totalSafeBalance = this.safeBalance;
            sh.nextOpeningBalance = this.nextOpeningBalance;
            sh.netCashPaid = this.netCashPaid;
            sh.custName = Program.fristName + " " + Program.lastName;
            sh.totalOnlinePaid = totalOnlinePaid;

            sh.printShift();
        }

        private void txtNextOpeningBalance_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal.Parse(txtNextOpeningBalance.Text.Trim());
            }
            catch
            {
                MessageBox.Show("يجب ادخال ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
            }
        }
    }
}


