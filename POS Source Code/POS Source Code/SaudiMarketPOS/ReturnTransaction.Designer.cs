﻿namespace SaudiMarketPOS
{
    partial class ReturnTransaction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReturnTransaction));
            this.lblCashir = new System.Windows.Forms.Label();
            this.txtTransactionID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtCustmoerName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtSafeNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCashierName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtChange = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPaid = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDueAmount = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtCredit = new System.Windows.Forms.TextBox();
            this.creditCheckBox = new System.Windows.Forms.CheckBox();
            this.txtCashDiscount = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDiscountPercent = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtInvTotal = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.btnCancelInv = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.dgvReturnTransaction = new System.Windows.Forms.DataGridView();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReturnTransaction)).BeginInit();
            this.SuspendLayout();
            // 
            // lblCashir
            // 
            this.lblCashir.AutoSize = true;
            this.lblCashir.ForeColor = System.Drawing.Color.Blue;
            this.lblCashir.Location = new System.Drawing.Point(361, 42);
            this.lblCashir.Name = "lblCashir";
            this.lblCashir.Size = new System.Drawing.Size(49, 19);
            this.lblCashir.TabIndex = 5;
            this.lblCashir.Text = "label3";
            // 
            // txtTransactionID
            // 
            this.txtTransactionID.Location = new System.Drawing.Point(285, 10);
            this.txtTransactionID.Name = "txtTransactionID";
            this.txtTransactionID.Size = new System.Drawing.Size(129, 26);
            this.txtTransactionID.TabIndex = 10;
            this.txtTransactionID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(416, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 19);
            this.label3.TabIndex = 9;
            this.label3.Text = "ادخل رقم البون";
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtDate);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtTransactionID);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(188, 81);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(524, 49);
            this.panel2.TabIndex = 11;
            // 
            // txtDate
            // 
            this.txtDate.Location = new System.Drawing.Point(7, 10);
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(206, 26);
            this.txtDate.TabIndex = 12;
            this.txtDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(212, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "تاريخ البون";
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.txtCustmoerName);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Location = new System.Drawing.Point(22, 147);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(413, 57);
            this.panel3.TabIndex = 13;
            // 
            // txtCustmoerName
            // 
            this.txtCustmoerName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.txtCustmoerName.Location = new System.Drawing.Point(27, 14);
            this.txtCustmoerName.Name = "txtCustmoerName";
            this.txtCustmoerName.ReadOnly = true;
            this.txtCustmoerName.Size = new System.Drawing.Size(266, 26);
            this.txtCustmoerName.TabIndex = 8;
            this.txtCustmoerName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(299, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 19);
            this.label7.TabIndex = 4;
            this.label7.Text = "اسم العميل";
            // 
            // panel4
            // 
            this.panel4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.txtSafeNumber);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.txtCashierName);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Location = new System.Drawing.Point(466, 146);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(413, 57);
            this.panel4.TabIndex = 12;
            // 
            // txtSafeNumber
            // 
            this.txtSafeNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.txtSafeNumber.Location = new System.Drawing.Point(230, 14);
            this.txtSafeNumber.Name = "txtSafeNumber";
            this.txtSafeNumber.ReadOnly = true;
            this.txtSafeNumber.Size = new System.Drawing.Size(90, 26);
            this.txtSafeNumber.TabIndex = 7;
            this.txtSafeNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(326, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 19);
            this.label5.TabIndex = 6;
            this.label5.Text = "رقم الخزينة";
            // 
            // txtCashierName
            // 
            this.txtCashierName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.txtCashierName.Location = new System.Drawing.Point(17, 14);
            this.txtCashierName.Name = "txtCashierName";
            this.txtCashierName.ReadOnly = true;
            this.txtCashierName.Size = new System.Drawing.Size(129, 26);
            this.txtCashierName.TabIndex = 5;
            this.txtCashierName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(152, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 19);
            this.label8.TabIndex = 4;
            this.label8.Text = "اسم الكاشير";
            // 
            // panel5
            // 
            this.panel5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.txtChange);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.txtPaid);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.txtDueAmount);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Location = new System.Drawing.Point(20, 410);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(857, 57);
            this.panel5.TabIndex = 18;
            // 
            // txtChange
            // 
            this.txtChange.Location = new System.Drawing.Point(120, 26);
            this.txtChange.Name = "txtChange";
            this.txtChange.ReadOnly = true;
            this.txtChange.Size = new System.Drawing.Size(191, 26);
            this.txtChange.TabIndex = 2;
            this.txtChange.Text = "0,00";
            this.txtChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(193, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 19);
            this.label11.TabIndex = 8;
            this.label11.Text = "المتبقى";
            // 
            // txtPaid
            // 
            this.txtPaid.Location = new System.Drawing.Point(333, 25);
            this.txtPaid.Name = "txtPaid";
            this.txtPaid.Size = new System.Drawing.Size(191, 26);
            this.txtPaid.TabIndex = 1;
            this.txtPaid.Text = "0,00";
            this.txtPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(409, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 19);
            this.label12.TabIndex = 6;
            this.label12.Text = "المدفوع";
            // 
            // txtDueAmount
            // 
            this.txtDueAmount.Location = new System.Drawing.Point(544, 25);
            this.txtDueAmount.Name = "txtDueAmount";
            this.txtDueAmount.ReadOnly = true;
            this.txtDueAmount.Size = new System.Drawing.Size(191, 26);
            this.txtDueAmount.TabIndex = 0;
            this.txtDueAmount.Text = "0,00";
            this.txtDueAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(594, 4);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 19);
            this.label13.TabIndex = 4;
            this.label13.Text = "المطلوب سداده";
            // 
            // panel7
            // 
            this.panel7.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.txtCredit);
            this.panel7.Controls.Add(this.creditCheckBox);
            this.panel7.Controls.Add(this.txtCashDiscount);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.txtDiscountPercent);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.txtInvTotal);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Location = new System.Drawing.Point(20, 340);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(857, 57);
            this.panel7.TabIndex = 17;
            // 
            // txtCredit
            // 
            this.txtCredit.Enabled = false;
            this.txtCredit.Location = new System.Drawing.Point(53, 15);
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.Size = new System.Drawing.Size(100, 26);
            this.txtCredit.TabIndex = 4;
            this.txtCredit.Text = "0,00";
            this.txtCredit.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // creditCheckBox
            // 
            this.creditCheckBox.AutoSize = true;
            this.creditCheckBox.Location = new System.Drawing.Point(159, 17);
            this.creditCheckBox.Name = "creditCheckBox";
            this.creditCheckBox.Size = new System.Drawing.Size(47, 23);
            this.creditCheckBox.TabIndex = 3;
            this.creditCheckBox.Text = "اجل";
            this.creditCheckBox.UseVisualStyleBackColor = true;
            // 
            // txtCashDiscount
            // 
            this.txtCashDiscount.Location = new System.Drawing.Point(254, 13);
            this.txtCashDiscount.Name = "txtCashDiscount";
            this.txtCashDiscount.Size = new System.Drawing.Size(97, 26);
            this.txtCashDiscount.TabIndex = 2;
            this.txtCashDiscount.Text = "0,00";
            this.txtCashDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(357, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 19);
            this.label10.TabIndex = 8;
            this.label10.Text = "خصم نقدي";
            // 
            // txtDiscountPercent
            // 
            this.txtDiscountPercent.Location = new System.Drawing.Point(450, 15);
            this.txtDiscountPercent.Name = "txtDiscountPercent";
            this.txtDiscountPercent.Size = new System.Drawing.Size(70, 26);
            this.txtDiscountPercent.TabIndex = 1;
            this.txtDiscountPercent.Text = "0,00";
            this.txtDiscountPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(526, 17);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 19);
            this.label9.TabIndex = 6;
            this.label9.Text = "خصم %";
            // 
            // txtInvTotal
            // 
            this.txtInvTotal.Location = new System.Drawing.Point(614, 14);
            this.txtInvTotal.Name = "txtInvTotal";
            this.txtInvTotal.ReadOnly = true;
            this.txtInvTotal.Size = new System.Drawing.Size(129, 26);
            this.txtInvTotal.TabIndex = 0;
            this.txtInvTotal.Text = "0,00";
            this.txtInvTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(749, 17);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 19);
            this.label16.TabIndex = 4;
            this.label16.Text = "الاجمالي";
            // 
            // btnCancelInv
            // 
            this.btnCancelInv.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancelInv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.btnCancelInv.Location = new System.Drawing.Point(297, 473);
            this.btnCancelInv.Name = "btnCancelInv";
            this.btnCancelInv.Size = new System.Drawing.Size(116, 54);
            this.btnCancelInv.TabIndex = 20;
            this.btnCancelInv.Text = "ارجاع البون بالكامل ";
            this.btnCancelInv.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.button2.Location = new System.Drawing.Point(488, 473);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(116, 54);
            this.button2.TabIndex = 21;
            this.button2.Text = "اتمام العملية";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.button3.Location = new System.Drawing.Point(20, 473);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(141, 54);
            this.button3.TabIndex = 22;
            this.button3.Text = "عودة للقائمة الرئيسية";
            this.button3.UseVisualStyleBackColor = false;
            // 
            // panel6
            // 
            this.panel6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.lblCashir);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.pictureBox1);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.lblDateTime);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Location = new System.Drawing.Point(22, 1);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(857, 69);
            this.panel6.TabIndex = 23;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(691, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 31);
            this.label4.TabIndex = 13;
            this.label4.Text = "شاشة المرتجعات";
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(200, 52);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(474, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(48, 19);
            this.label15.TabIndex = 4;
            this.label15.Text = "الكاشير";
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.ForeColor = System.Drawing.Color.Blue;
            this.lblDateTime.Location = new System.Drawing.Point(611, 42);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDateTime.Size = new System.Drawing.Size(49, 19);
            this.lblDateTime.TabIndex = 2;
            this.lblDateTime.Text = "label3";
            this.lblDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(792, 42);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 19);
            this.label17.TabIndex = 3;
            this.label17.Text = "التاريخ";
            // 
            // dgvReturnTransaction
            // 
            this.dgvReturnTransaction.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvReturnTransaction.Location = new System.Drawing.Point(197, 198);
            this.dgvReturnTransaction.Name = "dgvReturnTransaction";
            this.dgvReturnTransaction.Size = new System.Drawing.Size(240, 150);
            this.dgvReturnTransaction.TabIndex = 24;
            // 
            // ReturnTransaction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 539);
            this.Controls.Add(this.dgvReturnTransaction);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnCancelInv);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ReturnTransaction";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "شاشة المرتجعات";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReturnTransaction_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvReturnTransaction)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblCashir;
        private System.Windows.Forms.TextBox txtTransactionID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtSafeNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCashierName;
        private System.Windows.Forms.Label label8;
        //private System.Windows.Forms.Button btnPrint;
        //private System.Windows.Forms.Button btnCancelInv;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtChange;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPaid;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDueAmount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtCashDiscount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDiscountPercent;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtInvTotal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCancelInv;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtCustmoerName;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtCredit;
        private System.Windows.Forms.CheckBox creditCheckBox;
        private System.Windows.Forms.DataGridView dgvReturnTransaction;
    }
}