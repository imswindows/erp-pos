﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace SaudiMarketPOS
{
    static class Program
    {
        public static int empID;
        public static string fristName;
        public static string lastName;
        public static int roleID;
        public static int branch;
        public static string machine;
        public static string conString;
        public static int checkRole;
        public static bool delTransactionRow;
        public static bool enableDis;
        public static int supervisorID;
        public static int safeNumber;
        public static string machineName;
        public static DateTime ExpiryDate;
        //public static int usRole;
        //public static string selectedProduct;
        //public static string selectedProductInsert;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var macAddr = (from nic in NetworkInterface.GetAllNetworkInterfaces()
                           where nic.OperationalStatus == OperationalStatus.Up
                           select nic.GetPhysicalAddress().ToString()).FirstOrDefault();
            const string subkey = "Microsfot.Calendar";

            XmlDocument doc = new XmlDocument();
            doc.Load("ConnectionAndSafe.xml");
            XmlElement root = doc.DocumentElement;
            XmlNodeList elemlist = root.GetElementsByTagName("con");
            foreach (XmlNode conn in elemlist)
            {
                conString = conn.InnerText;
            }

            XmlNodeList elemlist2 = root.GetElementsByTagName("safeNumber");
            foreach (XmlNode safe in elemlist2)
            {
                safeNumber = int.Parse(safe.InnerText);
            }
            XmlNodeList elemlist3 = root.GetElementsByTagName("machineName");
            foreach (XmlNode name in elemlist3)
            {
                machineName = name.InnerText;
            }
            XmlNodeList elemlist4 = root.GetElementsByTagName("branch");
            foreach (XmlNode br in elemlist4)
            {
                branch = int.Parse(br.InnerText);
            }
            XmlNodeList elemlist5 = root.GetElementsByTagName("machine");
            foreach (XmlNode machineNumber in elemlist5)
            {
                machine = machineNumber.InnerText;
            }
            try
            {
                using (RegistryKey key = Registry.CurrentUser.OpenSubKey(subkey))
                {
                    if (key != null)
                    {
                        Object Format = key.GetValue("Format");
                        Object Sample = key.GetValue("Sample");
                        if (Format != null && Sample != null)
                        {
                            if (Format.ToString() == macAddr)
                            {
                                DateTime CloseDate = DateTime.Parse(Sample.ToString());
                                if (CloseDate >= DateTime.Now)
                                {
                                    if (CloseDate.AddDays(-3) <= DateTime.Now)
                                    {
                                        MessageBox.Show("سوف يتم الغاء تسجيل النسخة بعد أقل من 3 أيام. من فضلك راجع المشرف");
                                    }
                                    Application.EnableVisualStyles();
                                    Application.SetCompatibleTextRenderingDefault(false);
                                    Application.Run(new Login());
                                }
                                else
                                {
                                    MessageBox.Show("يجب اعادة تسجيل النسخه");
                                }
                            }
                            else
                            {
                                MessageBox.Show("من فضلك تحقق من اتصالك بالشبكه");
                            }
                        }
                        else
                        {
                            MessageBox.Show("لم يتم تسجيل النسخه. من فضلك راجع المشرف");
                        }
                    }
                    else
                    {
                        MessageBox.Show("لم يتم تسجيل النسخه. من فضلك راجع المشرف");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("");
            }
        }
    }
}
