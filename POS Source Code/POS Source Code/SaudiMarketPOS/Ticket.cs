﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Drawing;
using SaudiMarketPOS;
using System.Data;

namespace ReceiptCreator
{
    public class TransactionReceipt
    {
        PrintDocument pdoc = null;
        DateTime TransactionDate;
        DateTime TransactionTime;
        DateTime? UpdateTime;
        int transactionNo;
        String ReferanceTransaction;
        int ItemCount = 0;
        int SafeNo;
        bool Copy = false;
        String CashirName;
        String CustName;
        String CustPhone;
        String DeliveryEmpName;
        String ItemName;
        String CustAddress;
        String Notes;
        String PhoneEmp;
        String PaymentMethod;
        String CustomerData;
        decimal Subtotal = 0;
        decimal Price;
        decimal Qty;
        decimal ItemTot;
        decimal TransactionTot;
        decimal Discount;
        decimal Paid;
        decimal Change;
        decimal DeliveryCharge;
        decimal WalletValue;
        String ShiftStart;
        String ShiftEnd;
        decimal OpeningBalance;
        decimal TotalShiftSales;
        decimal TotalShiftBalance;
        decimal TotalCredit;
        decimal TotalUncollectedDO;
        decimal TotalCreditCard;
        decimal TotalReturns;
        decimal TotalDiscount;
        decimal TotalSafeBalance;
        decimal TotalOnlinePaid;
        decimal NetSales;
        decimal NextOpeningBalance;
        decimal NetCashPaid;
        decimal ReturnedQty;
        Bitmap SeLogo;
        public DataGridView GridViewRow;
        public DataRowCollection GridCollectionRow;

        public String shiftStart
        {
            //set the person name
            set { this.ShiftStart = value; }
            //get the person name 
            get { return this.ShiftStart; }
        }
        public Bitmap seLogo
        {
            set { this.SeLogo = value; }
            get { return this.SeLogo; }
        }
        public String notes
        {
            //set the person name
            set { this.Notes = value; }
            //get the person name 
            get { return this.Notes; }
        }

        public String phoneEmp
        {
            set { this.PhoneEmp = value; }
            //get the person name 
            get { return this.PhoneEmp; }
        }
        public String shiftEnd
        {
            //set the person name
            set { this.ShiftEnd = value; }
            //get the person name 
            get { return this.ShiftEnd; }
        }
        public String paymentMethod
        {
            set { PaymentMethod = value; }
            get { return PaymentMethod; }
        }
        public String customerData
        {
            get { return CustomerData; }
            set { CustomerData = value; }
        }
        public decimal openingBalance
        {
            //set the person name
            set { this.OpeningBalance = value; }
            //get the person name 
            get { return this.OpeningBalance; }
        }
        public decimal returnedQty
        {
            set { this.ReturnedQty = value; }

            get { return this.ReturnedQty; }

        }
        public decimal totalShiftSales
        {
            //set the person name
            set { this.TotalShiftSales = value; }
            //get the person name 
            get { return this.TotalShiftSales; }
        }
        public decimal totalShiftBalance
        {
            //set the person name
            set { this.TotalShiftBalance = value; }
            //get the person name 
            get { return this.TotalShiftBalance; }
        }
        public decimal totalCredit
        {
            //set the person name
            set { this.TotalCredit = value; }
            //get the person name 
            get { return this.TotalCredit; }
        }
        public decimal totalUncollectedDO
        {
            //set the person name
            set { this.TotalUncollectedDO = value; }
            //get the person name 
            get { return this.TotalUncollectedDO; }
        }
        public decimal totalCreditCard
        {
            //set the person name
            set { this.TotalCreditCard = value; }
            //get the person name 
            get { return this.TotalCreditCard; }
        }
        public decimal totalReturns
        {
            //set the person name
            set { this.TotalReturns = value; }
            //get the person name 
            get { return this.TotalReturns; }
        }
        public decimal totalOnlinePaid
        {
            get { return this.TotalOnlinePaid; }
            set { this.TotalOnlinePaid = value; }
        }
        public decimal totalDiscount
        {
            //set the person name
            set { this.TotalDiscount = value; }
            //get the person name 
            get { return this.TotalDiscount; }
        }
        public decimal totalSafeBalance
        {
            //set the person name
            set { this.TotalSafeBalance = value; }
            //get the person name 
            get { return this.TotalSafeBalance; }
        }
        public DateTime transactionDate
        {
            //set the person name
            set { this.TransactionDate = value; }
            //get the person name 
            get { return this.TransactionDate; }
        }

        public DateTime transactionTime
        {
            //set the person name
            set { this.TransactionTime = value; }
            //get the person name 
            get { return this.TransactionTime; }
        }

        public DateTime? updateTime
        {
            //set the person name
            set { this.UpdateTime = value; }
            //get the person name 
            get { return this.UpdateTime; }
        }
        public int TransactionNo
        {
            //set the person name
            set { this.transactionNo = value; }
            //get the person name 
            get { return this.transactionNo; }
        }
        public int safeNo
        {
            //set the person name
            set { this.SafeNo = value; }
            //get the person name 
            get { return this.SafeNo; }
        }
        public bool copy
        {
            //set the person name
            set { this.Copy = value; }
            //get the person name 
            get { return this.Copy; }
        }
        public String cashirName
        {
            //set the person name
            set { this.CashirName = value; }
            //get the person name 
            get { return this.CashirName; }
        }
        public String referanceTransaction
        {
            //set the person name
            set { this.ReferanceTransaction = value; }
            //get the person name 
            get { return this.ReferanceTransaction; }
        }
        public String custName
        {
            //set the person name
            set { this.CustName = value; }
            //get the person name 
            get { return this.CustName; }
        }
        public String custPhone
        {
            //set the person name
            set { this.CustPhone = value; }
            //get the person name 
            get { return this.CustPhone; }
        }
        public String custAdress
        {
            //set the person name
            set { this.CustAddress = value; }
            //get the person name 
            get { return this.CustAddress; }
        }
        public String deliveryEmpName
        {
            //set the person name
            set { this.DeliveryEmpName = value; }
            //get the person name 
            get { return this.DeliveryEmpName; }
        }
        public String itemName
        {
            //set the person name
            set { this.ItemName = value; }
            //get the person name 
            get { return this.ItemName; }
        }
        public decimal price
        {
            //set the person name
            set { this.Price = value; }
            //get the person name 
            get { return this.Price; }
        }
        public decimal qty
        {
            //set the person name
            set { this.Qty = value; }
            //get the person name 
            get { return this.Qty; }
        }
        public decimal itemTot
        {
            //set the person name
            set { this.ItemTot = value; }
            //get the person name 
            get { return this.ItemTot; }
        }
        public decimal transactionTot
        {
            //set the person name
            set { this.TransactionTot = value; }
            //get the person name 
            get { return this.TransactionTot; }
        }
        public decimal dueAmount
        {
            //set the person name
            set { this.DueAmount = value; }
            //get the person name 
            get { return this.DueAmount; }
        }
        public decimal discount
        {
            //set the person name
            set { this.Discount = value; }
            //get the person name 
            get { return this.Discount; }
        }
        public decimal paid
        {
            //set the person name
            set { this.Paid = value; }
            //get the person name 
            get { return this.Paid; }
        }
        public decimal deliveryCharge
        {
            get { return DeliveryCharge; }
            set { DeliveryCharge = value; }
        }
        public decimal change
        {
            //set the person name
            set { this.Change = value; }
            //get the person name 
            get { return this.Change; }
        }
        public decimal netSales
        {
            //set the person name
            set { this.NetSales = value; }
            //get the person name 
            get { return this.NetSales; }
        }
        public decimal nextOpeningBalance
        {
            //set the person name
            set { this.NextOpeningBalance = value; }
            //get the person name 
            get { return this.NextOpeningBalance; }
        }
        public decimal netCashPaid
        {
            //set the person name
            set { this.NetCashPaid = value; }
            //get the person name 
            get { return this.NetCashPaid; }
        }
        public decimal walletvalue
        {
            get { return WalletValue; }
            set { WalletValue = value; }
        }
        public TransactionReceipt()
        {

        }

        public TransactionReceipt
            (DateTime TransactionDate,
            DateTime TransactionTime,
            String ShiftStart,
            DateTime ShiftEnd,
            decimal OpeningBalance,
            decimal TotalShiftSales,
            decimal TotalShiftBalance,
            decimal TotalCredit,
            decimal TotalUncollectedDO,
            decimal TotalCreditCard,
            decimal TotalReturns,
            decimal TotalDiscount,
            decimal TotalSafeBalance,
            decimal TotalOnlinePaid,
            int transactionNo,
            String ReferanceTransaction,
            int SafeNo,
            String CashirName,
            String CustName,
            String DeliveryEmpName,
            String ItemName,
            String CustAddress,
            String PaymentMethod,
            String CustomerData,
            decimal Price,
            decimal Qty,
            decimal ItemTot,
            decimal TransactionTot,
            decimal DueAmount,
            decimal Discount,
            decimal Paid,
            decimal Change,
            decimal DeliveryCharge)
        {
            this.TransactionDate = TransactionDate;
            this.TransactionTime = TransactionTime;
            this.transactionNo = transactionNo;
            this.ReferanceTransaction = ReferanceTransaction;
            this.SafeNo = SafeNo;
            this.CashirName = CashirName;
            this.DeliveryEmpName = DeliveryEmpName;
            this.ItemName = ItemName;
            this.Price = Price;
            this.Qty = Qty;
            this.ItemTot = ItemTot;
            this.TransactionTot = TransactionTot;
            this.DueAmount = DueAmount;
            this.Discount = Discount;
            this.Paid = Paid;
            this.Change = Change;
            this.PaymentMethod = PaymentMethod;
            this.CustomerData = CustomerData;
            this.DeliveryCharge = DeliveryCharge;
            this.TotalOnlinePaid = TotalOnlinePaid;
        }

        public void print(int h)
        {

            PrintDialog pd = new PrintDialog();
            pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Times New Roman", 15);

            //PaperSize psize = new PaperSize();
            PaperSize psize = new PaperSize("Custom", 300, h);
            //ps.DefaultPageSettings.PaperSize = psize;

            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;
            //pdoc.DefaultPageSettings.PaperSize.Height =320;
            //pdoc.DefaultPageSettings.PaperSize.Height = 820 + h;

            //pdoc.DefaultPageSettings.PaperSize.Width = 520;

            pdoc.PrintPage += new PrintPageEventHandler(printTransaction);

            pdoc.Print();

            //DialogResult result = pd.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    PrintPreviewDialog pp = new PrintPreviewDialog();
            //    pp.Document = pdoc;
            //    result = pp.ShowDialog();
            //    if (result == DialogResult.OK)
            //    {
            //        pdoc.Print();
            //    }
            //}

        }

        public void printDeleted(int h)
        {

            PrintDialog pd = new PrintDialog();
            pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Times New Roman", 15);

            //PaperSize psize = new PaperSize();
            PaperSize psize = new PaperSize("Custom", 300, h);
            //ps.DefaultPageSettings.PaperSize = psize;

            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;
            //pdoc.DefaultPageSettings.PaperSize.Height =320;
            //pdoc.DefaultPageSettings.PaperSize.Height = 820 + h;

            //pdoc.DefaultPageSettings.PaperSize.Width = 520;

            pdoc.PrintPage += new PrintPageEventHandler(printDeletedTransaction);

            pdoc.Print();

            //DialogResult result = pd.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    PrintPreviewDialog pp = new PrintPreviewDialog();
            //    pp.Document = pdoc;
            //    result = pp.ShowDialog();
            //    if (result == DialogResult.OK)
            //    {
            //        pdoc.Print();
            //    }
            //}

        }
        public void printDelivery(int he)
        {
            PrintDialog pd = new PrintDialog();
            pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Times New Roman", 15);


            PaperSize psize = new PaperSize("Custom", 300, he);
            //ps.DefaultPageSettings.PaperSize = psize;
            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;
            //pdoc.DefaultPageSettings.PaperSize.Height =320;
            //pdoc.DefaultPageSettings.PaperSize.Height = 820 + he;

            //pdoc.DefaultPageSettings.PaperSize.Width = 520;

            pdoc.PrintPage += new PrintPageEventHandler(crDeliveryOrderInv_En);

            pdoc.Print();

            //DialogResult result = pd.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    PrintPreviewDialog pp = new PrintPreviewDialog();
            //    pp.Document = pdoc;
            //    result = pp.ShowDialog();
            //    if (result == DialogResult.OK)
            //    {
            //        pdoc.Print();
            //    }
            //}
        }

        public void printTranWiD(int he)
        {
            PrintDialog pd = new PrintDialog();
            pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Times New Roman", 15);


            PaperSize psize = new PaperSize("Custom", 300, he);
            //ps.DefaultPageSettings.PaperSize = psize;
            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;
            //pdoc.DefaultPageSettings.PaperSize.Height =320;
            //pdoc.DefaultPageSettings.PaperSize.Height = 820 + he;

            //pdoc.DefaultPageSettings.PaperSize.Width = 520;

            pdoc.PrintPage += new PrintPageEventHandler(printTransactionWithDelivery);
            
            pdoc.Print();

            //DialogResult result = pd.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    PrintPreviewDialog pp = new PrintPreviewDialog();
            //    pp.Document = pdoc;
            //    result = pp.ShowDialog();
            //    if (result == DialogResult.OK)
            //    {
            //        pdoc.Print();
            //    }
            //}
        }

        public void printReturnT(int he)
        {
            PrintDialog pd = new PrintDialog();
            pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Times New Roman", 15);


            PaperSize psize = new PaperSize("Custom", 300, he);
            //ps.DefaultPageSettings.PaperSize = psize;
            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;
            //pdoc.DefaultPageSettings.PaperSize.Height =320;
            pdoc.DefaultPageSettings.PaperSize.Height = 820 + he;

            pdoc.DefaultPageSettings.PaperSize.Width = 520;

            pdoc.PrintPage += new PrintPageEventHandler(printReturnTransaction);

            pdoc.Print();
        }
        public void printShift()
        {
            PrintDialog pd = new PrintDialog();
            pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Times New Roman", 15);


            PaperSize psize = new PaperSize("Custom", 100, 200);
            //ps.DefaultPageSettings.PaperSize = psize;
            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;
            //pdoc.DefaultPageSettings.PaperSize.Height =320;
            pdoc.DefaultPageSettings.PaperSize.Height = 820;

            pdoc.DefaultPageSettings.PaperSize.Width = 520;

            pdoc.PrintPage += new PrintPageEventHandler(printClosingShift);

            pdoc.Print();
        }

        public void printShiftByDay()
        {
            PrintDialog pd = new PrintDialog();
            pdoc = new PrintDocument();
            PrinterSettings ps = new PrinterSettings();
            Font font = new Font("Times New Roman", 15);


            PaperSize psize = new PaperSize("Custom", 100, 200);
            //ps.DefaultPageSettings.PaperSize = psize;
            pd.Document = pdoc;
            pd.Document.DefaultPageSettings.PaperSize = psize;
            //pdoc.DefaultPageSettings.PaperSize.Height =320;
            pdoc.DefaultPageSettings.PaperSize.Height = 820;

            pdoc.DefaultPageSettings.PaperSize.Width = 520;

            pdoc.PrintPage += new PrintPageEventHandler(printDayShift);

            pdoc.Print();
        }

        void printTransaction(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Times New Roman", 10);
            float fontHeight = font.GetHeight();
            int startX = 280;
            int startY = 110;
            int Offset = 10;
            StringFormat drawFormat = new StringFormat();
            StringFormat drawCenter = new StringFormat();
            drawCenter.LineAlignment = StringAlignment.Center;
            //drawCenter.FormatFlags=StringFormatFlags.FitBlackBox;
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            StringFormat centerFormmat = new StringFormat();
            centerFormmat.LineAlignment = StringAlignment.Center;
            centerFormmat.Alignment = StringAlignment.Center;
            graphics.DrawImage(this.seLogo, new Rectangle(50, 0, 200, 100));
            //graphics.DrawString("Seoudi", new Font("Agency FB", 20, FontStyle.Bold), new SolidBrush(Color.Black), 100, startY ,drawCenter);
            //Offset = Offset +10;
            //graphics.DrawString("Mohandseen Branch", new Font("Agency FB", 16, FontStyle.Bold), new SolidBrush(Color.Black), 70, startY + Offset,drawCenter );
            //Offset = Offset + 20;
            graphics.DrawString("التاريخ :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToShortDateString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            graphics.DrawString("الساعة :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToShortTimeString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("رقم البون :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.TransactionNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            graphics.DrawString("الخزنة :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.safeNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("اسم الخزنة :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Program.machineName.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("الكاشير :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.cashirName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            //graphics.DrawString("مندوب التوصيل :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            //graphics.DrawString(this.deliveryEmpName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 65, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("العميل :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.custName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            Offset = Offset + 20;
            if (this.copy)
            {
                graphics.DrawString("Copy", new Font("Times New Roman", 12, FontStyle.Bold), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
                Offset = Offset + 30;
            }
            String underLine = "*************************************";
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("اسم الصنف", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("السعر", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
            graphics.DrawString("الكمية", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset, drawFormat);
            graphics.DrawString("الإجمالي", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //----------------------------------------------------------
            foreach (DataGridViewRow dr in GridViewRow.Rows)
            {
                this.itemTot = decimal.Parse(dr.Cells["Total"].Value.ToString());
                this.qty = decimal.Parse(dr.Cells["ItemQty"].Value.ToString());
                this.price = decimal.Parse(dr.Cells["ItemPrice"].Value.ToString());
                this.itemName = dr.Cells["ItemName"].Value.ToString();
                graphics.DrawString(this.itemName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                Offset = Offset + 20;
                graphics.DrawString(Math.Round(this.price, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
                graphics.DrawString(Math.Round(this.qty, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 120, startY + Offset, drawFormat);
                graphics.DrawString(Math.Round(this.itemTot, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
                Offset = Offset + 20;
            }
            //----------------------------------------------------------
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("الإجمالي :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.transactionTot, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("خصم :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.discount, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("المستحق :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.dueAmount, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("المدفوع :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.paid, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;
            Math.Round(this.paid, 2).ToString();
            graphics.DrawString("المتبقى :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.change, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 40;
            if (this.copy)
            {
                graphics.DrawString("Copy", new Font("Times New Roman", 12, FontStyle.Bold), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
                Offset = Offset + 30;
            }
            graphics.DrawString("Thank you for shopping with us", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 245, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("For Home Delivery Call 19642", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 242, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("For Comments call 01005199199", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 247, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //graphics.DrawString("16962", new Font("Times New Roman", 14, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset);
            //Offset = Offset + 50;
            graphics.DrawString("aimseg.com", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 190, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString(" ", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 50;

        }
        void printReturnTransaction(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Times New Roman", 10);
            float fontHeight = font.GetHeight();
            int startX = 280;
            int startY = 110;
            int Offset = 10;
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            StringFormat centerFormmat = new StringFormat();
            centerFormmat.LineAlignment = StringAlignment.Center;
            centerFormmat.Alignment = StringAlignment.Center;

            graphics.DrawImage(this.seLogo, new Rectangle(50, 0, 200, 100));
            graphics.DrawString("بون مرتجعات", new Font("Times New Roman", 12, FontStyle.Bold), new SolidBrush(Color.Black), 100, startY + Offset);
            Offset = Offset + 40;
            graphics.DrawString("التاريخ :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToShortDateString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            graphics.DrawString("الساعة :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToShortTimeString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("رقم البون :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.TransactionNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            graphics.DrawString("الخزنة :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.safeNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("الكاشير :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.cashirName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            graphics.DrawString("العميل :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.custName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("رقم البون المرجعي :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.referanceTransaction.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
            Offset = Offset + 20;
            String underLine = "*************************************";
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("(اسم الـصــنف)", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("سعر", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 250, startY + Offset, drawFormat);
            graphics.DrawString("كمية بيع", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            graphics.DrawString("كمية مرتجع", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 140, startY + Offset, drawFormat);
            graphics.DrawString("ج مرتجع", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 65, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //----------------------------------------------------------
            foreach (DataGridViewRow dr in GridViewRow.Rows)
            {
                this.itemTot = decimal.Parse(dr.Cells["Total"].Value.ToString());
                this.qty = decimal.Parse(dr.Cells["ItemQty"].Value.ToString());
                this.price = decimal.Parse(dr.Cells["ItemPrice"].Value.ToString());
                this.returnedQty = decimal.Parse(dr.Cells["ReturnQty"].Value.ToString());
                this.itemName = dr.Cells["ItemName"].Value.ToString();
                if (returnedQty > 0)
                {
                    graphics.DrawString(this.itemName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                    Offset = Offset + 20;
                    graphics.DrawString(Math.Round(this.price, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 250, startY + Offset, drawFormat);
                    graphics.DrawString(Math.Round(this.qty, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
                    graphics.DrawString(Math.Round(this.returnedQty, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 140, startY + Offset, drawFormat);
                    graphics.DrawString(Math.Round(this.itemTot, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 65, startY + Offset, drawFormat);
                    Offset = Offset + 20;
                }
            }
            //----------------------------------------------------------
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("اجمالي قيمة المرتجعات :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.transactionTot, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("برجاء الاحتفاظ بالبون عند الاستبدال", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 220, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("شكرا لتسوقكم مع نوك مارت", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("Thank you for shopping with us", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 245, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("For Home Delivery Call 19642", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 242, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("For Comments call 01005199199", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 247, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //graphics.DrawString("16962", new Font("Times New Roman", 14, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset);
            //Offset = Offset + 50;
            graphics.DrawString("aimseg.com", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 190, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString(" ", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 50;

        }
        void printTransactionWithDelivery(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Times New Roman", 10);
            float fontHeight = font.GetHeight();
            int startX = 280;
            int startY = 20;
            int Offset = 10;
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            StringFormat centerFormmat = new StringFormat();
            centerFormmat.LineAlignment = StringAlignment.Center;
            centerFormmat.Alignment = StringAlignment.Center;

            graphics.DrawString("Knock Mart", new Font("Agency FB", 20, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset);
            Offset = Offset + 40;
            graphics.DrawString("التاريخ :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToShortDateString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            graphics.DrawString("الساعة :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToShortTimeString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("رقم البون :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.TransactionNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            graphics.DrawString("الخزنة :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.safeNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("الكاشير :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.cashirName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            graphics.DrawString("مندوب التوصيل :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.deliveryEmpName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 65, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("العميل :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.custName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            graphics.DrawString("تليفون العميل", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.custPhone, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 80, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("العنوان :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.custAdress, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("طريقة الدفع :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.paymentMethod, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            Offset = Offset + 20;
            String underLine = "*************************************";
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("اسم الصنف", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("السعر", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
            graphics.DrawString("الكمية", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset, drawFormat);
            graphics.DrawString("الإجمالي", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //----------------------------------------------------------
            foreach (DataGridViewRow dr in GridViewRow.Rows)
            {
                this.itemTot = decimal.Parse(dr.Cells["Total"].Value.ToString());
                this.qty = decimal.Parse(dr.Cells["ItemQty"].Value.ToString());
                this.price = decimal.Parse(dr.Cells["ItemPrice"].Value.ToString());
                this.itemName = dr.Cells["ItemName"].Value.ToString();
                graphics.DrawString(this.itemName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                Offset = Offset + 20;
                graphics.DrawString(Math.Round(this.price, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
                graphics.DrawString(Math.Round(this.qty, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 120, startY + Offset, drawFormat);
                graphics.DrawString(Math.Round(this.itemTot, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
                Offset = Offset + 20;
            }
            //----------------------------------------------------------
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("الإجمالي :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.transactionTot, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("خصم :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.discount, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("المستحق :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.dueAmount, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("المدفوع :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.paid, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;
            Math.Round(this.paid, 2).ToString();
            graphics.DrawString("المتبقى :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.change, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 40;

            graphics.DrawString("Thank you for shopping with us", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 220, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("For Home Delivery Call 19642", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("For Comments call 01005199199", new Font("Times New Roman", 14, FontStyle.Bold), new SolidBrush(Color.Black), 20, startY + Offset);
            Offset = Offset + 20;
            //graphics.DrawString("16962", new Font("Times New Roman", 14, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset);
            //Offset = Offset + 50;
            graphics.DrawString("aimseg.com", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString(" ", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 50;

        }
        void printDeletedTransaction(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Times New Roman", 10);
            float fontHeight = font.GetHeight();
            int startX = 280;
            int startY = 110;
            int Offset = 10;
            StringFormat drawFormat = new StringFormat();
            StringFormat drawCenter = new StringFormat();
            drawCenter.LineAlignment = StringAlignment.Center;
            //drawCenter.FormatFlags=StringFormatFlags.FitBlackBox;
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            StringFormat centerFormmat = new StringFormat();
            centerFormmat.LineAlignment = StringAlignment.Center;
            centerFormmat.Alignment = StringAlignment.Center;
            graphics.DrawImage(this.seLogo, new Rectangle(50, 0, 200, 100));
            //graphics.DrawString("Seoudi", new Font("Agency FB", 20, FontStyle.Bold), new SolidBrush(Color.Black), 100, startY ,drawCenter);
            //Offset = Offset +10;
            //graphics.DrawString("Mohandseen Branch", new Font("Agency FB", 16, FontStyle.Bold), new SolidBrush(Color.Black), 70, startY + Offset,drawCenter );
            //Offset = Offset + 20;
            graphics.DrawString("التاريخ :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToString("dd/MMM/yyyy hh:mm"), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("آخر تعديل :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.updateTime?.ToString("dd/MMM/yyyy hh:mm"), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("رقم البون :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.TransactionNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            graphics.DrawString("الخزنة :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.safeNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("الكاشير :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.cashirName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            //graphics.DrawString("مندوب التوصيل :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            //graphics.DrawString(this.deliveryEmpName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 65, startY + Offset, drawFormat);
            Offset = Offset + 20;
            if (this.copy)
            {
                graphics.DrawString("Copy", new Font("Times New Roman", 12, FontStyle.Bold), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
                Offset = Offset + 30;
            }
            String underLine = "*************************************";
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("اسم الصنف", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("السعر", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
            graphics.DrawString("الكمية", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset, drawFormat);
            graphics.DrawString("الإجمالي", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //----------------------------------------------------------
            foreach (DataRow dr in GridCollectionRow)
            {
                this.itemTot = decimal.Parse(dr["ItemTotal"].ToString());
                this.qty = decimal.Parse(dr["ItemQty"].ToString());
                this.price = decimal.Parse(dr["ItemPrice"].ToString());
                this.itemName = dr["ItemName"].ToString();
                graphics.DrawString(this.itemName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                Offset = Offset + 20;
                graphics.DrawString(Math.Round(this.price, 4).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
                graphics.DrawString(Math.Round(this.qty, 4).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 120, startY + Offset, drawFormat);
                graphics.DrawString(Math.Round(this.itemTot, 4).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
                Offset = Offset + 20;
            }
            //----------------------------------------------------------
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("الإجمالي :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.transactionTot, 4).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;
            
            graphics.DrawString("Thank you for shopping with us", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 245, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("For Home Delivery Call 19642", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 242, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("For Comments call 01005199199", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 247, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //graphics.DrawString("16962", new Font("Times New Roman", 14, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset);
            //Offset = Offset + 50;
            graphics.DrawString("aimseg.com", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 190, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString(" ", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 50;

        }
        void crDeliveryOrderInv_En(object sender, PrintPageEventArgs e)
        {
            bool IsCustomerData = false;
            Graphics graphics = e.Graphics;
            Font font = new Font("Arial", 10);
            float fontHeight = font.GetHeight();
            int startX = 0;
            int startY = 20;
            int Offset = 10;
            StringFormat drawFormat = new StringFormat();
            //drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            StringFormat rtlFormat = new StringFormat();
            rtlFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            StringFormat centerFormmat = new StringFormat();
            centerFormmat.LineAlignment = StringAlignment.Center;
            centerFormmat.Alignment = StringAlignment.Center;
            string[] CustomerData;
            int OrderID = 0;
            string CustomerName = "";
            string District = "";
            string Zone = "";
            string Street = "";
            string Building = "";
            string Floor = "";
            string Flat = "";
            string Landmark = "";
            string PhoneNumber = "";
            if (this.customerData.Trim() != "")
            {
                IsCustomerData = true;
                CustomerData = customerData.Split('^');
                OrderID = int.Parse(CustomerData[0]);
                CustomerName = CustomerData[1];
                District = CustomerData[2];
                Zone = CustomerData[3];
                Street = CustomerData[4];
                Building = CustomerData[5];
                Floor = CustomerData[6];
                Flat = CustomerData[7];
                Landmark = CustomerData[8];
                PhoneNumber = CustomerData[9];
            }
            else
            {
                IsCustomerData = false;
                OrderID = this.transactionNo;
            }

            graphics.DrawImage(this.seLogo, new Rectangle(50, 0, 200, 100));
            Offset = Offset + 100;
            graphics.DrawString("Order ID" + Environment.NewLine + OrderID.ToString(), new Font("Arial", 9), new SolidBrush(Color.Black), 100, startY + Offset, centerFormmat);
            Offset = Offset + 70;
            //graphics.DrawString("أمر تحضير", new Font("Agency FB", 16, FontStyle.Bold), new SolidBrush(Color.Black), 220, startY + Offset, drawFormat);
            //Offset = Offset + 40;
            if (IsCustomerData)
            {
                graphics.DrawString("Serial: ", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(this.TransactionNo.ToString(), new Font("Arial", 9), new SolidBrush(Color.Black), 40, startY + Offset, drawFormat);
                Offset = Offset + 20;
            }
            graphics.DrawString("Date: ", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToShortDateString(), new Font("Arial", 9), new SolidBrush(Color.Black), 35, startY + Offset, drawFormat);
            graphics.DrawString("Time: ", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToShortTimeString(), new Font("Arial", 9), new SolidBrush(Color.Black), 185, startY + Offset, drawFormat);
            //graphics.DrawString("Order ID", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
            //Offset = Offset + 20;
            //graphics.DrawString(this.transactionDate.ToShortDateString(), new Font("Arial", 9), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString(this.transactionDate.ToShortTimeString(), new Font("Arial", 9), new SolidBrush(Color.Black), 80, startY + Offset, drawFormat);
            //graphics.DrawString(this.TransactionNo.ToString(), new Font("Arial", 9), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //graphics.DrawString("متلقى الطلب :  ", new Font("Arial", 10, FontStyle.Bold), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
            //graphics.DrawString(this.phoneEmp.ToString(), new Font("Arial", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            //Offset = Offset + 20;
            graphics.DrawString("Pilot: ", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.deliveryEmpName, new Font("Arial", 9), new SolidBrush(Color.Black), 35, startY + Offset, drawFormat);
            Offset = Offset + 20;
            if (!IsCustomerData)
            {
                graphics.DrawString("Cust:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(this.custName, new Font("Arial", 9), new SolidBrush(Color.Black), 35, startY + Offset, drawFormat);
                Offset = Offset + 20;
                graphics.DrawString("Phone:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(this.custPhone, new Font("Arial", 9), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
                Offset = Offset + 20;
                graphics.DrawString("Address:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                Offset = Offset + 20;
                graphics.DrawString(this.custAdress, new Font("Arial", 9), new SolidBrush(Color.Black), new RectangleF(startX, startY + Offset, 280, 20));
                Offset = Offset + 20;
            }
            else
            {
                //graphics.DrawString("بيانات العميل :  ", new Font("Arial", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                //Offset = Offset + 20;
                //graphics.DrawString(this.customerData, new Font("Arial", 10), new SolidBrush(Color.Black), 240, startY + Offset, drawFormat);
                //Offset = Offset + 40;

                graphics.DrawString("Cust:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(CustomerName, new Font("Arial", 9), new SolidBrush(Color.Black), 35, startY + Offset, drawFormat);
                Offset = Offset + 20;

                graphics.DrawString("Dist /Zone:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(District + " /" + Zone, new Font("Arial", 9), new SolidBrush(Color.Black), 70, startY + Offset, drawFormat);
                Offset = Offset + 20;

                //graphics.DrawString("Zone:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                //graphics.DrawString(Zone, new Font("Arial", 9), new SolidBrush(Color.Black), 80, startY + Offset, drawFormat);
                //Offset = Offset + 20;

                graphics.DrawString("St:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                //Offset = Offset + 20;
                graphics.DrawString(Street, new Font("Arial", 9), new SolidBrush(Color.Black), 20, startY + Offset, drawFormat);
                Offset = Offset + 20;

                graphics.DrawString("Building:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(Building, new Font("Arial", 9), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);

                graphics.DrawString("Floor:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
                graphics.DrawString(Floor, new Font("Arial", 9), new SolidBrush(Color.Black), 140, startY + Offset, drawFormat);

                graphics.DrawString("Flat:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
                graphics.DrawString(Flat, new Font("Arial", 9), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
                Offset = Offset + 20;

                //graphics.DrawString("Landmark:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                //Offset = Offset + 20;
                if (Landmark.Trim() != "")
                {
                    graphics.DrawString(Landmark, new Font("Arial", 9), new SolidBrush(Color.Black), new RectangleF(0, startY + Offset, 250, 50));
                    Offset = Offset + 50;
                }

                graphics.DrawString("Phone:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(PhoneNumber, new Font("Arial", 9), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
                Offset = Offset + 20;
            }
            graphics.DrawString("Payment:", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            if (paymentMethod.Trim() != "")
            {
                graphics.DrawString(this.paymentMethod, new Font("Arial", 9), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
            }
            else
            {
                graphics.DrawString("Cash On Delivery", new Font("Arial", 9), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
            }
            Offset = Offset + 20;
            graphics.DrawString("Machine Name :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Program.machineName.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            Offset = Offset + 20;
            if (this.copy)
            {
                graphics.DrawString("Copy", new Font("Times New Roman", 12, FontStyle.Bold), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
                Offset = Offset + 30;
            }
            String underLine = "------------------------------------------------------------------";
            graphics.DrawString(underLine, new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("Name", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("QTY", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString("Unit Price", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), new RectangleF(190, startY + Offset, 50, 40));
            graphics.DrawString("Total", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), 240, startY + Offset, drawFormat);
            Offset = Offset + 30;
            //----------------------------------------------------------
            foreach (DataGridViewRow dr in GridViewRow.Rows)
            {
                this.itemTot = decimal.Parse(dr.Cells["Total"].Value.ToString());
                this.qty = decimal.Parse(dr.Cells["ItemQty"].Value.ToString());
                this.price = decimal.Parse(dr.Cells["ItemPrice"].Value.ToString());
                this.itemName = dr.Cells["ItemName"].Value.ToString();
                graphics.DrawString(this.itemName, new Font("Arial", 9), new SolidBrush(Color.Black), new RectangleF(startX, startY + Offset, 150, 40));
                //Offset = Offset + 20;
                graphics.DrawString(Math.Round(this.qty, 2).ToString(), new Font("Arial", 9), new SolidBrush(Color.Black), 180, startY + Offset, rtlFormat);
                graphics.DrawString(Math.Round(this.price, 2).ToString(), new Font("Arial", 9), new SolidBrush(Color.Black), 220, startY + Offset, rtlFormat);
                graphics.DrawString(Math.Round(this.itemTot, 2).ToString(), new Font("Arial", 9), new SolidBrush(Color.Black), 270, startY + Offset, rtlFormat);
                Offset = Offset + 30;
                Subtotal += this.itemTot;
                ItemCount++;
            }
            //----------------------------------------------------------
            graphics.DrawString(underLine, new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            if (this.copy)
            {
                graphics.DrawString("Copy", new Font("Times New Roman", 12, FontStyle.Bold), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
                Offset = Offset + 30;
            }
            //graphics.DrawString("ملاحظات :", new Font("Arial", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString(this.notes.ToString(), new Font("Arial", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            //Offset = Offset + 20;
            //graphics.DrawString(underLine, new Font("Arial", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            graphics.DrawString("Subtotal", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.Subtotal, 2).ToString(), new Font("Arial", 9), new SolidBrush(Color.Black), 270, startY + Offset, rtlFormat);
            Offset = Offset + 20;

            graphics.DrawString("Discounts", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString((Math.Round(this.discount, 2)).ToString() + (this.discount == 0?"":"-"), new Font("Arial", 9), new SolidBrush(Color.Black), 270, startY + Offset, rtlFormat);
            Offset = Offset + 20;

            graphics.DrawString("Wallet", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString((Math.Round(this.walletvalue, 2)).ToString() + (this.walletvalue == 0 ? "" : "-"), new Font("Arial", 9), new SolidBrush(Color.Black), 270, startY + Offset, rtlFormat);
            Offset = Offset + 20;

            graphics.DrawString("Delivery Fees", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.deliveryCharge, 2).ToString(), new Font("Arial", 9), new SolidBrush(Color.Black), 270, startY + Offset, rtlFormat);
            Offset = Offset + 20;

            graphics.DrawString("Total", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.transactionTot, 2).ToString(), new Font("Arial", 9), new SolidBrush(Color.Black), 270, startY + Offset, rtlFormat);
            Offset = Offset + 30;

            graphics.DrawString("Notes: ", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.notes.ToString(), new Font("Arial", 9), new SolidBrush(Color.Black), 270, startY + Offset, rtlFormat);
            Offset = Offset + 20;

            graphics.DrawString("Number if items (" + this.ItemCount.ToString() + ")", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), new RectangleF(0, startY + Offset, 280, 20), centerFormmat);
            Offset = Offset + 20;

            graphics.DrawString("Thank you for shopping with us", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 35, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("For Home Delivery Call 16962", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 38, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("For Comments call 01005199199", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 38, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //graphics.DrawString("16962", new Font("Times New Roman", 14, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset);
            //Offset = Offset + 50;
            graphics.DrawString("www.aimseg.com", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), new RectangleF(0, startY + Offset, 280, 20), centerFormmat);
            Offset = Offset + 20;

            //graphics.DrawString("Thank you for shopping" + Environment.NewLine + "Knockmart.com", new Font("Arial", 9), new SolidBrush(Color.Black), new RectangleF(0, startY + Offset, 280, 40), centerFormmat);
            //Offset = Offset + 40;

            //graphics.DrawString("I confirm receiving" + Environment.NewLine + "Signature: ..................", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), new RectangleF(0, startY + Offset, 280, 40), centerFormmat);
            //Offset = Offset + 40;

            //graphics.DrawString(underLine, new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 15;

            ////graphics.DrawString("Knockmart.com", new Font("Arial", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, centerFormmat);
            ////Offset = Offset + 20;

            //graphics.DrawString("برجاء الاحتفاظ بالايصال لعملية الارجاع" + Environment.NewLine 
            //    + "المنتجات الطازجه والمجمده والالبان والبقاله خلال ساعتين من تاريخ الشراء" + Environment.NewLine
            //    + "باقي السلع في خلال يومين من تاريخ الشراء", new Font("Arial", 8, FontStyle.Bold), new SolidBrush(Color.Black), new RectangleF(0, startY + Offset, 300, 80), centerFormmat);
            //Offset = Offset + 70;

            ////graphics.DrawString("المنتجات الطازجه والمجمده والالبان والبقاله خلال ساعتين من تاريخ الشراء", new Font("Arial", 8, FontStyle.Bold), new SolidBrush(Color.Black), 250, startY + Offset, drawFormat);
            ////Offset = Offset + 20;

            ////graphics.DrawString("باقي السلع في خلال يومين من تاريخ الشراء", new Font("Arial", 8, FontStyle.Bold), new SolidBrush(Color.Black), 250, startY + Offset, drawFormat);
            ////Offset = Offset + 20;

            ////graphics.DrawString("الاسعار شامله ضريبة المبيعات علي الاصناف الخاضعه فقط", new Font("Arial", 8, FontStyle.Bold), new SolidBrush(Color.Black), 240, startY + Offset, drawFormat);
            ////Offset = Offset + 20;

            //graphics.DrawString("www.aimseg.com", new Font("Arial", 9, FontStyle.Bold), new SolidBrush(Color.Black), new RectangleF(0, startY + Offset, 280, 20), centerFormmat);
            //Offset = Offset + 20;
        }

        void crDeliveryOrderInv(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Times New Roman", 10);
            float fontHeight = font.GetHeight();
            int startX = 280;
            int startY = 20;
            int Offset = 10;
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            StringFormat centerFormmat = new StringFormat();
            centerFormmat.LineAlignment = StringAlignment.Center;
            centerFormmat.Alignment = StringAlignment.Center;

            graphics.DrawImage(this.seLogo, new Rectangle(100, 0, 100, 100));
            Offset = Offset + 70;
            graphics.DrawString("أمر تحضير", new Font("Agency FB", 16, FontStyle.Bold), new SolidBrush(Color.Black), 220, startY + Offset, drawFormat);
            Offset = Offset + 40;
            graphics.DrawString("التاريخ :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToShortDateString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
            graphics.DrawString("الساعة :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
            graphics.DrawString(this.transactionDate.ToShortTimeString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("رقم البون :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.TransactionNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            graphics.DrawString("متلقى الطلب :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
            graphics.DrawString(this.phoneEmp.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("مندوب التحضير", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.cashirName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;
            if (this.customerData.Trim() == "")
            {
                graphics.DrawString("العميل :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(this.custName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 235, startY + Offset, drawFormat);
                graphics.DrawString("تليفون العميل :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 150, startY + Offset, drawFormat);
                graphics.DrawString(this.custPhone, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 80, startY + Offset, drawFormat);
                Offset = Offset + 20;
                graphics.DrawString("العنوان :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(this.custAdress, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 230, startY + Offset, drawFormat);
                Offset = Offset + 20;
            }
            else
            {
                //graphics.DrawString("بيانات العميل :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                //Offset = Offset + 20;
                //graphics.DrawString(this.customerData, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 240, startY + Offset, drawFormat);
                //Offset = Offset + 40;
                string[] CustomerData = customerData.Split('^');
                int CustomerID = int.Parse(CustomerData[0]);
                string CustomerName = CustomerData[1];
                string District = CustomerData[2];
                string Zone = CustomerData[3];
                string Street = CustomerData[4];
                string Building = CustomerData[5];
                string Floor = CustomerData[6];
                string Flat = CustomerData[7];
                string Landmark = CustomerData[8];
                string PhoneNumber = CustomerData[9];


                graphics.DrawString("اسم العميل :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(CustomerName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
                Offset = Offset + 20;

                graphics.DrawString("الحي :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(District, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
                Offset = Offset + 20;

                graphics.DrawString("منطقه :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(Zone, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
                Offset = Offset + 20;

                graphics.DrawString("شارع :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                Offset = Offset + 20;
                graphics.DrawString(Street, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 280, startY + Offset, drawFormat);
                Offset = Offset + 20;

                graphics.DrawString("مبني :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(Building, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 240, startY + Offset, drawFormat);

                graphics.DrawString("دور :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
                graphics.DrawString(Floor, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 140, startY + Offset, drawFormat);

                graphics.DrawString("شقه :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 100, startY + Offset, drawFormat);
                graphics.DrawString(Floor, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
                Offset = Offset + 20;

                graphics.DrawString("علامه مميزه :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                Offset = Offset + 20;
                //if (Landmark.Length > 270)
                //{
                    graphics.DrawString(Landmark, new Font("Times New Roman", 10), new SolidBrush(Color.Black), new RectangleF(0, startY + Offset, 240, 40));
                //}
                //else
                //{
                //    graphics.DrawString(Landmark, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 280, startY + Offset, drawFormat);
                Offset = Offset + 40;
                //}

                graphics.DrawString("تليفون :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                graphics.DrawString(PhoneNumber, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
                Offset = Offset + 20;
            }
            graphics.DrawString("طريقة الدفع :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            if (paymentMethod.Trim() != "")
            {
                graphics.DrawString(this.paymentMethod, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            }
            else
            {
                graphics.DrawString("Cash On Delivery", new Font("Times New Roman", 10), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            }
            Offset = Offset + 20;
            String underLine = "-------------------------------------------------------------";
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("اسم الصنف", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("السعر", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
            graphics.DrawString("الكمية", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset, drawFormat);
            graphics.DrawString("الإجمالي", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //----------------------------------------------------------
            foreach (DataGridViewRow dr in GridViewRow.Rows)
            {
                this.itemTot = decimal.Parse(dr.Cells["Total"].Value.ToString());
                this.qty = decimal.Parse(dr.Cells["ItemQty"].Value.ToString());
                this.price = decimal.Parse(dr.Cells["ItemPrice"].Value.ToString());
                this.itemName = dr.Cells["ItemName"].Value.ToString();
                graphics.DrawString(this.itemName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
                Offset = Offset + 20;
                graphics.DrawString(Math.Round(this.price, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
                graphics.DrawString(Math.Round(this.qty, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 120, startY + Offset, drawFormat);
                graphics.DrawString(Math.Round(this.itemTot, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 60, startY + Offset, drawFormat);
                Offset = Offset + 20;
            }
            //----------------------------------------------------------
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("ملاحظات :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString (this.notes.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("خصم :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.discount, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("الإجمالي :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(Math.Round(this.transactionTot, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 10;
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 15;

            graphics.DrawString("Thank you for shopping", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("Knockmart.com", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 190, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("برجاء الاحتفاظ بالايصال لعملية الاسترجاع", new Font("Times New Roman", 8, FontStyle.Bold), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("المنتجات الطازجه والمجمده والالبان والبقاله خلال ساعتين من تاريخ الشراء", new Font("Times New Roman", 8, FontStyle.Bold), new SolidBrush(Color.Black), 250, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("باقي السلع في خلال يومين من تاريخ الشراء", new Font("Times New Roman", 8, FontStyle.Bold), new SolidBrush(Color.Black), 250, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("الاسعار شامله ضريبة المبيعات علي الاصناف الخاضعه فقط", new Font("Times New Roman", 8, FontStyle.Bold), new SolidBrush(Color.Black), 240, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("www.aimseg.com", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 190, startY + Offset, drawFormat);
            Offset = Offset + 20;

            //graphics.DrawString("المستحق :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString(Math.Round(this.dueAmount, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString("المدفوع :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString(Math.Round(this.paid, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            //Offset = Offset + 20;
            //Math.Round(this.paid, 2).ToString();
            //graphics.DrawString("المتبقى :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString(Math.Round(this.change, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            //Offset = Offset + 40;

            //graphics.DrawString("برجاء الاحتفاظ بالبون عند الاستبدال", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 220, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString("شكرا لتسوقكم مع سعودي ماركت", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 210, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString("For home delivery please call", new Font("Times New Roman", 14, FontStyle.Bold), new SolidBrush(Color.Black), 20, startY + Offset);
            //Offset = Offset + 20;
            //graphics.DrawString("16684", new Font("Times New Roman", 14, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset);
            //Offset = Offset + 50;
            //graphics.DrawString(" ", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 50;
        }

        private string InsertTitleHeader(string Header)
        {
            string Underline = "";
            int strLength = Header.Length;
            int strdotLength = (50 - strLength) / 2;
            for (int i = 0; i < strdotLength; i++)
            {
                Underline += "-";
            }
            return Underline + " " + Header + " " + Underline;
        }

        void printDayShift(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Times New Roman", 10);
            float fontHeight = font.GetHeight();
            int startX = 280;
            int startY = 20;
            int Offset = 10;
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            StringFormat centerFormmat = new StringFormat();
            centerFormmat.LineAlignment = StringAlignment.Center;
            centerFormmat.Alignment = StringAlignment.Center;

            graphics.DrawString("Knock Mart", new Font("Agency FB", 20, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset);
            Offset = Offset + 40;

            //graphics.DrawString("الكاشير :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString(this.cashirName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString("رقم الخزنة :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString(this.safeNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            graphics.DrawString("تقرير ورديات يوم: ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(DateTime.Parse(shiftStart).ToString("dd/MM/yyyy"), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 180, startY + Offset, drawFormat);
            Offset = Offset + 20;
            //graphics.DrawString("نهاية الوردية :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString(this.shiftEnd, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            String underLine = "----------------------------------------------------";
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            String underLine2 = "----------------";
            //---------------------------------------------------------
            //graphics.DrawString(Math.Round(this.openingBalance, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString("رصيد استلام الوردية السابقة", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);

            //Offset = Offset + 20;
            //graphics.DrawString("+", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            graphics.DrawString(InsertTitleHeader("مبيعات"), new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(Math.Round(this.totalShiftSales, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("اجمالي عام مبيعات", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            //graphics.DrawString(underLine2, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString(Math.Round(this.totalShiftBalance, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString("الإجمالي", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            //Offset = Offset + 40;

            //graphics.DrawString("يخصم", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            graphics.DrawString("(" + Math.Round(this.totalReturns, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("مرتجعات", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("(" + Math.Round(this.totalDiscount, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("خصومات نقدية", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(underLine2, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(Math.Round(this.netSales, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("اجمالي المبيعات", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            //graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(InsertTitleHeader("مبيعات غير محصله"), new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            //graphics.DrawString("تفصيل المبيعات", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString(underLine2, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            decimal TotalUncollected = 0;

            graphics.DrawString("(" + Math.Round(this.totalOnlinePaid, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("المدفوع اونلاين", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;
            TotalUncollected += totalOnlinePaid;

            graphics.DrawString("(" + Math.Round(this.totalCredit, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("مبيعات اجلة", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;
            TotalUncollected += totalCredit;

            graphics.DrawString("(" + Math.Round(this.totalCreditCard, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("مدفوعات ببطاقات ائتمان", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;
            TotalUncollected += totalCreditCard;

            //Math.Round(this.paid, 2).ToString();
            graphics.DrawString("(" + Math.Round(this.totalUncollectedDO, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("اوامر توصيل غير محصلة", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;
            TotalUncollected += totalUncollectedDO;

            graphics.DrawString(underLine2, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("(" + Math.Round(TotalUncollected, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("اجمالي", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            //graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(InsertTitleHeader("نقديه"), new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            //graphics.DrawString(Math.Round(this.openingBalance, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString("رصيد استلام الوردية السابقة", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString(Math.Round(this.totalSafeBalance - openingBalance, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString("نقديه محصله", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            //Offset = Offset + 40;

            //graphics.DrawString("يخصم", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString("(" + Math.Round(this.nextOpeningBalance, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString("استلام الوردية التالية", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString(underLine2, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            graphics.DrawString(Math.Round(this.netCashPaid, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("صافي النقدية المسلمة", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 5;
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("طبع التقرير بواسطة :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.custName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 170, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
        }

        void printClosingShift(object sender, PrintPageEventArgs e)
        {
            Graphics graphics = e.Graphics;
            Font font = new Font("Times New Roman", 10);
            float fontHeight = font.GetHeight();
            int startX = 280;
            int startY = 20;
            int Offset = 10;
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionRightToLeft;
            StringFormat centerFormmat = new StringFormat();
            centerFormmat.LineAlignment = StringAlignment.Center;
            centerFormmat.Alignment = StringAlignment.Center;

            graphics.DrawString("Knock Mart", new Font("Agency FB", 20, FontStyle.Bold), new SolidBrush(Color.Black), 120, startY + Offset);
            Offset = Offset + 40;

            graphics.DrawString("الكاشير :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.cashirName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("رقم الخزنة :   ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.safeNo.ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("بداية الوردية  :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.shiftStart, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString("نهاية الوردية :  ", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.shiftEnd, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 200, startY + Offset, drawFormat);
            Offset = Offset + 20;
            
            String underLine = "----------------------------------------------------";
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
            String underLine2 = "----------------";
            //---------------------------------------------------------
            //graphics.DrawString(Math.Round(this.openingBalance, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString("رصيد استلام الوردية السابقة", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);

            //Offset = Offset + 20;
            //graphics.DrawString("+", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            graphics.DrawString(InsertTitleHeader("مبيعات"), new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(Math.Round(this.totalShiftSales, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("اجمالي عام مبيعات", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            //graphics.DrawString(underLine2, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString(Math.Round(this.totalShiftBalance, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //graphics.DrawString("الإجمالي", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            //Offset = Offset + 40;

            //graphics.DrawString("يخصم", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            graphics.DrawString("(" + Math.Round(this.totalReturns, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("مرتجعات", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("(" + Math.Round(this.totalDiscount, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("خصومات نقدية", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(underLine2, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(Math.Round(this.netSales, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("اجمالي المبيعات", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            //graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(InsertTitleHeader("مبيعات غير محصله"), new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            //graphics.DrawString("تفصيل المبيعات", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            //graphics.DrawString(underLine2, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            decimal TotalUncollected = 0;

            graphics.DrawString("(" + Math.Round(this.totalOnlinePaid, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("المدفوع اونلاين", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;
            TotalUncollected += totalOnlinePaid;

            graphics.DrawString("(" + Math.Round(this.totalCredit, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("مبيعات اجلة", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;
            TotalUncollected += totalCredit;

            graphics.DrawString("(" + Math.Round(this.totalCreditCard, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("مدفوعات ببطاقات ائتمان", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;
            TotalUncollected += totalCreditCard;

            //Math.Round(this.paid, 2).ToString();
            graphics.DrawString("(" + Math.Round(this.totalUncollectedDO, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("اوامر توصيل غير محصلة", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;
            TotalUncollected += totalUncollectedDO;

            graphics.DrawString(underLine2, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("(" + Math.Round(TotalUncollected, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("اجمالي", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            //graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(InsertTitleHeader("نقديه"), new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(Math.Round(this.openingBalance, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("رصيد استلام الوردية السابقة", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(Math.Round(this.totalSafeBalance - openingBalance, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("نقديه محصله", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 40;

            //graphics.DrawString("يخصم", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            //Offset = Offset + 20;

            graphics.DrawString("(" + Math.Round(this.nextOpeningBalance, 2).ToString() + ")", new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("استلام الوردية التالية", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat); 
            Offset = Offset + 20;

            graphics.DrawString(underLine2, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(Math.Round(this.netCashPaid, 2).ToString(), new Font("Times New Roman", 10), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString("صافي النقدية المسلمة", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), 175, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 5;
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;

            graphics.DrawString("طبع التقرير بواسطة :", new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            graphics.DrawString(this.custName, new Font("Times New Roman", 10), new SolidBrush(Color.Black), 170, startY + Offset, drawFormat);
            Offset = Offset + 20;
            graphics.DrawString(underLine, new Font("Times New Roman", 10, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + Offset, drawFormat);
            Offset = Offset + 20;
        }

        public decimal DueAmount { get; set; }
    }
}
