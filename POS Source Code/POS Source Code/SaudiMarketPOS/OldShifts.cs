﻿using DAL;
using ReceiptCreator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SaudiMarketPOS
{
    public partial class OldShifts : Form
    {
        DateTime StartingDate;
        DateTime EndingDate;
        decimal openingBalance;

        decimal totalSales;
        string chkTotSales;

        decimal totDueAmount;
        string chkTotDueAmount;

        decimal totCredit;
        string chkCredit;

        decimal totalUncollected;
        string chkUncollcted;

        decimal totalRetrun;
        string chkReturn;

        decimal totalCreditCard;
        string chkCreditCard;

        decimal totalOnlinePaid;
        string chkTotalOnline;

        decimal discountValue;

        decimal safeBalance;
        decimal allShiftBalance;

        decimal netSales;
        decimal nextOpeningBalance;
        decimal netCashPaid;
        DateTime toCheckDelivery;
        SqlConnection con = new SqlConnection();

        public OldShifts()
        {
            InitializeComponent();
        }

        private void btnPrintDay_Click(object sender, EventArgs e)
        {
            try
            {
                int UserID = (int)cmbCashierName.SelectedValue;
                StartingDate = DateTime.Parse(dtpReportDay.Value.ToString("dd/MM/yyyy 00:00:00"));
                EndingDate = DateTime.Parse(dtpReportDay.Value.ToString("dd/MM/yyyy 23:59:59"));

                SqlCommand AllShiftscmd = new SqlCommand();
                AllShiftscmd.Connection = con;
                AllShiftscmd.CommandType = CommandType.Text;
                if (!chkIncludeCloseTime.Checked)
                {
                    AllShiftscmd.CommandText = @"SELECT
                (SELECT TOP 1 CreatedTime FROM[dbo].[OpenningBalance] WHERE CreatedTime BETWEEN @StartingShift AND @EndingShift ORDER BY CreatedTime ASC
                ) AS CreatedTime,
                (SELECT TOP 1 ClosedTime FROM[dbo].[OpenningBalance] WHERE CreatedTime BETWEEN @StartingShift AND @EndingShift ORDER BY ClosedTime DESC
                ) AS ClosedTime";
                }
                else
                {
                    AllShiftscmd.CommandText = @"SELECT
                (SELECT TOP 1 CreatedTime FROM [dbo].[OpenningBalance]
                WHERE (CreatedTime BETWEEN @StartingShift AND @EndingShift) AND (ClosedTime BETWEEN @StartingShift AND @EndingShift) ORDER BY CreatedTime ASC
                ) AS CreatedTime,
                (SELECT TOP 1 ClosedTime FROM [dbo].[OpenningBalance] 
                WHERE (CreatedTime BETWEEN @StartingShift AND @EndingShift) AND (ClosedTime BETWEEN @StartingShift AND @EndingShift) ORDER BY ClosedTime DESC
                ) AS ClosedTime";
                }
                AllShiftscmd.Parameters.AddWithValue("@StartingShift", StartingDate);
                AllShiftscmd.Parameters.AddWithValue("@EndingShift", EndingDate);
                if (con.State == ConnectionState.Closed) con.Open();
                SqlDataReader readAllShifts = AllShiftscmd.ExecuteReader();
                DateTime ActualStartTime = new DateTime();
                DateTime ActualEndTime = new DateTime();
                if (readAllShifts.HasRows)
                {
                    while (readAllShifts.Read())
                    {
                        ActualStartTime = DateTime.Parse(readAllShifts["CreatedTime"].ToString());
                        ActualEndTime = DateTime.Parse(readAllShifts["ClosedTime"].ToString());
                    }
                    readAllShifts.Close();
                    SqlCommand totalSalesCmd = new SqlCommand();
                    totalSalesCmd.Connection = con;
                    totalSalesCmd.CommandType = CommandType.StoredProcedure;
                    totalSalesCmd.CommandText = "uspGetTotalSalesZ";
                    totalSalesCmd.Parameters.AddWithValue("@StartingShift", ActualStartTime);
                    totalSalesCmd.Parameters.AddWithValue("@EndingShift", ActualEndTime);
                    totalSalesCmd.Parameters.AddWithValue("@UserId", UserID);
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlDataReader readTotalSales = totalSalesCmd.ExecuteReader();
                    while (readTotalSales.Read())
                    {
                        chkTotSales = readTotalSales[0].ToString();
                    }
                    readTotalSales.Close();
                    if (con.State == ConnectionState.Open) con.Close();

                    if (chkTotSales == string.Empty)
                    {
                        totalSales = 0;
                        Math.Round(totalSales, 2);
                    }
                    else
                    {
                        totalSales = Math.Round(decimal.Parse(chkTotSales.ToString()), 2);
                    }

                    //Get total due amount during the shift to calculate the discounts
                    SqlCommand totDueAmountCmd = new SqlCommand();
                    totDueAmountCmd.Connection = con;
                    totDueAmountCmd.CommandType = CommandType.StoredProcedure;
                    totDueAmountCmd.CommandText = "uspGetTotalDueAmountZ";
                    totDueAmountCmd.Parameters.AddWithValue("@StartingShift", ActualStartTime);
                    totDueAmountCmd.Parameters.AddWithValue("@EndingShift", ActualEndTime);
                    totDueAmountCmd.Parameters.AddWithValue("@UserId", UserID);
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlDataReader readTotDueAmount = totDueAmountCmd.ExecuteReader();
                    while (readTotDueAmount.Read())
                    {
                        chkTotDueAmount = readTotDueAmount[0].ToString();
                        //totDueAmount = Math.Round(decimal.Parse(readTotDueAmount[0].ToString()), 2);
                    }
                    readTotDueAmount.Close();
                    if (con.State == ConnectionState.Open) con.Close();

                    if (chkTotDueAmount == string.Empty)
                    {
                        totDueAmount = 0;
                        Math.Round(totDueAmount, 2);
                    }
                    else
                    {
                        totDueAmount = Math.Round(decimal.Parse(chkTotDueAmount.ToString()), 2);
                    }
                    //Get the credit invoces total
                    SqlCommand totCreditAmountCmd = new SqlCommand();
                    totCreditAmountCmd.Connection = con;
                    totCreditAmountCmd.CommandType = CommandType.StoredProcedure;
                    totCreditAmountCmd.CommandText = "uspGetCreditTotalZ";
                    totCreditAmountCmd.Parameters.AddWithValue("@StartingShift", ActualStartTime);
                    totCreditAmountCmd.Parameters.AddWithValue("@EndingShift", ActualEndTime);
                    totCreditAmountCmd.Parameters.AddWithValue("@UserId", UserID);
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlDataReader readCreditTotal = totCreditAmountCmd.ExecuteReader();

                    while (readCreditTotal.Read())
                    {
                        chkCredit = readCreditTotal[0].ToString();
                    }
                    readCreditTotal.Close();
                    if (con.State == ConnectionState.Open) con.Close();

                    if (chkCredit == string.Empty)
                    {
                        totCredit = 0;
                        Math.Round(totCredit, 2);
                    }
                    else
                    {
                        totCredit = Math.Round(decimal.Parse(chkCredit.ToString()), 2);
                    }

                    //Get the uncollected Delivery Orders Total
                    SqlCommand uncollectedCmd = new SqlCommand();
                    uncollectedCmd.Connection = con;
                    uncollectedCmd.CommandType = CommandType.StoredProcedure;
                    uncollectedCmd.CommandText = "uspGetunCollectedDOTotalZ";
                    uncollectedCmd.Parameters.AddWithValue("@StartingShift", ActualStartTime);
                    uncollectedCmd.Parameters.AddWithValue("@EndingShift", ActualEndTime);
                    uncollectedCmd.Parameters.AddWithValue("@UserId", UserID);
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlDataReader readUncollectedTot = uncollectedCmd.ExecuteReader();

                    while (readUncollectedTot.Read())
                    {
                        chkUncollcted = readUncollectedTot[0].ToString();
                    }
                    readUncollectedTot.Close();
                    if (con.State == ConnectionState.Open) con.Close();

                    if (chkUncollcted == string.Empty)
                    {
                        totalUncollected = 0;
                        Math.Round(totalUncollected, 2);
                    }
                    else
                    {
                        totalUncollected = Math.Round(decimal.Parse(chkUncollcted.ToString()), 2);
                    }
                    //Get the return total
                    SqlCommand returnCmd = new SqlCommand();
                    returnCmd.Connection = con;
                    returnCmd.CommandType = CommandType.StoredProcedure;
                    returnCmd.CommandText = "uspGetReturnTotalZ";
                    returnCmd.Parameters.AddWithValue("@StartingShift", ActualStartTime);
                    returnCmd.Parameters.AddWithValue("@EndingShift", ActualEndTime);
                    returnCmd.Parameters.AddWithValue("@UserId", UserID);
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlDataReader readReturnTotal = returnCmd.ExecuteReader();

                    while (readReturnTotal.Read())
                    {
                        chkReturn = readReturnTotal[0].ToString();
                    }
                    readReturnTotal.Close();
                    if (con.State == ConnectionState.Open) con.Close();

                    if (chkReturn == string.Empty)
                    {
                        totalRetrun = 0;
                        Math.Round(totalRetrun, 2);
                    }
                    else
                    {
                        totalRetrun = Math.Round(decimal.Parse(chkReturn.ToString()), 2);
                    }
                    //Get the collected Credit Card During the shift
                    SqlCommand creditCardCmd = new SqlCommand();
                    creditCardCmd.Connection = con;
                    creditCardCmd.CommandType = CommandType.StoredProcedure;
                    creditCardCmd.CommandText = "uspGetCollectedCreditCZ";
                    creditCardCmd.Parameters.AddWithValue("@StartingShift", ActualStartTime);
                    creditCardCmd.Parameters.AddWithValue("@EndingShift", ActualEndTime);
                    creditCardCmd.Parameters.AddWithValue("@UserId", UserID);
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlDataReader readCreditCard = creditCardCmd.ExecuteReader();

                    while (readCreditCard.Read())
                    {
                        chkCreditCard = readCreditCard[0].ToString();
                    }
                    readCreditCard.Close();
                    if (con.State == ConnectionState.Open) con.Close();

                    if (chkCreditCard == string.Empty)
                    {
                        totalCreditCard = 0;
                        Math.Round(totalCreditCard, 2);
                    }
                    else
                    {
                        totalCreditCard = Math.Round(decimal.Parse(chkCreditCard.ToString()), 2);
                    }
                    //Get Total Online Paid
                    SqlCommand onlinePaidCmd = new SqlCommand();
                    onlinePaidCmd.Connection = con;
                    onlinePaidCmd.CommandType = CommandType.StoredProcedure;
                    onlinePaidCmd.CommandText = "uspGetPaidOnLineTotalZ";
                    onlinePaidCmd.Parameters.AddWithValue("@StartingShift", ActualStartTime);
                    onlinePaidCmd.Parameters.AddWithValue("@EndingShift", ActualEndTime);
                    onlinePaidCmd.Parameters.AddWithValue("@UserId", UserID);
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlDataReader readOnlinePaid = onlinePaidCmd.ExecuteReader();
                    while (readOnlinePaid.Read())
                    {
                        chkTotalOnline = readOnlinePaid[0].ToString();
                    }
                    readOnlinePaid.Close();
                    if (con.State == ConnectionState.Open) con.Close();
                    if (chkTotalOnline == string.Empty)
                    {
                        totalOnlinePaid = 0;
                        Math.Round(totalOnlinePaid, 2);
                    }
                    else
                    {
                        totalOnlinePaid = Math.Round(decimal.Parse(chkTotalOnline.ToString()), 2);
                    }
                    ///////////////////////////////

                    //Calculate the discount value
                    discountValue = totalSales - totDueAmount;
                    Math.Round(discountValue, 2);

                    calculateNetSales();
                    calculateSafeBalance();
                    calculateNetCashPaid();
                    printShiftDAta();
                }
                else
                {
                    MessageBox.Show("لم يتم العثور على ورديات");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("لم يتم العثور على ورديات");
                con.Close();
            }
        }

        private void printShiftDAta()
        {
            TransactionReceipt sh = new TransactionReceipt();
            sh.cashirName = this.cmbCashierName.Text;
            //sh.safeNo = safeNumber;
            sh.shiftStart = this.StartingDate.ToString();
            sh.shiftEnd = this.EndingDate.ToString();
            sh.openingBalance = this.openingBalance;
            sh.totalShiftSales = this.totalSales;
            sh.totalShiftBalance = this.allShiftBalance;
            sh.totalReturns = this.totalRetrun;
            sh.totalDiscount = this.discountValue;
            sh.netSales = this.netSales;
            sh.totalCredit = this.totCredit;
            sh.totalCreditCard = this.totalCreditCard;
            sh.totalUncollectedDO = this.totalUncollected;
            sh.totalSafeBalance = this.safeBalance;
            sh.nextOpeningBalance = this.nextOpeningBalance;
            sh.netCashPaid = this.netCashPaid;
            sh.custName = Program.fristName + " " + Program.lastName;
            sh.totalOnlinePaid = totalOnlinePaid;

            sh.printShiftByDay();
        }

        private void printUserShiftDAta(string CashierName, int safeNumber, DateTime strating, DateTime ending)
        {
            TransactionReceipt sh = new TransactionReceipt();
            sh.cashirName = CashierName;
            sh.safeNo = safeNumber;
            sh.shiftStart = strating.ToString();
            sh.shiftEnd = ending.ToString();
            sh.openingBalance = this.openingBalance;
            sh.totalShiftSales = this.totalSales;
            sh.totalShiftBalance = this.allShiftBalance;
            sh.totalReturns = this.totalRetrun;
            sh.totalDiscount = this.discountValue;
            sh.netSales = this.netSales;
            sh.totalCredit = this.totCredit;
            sh.totalCreditCard = this.totalCreditCard;
            sh.totalUncollectedDO = this.totalUncollected;
            sh.totalSafeBalance = this.safeBalance;
            sh.nextOpeningBalance = this.nextOpeningBalance;
            sh.netCashPaid = this.netCashPaid;
            sh.custName = Program.fristName + " " + Program.lastName;
            sh.totalOnlinePaid = totalOnlinePaid;

            sh.printShift();
        }

        private void calculateNetSales()
        {
            netSales = totalSales - totalRetrun - discountValue;
        }
        private void calculateSafeBalance()
        {
            allShiftBalance = openingBalance + totalSales;
            safeBalance = openingBalance + netSales - totCredit - totalCreditCard - totalUncollected - totalOnlinePaid;
            Math.Round(safeBalance, 2);
        }
        private void calculateNetCashPaid()
        {
            netCashPaid = safeBalance - nextOpeningBalance;
        }

        private void OldShifts_Load(object sender, EventArgs e)
        {
            con.ConnectionString = Program.conString;
            try
            {
                Users UserObj = new Users();
                UserObj.con = con;
                DataTable dtEmpName = new DataTable();
                dtEmpName = UserObj.SelectCashierNames();
                cmbCashierName.DataSource = dtEmpName;
                cmbCashierName.ValueMember = "ID";
                cmbCashierName.DisplayMember = "Name";
            }
            catch (Exception ex)
            { }
        }

        private void btnSearch_Click(object sender, EventArgs e)
       {
            try
            {
                
                if (chkIncludeCloseTime.CheckState == CheckState.Checked)
                {
                    DateTime dateFrom = DateTime.Parse(dtpReportDay.Value.ToString("yyyy-MM-dd"));
                    DateTime dateTo = dateFrom.AddDays(1);



                    int UserID = (int)cmbCashierName.SelectedValue;
                    SqlCommand UserShiftscmd = new SqlCommand();
                    UserShiftscmd.Connection = con;
                    UserShiftscmd.CommandType = CommandType.Text;
                    UserShiftscmd.CommandText = @"SELECT OB.[UserID], OB.[OpenningBalance], OB.[CreatedTime], OB.[ClosedTime], OB.[SafeNumberID], OB.[Closed]
                , SUM(CASE WHEN (TR.CashingTime >= OB.CreatedTime and TR.CashingTime <= OB.ClosedTime) THEN  TR.Total ELSE 0 END) AS TotalSales FROM [dbo].[OpenningBalance] OB
                    LEFT JOIN [dbo].[TransactionTableH_Main] TR ON TR.[UserID] = OB.UserID
                    WHERE  OB.CreatedTime >= @CreatedTime AND OB.CreatedTime <= @CreatedTimeTo AND OB.UserID = @UserID AND TR.Collected = 1
                    GROUP BY OB.[UserID], OB.[OpenningBalance], OB.[CreatedTime], OB.[ClosedTime], OB.[SafeNumberID], OB.[Closed] ORDER BY OB.CreatedTime DESC";
                    UserShiftscmd.Parameters.AddWithValue("@UserID", UserID);
                    UserShiftscmd.Parameters.AddWithValue("@CreatedTime", dateFrom);
                    UserShiftscmd.Parameters.AddWithValue("@CreatedTimeTo", dateTo);
                    if (con.State == ConnectionState.Closed) con.Open();
                    DataTable dt = new DataTable();
                    dt.Load(UserShiftscmd.ExecuteReader());
                    dgvClosedShifts.AutoGenerateColumns = false;
                    dgvClosedShifts.DataSource = dt;
                }
                else
                {
                    DateTime dateFrom = DateTime.Parse(dtpReportDay.Value.ToString("yyyy-MM-dd"));
                    int UserID = (int)cmbCashierName.SelectedValue;
                    SqlCommand UserShiftscmd = new SqlCommand();
                    UserShiftscmd.Connection = con;
                    UserShiftscmd.CommandType = CommandType.Text;
                    UserShiftscmd.CommandText = @"SELECT OB.[UserID], OB.[OpenningBalance], OB.[CreatedTime], OB.[ClosedTime], OB.[SafeNumberID], OB.[Closed]
                , SUM(CASE WHEN (TR.CashingTime >= OB.CreatedTime and (TR.CashingTime <= OB.ClosedTime OR OB.ClosedTime is NULL)) THEN  TR.Total ELSE 0 END) AS TotalSales FROM [dbo].[OpenningBalance] OB
                INNER JOIN [dbo].[TransactionTableH_Main] TR ON TR.[UserID] = OB.UserID
                WHERE  OB.CreatedTime >= @CreatedTime AND OB.UserID = @UserID AND TR.Collected = 1
                GROUP BY OB.[UserID], OB.[OpenningBalance], OB.[CreatedTime], OB.[ClosedTime], OB.[SafeNumberID], OB.[Closed] ORDER BY OB.CreatedTime DESC";
                    UserShiftscmd.Parameters.AddWithValue("@UserID", UserID);
                    UserShiftscmd.Parameters.AddWithValue("@CreatedTime", dateFrom);
                    if (con.State == ConnectionState.Closed) con.Open();
                    DataTable dt = new DataTable();
                    dt.Load(UserShiftscmd.ExecuteReader());
                    dgvClosedShifts.AutoGenerateColumns = false;
                    dgvClosedShifts.DataSource = dt;
                }
            }
            catch (Exception ex){
            }
        }

        private void dgvClosedShifts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvClosedShifts.Columns["PrintShiftReport"].Index)
                {
                    try
                    {
                        DateTime strating = DateTime.Parse(dgvClosedShifts.Rows[e.RowIndex].Cells["ShiftCreatedTime"].Value.ToString());
                        DateTime ending = DateTime.Parse(dgvClosedShifts.Rows[e.RowIndex].Cells["ShiftClosedTime"].Value.ToString());
                        int SafeNumber = (int)dgvClosedShifts.Rows[e.RowIndex].Cells["ShiftSafeNumberID"].Value;
                        openingBalance = decimal.Parse(dgvClosedShifts.Rows[e.RowIndex].Cells["ShiftOpeningBalance"].Value.ToString());

                        //Get total sales during the shift
                        SqlCommand totalSalesCmd = new SqlCommand();
                        totalSalesCmd.Connection = con;
                        totalSalesCmd.CommandType = CommandType.StoredProcedure;
                        totalSalesCmd.CommandText = "uspGetTotalSalesDuringShift";
                        totalSalesCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
                        totalSalesCmd.Parameters.AddWithValue("@StartingShift", strating);
                        totalSalesCmd.Parameters.AddWithValue("@EndingShift", ending);
                        if (con.State == ConnectionState.Closed) con.Open();
                        SqlDataReader readTotalSales = totalSalesCmd.ExecuteReader();
                        while (readTotalSales.Read())
                        {
                            chkTotSales = readTotalSales[0].ToString();
                        }
                        readTotalSales.Close();
                        if (con.State == ConnectionState.Open) con.Close();

                        if (chkTotSales == string.Empty)
                        {
                            totalSales = 0;
                            Math.Round(totalSales, 2);
                        }
                        else
                        {
                            totalSales = Math.Round(decimal.Parse(chkTotSales.ToString()), 2);
                        }

                        //Get total due amount during the shift to calculate the discounts
                        SqlCommand totDueAmountCmd = new SqlCommand();
                        totDueAmountCmd.Connection = con;
                        totDueAmountCmd.CommandType = CommandType.StoredProcedure;
                        totDueAmountCmd.CommandText = "uspGetTotalDueAmountDuringShift";
                        totDueAmountCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
                        totDueAmountCmd.Parameters.AddWithValue("@StartingShift", strating);
                        totDueAmountCmd.Parameters.AddWithValue("@EndingShift", ending);
                        if (con.State == ConnectionState.Closed) con.Open();
                        SqlDataReader readTotDueAmount = totDueAmountCmd.ExecuteReader();
                        while (readTotDueAmount.Read())
                        {
                            chkTotDueAmount = readTotDueAmount[0].ToString();
                            //totDueAmount = Math.Round(decimal.Parse(readTotDueAmount[0].ToString()), 2);
                        }
                        readTotDueAmount.Close();
                        if (con.State == ConnectionState.Open) con.Close();

                        if (chkTotDueAmount == string.Empty)
                        {
                            totDueAmount = 0;
                            Math.Round(totDueAmount, 2);
                        }
                        else
                        {
                            totDueAmount = Math.Round(decimal.Parse(chkTotDueAmount.ToString()), 2);
                        }
                        //Get the credit invoces total
                        SqlCommand totCreditAmountCmd = new SqlCommand();
                        totCreditAmountCmd.Connection = con;
                        totCreditAmountCmd.CommandType = CommandType.StoredProcedure;
                        totCreditAmountCmd.CommandText = "uspGetCreditTotalDuringShift";
                        totCreditAmountCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
                        totCreditAmountCmd.Parameters.AddWithValue("@StartingShift", strating);
                        totCreditAmountCmd.Parameters.AddWithValue("@EndingShift", ending);
                        if (con.State == ConnectionState.Closed) con.Open();
                        SqlDataReader readCreditTotal = totCreditAmountCmd.ExecuteReader();

                        while (readCreditTotal.Read())
                        {
                            chkCredit = readCreditTotal[0].ToString();
                        }
                        readCreditTotal.Close();
                        if (con.State == ConnectionState.Open) con.Close();

                        if (chkCredit == string.Empty)
                        {
                            totCredit = 0;
                            Math.Round(totCredit, 2);
                        }
                        else
                        {
                            totCredit = Math.Round(decimal.Parse(chkCredit.ToString()), 2);
                        }

                        //Get the uncollected Delivery Orders Total
                        SqlCommand uncollectedCmd = new SqlCommand();
                        uncollectedCmd.Connection = con;
                        uncollectedCmd.CommandType = CommandType.StoredProcedure;
                        uncollectedCmd.CommandText = "uspGetunCollectedDOTotalDuringShift";
                        uncollectedCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
                        uncollectedCmd.Parameters.AddWithValue("@StartingShift", strating);
                        uncollectedCmd.Parameters.AddWithValue("@EndingShift", ending);
                        if (con.State == ConnectionState.Closed) con.Open();
                        SqlDataReader readUncollectedTot = uncollectedCmd.ExecuteReader();

                        while (readUncollectedTot.Read())
                        {
                            chkUncollcted = readUncollectedTot[0].ToString();
                        }
                        readUncollectedTot.Close();
                        if (con.State == ConnectionState.Open) con.Close();

                        if (chkUncollcted == string.Empty)
                        {
                            totalUncollected = 0;
                            Math.Round(totalUncollected, 2);
                        }
                        else
                        {
                            totalUncollected = Math.Round(decimal.Parse(chkUncollcted.ToString()), 2);
                        }
                        //Get the return total
                        SqlCommand returnCmd = new SqlCommand();
                        returnCmd.Connection = con;
                        returnCmd.CommandType = CommandType.StoredProcedure;
                        returnCmd.CommandText = "uspGetReturnTotalDuringShift";
                        returnCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
                        returnCmd.Parameters.AddWithValue("@StartingShift", strating);
                        returnCmd.Parameters.AddWithValue("@EndingShift", ending);
                        if (con.State == ConnectionState.Closed) con.Open();
                        SqlDataReader readReturnTotal = returnCmd.ExecuteReader();

                        while (readReturnTotal.Read())
                        {
                            chkReturn = readReturnTotal[0].ToString();
                        }
                        readReturnTotal.Close();
                        if (con.State == ConnectionState.Open) con.Close();

                        if (chkReturn == string.Empty)
                        {
                            totalRetrun = 0;
                            Math.Round(totalRetrun, 2);
                        }
                        else
                        {
                            totalRetrun = Math.Round(decimal.Parse(chkReturn.ToString()), 2);
                        }
                        //Get the collected Credit Card During the shift
                        SqlCommand creditCardCmd = new SqlCommand();
                        creditCardCmd.Connection = con;
                        creditCardCmd.CommandType = CommandType.StoredProcedure;
                        creditCardCmd.CommandText = "uspGetCollectedCreditCDuringShift";
                        creditCardCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
                        creditCardCmd.Parameters.AddWithValue("@StartingShift", strating);
                        creditCardCmd.Parameters.AddWithValue("@EndingShift", ending);
                        if (con.State == ConnectionState.Closed) con.Open();
                        SqlDataReader readCreditCard = creditCardCmd.ExecuteReader();

                        while (readCreditCard.Read())
                        {
                            chkCreditCard = readCreditCard[0].ToString();
                        }
                        readCreditCard.Close();
                        if (con.State == ConnectionState.Open) con.Close();

                        if (chkCreditCard == string.Empty)
                        {
                            totalCreditCard = 0;
                            Math.Round(totalCreditCard, 2);
                        }
                        else
                        {
                            totalCreditCard = Math.Round(decimal.Parse(chkCreditCard.ToString()), 2);
                        }
                        //Get Total Online Paid
                        SqlCommand onlinePaidCmd = new SqlCommand();
                        onlinePaidCmd.Connection = con;
                        onlinePaidCmd.CommandType = CommandType.StoredProcedure;
                        onlinePaidCmd.CommandText = "uspGetPaidOnLineTotalDuringShift";
                        onlinePaidCmd.Parameters.AddWithValue("@UserID", cmbCashierName.SelectedValue);
                        onlinePaidCmd.Parameters.AddWithValue("@StartingShift", strating);
                        onlinePaidCmd.Parameters.AddWithValue("@EndingShift", ending);
                        if (con.State == ConnectionState.Closed) con.Open();
                        SqlDataReader readOnlinePaid = onlinePaidCmd.ExecuteReader();
                        while (readOnlinePaid.Read())
                        {
                            chkTotalOnline = readOnlinePaid[0].ToString();
                        }
                        readOnlinePaid.Close();
                        if (con.State == ConnectionState.Open) con.Close();
                        if (chkTotalOnline == string.Empty)
                        {
                            totalOnlinePaid = 0;
                            Math.Round(totalOnlinePaid, 2);
                        }
                        else
                        {
                            totalOnlinePaid = Math.Round(decimal.Parse(chkTotalOnline.ToString()), 2);
                        }
                        ///////////////////////////////

                        //Calculate the discount value
                        discountValue = totalSales - totDueAmount;
                        Math.Round(discountValue, 2);

                        calculateNetSales();
                        calculateSafeBalance();
                        calculateNetCashPaid();
                        printUserShiftDAta(cmbCashierName.Text, SafeNumber, strating, ending);
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

      
    }
}
