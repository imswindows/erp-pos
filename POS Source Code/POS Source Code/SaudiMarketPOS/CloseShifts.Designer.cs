﻿namespace SaudiMarketPOS
{
    partial class CloseShifts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbImsLogo = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNextOpeningBalance = new System.Windows.Forms.TextBox();
            this.cmbCashierName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCloseShift = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.pbImsLogo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(683, 55);
            this.panel1.TabIndex = 8;
            // 
            // pbImsLogo
            // 
            this.pbImsLogo.ErrorImage = null;
            this.pbImsLogo.Location = new System.Drawing.Point(11, 11);
            this.pbImsLogo.Name = "pbImsLogo";
            this.pbImsLogo.Size = new System.Drawing.Size(90, 37);
            this.pbImsLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImsLogo.TabIndex = 19;
            this.pbImsLogo.TabStop = false;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("STC Bold", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(524, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 32);
            this.label2.TabIndex = 13;
            this.label2.Text = "إغلاق الورديات";
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txtNextOpeningBalance);
            this.panel2.Controls.Add(this.cmbCashierName);
            this.panel2.Location = new System.Drawing.Point(139, 204);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(405, 133);
            this.panel2.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("STC Bold", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(139, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 22);
            this.label1.TabIndex = 11;
            this.label1.Text = "استلام الوردية التالية";
            // 
            // txtNextOpeningBalance
            // 
            this.txtNextOpeningBalance.Font = new System.Drawing.Font("STC Bold", 12F, System.Drawing.FontStyle.Bold);
            this.txtNextOpeningBalance.Location = new System.Drawing.Point(139, 82);
            this.txtNextOpeningBalance.Name = "txtNextOpeningBalance";
            this.txtNextOpeningBalance.Size = new System.Drawing.Size(132, 35);
            this.txtNextOpeningBalance.TabIndex = 1;
            this.txtNextOpeningBalance.Text = "0.00";
            this.txtNextOpeningBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtNextOpeningBalance.TextChanged += new System.EventHandler(this.txtNextOpeningBalance_TextChanged);
            this.txtNextOpeningBalance.Enter += new System.EventHandler(this.selectTxt);
            // 
            // cmbCashierName
            // 
            this.cmbCashierName.Font = new System.Drawing.Font("STC Bold", 12F, System.Drawing.FontStyle.Bold);
            this.cmbCashierName.FormattingEnabled = true;
            this.cmbCashierName.Location = new System.Drawing.Point(90, 19);
            this.cmbCashierName.Name = "cmbCashierName";
            this.cmbCashierName.Size = new System.Drawing.Size(235, 30);
            this.cmbCashierName.TabIndex = 0;
            this.cmbCashierName.Leave += new System.EventHandler(this.cmbCashierName_Leave);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("STC Bold", 12F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(230, 171);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(239, 22);
            this.label3.TabIndex = 10;
            this.label3.Text = "قم بتحديد الكاشير المراد اغلاق ورديته";
            // 
            // btnCloseShift
            // 
            this.btnCloseShift.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCloseShift.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnCloseShift.FlatAppearance.BorderSize = 0;
            this.btnCloseShift.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCloseShift.Font = new System.Drawing.Font("STC Bold", 12F, System.Drawing.FontStyle.Bold);
            this.btnCloseShift.ForeColor = System.Drawing.Color.White;
            this.btnCloseShift.Location = new System.Drawing.Point(420, 341);
            this.btnCloseShift.Margin = new System.Windows.Forms.Padding(2);
            this.btnCloseShift.Name = "btnCloseShift";
            this.btnCloseShift.Size = new System.Drawing.Size(124, 52);
            this.btnCloseShift.TabIndex = 41;
            this.btnCloseShift.Text = "إغلاق الوردية";
            this.btnCloseShift.UseVisualStyleBackColor = false;
            this.btnCloseShift.Click += new System.EventHandler(this.btnCloseShift_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("STC Bold", 12F, System.Drawing.FontStyle.Bold);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(139, 341);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(124, 52);
            this.btnClose.TabIndex = 40;
            this.btnClose.Text = "خروج";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // CloseShifts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 500);
            this.Controls.Add(this.btnCloseShift);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CloseShifts";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "إغلاق الورديات";
            this.Load += new System.EventHandler(this.CloseShifts_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cmbCashierName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCloseShift;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNextOpeningBalance;
        private System.Windows.Forms.PictureBox pbImsLogo;
    }
}