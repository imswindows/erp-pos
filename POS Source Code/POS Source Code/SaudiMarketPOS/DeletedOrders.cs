﻿using DAL;
using ReceiptCreator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SaudiMarketPOS
{
    public partial class DeletedOrders : Form
    {
        DateTime StartingDate;
        DateTime EndingDate;
        decimal openingBalance;

        decimal totalSales;
        string chkTotSales;

        decimal totDueAmount;
        string chkTotDueAmount;

        decimal totCredit;
        string chkCredit;

        decimal totalUncollected;
        string chkUncollcted;

        decimal totalRetrun;
        string chkReturn;

        decimal totalCreditCard;
        string chkCreditCard;

        decimal totalOnlinePaid;
        string chkTotalOnline;

        decimal discountValue;

        decimal safeBalance;
        decimal allShiftBalance;

        decimal netSales;
        decimal nextOpeningBalance;
        decimal netCashPaid;
        DateTime toCheckDelivery;
        SqlConnection con = new SqlConnection();

        public DeletedOrders()
        {
            InitializeComponent();
        }

        private void btnPrintDay_Click(object sender, EventArgs e)
        {
            try
            {
                StartingDate = DateTime.Parse(dtpReportDay.Value.ToString("dd/MM/yyyy 00:00:00"));
                EndingDate = DateTime.Parse(dtpReportDay.Value.ToString("dd/MM/yyyy 23:59:59"));

                SqlCommand AllShiftscmd = new SqlCommand();
                AllShiftscmd.Connection = con;
                AllShiftscmd.CommandType = CommandType.Text;
                if (!chkIncludeCloseTime.Checked)
                {
                    AllShiftscmd.CommandText = @"SELECT
                (SELECT TOP 1 CreatedTime FROM[dbo].[OpenningBalance] WHERE CreatedTime BETWEEN @StartingShift AND @EndingShift ORDER BY CreatedTime ASC
                ) AS CreatedTime,
                (SELECT TOP 1 ClosedTime FROM[dbo].[OpenningBalance] WHERE CreatedTime BETWEEN @StartingShift AND @EndingShift ORDER BY ClosedTime DESC
                ) AS ClosedTime";
                }
                else
                {
                    AllShiftscmd.CommandText = @"SELECT
                (SELECT TOP 1 CreatedTime FROM [dbo].[OpenningBalance]
                WHERE (CreatedTime BETWEEN @StartingShift AND @EndingShift) AND (ClosedTime BETWEEN @StartingShift AND @EndingShift) ORDER BY CreatedTime ASC
                ) AS CreatedTime,
                (SELECT TOP 1 ClosedTime FROM [dbo].[OpenningBalance] 
                WHERE (CreatedTime BETWEEN @StartingShift AND @EndingShift) AND (ClosedTime BETWEEN @StartingShift AND @EndingShift) ORDER BY ClosedTime DESC
                ) AS ClosedTime";
                }
                AllShiftscmd.Parameters.AddWithValue("@StartingShift", StartingDate);
                AllShiftscmd.Parameters.AddWithValue("@EndingShift", EndingDate);
                if (con.State == ConnectionState.Closed) con.Open();
                SqlDataReader readAllShifts = AllShiftscmd.ExecuteReader();
                DateTime ActualStartTime = new DateTime();
                DateTime ActualEndTime = new DateTime();
                if (readAllShifts.HasRows)
                {
                    while (readAllShifts.Read())
                    {
                        ActualStartTime = DateTime.Parse(readAllShifts["CreatedTime"].ToString());
                        ActualEndTime = DateTime.Parse(readAllShifts["ClosedTime"].ToString());
                    }
                    readAllShifts.Close();
                    if (chkTotalOnline == string.Empty)
                    {
                        totalOnlinePaid = 0;
                        Math.Round(totalOnlinePaid, 2);
                    }
                    else
                    {
                        totalOnlinePaid = Math.Round(decimal.Parse(chkTotalOnline.ToString()), 2);
                    }
                    ///////////////////////////////

                    //Calculate the discount value
                    discountValue = totalSales - totDueAmount;
                    Math.Round(discountValue, 2);

                    calculateNetSales();
                    calculateSafeBalance();
                    calculateNetCashPaid();
                    printShiftDAta();
                }
                else
                {
                    MessageBox.Show("لم يتم العثور على ورديات");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("لم يتم العثور على ورديات");
                con.Close();
            }
        }

        private void printShiftDAta()
        {
            TransactionReceipt sh = new TransactionReceipt();
            //sh.safeNo = safeNumber;
            sh.shiftStart = this.StartingDate.ToString();
            sh.shiftEnd = this.EndingDate.ToString();
            sh.openingBalance = this.openingBalance;
            sh.totalShiftSales = this.totalSales;
            sh.totalShiftBalance = this.allShiftBalance;
            sh.totalReturns = this.totalRetrun;
            sh.totalDiscount = this.discountValue;
            sh.netSales = this.netSales;
            sh.totalCredit = this.totCredit;
            sh.totalCreditCard = this.totalCreditCard;
            sh.totalUncollectedDO = this.totalUncollected;
            sh.totalSafeBalance = this.safeBalance;
            sh.nextOpeningBalance = this.nextOpeningBalance;
            sh.netCashPaid = this.netCashPaid;
            sh.custName = Program.fristName + " " + Program.lastName;
            sh.totalOnlinePaid = totalOnlinePaid;

            sh.printShiftByDay();
        }

        private void printUserShiftDAta(string CashierName, int safeNumber, DateTime strating, DateTime ending)
        {
            TransactionReceipt sh = new TransactionReceipt();
            sh.cashirName = CashierName;
            sh.safeNo = safeNumber;
            sh.shiftStart = strating.ToString();
            sh.shiftEnd = ending.ToString();
            sh.openingBalance = this.openingBalance;
            sh.totalShiftSales = this.totalSales;
            sh.totalShiftBalance = this.allShiftBalance;
            sh.totalReturns = this.totalRetrun;
            sh.totalDiscount = this.discountValue;
            sh.netSales = this.netSales;
            sh.totalCredit = this.totCredit;
            sh.totalCreditCard = this.totalCreditCard;
            sh.totalUncollectedDO = this.totalUncollected;
            sh.totalSafeBalance = this.safeBalance;
            sh.nextOpeningBalance = this.nextOpeningBalance;
            sh.netCashPaid = this.netCashPaid;
            sh.custName = Program.fristName + " " + Program.lastName;
            sh.totalOnlinePaid = totalOnlinePaid;

            sh.printShift();
        }

        private void calculateNetSales()
        {
            netSales = totalSales - totalRetrun - discountValue;
        }
        private void calculateSafeBalance()
        {
            allShiftBalance = openingBalance + totalSales;
            safeBalance = openingBalance + netSales - totCredit - totalCreditCard - totalUncollected - totalOnlinePaid;
            Math.Round(safeBalance, 2);
        }
        private void calculateNetCashPaid()
        {
            netCashPaid = safeBalance - nextOpeningBalance;
        }

        private void OldShifts_Load(object sender, EventArgs e)
        {
            con.ConnectionString = Program.conString;
            try
            {
                Users UserObj = new Users();
                UserObj.con = con;
            }
            catch (Exception ex)
            { }
        }

        private void btnSearch_Click(object sender, EventArgs e)
       {
            try
            {
                
                if (chkIncludeCloseTime.CheckState == CheckState.Checked)
                {
                    DateTime dateFrom = DateTime.Parse(dtpReportDay.Value.ToString("yyyy-MM-dd"));
                    DateTime dateTo = dateFrom.AddDays(1);

                    SqlCommand deletedTranscmd = new SqlCommand();
                    deletedTranscmd.Connection = con;
                    deletedTranscmd.CommandType = CommandType.Text;
                    deletedTranscmd.CommandText = @"SELECT DeletedTransactionTable.Id, Total, CreatedTime, SafeNumber, UpdateTime, Users.UserName, COUNT(DeletedTransactionDetail.ID) as Detailscount, DeletedTransactionTable.TransactionID as TransactionID FROM DeletedTransactionTable
                    INNER JOin DeletedTransactionDetail on DeletedTransactionDetail.TransactionID = DeletedTransactionTable.ID
                    INNER JOIN Users on DeletedTransactionTable.UserID = Users.Id
                    WHERE CreatedTime >= @DateFrom and CreatedTime <= @DateTo
                    GROUP BY DeletedTransactionTable.Id, Total, CreatedTime, SafeNumber, UpdateTime, Users.UserName, DeletedTransactionTable.TransactionID";
                    deletedTranscmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                    deletedTranscmd.Parameters.AddWithValue("@DateTo", dateFrom);
                    if (con.State == ConnectionState.Closed) con.Open();
                    DataTable dt = new DataTable();
                    dt.Load(deletedTranscmd.ExecuteReader());
                    dgvDeletedTrans.AutoGenerateColumns = false;
                    dgvDeletedTrans.DataSource = dt;
                }
                else
                {
                    DateTime dateFrom = DateTime.Parse(dtpReportDay.Value.ToString("yyyy-MM-dd"));
                    SqlCommand deletedTranscmd = new SqlCommand();
                    deletedTranscmd.Connection = con;
                    deletedTranscmd.CommandType = CommandType.Text;
                    deletedTranscmd.CommandText = @"SELECT DeletedTransactionTable.Id, Total, CreatedTime, SafeNumber, UpdateTime, Users.UserName, COUNT(DeletedTransactionDetail.ID) as Detailscount, DeletedTransactionTable.TransactionID as TransactionID FROM DeletedTransactionTable 
                    INNER JOin DeletedTransactionDetail on DeletedTransactionDetail.TransactionID = DeletedTransactionTable.ID
                    INNER JOIN Users on DeletedTransactionTable.UserID = Users.Id
                    WHERE CreatedTime >= @DateFrom
                    GROUP BY DeletedTransactionTable.Id, Total, CreatedTime, SafeNumber, UpdateTime, Users.UserName, DeletedTransactionTable.TransactionID";
                    deletedTranscmd.Parameters.AddWithValue("@DateFrom", dateFrom);
                    if (con.State == ConnectionState.Closed) con.Open();
                    DataTable dt = new DataTable();
                    dt.Load(deletedTranscmd.ExecuteReader());
                    dgvDeletedTrans.AutoGenerateColumns = false;
                    dgvDeletedTrans.DataSource = dt;
                }
            }
            catch (Exception ex){
            }
        }

        private void dgvClosedShifts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvDeletedTrans.Columns["PrintReport"].Index)
                {
                    try
                    {
                        int dtransId = int.Parse(dgvDeletedTrans.Rows[e.RowIndex].Cells["DTransactionId"].Value.ToString());
                        DateTime createTime = DateTime.Parse(dgvDeletedTrans.Rows[e.RowIndex].Cells["CreatedTime"].Value.ToString());
                        DateTime? updateTime = !string.IsNullOrEmpty(dgvDeletedTrans.Rows[e.RowIndex].Cells["UpdateTime"].Value.ToString()) ? (DateTime?)dgvDeletedTrans.Rows[e.RowIndex].Cells["UpdateTime"].Value : null;
                        int? transId = !string.IsNullOrEmpty(dgvDeletedTrans.Rows[e.RowIndex].Cells["TransactionID"].Value.ToString())? (int?)dgvDeletedTrans.Rows[e.RowIndex].Cells["TransactionID"].Value: 0;
                        int detailCount = (int)dgvDeletedTrans.Rows[e.RowIndex].Cells["Detailscount"].Value;
                        string total = dgvDeletedTrans.Rows[e.RowIndex].Cells["Total"].Value.ToString();
                        int safeNumber = (int)dgvDeletedTrans.Rows[e.RowIndex].Cells["SafeNumber"].Value;
                        var userName = dgvDeletedTrans.Rows[e.RowIndex].Cells["UserName"].Value.ToString();

                        //Get total sales during the shift
                        SqlCommand detailsDeleted = new SqlCommand();
                        detailsDeleted.Connection = con;
                        detailsDeleted.CommandType = CommandType.StoredProcedure;
                        detailsDeleted.CommandText = "uspDeletedTransactionDetailsSelect";
                        detailsDeleted.Parameters.AddWithValue("@DTransactionID", dtransId);
                        if (con.State == ConnectionState.Closed) con.Open();
                        DataTable dt = new DataTable();
                        dt.Load(detailsDeleted.ExecuteReader());
                        if (con.State == ConnectionState.Open) con.Close();
                        TransactionReceipt tr = new TransactionReceipt();
                        tr.transactionDate = createTime;
                        tr.updateTime = updateTime;
                        tr.TransactionNo = dtransId;
                        tr.safeNo = safeNumber;
                        tr.cashirName = userName;
                        tr.GridCollectionRow = dt.Rows;
                        //foreach (DataGridViewRow dr in dgvTransactionsss.Rows)
                        //{
                        //    tr.itemTot= float.Parse(dr.Cells["Total"].Value.ToString());
                        //    tr.qty=float.Parse(dr.Cells["ItemQty"].Value.ToString());
                        //    tr.price=float.Parse(dr.Cells["ItemPrice"].Value.ToString());
                        //    tr.itemName=dr.Cells["ItemName"].Value.ToString();
                        //}
                        tr.transactionTot = Math.Round(decimal.Parse(total), 4);
                        tr.seLogo = (Bitmap)Image.FromFile("s1.png");
                        int rowsCount = dgvDeletedTrans.RowCount * 40;
                        tr.printDeleted(rowsCount + 820);
                    }
                    catch (Exception ex) { MessageBox.Show(ex.Message); }
                }
            }
        }

      
    }
}
