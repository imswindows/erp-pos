﻿namespace SaudiMarketPOS
{
    partial class OldShifts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chkIncludeCloseTime = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPrintDay = new System.Windows.Forms.Button();
            this.dtpReportDay = new System.Windows.Forms.DateTimePicker();
            this.dgvClosedShifts = new System.Windows.Forms.DataGridView();
            this.ShiftUserID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShiftOpeningBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShiftCreatedTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShiftClosedTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShiftSafeNumberID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ShiftTotalSales = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrintShiftReport = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnSearch = new System.Windows.Forms.Button();
            this.cmbCashierName = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvClosedShifts)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chkIncludeCloseTime);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.btnPrintDay);
            this.splitContainer1.Panel1.Controls.Add(this.dtpReportDay);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvClosedShifts);
            this.splitContainer1.Panel2.Controls.Add(this.btnSearch);
            this.splitContainer1.Panel2.Controls.Add(this.cmbCashierName);
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Size = new System.Drawing.Size(848, 308);
            this.splitContainer1.SplitterDistance = 58;
            this.splitContainer1.SplitterWidth = 7;
            this.splitContainer1.TabIndex = 0;
            // 
            // chkIncludeCloseTime
            // 
            this.chkIncludeCloseTime.AutoSize = true;
            this.chkIncludeCloseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkIncludeCloseTime.Location = new System.Drawing.Point(347, 15);
            this.chkIncludeCloseTime.Name = "chkIncludeCloseTime";
            this.chkIncludeCloseTime.Size = new System.Drawing.Size(207, 26);
            this.chkIncludeCloseTime.TabIndex = 44;
            this.chkIncludeCloseTime.Text = "ورديات مغلقة في نفس اليوم";
            this.chkIncludeCloseTime.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(717, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 22);
            this.label2.TabIndex = 43;
            this.label2.Text = "طباعة تقرير باليوم";
            // 
            // btnPrintDay
            // 
            this.btnPrintDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrintDay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnPrintDay.FlatAppearance.BorderSize = 0;
            this.btnPrintDay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrintDay.Font = new System.Drawing.Font("STC Bold", 12F, System.Drawing.FontStyle.Bold);
            this.btnPrintDay.ForeColor = System.Drawing.Color.White;
            this.btnPrintDay.Location = new System.Drawing.Point(218, 11);
            this.btnPrintDay.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrintDay.Name = "btnPrintDay";
            this.btnPrintDay.Size = new System.Drawing.Size(124, 35);
            this.btnPrintDay.TabIndex = 42;
            this.btnPrintDay.Text = "طباعة";
            this.btnPrintDay.UseVisualStyleBackColor = false;
            this.btnPrintDay.Click += new System.EventHandler(this.btnPrintDay_Click);
            // 
            // dtpReportDay
            // 
            this.dtpReportDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpReportDay.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpReportDay.Location = new System.Drawing.Point(561, 13);
            this.dtpReportDay.Margin = new System.Windows.Forms.Padding(4);
            this.dtpReportDay.Name = "dtpReportDay";
            this.dtpReportDay.Size = new System.Drawing.Size(118, 35);
            this.dtpReportDay.TabIndex = 0;
            // 
            // dgvClosedShifts
            // 
            this.dgvClosedShifts.AllowUserToAddRows = false;
            this.dgvClosedShifts.AllowUserToDeleteRows = false;
            this.dgvClosedShifts.AllowUserToOrderColumns = true;
            this.dgvClosedShifts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvClosedShifts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvClosedShifts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ShiftUserID,
            this.ShiftOpeningBalance,
            this.ShiftCreatedTime,
            this.ShiftClosedTime,
            this.ShiftSafeNumberID,
            this.ShiftTotalSales,
            this.PrintShiftReport});
            this.dgvClosedShifts.Location = new System.Drawing.Point(12, 50);
            this.dgvClosedShifts.Name = "dgvClosedShifts";
            this.dgvClosedShifts.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dgvClosedShifts.RowHeadersVisible = false;
            this.dgvClosedShifts.Size = new System.Drawing.Size(824, 181);
            this.dgvClosedShifts.TabIndex = 48;
            this.dgvClosedShifts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvClosedShifts_CellClick);
            // 
            // ShiftUserID
            // 
            this.ShiftUserID.DataPropertyName = "UserID";
            this.ShiftUserID.HeaderText = "UserID";
            this.ShiftUserID.Name = "ShiftUserID";
            this.ShiftUserID.ReadOnly = true;
            this.ShiftUserID.Visible = false;
            // 
            // ShiftOpeningBalance
            // 
            this.ShiftOpeningBalance.DataPropertyName = "OpenningBalance";
            this.ShiftOpeningBalance.FillWeight = 70F;
            this.ShiftOpeningBalance.HeaderText = "رصيد الافتتاح";
            this.ShiftOpeningBalance.Name = "ShiftOpeningBalance";
            // 
            // ShiftCreatedTime
            // 
            this.ShiftCreatedTime.DataPropertyName = "CreatedTime";
            this.ShiftCreatedTime.HeaderText = "تاريخ فتح الوردية";
            this.ShiftCreatedTime.Name = "ShiftCreatedTime";
            this.ShiftCreatedTime.ReadOnly = true;
            // 
            // ShiftClosedTime
            // 
            this.ShiftClosedTime.DataPropertyName = "ClosedTime";
            this.ShiftClosedTime.HeaderText = "تاريخ غلق الوردية";
            this.ShiftClosedTime.Name = "ShiftClosedTime";
            this.ShiftClosedTime.ReadOnly = true;
            // 
            // ShiftSafeNumberID
            // 
            this.ShiftSafeNumberID.DataPropertyName = "SafeNumberID";
            this.ShiftSafeNumberID.FillWeight = 60F;
            this.ShiftSafeNumberID.HeaderText = "رقم الخزنة";
            this.ShiftSafeNumberID.Name = "ShiftSafeNumberID";
            this.ShiftSafeNumberID.ReadOnly = true;
            // 
            // ShiftTotalSales
            // 
            this.ShiftTotalSales.DataPropertyName = "TotalSales";
            this.ShiftTotalSales.HeaderText = "اجمالي المبيعات";
            this.ShiftTotalSales.Name = "ShiftTotalSales";
            this.ShiftTotalSales.ReadOnly = true;
            // 
            // PrintShiftReport
            // 
            this.PrintShiftReport.FillWeight = 50F;
            this.PrintShiftReport.HeaderText = "طباعة";
            this.PrintShiftReport.Name = "PrintShiftReport";
            this.PrintShiftReport.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PrintShiftReport.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PrintShiftReport.ToolTipText = "طباعة";
            this.PrintShiftReport.UseColumnTextForButtonValue = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("STC Bold", 12F, System.Drawing.FontStyle.Bold);
            this.btnSearch.ForeColor = System.Drawing.Color.White;
            this.btnSearch.Location = new System.Drawing.Point(218, 8);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(124, 35);
            this.btnSearch.TabIndex = 49;
            this.btnSearch.Text = "بحث";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // cmbCashierName
            // 
            this.cmbCashierName.Font = new System.Drawing.Font("STC Bold", 12F, System.Drawing.FontStyle.Bold);
            this.cmbCashierName.FormattingEnabled = true;
            this.cmbCashierName.Location = new System.Drawing.Point(431, 11);
            this.cmbCashierName.Name = "cmbCashierName";
            this.cmbCashierName.Size = new System.Drawing.Size(248, 30);
            this.cmbCashierName.TabIndex = 45;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(685, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 22);
            this.label3.TabIndex = 44;
            this.label3.Text = "طباعة تقرير بالمستخدم";
            // 
            // OldShifts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 308);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("STC Bold", 12F, System.Drawing.FontStyle.Bold);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "OldShifts";
            this.Text = "OldShifts";
            this.Load += new System.EventHandler(this.OldShifts_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvClosedShifts)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DateTimePicker dtpReportDay;
        private System.Windows.Forms.Button btnPrintDay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbCashierName;
        private System.Windows.Forms.DataGridView dgvClosedShifts;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox chkIncludeCloseTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShiftUserID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShiftOpeningBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShiftCreatedTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShiftClosedTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShiftSafeNumberID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ShiftTotalSales;
        private System.Windows.Forms.DataGridViewButtonColumn PrintShiftReport;
    }
}