﻿namespace SaudiMarketPOS
{
    partial class TransactionScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pbXreca = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.pbImsLogo = new System.Windows.Forms.PictureBox();
            this.txtTransactionID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSafeNumber = new System.Windows.Forms.TextBox();
            this.lblCashir = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbCustomerName = new System.Windows.Forms.ComboBox();
            this.txtCustomerID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.ItemID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemBarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemQty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chkCredit = new System.Windows.Forms.CheckBox();
            this.txtCashDiscount = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDiscountPercent = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtInvTotal = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtChange = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDueAmount = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPaid = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label16 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.chkDeliveryEmp = new System.Windows.Forms.CheckBox();
            this.cmbDeliveryEmp = new System.Windows.Forms.ComboBox();
            this.txtItemBarcode = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.dgvTransactionsss = new System.Windows.Forms.DataGridView();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label14 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.txtEnableDiscount = new System.Windows.Forms.Button();
            this.btnCreditCard = new System.Windows.Forms.Button();
            this.btnDeliveryStatus = new System.Windows.Forms.Button();
            this.btnReturns = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.txtMachineName = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbXreca)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTransactionsss)).BeginInit();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel1.Controls.Add(this.pbXreca);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.pbImsLogo);
            this.panel1.Controls.Add(this.txtTransactionID);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(197, 3);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(736, 58);
            this.panel1.TabIndex = 7;
            // 
            // pbXreca
            // 
            this.pbXreca.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pbXreca.ErrorImage = null;
            this.pbXreca.Location = new System.Drawing.Point(603, 14);
            this.pbXreca.Name = "pbXreca";
            this.pbXreca.Size = new System.Drawing.Size(120, 27);
            this.pbXreca.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbXreca.TabIndex = 16;
            this.pbXreca.TabStop = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label3.Location = new System.Drawing.Point(191, 24);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "رقم البون";
            this.label3.Visible = false;
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(236, 17);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(10, 29);
            this.label22.TabIndex = 52;
            this.label22.Text = "|";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label22.Visible = false;
            // 
            // pbImsLogo
            // 
            this.pbImsLogo.ErrorImage = null;
            this.pbImsLogo.Location = new System.Drawing.Point(15, 9);
            this.pbImsLogo.Name = "pbImsLogo";
            this.pbImsLogo.Size = new System.Drawing.Size(90, 37);
            this.pbImsLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImsLogo.TabIndex = 14;
            this.pbImsLogo.TabStop = false;
            // 
            // txtTransactionID
            // 
            this.txtTransactionID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTransactionID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.txtTransactionID.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTransactionID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.txtTransactionID.ForeColor = System.Drawing.Color.White;
            this.txtTransactionID.Location = new System.Drawing.Point(143, 24);
            this.txtTransactionID.Margin = new System.Windows.Forms.Padding(2);
            this.txtTransactionID.Name = "txtTransactionID";
            this.txtTransactionID.ReadOnly = true;
            this.txtTransactionID.Size = new System.Drawing.Size(39, 13);
            this.txtTransactionID.TabIndex = 7;
            this.txtTransactionID.Text = "1";
            this.txtTransactionID.Visible = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(265, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(201, 29);
            this.label2.TabIndex = 13;
            this.label2.Text = "شاشة الحركة الرئيسية";
            // 
            // txtSafeNumber
            // 
            this.txtSafeNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSafeNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.txtSafeNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSafeNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.txtSafeNumber.ForeColor = System.Drawing.Color.White;
            this.txtSafeNumber.Location = new System.Drawing.Point(612, 4);
            this.txtSafeNumber.Margin = new System.Windows.Forms.Padding(2);
            this.txtSafeNumber.Name = "txtSafeNumber";
            this.txtSafeNumber.ReadOnly = true;
            this.txtSafeNumber.Size = new System.Drawing.Size(48, 13);
            this.txtSafeNumber.TabIndex = 9;
            this.txtSafeNumber.TextChanged += new System.EventHandler(this.txtSafeNumber_TextChanged);
            // 
            // lblCashir
            // 
            this.lblCashir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCashir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lblCashir.ForeColor = System.Drawing.Color.White;
            this.lblCashir.Location = new System.Drawing.Point(437, 4);
            this.lblCashir.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCashir.Name = "lblCashir";
            this.lblCashir.Size = new System.Drawing.Size(104, 15);
            this.lblCashir.TabIndex = 5;
            this.lblCashir.Text = "lblCashier";
            this.lblCashir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label5.Location = new System.Drawing.Point(664, 4);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "رقم الخزينة";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label1.Location = new System.Drawing.Point(545, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "الكاشير";
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.BackColor = System.Drawing.Color.Transparent;
            this.lblDateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.lblDateTime.ForeColor = System.Drawing.Color.White;
            this.lblDateTime.Location = new System.Drawing.Point(5, 4);
            this.lblDateTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblDateTime.Size = new System.Drawing.Size(41, 13);
            this.lblDateTime.TabIndex = 2;
            this.lblDateTime.Text = "label3";
            this.lblDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label4.Location = new System.Drawing.Point(176, 4);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "التوقيت";
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.cmbCustomerName);
            this.panel3.Controls.Add(this.txtCustomerID);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Location = new System.Drawing.Point(401, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(335, 45);
            this.panel3.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(269, 13);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "رقم ح العميل";
            // 
            // cmbCustomerName
            // 
            this.cmbCustomerName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbCustomerName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCustomerName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCustomerName.DisplayMember = "FirstName";
            this.cmbCustomerName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.cmbCustomerName.FormattingEnabled = true;
            this.cmbCustomerName.Location = new System.Drawing.Point(2, 10);
            this.cmbCustomerName.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCustomerName.Name = "cmbCustomerName";
            this.cmbCustomerName.Size = new System.Drawing.Size(118, 21);
            this.cmbCustomerName.TabIndex = 8;
            this.cmbCustomerName.ValueMember = "ID";
            this.cmbCustomerName.SelectedIndexChanged += new System.EventHandler(this.cmbCustomerName_SelectedIndexChanged);
            this.cmbCustomerName.Leave += new System.EventHandler(this.cmbCustomerName_Leave);
            // 
            // txtCustomerID
            // 
            this.txtCustomerID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCustomerID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.txtCustomerID.Location = new System.Drawing.Point(183, 10);
            this.txtCustomerID.Margin = new System.Windows.Forms.Padding(2);
            this.txtCustomerID.Name = "txtCustomerID";
            this.txtCustomerID.Size = new System.Drawing.Size(86, 20);
            this.txtCustomerID.TabIndex = 7;
            this.txtCustomerID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCustomerID.TextChanged += new System.EventHandler(this.txtCustomerID_TextChanged);
            this.txtCustomerID.Enter += new System.EventHandler(this.txtCustomerID_Enter);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(121, 13);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "اسم العميل";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel4.Controls.Add(this.label24);
            this.panel4.Controls.Add(this.txtMachineName);
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.label15);
            this.panel4.Controls.Add(this.label19);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.lblDateTime);
            this.panel4.Controls.Add(this.txtSafeNumber);
            this.panel4.Controls.Add(this.label1);
            this.panel4.Controls.Add(this.lblCashir);
            this.panel4.Location = new System.Drawing.Point(197, 623);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(736, 24);
            this.panel4.TabIndex = 1;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::SaudiMarketPOS.Properties.Resources.time;
            this.pictureBox3.Location = new System.Drawing.Point(213, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::SaudiMarketPOS.Properties.Resources.Cahsier;
            this.pictureBox2.Location = new System.Drawing.Point(582, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(16, 16);
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::SaudiMarketPOS.Properties.Resources.cash_box;
            this.pictureBox1.Location = new System.Drawing.Point(717, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(229, -3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(10, 29);
            this.label15.TabIndex = 51;
            this.label15.Text = "|";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Trebuchet MS", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(597, -3);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(10, 29);
            this.label19.TabIndex = 50;
            this.label19.Text = "|";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ItemID
            // 
            this.ItemID.HeaderText = "ItemID";
            this.ItemID.Name = "ItemID";
            this.ItemID.ReadOnly = true;
            this.ItemID.Visible = false;
            // 
            // ItemBarcode
            // 
            this.ItemBarcode.FillWeight = 132.5479F;
            this.ItemBarcode.HeaderText = "رقم الصنف";
            this.ItemBarcode.Name = "ItemBarcode";
            this.ItemBarcode.ReadOnly = true;
            // 
            // ItemName
            // 
            this.ItemName.FillWeight = 241.91F;
            this.ItemName.HeaderText = "اسم الصنف";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // ItemUnit
            // 
            this.ItemUnit.FillWeight = 51.12155F;
            this.ItemUnit.HeaderText = "الوحدة";
            this.ItemUnit.Name = "ItemUnit";
            this.ItemUnit.ReadOnly = true;
            // 
            // ItemPrice
            // 
            this.ItemPrice.FillWeight = 54.95871F;
            this.ItemPrice.HeaderText = "السعر";
            this.ItemPrice.Name = "ItemPrice";
            this.ItemPrice.ReadOnly = true;
            // 
            // ItemQty
            // 
            this.ItemQty.FillWeight = 58.28725F;
            this.ItemQty.HeaderText = "الكمية";
            this.ItemQty.Name = "ItemQty";
            this.ItemQty.ReadOnly = true;
            // 
            // chkCredit
            // 
            this.chkCredit.AutoSize = true;
            this.chkCredit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.chkCredit.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.chkCredit.Location = new System.Drawing.Point(45, 336);
            this.chkCredit.Margin = new System.Windows.Forms.Padding(2);
            this.chkCredit.Name = "chkCredit";
            this.chkCredit.Size = new System.Drawing.Size(126, 24);
            this.chkCredit.TabIndex = 39;
            this.chkCredit.Text = "اجـــــــــــــــل";
            this.chkCredit.UseVisualStyleBackColor = true;
            this.chkCredit.Visible = false;
            this.chkCredit.CheckedChanged += new System.EventHandler(this.chkCredit_CheckedChanged);
            this.chkCredit.Enter += new System.EventHandler(this.chkCredit_Enter);
            this.chkCredit.Leave += new System.EventHandler(this.chkCredit_Leave);
            // 
            // txtCashDiscount
            // 
            this.txtCashDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCashDiscount.Enabled = false;
            this.txtCashDiscount.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.txtCashDiscount.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCashDiscount.Location = new System.Drawing.Point(11, 25);
            this.txtCashDiscount.Margin = new System.Windows.Forms.Padding(2);
            this.txtCashDiscount.Name = "txtCashDiscount";
            this.txtCashDiscount.Size = new System.Drawing.Size(118, 35);
            this.txtCashDiscount.TabIndex = 38;
            this.txtCashDiscount.Text = "0,00";
            this.txtCashDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCashDiscount.Click += new System.EventHandler(this.selectTxt);
            this.txtCashDiscount.TextChanged += new System.EventHandler(this.txtCashDiscount_TextChanged);
            this.txtCashDiscount.Enter += new System.EventHandler(this.selectTxt);
            this.txtCashDiscount.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCashDiscount_KeyDown);
            this.txtCashDiscount.Leave += new System.EventHandler(this.txtCashDiscount_Leave);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Silver;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label10.Location = new System.Drawing.Point(51, 1);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(37, 20);
            this.label10.TabIndex = 8;
            this.label10.Text = "نقدي";
            // 
            // txtDiscountPercent
            // 
            this.txtDiscountPercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDiscountPercent.Enabled = false;
            this.txtDiscountPercent.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold);
            this.txtDiscountPercent.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtDiscountPercent.Location = new System.Drawing.Point(133, 25);
            this.txtDiscountPercent.Margin = new System.Windows.Forms.Padding(2);
            this.txtDiscountPercent.Name = "txtDiscountPercent";
            this.txtDiscountPercent.Size = new System.Drawing.Size(118, 35);
            this.txtDiscountPercent.TabIndex = 37;
            this.txtDiscountPercent.Text = "0,00";
            this.txtDiscountPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDiscountPercent.Click += new System.EventHandler(this.selectTxt);
            this.txtDiscountPercent.TextChanged += new System.EventHandler(this.txtDiscountPercent_TextChanged);
            this.txtDiscountPercent.Enter += new System.EventHandler(this.selectTxt);
            this.txtDiscountPercent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDiscountPercent_KeyDown);
            this.txtDiscountPercent.Leave += new System.EventHandler(this.txtDiscountPercent_Leave);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Silver;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label8.Location = new System.Drawing.Point(185, 1);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 20);
            this.label8.TabIndex = 6;
            this.label8.Text = "%";
            // 
            // txtInvTotal
            // 
            this.txtInvTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtInvTotal.Location = new System.Drawing.Point(15, 32);
            this.txtInvTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtInvTotal.Name = "txtInvTotal";
            this.txtInvTotal.ReadOnly = true;
            this.txtInvTotal.Size = new System.Drawing.Size(155, 26);
            this.txtInvTotal.TabIndex = 0;
            this.txtInvTotal.TabStop = false;
            this.txtInvTotal.Text = "0,00";
            this.txtInvTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtInvTotal.Click += new System.EventHandler(this.selectTxt);
            this.txtInvTotal.Enter += new System.EventHandler(this.selectTxt);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label9.Location = new System.Drawing.Point(63, 5);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 20);
            this.label9.TabIndex = 4;
            this.label9.Text = "الاجمالي";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.chkCredit);
            this.panel5.Controls.Add(this.label9);
            this.panel5.Controls.Add(this.txtChange);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Controls.Add(this.txtDueAmount);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.txtInvTotal);
            this.panel5.Controls.Add(this.txtPaid);
            this.panel5.Location = new System.Drawing.Point(4, 3);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(189, 372);
            this.panel5.TabIndex = 2;
            // 
            // txtChange
            // 
            this.txtChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtChange.Location = new System.Drawing.Point(15, 297);
            this.txtChange.Margin = new System.Windows.Forms.Padding(2);
            this.txtChange.Name = "txtChange";
            this.txtChange.ReadOnly = true;
            this.txtChange.Size = new System.Drawing.Size(155, 26);
            this.txtChange.TabIndex = 2;
            this.txtChange.TabStop = false;
            this.txtChange.Text = "0,00";
            this.txtChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtChange.Click += new System.EventHandler(this.selectTxt);
            this.txtChange.Enter += new System.EventHandler(this.selectTxt);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label13.Location = new System.Drawing.Point(4, 87);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(157, 31);
            this.label13.TabIndex = 4;
            this.label13.Text = "المطلوب سداده";
            // 
            // txtDueAmount
            // 
            this.txtDueAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold);
            this.txtDueAmount.Location = new System.Drawing.Point(15, 126);
            this.txtDueAmount.Margin = new System.Windows.Forms.Padding(2);
            this.txtDueAmount.Name = "txtDueAmount";
            this.txtDueAmount.ReadOnly = true;
            this.txtDueAmount.Size = new System.Drawing.Size(155, 38);
            this.txtDueAmount.TabIndex = 0;
            this.txtDueAmount.TabStop = false;
            this.txtDueAmount.Text = "0,00";
            this.txtDueAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDueAmount.Click += new System.EventHandler(this.selectTxt);
            this.txtDueAmount.TextChanged += new System.EventHandler(this.txtDueAmount_TextChanged);
            this.txtDueAmount.Enter += new System.EventHandler(this.selectTxt);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label11.Location = new System.Drawing.Point(61, 270);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(50, 20);
            this.label11.TabIndex = 8;
            this.label11.Text = "المتبقى";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label12.Location = new System.Drawing.Point(41, 204);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(96, 20);
            this.label12.TabIndex = 6;
            this.label12.Text = "المدفوع  (F5)";
            // 
            // txtPaid
            // 
            this.txtPaid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.txtPaid.Location = new System.Drawing.Point(15, 228);
            this.txtPaid.Margin = new System.Windows.Forms.Padding(2);
            this.txtPaid.Name = "txtPaid";
            this.txtPaid.Size = new System.Drawing.Size(155, 26);
            this.txtPaid.TabIndex = 40;
            this.txtPaid.Text = "0,00";
            this.txtPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPaid.Click += new System.EventHandler(this.selectTxt);
            this.txtPaid.TextChanged += new System.EventHandler(this.txtPaid_TextChanged);
            this.txtPaid.Enter += new System.EventHandler(this.selectTxt);
            this.txtPaid.Leave += new System.EventHandler(this.txtPaid_Leave);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label16.Location = new System.Drawing.Point(809, 112);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(144, 13);
            this.label16.TabIndex = 17;
            this.label16.Text = "[ F7 ]  عرض قائمة العملاء";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.chkDeliveryEmp);
            this.panel2.Controls.Add(this.cmbDeliveryEmp);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(397, 45);
            this.panel2.TabIndex = 19;
            this.panel2.Visible = false;
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(2, 11);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 17);
            this.label18.TabIndex = 4;
            this.label18.Text = "[ F4 ]";
            // 
            // chkDeliveryEmp
            // 
            this.chkDeliveryEmp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkDeliveryEmp.AutoSize = true;
            this.chkDeliveryEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.chkDeliveryEmp.Location = new System.Drawing.Point(289, 12);
            this.chkDeliveryEmp.Margin = new System.Windows.Forms.Padding(2);
            this.chkDeliveryEmp.Name = "chkDeliveryEmp";
            this.chkDeliveryEmp.Size = new System.Drawing.Size(106, 17);
            this.chkDeliveryEmp.TabIndex = 0;
            this.chkDeliveryEmp.Text = "مندوب التوصيل";
            this.chkDeliveryEmp.CheckedChanged += new System.EventHandler(this.chkDeliveryEmp_CheckedChanged);
            this.chkDeliveryEmp.Enter += new System.EventHandler(this.chkDeliveryEmp_Enter);
            this.chkDeliveryEmp.Leave += new System.EventHandler(this.chkDeliveryEmp_Leave);
            // 
            // cmbDeliveryEmp
            // 
            this.cmbDeliveryEmp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbDeliveryEmp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbDeliveryEmp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbDeliveryEmp.Enabled = false;
            this.cmbDeliveryEmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.cmbDeliveryEmp.Location = new System.Drawing.Point(45, 10);
            this.cmbDeliveryEmp.Margin = new System.Windows.Forms.Padding(2);
            this.cmbDeliveryEmp.Name = "cmbDeliveryEmp";
            this.cmbDeliveryEmp.Size = new System.Drawing.Size(257, 21);
            this.cmbDeliveryEmp.TabIndex = 1;
            this.cmbDeliveryEmp.Leave += new System.EventHandler(this.cmbDeliveryEmp_Leave);
            // 
            // txtItemBarcode
            // 
            this.txtItemBarcode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.txtItemBarcode.BackColor = System.Drawing.Color.DimGray;
            this.txtItemBarcode.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtItemBarcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.txtItemBarcode.ForeColor = System.Drawing.Color.White;
            this.txtItemBarcode.Location = new System.Drawing.Point(744, 594);
            this.txtItemBarcode.Margin = new System.Windows.Forms.Padding(2);
            this.txtItemBarcode.Name = "txtItemBarcode";
            this.txtItemBarcode.Size = new System.Drawing.Size(189, 17);
            this.txtItemBarcode.TabIndex = 35;
            this.txtItemBarcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtItemBarcode.TextChanged += new System.EventHandler(this.txtItemBarcode_TextChanged);
            this.txtItemBarcode.Enter += new System.EventHandler(this.selectTxt);
            this.txtItemBarcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtItemBarcode_KeyDown);
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label17.Location = new System.Drawing.Point(476, 808);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(294, 15);
            this.label17.TabIndex = 36;
            this.label17.Text = "أدخل رقم الصنف أو  [ F6 ] لعرض قائمة الاصناف و البحث";
            // 
            // dgvTransactionsss
            // 
            this.dgvTransactionsss.AllowUserToAddRows = false;
            this.dgvTransactionsss.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvTransactionsss.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvTransactionsss.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTransactionsss.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvTransactionsss.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTransactionsss.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemID,
            this.ItemBarcode,
            this.ItemName,
            this.ItemUnit,
            this.ItemPrice,
            this.ItemQty,
            this.Total});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvTransactionsss.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvTransactionsss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTransactionsss.EnableHeadersVisualStyles = false;
            this.dgvTransactionsss.Location = new System.Drawing.Point(0, 0);
            this.dgvTransactionsss.Margin = new System.Windows.Forms.Padding(2);
            this.dgvTransactionsss.MultiSelect = false;
            this.dgvTransactionsss.Name = "dgvTransactionsss";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Yellow;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvTransactionsss.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvTransactionsss.RowHeadersVisible = false;
            this.dgvTransactionsss.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTransactionsss.Size = new System.Drawing.Size(736, 460);
            this.dgvTransactionsss.TabIndex = 0;
            this.dgvTransactionsss.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTransactionsss_CellEndEdit);
            this.dgvTransactionsss.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvTransactionsss_CellValidating);
            this.dgvTransactionsss.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTransactionsss_RowEnter);
            this.dgvTransactionsss.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dgvTransactionsss_RowsAdded);
            this.dgvTransactionsss.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvTransactionsss_UserDeletedRow);
            this.dgvTransactionsss.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvTransactionsss_UserDeletingRow);
            this.dgvTransactionsss.Enter += new System.EventHandler(this.dgvTransactionsss_Enter);
            this.dgvTransactionsss.Leave += new System.EventHandler(this.dgvTransactionsss_Leave);
            // 
            // Total
            // 
            this.Total.FillWeight = 72.02553F;
            this.Total.HeaderText = "الاجمالي";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label14.Location = new System.Drawing.Point(209, 808);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(107, 15);
            this.label14.TabIndex = 37;
            this.label14.Text = "[ F9 ] لتعديل الكمية";
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.Controls.Add(this.panel3);
            this.panel6.Controls.Add(this.panel2);
            this.panel6.Location = new System.Drawing.Point(197, 65);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(736, 45);
            this.panel6.TabIndex = 46;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.BackColor = System.Drawing.Color.Silver;
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.txtDiscountPercent);
            this.panel7.Controls.Add(this.txtCashDiscount);
            this.panel7.Location = new System.Drawing.Point(938, 370);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(259, 87);
            this.panel7.TabIndex = 48;
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.dgvTransactionsss);
            this.panel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel8.Location = new System.Drawing.Point(197, 129);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(736, 460);
            this.panel8.TabIndex = 49;
            // 
            // txtEnableDiscount
            // 
            this.txtEnableDiscount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEnableDiscount.BackColor = System.Drawing.Color.Violet;
            this.txtEnableDiscount.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.sales;
            this.txtEnableDiscount.FlatAppearance.BorderSize = 0;
            this.txtEnableDiscount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtEnableDiscount.Location = new System.Drawing.Point(938, 277);
            this.txtEnableDiscount.Name = "txtEnableDiscount";
            this.txtEnableDiscount.Size = new System.Drawing.Size(259, 87);
            this.txtEnableDiscount.TabIndex = 36;
            this.txtEnableDiscount.UseVisualStyleBackColor = false;
            this.txtEnableDiscount.Click += new System.EventHandler(this.txtEnableDiscount_Click);
            this.txtEnableDiscount.Enter += new System.EventHandler(this.txtEnableDiscount_Enter);
            this.txtEnableDiscount.Leave += new System.EventHandler(this.txtEnableDiscount_Leave);
            this.txtEnableDiscount.MouseEnter += new System.EventHandler(this.txtEnableDiscount_MouseEnter);
            this.txtEnableDiscount.MouseLeave += new System.EventHandler(this.txtEnableDiscount_MouseLeave);
            // 
            // btnCreditCard
            // 
            this.btnCreditCard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCreditCard.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.btnCreditCard.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.Credit_card;
            this.btnCreditCard.Enabled = false;
            this.btnCreditCard.FlatAppearance.BorderSize = 0;
            this.btnCreditCard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreditCard.Location = new System.Drawing.Point(940, 462);
            this.btnCreditCard.Margin = new System.Windows.Forms.Padding(2);
            this.btnCreditCard.Name = "btnCreditCard";
            this.btnCreditCard.Size = new System.Drawing.Size(259, 87);
            this.btnCreditCard.TabIndex = 43;
            this.btnCreditCard.UseVisualStyleBackColor = false;
            this.btnCreditCard.Visible = false;
            this.btnCreditCard.Click += new System.EventHandler(this.btnCreditCard_Click);
            this.btnCreditCard.MouseEnter += new System.EventHandler(this.btnCreditCard_MouseEnter);
            this.btnCreditCard.MouseLeave += new System.EventHandler(this.btnCreditCard_MouseLeave);
            // 
            // btnDeliveryStatus
            // 
            this.btnDeliveryStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeliveryStatus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.btnDeliveryStatus.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.Delivery;
            this.btnDeliveryStatus.FlatAppearance.BorderSize = 0;
            this.btnDeliveryStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeliveryStatus.Location = new System.Drawing.Point(938, 94);
            this.btnDeliveryStatus.Margin = new System.Windows.Forms.Padding(2);
            this.btnDeliveryStatus.Name = "btnDeliveryStatus";
            this.btnDeliveryStatus.Size = new System.Drawing.Size(259, 87);
            this.btnDeliveryStatus.TabIndex = 42;
            this.btnDeliveryStatus.UseVisualStyleBackColor = false;
            this.btnDeliveryStatus.Click += new System.EventHandler(this.eliveryStatus_Click);
            this.btnDeliveryStatus.MouseEnter += new System.EventHandler(this.btnDeliveryStatus_MouseEnter);
            this.btnDeliveryStatus.MouseLeave += new System.EventHandler(this.btnDeliveryStatus_MouseLeave);
            // 
            // btnReturns
            // 
            this.btnReturns.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReturns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.btnReturns.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.Returns;
            this.btnReturns.FlatAppearance.BorderSize = 0;
            this.btnReturns.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReturns.Location = new System.Drawing.Point(938, 185);
            this.btnReturns.Margin = new System.Windows.Forms.Padding(2);
            this.btnReturns.Name = "btnReturns";
            this.btnReturns.Size = new System.Drawing.Size(259, 87);
            this.btnReturns.TabIndex = 41;
            this.btnReturns.UseVisualStyleBackColor = false;
            this.btnReturns.Click += new System.EventHandler(this.btnReturns_Click);
            this.btnReturns.MouseEnter += new System.EventHandler(this.btnReturns_MouseEnter);
            this.btnReturns.MouseLeave += new System.EventHandler(this.btnReturns_MouseLeave);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.button1.BackgroundImage = global::SaudiMarketPOS.Properties.Resources._out;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(939, 560);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(259, 87);
            this.button1.TabIndex = 45;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(212)))), ((int)(((byte)(82)))));
            this.btnPrint.BackgroundImage = global::SaudiMarketPOS.Properties.Resources.print_green;
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Location = new System.Drawing.Point(938, 3);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(259, 87);
            this.btnPrint.TabIndex = 44;
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            this.btnPrint.MouseEnter += new System.EventHandler(this.btnPrint_MouseEnter);
            this.btnPrint.MouseLeave += new System.EventHandler(this.btnPrint_MouseLeave);
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(197, 598);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(97, 13);
            this.label20.TabIndex = 51;
            this.label20.Text = "F9 لتعديل الكمية";
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label21.Location = new System.Drawing.Point(515, 598);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(286, 13);
            this.label21.TabIndex = 50;
            this.label21.Text = "أدخل رقم الصنف أو F6 لعرض قائمة الاصناف و البحث";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label23.Location = new System.Drawing.Point(197, 112);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(183, 13);
            this.label23.TabIndex = 55;
            this.label23.Text = "[ Alt + E ]  للدفع عن طريق الفيزا";
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label24.Location = new System.Drawing.Point(360, 6);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(69, 13);
            this.label24.TabIndex = 52;
            this.label24.Text = "اسم الخزينة";
            this.label24.Click += new System.EventHandler(this.label24_Click);
            // 
            // txtMachineName
            // 
            this.txtMachineName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMachineName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.txtMachineName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMachineName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold);
            this.txtMachineName.ForeColor = System.Drawing.Color.White;
            this.txtMachineName.Location = new System.Drawing.Point(308, 6);
            this.txtMachineName.Margin = new System.Windows.Forms.Padding(2);
            this.txtMachineName.Name = "txtMachineName";
            this.txtMachineName.ReadOnly = true;
            this.txtMachineName.Size = new System.Drawing.Size(48, 13);
            this.txtMachineName.TabIndex = 53;
            this.txtMachineName.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // TransactionScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 652);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.btnCreditCard);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.txtEnableDiscount);
            this.Controls.Add(this.btnDeliveryStatus);
            this.Controls.Add(this.btnReturns);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtItemBarcode);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel8);
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "TransactionScreen";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "شاشة الحركة";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TransactionScreen_FormClosing);
            this.Load += new System.EventHandler(this.TransactionScreen_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ctl_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TransactionScreen_KeyPress);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbXreca)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTransactionsss)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCashir;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.CheckBox chkCredit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtInvTotal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtChange;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPaid;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDueAmount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox cmbCustomerName;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox txtCustomerID;
        private System.Windows.Forms.TextBox txtSafeNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtTransactionID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtItemBarcode;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvTransactionsss;
        private System.Windows.Forms.CheckBox chkDeliveryEmp;
        private System.Windows.Forms.ComboBox cmbDeliveryEmp;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnReturns;
        private System.Windows.Forms.Button btnDeliveryStatus;
        private System.Windows.Forms.Button btnCreditCard;
        public System.Windows.Forms.TextBox txtCashDiscount;
        public System.Windows.Forms.TextBox txtDiscountPercent;
        private System.Windows.Forms.Button txtEnableDiscount;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.PictureBox pbImsLogo;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemQty;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemBarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pbXreca;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtMachineName;
    }
}