﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SaudiMarketPOS.Properties;

namespace SaudiMarketPOS
{
    public partial class CustomerAdd : Form
    {
        SqlConnection con = new SqlConnection();
        public CustomerAdd()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (txtFirstName.Text.Trim() == "" || txtLastName.Text.Trim() == "" || txtPhone.Text.Trim() == "" || txtAddress1.Text == "")
            {
                MessageBox.Show("يجب استكمال جميع البيانات الاساسية","خطأ",MessageBoxButtons.OK,MessageBoxIcon.Error,MessageBoxDefaultButton.Button1,MessageBoxOptions.RightAlign );
            }
            else
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "InsertCustomer";

                cmd.Parameters.AddWithValue("@Titel", cmbTitle.Text.Trim());
                cmd.Parameters.AddWithValue("@FirstName", txtFirstName.Text.Trim());
                cmd.Parameters.AddWithValue("@LastName", txtLastName.Text.Trim());
                cmd.Parameters.AddWithValue("@Company", txtCompany.Text.Trim());
                cmd.Parameters.AddWithValue("@EmailAddress", txtEmail.Text.Trim());
                cmd.Parameters.AddWithValue("@PhoneNumber", txtPhone.Text.Trim());
                cmd.Parameters.AddWithValue("@FaxNumber", txtFax.Text.Trim());
                cmd.Parameters.AddWithValue("@Address1", txtAddress1.Text.Trim());
                cmd.Parameters.AddWithValue("@Address2", txtAddress2.Text.Trim());
                cmd.Parameters.AddWithValue("@City", txtCity.Text.Trim());
                cmd.Parameters.AddWithValue("@State", txtState.Text.Trim());
                cmd.Parameters.AddWithValue("@ZipCode", txtPostalCode.Text.Trim());
                cmd.Parameters.AddWithValue("@Country", txtCountry.Text.Trim());
                cmd.Parameters.AddWithValue("@PriceLevel", txtPriceLevel.Text.Trim());
                cmd.Parameters.AddWithValue("@Discount", txtDiscount.Text.Trim());
                cmd.Parameters.AddWithValue("@CreditLimit", txtCreditLimit.Text.Trim());
                cmd.Parameters.AddWithValue("@AccountBalance", txtAccountBalance.Text.Trim());
                cmd.Parameters.AddWithValue("@TotalSales", txtTatalSales.Text.Trim());
                cmd.Parameters.AddWithValue("@AllowCredit", true);
                cmd.Parameters.AddWithValue("@PhoneNumber2", txtPhone2.Text.Trim());

                cmd.Connection = con;
                if (con.State == ConnectionState.Closed) con.Open();
                cmd.ExecuteNonQuery();
                if (con.State == ConnectionState.Open) con.Close();
                clearData();
                MessageBox.Show("تم التسجيل بنجاح", "تأكيد", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CustomerAdd_Load(object sender, EventArgs e)
        {
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            con.ConnectionString = Program.conString;
        }
        private void clearData()
        {
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtPhone.Text = "";
            txtPhone2.Text = "";
            txtAddress1.Text = "";
        }
        private void txtCompany_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtPhone2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
