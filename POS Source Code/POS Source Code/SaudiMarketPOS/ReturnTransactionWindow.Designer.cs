﻿namespace SaudiMarketPOS
{
    partial class ReturnTransactionWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel6 = new System.Windows.Forms.Panel();
            this.pbImsLogo = new System.Windows.Forms.PictureBox();
            this.txtMainSafe = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblCashir = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblDateTime = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtDate = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTransactionID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.txtReturnTotal = new System.Windows.Forms.TextBox();
            this.txtChange = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPaid = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDueAmount = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtCashDiscount = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDiscountPercent = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtInvTotal = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.dgvInvoiceItems = new System.Windows.Forms.DataGridView();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel4 = new System.Windows.Forms.Panel();
            this.txtCustmoerName = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtSafeNumber = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCashierName = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItems)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel6.Controls.Add(this.pbImsLogo);
            this.panel6.Controls.Add(this.txtMainSafe);
            this.panel6.Controls.Add(this.label6);
            this.panel6.Controls.Add(this.lblCashir);
            this.panel6.Controls.Add(this.label4);
            this.panel6.Controls.Add(this.label15);
            this.panel6.Controls.Add(this.lblDateTime);
            this.panel6.Controls.Add(this.label17);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(784, 55);
            this.panel6.TabIndex = 27;
            // 
            // pbImsLogo
            // 
            this.pbImsLogo.ErrorImage = null;
            this.pbImsLogo.Location = new System.Drawing.Point(9, 9);
            this.pbImsLogo.Name = "pbImsLogo";
            this.pbImsLogo.Size = new System.Drawing.Size(90, 37);
            this.pbImsLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImsLogo.TabIndex = 16;
            this.pbImsLogo.TabStop = false;
            // 
            // txtMainSafe
            // 
            this.txtMainSafe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMainSafe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.txtMainSafe.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMainSafe.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtMainSafe.ForeColor = System.Drawing.Color.White;
            this.txtMainSafe.Location = new System.Drawing.Point(639, 8);
            this.txtMainSafe.Margin = new System.Windows.Forms.Padding(2);
            this.txtMainSafe.Name = "txtMainSafe";
            this.txtMainSafe.ReadOnly = true;
            this.txtMainSafe.Size = new System.Drawing.Size(70, 19);
            this.txtMainSafe.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label6.Location = new System.Drawing.Point(713, 8);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 15);
            this.label6.TabIndex = 14;
            this.label6.Text = "رقم الخزينة";
            // 
            // lblCashir
            // 
            this.lblCashir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCashir.AutoSize = true;
            this.lblCashir.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.lblCashir.ForeColor = System.Drawing.Color.White;
            this.lblCashir.Location = new System.Drawing.Point(595, 33);
            this.lblCashir.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCashir.Name = "lblCashir";
            this.lblCashir.Size = new System.Drawing.Size(35, 15);
            this.lblCashir.TabIndex = 5;
            this.lblCashir.Text = "label3";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("STC Bold", 18F, System.Drawing.FontStyle.Bold);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(338, 9);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(168, 32);
            this.label4.TabIndex = 13;
            this.label4.Text = "شاشة المرتجعات";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label15.Location = new System.Drawing.Point(729, 33);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 15);
            this.label15.TabIndex = 4;
            this.label15.Text = "الكاشير";
            // 
            // lblDateTime
            // 
            this.lblDateTime.AutoSize = true;
            this.lblDateTime.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.lblDateTime.ForeColor = System.Drawing.Color.White;
            this.lblDateTime.Location = new System.Drawing.Point(113, 33);
            this.lblDateTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDateTime.Name = "lblDateTime";
            this.lblDateTime.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDateTime.Size = new System.Drawing.Size(35, 15);
            this.lblDateTime.TabIndex = 2;
            this.lblDateTime.Text = "label3";
            this.lblDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label17.Location = new System.Drawing.Point(256, 33);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 15);
            this.label17.TabIndex = 3;
            this.label17.Text = "التاريخ";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.txtDate);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtTransactionID);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 55);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(784, 39);
            this.panel2.TabIndex = 24;
            // 
            // txtDate
            // 
            this.txtDate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtDate.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtDate.Location = new System.Drawing.Point(219, 8);
            this.txtDate.Margin = new System.Windows.Forms.Padding(2);
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = true;
            this.txtDate.Size = new System.Drawing.Size(161, 26);
            this.txtDate.TabIndex = 12;
            this.txtDate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label2.Location = new System.Drawing.Point(388, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 11;
            this.label2.Text = "تاريخ البون";
            // 
            // txtTransactionID
            // 
            this.txtTransactionID.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtTransactionID.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtTransactionID.Location = new System.Drawing.Point(460, 8);
            this.txtTransactionID.Margin = new System.Windows.Forms.Padding(2);
            this.txtTransactionID.Name = "txtTransactionID";
            this.txtTransactionID.Size = new System.Drawing.Size(101, 26);
            this.txtTransactionID.TabIndex = 10;
            this.txtTransactionID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtTransactionID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTransactionID_KeyDown);
            this.txtTransactionID.Leave += new System.EventHandler(this.txtTransactionID_Leave);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label3.Location = new System.Drawing.Point(565, 11);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 15);
            this.label3.TabIndex = 9;
            this.label3.Text = "ادخل رقم البون";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(7, 514);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(228, 43);
            this.btnClose.TabIndex = 32;
            this.btnClose.Text = "الغاء الأمر";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnPrint.FlatAppearance.BorderSize = 0;
            this.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrint.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.btnPrint.ForeColor = System.Drawing.Color.White;
            this.btnPrint.Location = new System.Drawing.Point(545, 514);
            this.btnPrint.Margin = new System.Windows.Forms.Padding(2);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(228, 43);
            this.btnPrint.TabIndex = 31;
            this.btnPrint.Text = "اتمام عملية المرتجعات (F8)";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.label14);
            this.panel5.Controls.Add(this.txtReturnTotal);
            this.panel5.Controls.Add(this.txtChange);
            this.panel5.Controls.Add(this.label11);
            this.panel5.Controls.Add(this.txtPaid);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.txtDueAmount);
            this.panel5.Controls.Add(this.label13);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 403);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(784, 61);
            this.panel5.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label14.Location = new System.Drawing.Point(112, 3);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 15);
            this.label14.TabIndex = 10;
            this.label14.Text = "اجمالي المرتجعات";
            // 
            // txtReturnTotal
            // 
            this.txtReturnTotal.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtReturnTotal.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtReturnTotal.Location = new System.Drawing.Point(75, 20);
            this.txtReturnTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtReturnTotal.Name = "txtReturnTotal";
            this.txtReturnTotal.ReadOnly = true;
            this.txtReturnTotal.Size = new System.Drawing.Size(149, 26);
            this.txtReturnTotal.TabIndex = 9;
            this.txtReturnTotal.Text = "0,00";
            this.txtReturnTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtChange
            // 
            this.txtChange.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtChange.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtChange.Location = new System.Drawing.Point(243, 20);
            this.txtChange.Margin = new System.Windows.Forms.Padding(2);
            this.txtChange.Name = "txtChange";
            this.txtChange.ReadOnly = true;
            this.txtChange.Size = new System.Drawing.Size(149, 26);
            this.txtChange.TabIndex = 2;
            this.txtChange.Text = "0,00";
            this.txtChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label11.Location = new System.Drawing.Point(300, 1);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 15);
            this.label11.TabIndex = 8;
            this.label11.Text = "المتبقى";
            // 
            // txtPaid
            // 
            this.txtPaid.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtPaid.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtPaid.Location = new System.Drawing.Point(409, 20);
            this.txtPaid.Margin = new System.Windows.Forms.Padding(2);
            this.txtPaid.Name = "txtPaid";
            this.txtPaid.ReadOnly = true;
            this.txtPaid.Size = new System.Drawing.Size(149, 26);
            this.txtPaid.TabIndex = 1;
            this.txtPaid.Text = "0,00";
            this.txtPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPaid.TextChanged += new System.EventHandler(this.txtPaid_TextChanged);
            this.txtPaid.Leave += new System.EventHandler(this.txtPaid_Leave);
            // 
            // label12
            // 
            this.label12.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label12.Location = new System.Drawing.Point(468, 1);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 15);
            this.label12.TabIndex = 6;
            this.label12.Text = "المدفوع";
            // 
            // txtDueAmount
            // 
            this.txtDueAmount.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtDueAmount.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtDueAmount.Location = new System.Drawing.Point(573, 19);
            this.txtDueAmount.Margin = new System.Windows.Forms.Padding(2);
            this.txtDueAmount.Name = "txtDueAmount";
            this.txtDueAmount.ReadOnly = true;
            this.txtDueAmount.Size = new System.Drawing.Size(149, 26);
            this.txtDueAmount.TabIndex = 0;
            this.txtDueAmount.Text = "0,00";
            this.txtDueAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDueAmount.TextChanged += new System.EventHandler(this.txtDueAmount_TextChanged);
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label13.Location = new System.Drawing.Point(612, 2);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 15);
            this.label13.TabIndex = 4;
            this.label13.Text = "المطلوب سداده";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.txtCashDiscount);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.txtDiscountPercent);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Controls.Add(this.txtInvTotal);
            this.panel7.Controls.Add(this.label16);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(0, 464);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(784, 45);
            this.panel7.TabIndex = 28;
            // 
            // txtCashDiscount
            // 
            this.txtCashDiscount.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtCashDiscount.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtCashDiscount.Location = new System.Drawing.Point(76, 9);
            this.txtCashDiscount.Margin = new System.Windows.Forms.Padding(2);
            this.txtCashDiscount.Name = "txtCashDiscount";
            this.txtCashDiscount.ReadOnly = true;
            this.txtCashDiscount.Size = new System.Drawing.Size(76, 26);
            this.txtCashDiscount.TabIndex = 2;
            this.txtCashDiscount.Text = "0,00";
            this.txtCashDiscount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtCashDiscount.TextChanged += new System.EventHandler(this.txtCashDiscount_TextChanged);
            this.txtCashDiscount.Leave += new System.EventHandler(this.txtCashDiscount_Leave);
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label10.Location = new System.Drawing.Point(161, 14);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 15);
            this.label10.TabIndex = 8;
            this.label10.Text = "خصم نقدي";
            // 
            // txtDiscountPercent
            // 
            this.txtDiscountPercent.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtDiscountPercent.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtDiscountPercent.Location = new System.Drawing.Point(356, 9);
            this.txtDiscountPercent.Margin = new System.Windows.Forms.Padding(2);
            this.txtDiscountPercent.Name = "txtDiscountPercent";
            this.txtDiscountPercent.ReadOnly = true;
            this.txtDiscountPercent.Size = new System.Drawing.Size(55, 26);
            this.txtDiscountPercent.TabIndex = 1;
            this.txtDiscountPercent.Text = "0,00";
            this.txtDiscountPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtDiscountPercent.TextChanged += new System.EventHandler(this.txtDiscountPercent_TextChanged);
            this.txtDiscountPercent.Leave += new System.EventHandler(this.txtDiscountPercent_Leave);
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label9.Location = new System.Drawing.Point(415, 14);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 15);
            this.label9.TabIndex = 6;
            this.label9.Text = "خصم %";
            // 
            // txtInvTotal
            // 
            this.txtInvTotal.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtInvTotal.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtInvTotal.Location = new System.Drawing.Point(573, 9);
            this.txtInvTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtInvTotal.Name = "txtInvTotal";
            this.txtInvTotal.ReadOnly = true;
            this.txtInvTotal.Size = new System.Drawing.Size(101, 26);
            this.txtInvTotal.TabIndex = 0;
            this.txtInvTotal.Text = "0,00";
            this.txtInvTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label16.Location = new System.Drawing.Point(683, 14);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(42, 15);
            this.label16.TabIndex = 4;
            this.label16.Text = "الاجمالي";
            // 
            // dgvInvoiceItems
            // 
            this.dgvInvoiceItems.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvInvoiceItems.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvInvoiceItems.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvInvoiceItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvInvoiceItems.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvInvoiceItems.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvInvoiceItems.EnableHeadersVisualStyles = false;
            this.dgvInvoiceItems.Location = new System.Drawing.Point(0, 134);
            this.dgvInvoiceItems.Margin = new System.Windows.Forms.Padding(2);
            this.dgvInvoiceItems.Name = "dgvInvoiceItems";
            this.dgvInvoiceItems.RowHeadersVisible = false;
            this.dgvInvoiceItems.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvInvoiceItems.Size = new System.Drawing.Size(784, 269);
            this.dgvInvoiceItems.TabIndex = 33;
            this.dgvInvoiceItems.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInvoiceItems_CellDoubleClick);
            this.dgvInvoiceItems.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInvoiceItems_CellEndEdit);
            this.dgvInvoiceItems.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dgvInvoiceItems_CellValidating);
            this.dgvInvoiceItems.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgvInvoiceItems_UserDeletedRow);
            this.dgvInvoiceItems.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgvInvoiceItems_UserDeletingRow);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.txtCustmoerName);
            this.panel4.Controls.Add(this.label7);
            this.panel4.Controls.Add(this.txtSafeNumber);
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.txtCashierName);
            this.panel4.Controls.Add(this.label8);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 94);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(784, 40);
            this.panel4.TabIndex = 26;
            // 
            // txtCustmoerName
            // 
            this.txtCustmoerName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtCustmoerName.BackColor = System.Drawing.SystemColors.Control;
            this.txtCustmoerName.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtCustmoerName.Location = new System.Drawing.Point(460, 6);
            this.txtCustmoerName.Margin = new System.Windows.Forms.Padding(2);
            this.txtCustmoerName.Name = "txtCustmoerName";
            this.txtCustmoerName.ReadOnly = true;
            this.txtCustmoerName.Size = new System.Drawing.Size(208, 26);
            this.txtCustmoerName.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label7.Location = new System.Drawing.Point(672, 9);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 15);
            this.label7.TabIndex = 9;
            this.label7.Text = "اسم العميل";
            // 
            // txtSafeNumber
            // 
            this.txtSafeNumber.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtSafeNumber.BackColor = System.Drawing.SystemColors.Control;
            this.txtSafeNumber.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtSafeNumber.Location = new System.Drawing.Point(309, 6);
            this.txtSafeNumber.Margin = new System.Windows.Forms.Padding(2);
            this.txtSafeNumber.Name = "txtSafeNumber";
            this.txtSafeNumber.ReadOnly = true;
            this.txtSafeNumber.Size = new System.Drawing.Size(71, 26);
            this.txtSafeNumber.TabIndex = 7;
            this.txtSafeNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label5.Location = new System.Drawing.Point(384, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 15);
            this.label5.TabIndex = 6;
            this.label5.Text = "رقم الخزينة";
            // 
            // txtCashierName
            // 
            this.txtCashierName.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.txtCashierName.BackColor = System.Drawing.SystemColors.Control;
            this.txtCashierName.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtCashierName.Location = new System.Drawing.Point(76, 6);
            this.txtCashierName.Margin = new System.Windows.Forms.Padding(2);
            this.txtCashierName.Name = "txtCashierName";
            this.txtCashierName.ReadOnly = true;
            this.txtCashierName.Size = new System.Drawing.Size(158, 26);
            this.txtCashierName.TabIndex = 5;
            this.txtCashierName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            this.label8.Location = new System.Drawing.Point(238, 9);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 15);
            this.label8.TabIndex = 4;
            this.label8.Text = "اسم الكاشير";
            // 
            // ReturnTransactionWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.dgvInvoiceItems);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.panel6);
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReturnTransactionWindow";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "شاشة المرتجعات";
            this.Load += new System.EventHandler(this.ReturnTransactionWindow_Load);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInvoiceItems)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label lblCashir;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblDateTime;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtTransactionID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox txtChange;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPaid;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDueAmount;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtCashDiscount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDiscountPercent;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtInvTotal;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridView dgvInvoiceItems;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtReturnTotal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtMainSafe;
        private System.Windows.Forms.PictureBox pbImsLogo;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox txtCustmoerName;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtSafeNumber;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCashierName;
        private System.Windows.Forms.Label label8;
    }
}