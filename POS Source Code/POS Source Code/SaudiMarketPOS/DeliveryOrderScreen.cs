﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SaudiMarketPOS.Properties;
//using gma.System.Windows;
using System.Threading;
using System.Collections;
using SaudiMarketPOS.Properties;
using ReceiptCreator;

namespace SaudiMarketPOS
{
    public partial class DeliveryOrderScreen : Form
    {
        int LastOrderId = 0;
        string CreditCTransNo = "";

        private class DeliveryDetails
        {
            public string ID { get; set; }
            public decimal UnitPrice { get; set; }
        }
        //private string GetPaymentMethod(int ID)
        //{
        //    switch (ID)
        //    {
        //        default:
        //        case 0:
        //            return "Cash On Delivery";
        //        case 1:
        //            return "Credit Card";
        //        case 2:
        //            return "Online Paid";
        //        case 3:
        //            return "Cash Payment";
        //    }
        //}

        SqlConnection con = new SqlConnection();

        string itemID = string.Empty;
        string itemBarcode = string.Empty;
        string itemName = string.Empty;
        string itemUnit = string.Empty;
        string itemPrice = string.Empty;
        string custPhone = string.Empty;
        string custAddress = string.Empty;
        string paymentMethod = string.Empty;
        string customerData = string.Empty;
        decimal itemQty;
        decimal itemTotal;
        decimal cashDiscount;
        decimal discountPercent;
        decimal invTotal;
        decimal paidCash;
        decimal dueAmount;
        decimal change;
        decimal deliveryCharge;
        decimal WalletValue;
        DeliveryDetails DeliveryObj = null;
        int invID;
        int hID;
        bool IsEdit = false;

        public DeliveryOrderScreen()
        {
            InitializeComponent();
        }
        //private void SetKeyDownEvent(Control ctlr)
        //{
        //    ctlr.KeyDown += ctl_KeyDown;
        //    if (ctlr.HasChildren)
        //        foreach (Control c in ctlr.Controls)
        //            this.SetKeyDownEvent(c);
        //}
        void ctl_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F1)
                {
                    txtItemBarcode.Focus();
                }
                else if (e.KeyCode == Keys.F2)
                {
                    if (dgvDeliveryOrder.Rows.Count <= 0)
                    {
                        GetLastTransaction();
                        Program.enableDis = false;
                        if (dgvDeliveryOrder.Rows.Count > 0)
                        {
                            printTransaction(true);
                            printTransaction(true);
                            clearData();
                            EnableForm();
                        }
                    }
                    
                }
                else if (e.KeyCode == Keys.F3)
                {
                    txtItemBarcode.Focus();
                    selectTxt(txtItemBarcode, e);
                }
                else if (e.KeyCode == Keys.F4)
                {
                    cmbDeliveryEmp.Focus();
                }
                else if (e.KeyCode == Keys.F5)
                {
                    this.txtPaid.Select();
                }
                else if (e.KeyCode == Keys.F6)
                {
                    ItemSearsh itms = new ItemSearsh(this);
                    itms.ShowDialog(this);
                    //this.dgvDeliveryOrder.Focus();
                    this.txtItemBarcode.Focus();
                }
                else if (e.KeyCode == Keys.F7)
                {
                    fillCustomer();
                    CustomerSearsh custSearsh = new CustomerSearsh(this);
                    custSearsh.ShowDialog(this);
                    this.txtItemBarcode.Focus();
                }
                else if (e.KeyCode == Keys.F8)
                {
                    calculateChange();
                    this.btnPrint_Click(this.btnPrint, new EventArgs());
                }
                //else if (e.KeyCode == Keys.F9)
                //{
                //    if (dgvDeliveryOrder.Rows.Count == 0) return;
                //    dgvDeliveryOrder.ClearSelection();
                //    dgvDeliveryOrder.CurrentCell = dgvDeliveryOrder.Rows[dgvDeliveryOrder.Rows.Count - 1].Cells["ItemQty"];
                //    dgvDeliveryOrder.BeginEdit(true);
                //}
                else if (e.KeyCode == Keys.F10)
                {
                    this.btnCancelInv_Click(this.btnCancelInv, new EventArgs());
                }
                else if (e.KeyCode == Keys.F11)
                {
                    AdminEnableDiscount adedis = new AdminEnableDiscount();
                    adedis.ShowDialog();
                    if (Program.enableDis == true)
                    {
                        deliveryCharge = 0;
                        this.deliveryFees.Text = "0,00";
                        calculateInvoiceTotal();
                        CalcudalteDueAmount();
                        Program.enableDis = false;
                    }
                }
                else if (e.KeyCode == Keys.D && e.Modifiers == Keys.Alt)
                {
                    dgvDeliveryOrder.ClearSelection();
                    dgvDeliveryOrder.CurrentCell = dgvDeliveryOrder.Rows[dgvDeliveryOrder.Rows.Count - 1].Cells["ItemQty"];
                    dgvDeliveryOrder.Rows[dgvDeliveryOrder.Rows.Count - 1].Selected = true;
                    dgvDeliveryOrder.Focus();
                }
                else if (e.KeyCode == Keys.E && e.Modifiers == Keys.Alt)
                {
                    DialogResult Result = MessageBox.Show("سيتم دفع هذا الطلب عن طريق الفيزا. هل تريد الاستمرار؟", "", MessageBoxButtons.YesNo);
                    if (Result == DialogResult.Yes)
                    {
                        chkCredit.Checked = true;
                        paymentMethod = WebOrders.GetPaymentMethod(1);
                    }
                    else
                    {
                        chkCredit.Checked = false;
                        paymentMethod = WebOrders.GetPaymentMethod(0);
                    }
                }
            }
            catch (Exception ex)
            { }
        }
        private void DeliveryOrderScreen_Load(object sender, EventArgs e)
        {
            this.Icon = Icon.ExtractAssociatedIcon(Application.ExecutablePath);
            LoadWebOrders();
            foreach (DataGridViewColumn dgvc in dgvDeliveryOrder.Columns)
            {
                dgvc.SortMode = DataGridViewColumnSortMode.NotSortable;
            }
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            pbXreca.Image = Image.FromFile(apppath + "xreca-logo.png");
            try
            {
                
                //acHook = new UserActivityHook();
                //acHook.KeyDown += acHook_KeyDown;
                //con.ConnectionString = Settings.Default.ConnectionString;
                con.ConnectionString = Program.conString;
                //this.SetKeyDownEvent(this);
                
                lblDateTime.Text = DateTime.Now.ToString();
                lblCashir.Text = Program.fristName.ToString() + " " + Program.lastName.ToString();
                txtItemBarcode.Focus();
                //this.txtSafeNumber.Text = Settings.Default.SafeNumber.ToString();

                //Fill Employee Combobox
                SqlCommand cmdFillEmpName = new SqlCommand();
                cmdFillEmpName.CommandType = CommandType.StoredProcedure;
                cmdFillEmpName.CommandText = "uspSelectDelivery&preEmpName";
                cmdFillEmpName.Connection = con;
                DataTable dtEmpName = new DataTable();
                if (con.State == ConnectionState.Closed) con.Open();
                dtEmpName.Load(cmdFillEmpName.ExecuteReader());
                cmbDeliveryEmp.DataSource = dtEmpName;
                cmbDeliveryEmp.ValueMember = "ID";
                cmbDeliveryEmp.DisplayMember = "EmpName";
                cmbDeliveryEmp.Text = "";

                //Fill Customer Combobox
                fillCustomer();

                //getMaxInvID();
                
                
                dgvDeliveryOrder.Columns["ItemBarcode"].ReadOnly = true;
                dgvDeliveryOrder.Columns["ItemQty"].ReadOnly = false;
                dgvDeliveryOrder.Columns["ItemName"].Width = 235;
                dgvDeliveryOrder.Columns["ItemUnit"].Width = 70;
                this.clearData();
            }
            catch (Exception ex)
            { }
        }
        //UserActivityHook acHook;
        private void fillCustomer()
        {
            SqlCommand cmdFillCustName = new SqlCommand();
            cmdFillCustName.CommandType = CommandType.StoredProcedure;
            cmdFillCustName.CommandText = "uspSelectCustomer";
            cmdFillCustName.Connection = con;
            if (con.State == ConnectionState.Closed) con.Open();

            DataTable dtCustName = new DataTable();
            dtCustName.Load(cmdFillCustName.ExecuteReader());
            cmbCustomerName.DataSource = dtCustName;
            cmbCustomerName.ValueMember = "ID";
            cmbCustomerName.DisplayMember = "Name";
            if (con.State == ConnectionState.Open) con.Close();
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //void acHook_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyData == Keys.F6)
        //    {
        //        ItemSearsh itms = new ItemSearsh(this);
        //        itms.ShowDialog();
        //    }
        //    if (e.KeyData == Keys.F7)
        //    {
        //        CustomerSearsh custSearsh = new CustomerSearsh();
        //        custSearsh.ShowDialog();
        //    }
        //}
        private void getMaxInvID()
        {
            SqlCommand getInvIDCmd = new SqlCommand();
            getInvIDCmd.Connection = con;
            getInvIDCmd.CommandType = CommandType.StoredProcedure;
            getInvIDCmd.CommandText = "uspGetMaxPurchaseOrderHID";
            if (con.State == ConnectionState.Closed) con.Open();
            object iid = getInvIDCmd.ExecuteScalar();
            invID = int.Parse(iid.ToString());
            if (con.State == ConnectionState.Open) con.Close();

        }
        private void calculateInvoiceTotal()

        {
            invTotal = 0;

            foreach (DataGridViewRow totDr in dgvDeliveryOrder.Rows)
            {
                // حساب الاجمالى
                if (totDr.Cells[6].Value != null && totDr.Cells[6].Value.ToString() != "")
                {
                    invTotal += Convert.ToDecimal(totDr.Cells[6].Value);
                }
            }
            this.deliveryFees.Text = deliveryCharge.ToString();
            invTotal += deliveryCharge;
            txtInvTotal.Text = invTotal.ToString();
        }
        private void CalcudalteDueAmount()
        {
            decimal totalDiscount = 0.0m;
            cashDiscount = decimal.Parse(txtCashDiscount.Text.ToString());
            totalDiscount = cashDiscount;
            discountPercent = decimal.Parse(txtDiscountPercent.Text.Trim());
            if (discountPercent != 0)
            {
                discountPercent = discountPercent / 100.0m;
                decimal calcDiscountPercent = invTotal * discountPercent;
                totalDiscount += calcDiscountPercent;
            }
            txtDueAmount.Text = (invTotal - totalDiscount).ToString();
        }
        private void calculateChange()
        {
            if (paidCash == 0) return;
            change = paidCash - dueAmount;
            txtChange.Text = change.ToString();
        }
        private void selectTxt(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).SelectionStart = 0;
                ((TextBox)sender).SelectionLength = ((TextBox)sender).Text.Length;
            }
            catch { }

        }
        private void txtItemBarcode_TextChanged(object sender, EventArgs e)
        {
            if (txtItemBarcode.Text.Length > 16)
            {
                txtItemBarcode.Text = "";
                MessageBox.Show("خطأ في الادخال", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                txtItemBarcode.Focus();
                return;
            }
        }
        private void btnCancelInv_Click(object sender, EventArgs e)
        {
            AdminLoginQty adlog = new AdminLoginQty();
            adlog.ShowDialog();
            if (Program.checkRole == 1 || Program.checkRole == 2)
            {
                if (MessageBox.Show("حذف الجميع", "تأكيد", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.RightAlign) == DialogResult.No)
                {
                    return;
                }
                else
                {
                    clearData();
                }
            }

        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDateTime.Text = DateTime.Now.ToString();
        }
        private void txtDiscountPercent_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtDiscountPercent.Text.Trim() == "")
                {
                    txtDiscountPercent.Text = "0,00";
                }
                discountPercent = decimal.Parse(txtDiscountPercent.Text.Trim());
                if (discountPercent == 0)
                { return; }

                else if (discountPercent < 0 || discountPercent > 100)
                {
                    MessageBox.Show("قيمة غير مناسبة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDiscountPercent.Text = "0,00";
                    txtDiscountPercent.Focus();
                    selectTxt(this, e);
                    return;
                }
                else
                {
                    CalcudalteDueAmount();
                }
                calculateChange();
            }
            catch (Exception ex)
            { }
                
        }
        private void txtCashDiscount_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtCashDiscount.Text == "")
                {
                    txtCashDiscount.Text = "0,00";
                }
                cashDiscount = decimal.Parse(txtCashDiscount.Text.Trim());
                if (cashDiscount < 0 || cashDiscount > invTotal)
                {
                    MessageBox.Show("قيمة غير مناسبة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCashDiscount.Text = "0,00";
                    txtCashDiscount.Focus();
                    selectTxt(this, e);
                }
                else
                {
                    CalcudalteDueAmount();
                }
                calculateChange();
            }
            catch (Exception ex)
            { }
        }
        private void txtPaid_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtPaid.Text == "")
                {
                    txtPaid.Text = "0.00";
                    MessageBox.Show("لا يمكن ترك القيمة فارغة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPaid.Focus();
                    selectTxt(this, e);
                    return;
                }

                if (paidCash < dueAmount)
                {
                    MessageBox.Show("المبلغ غير صحيح", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPaid.Clear();
                    txtPaid.Focus();
                }
                else
                {
                    calculateChange();
                }
            }
            catch (Exception ex)
            { }
        }
        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (!IsEdit)
                {
                    if (dgvDeliveryOrder.Rows.Count > 0)
                    {
                        if (cmbDeliveryEmp.Text != "")
                        {
                            SqlCommand insertTransactionCmd = new SqlCommand();

                            insertTransactionCmd.Connection = con;
                            insertTransactionCmd.CommandType = CommandType.StoredProcedure;
                            insertTransactionCmd.CommandText = "uspPurchaseOrderHInsert";
                            insertTransactionCmd.Parameters.AddWithValue("@UserID", Program.empID);
                            insertTransactionCmd.Parameters.AddWithValue("@CustomerID", cmbCustomerName.SelectedValue);
                            insertTransactionCmd.Parameters.AddWithValue("@DeliveryEmloyeeID", cmbDeliveryEmp.SelectedValue);
                            insertTransactionCmd.Parameters.AddWithValue("@Total", decimal.Parse(txtInvTotal.Text.Trim()));
                            insertTransactionCmd.Parameters.AddWithValue("@DiscountPercent", decimal.Parse(txtDiscountPercent.Text.Trim()));
                            insertTransactionCmd.Parameters.AddWithValue("@CashDiscount", decimal.Parse(txtCashDiscount.Text.Trim()));
                            if (txtCredit.Enabled == true)
                            {
                                insertTransactionCmd.Parameters.AddWithValue("@CreditAmount", decimal.Parse(txtCredit.Text.Trim()));
                            }
                            else
                            {
                                decimal x = 0;
                                insertTransactionCmd.Parameters.AddWithValue("@CreditAmount", x);
                            }
                            insertTransactionCmd.Parameters.AddWithValue("@DueAmount", decimal.Parse(txtDueAmount.Text.Trim()));
                            insertTransactionCmd.Parameters.AddWithValue("@Paid", decimal.Parse(txtPaid.Text.Trim()));
                            insertTransactionCmd.Parameters.AddWithValue("@Change", decimal.Parse(txtChange.Text.Trim()));
                            insertTransactionCmd.Parameters.AddWithValue("@SafeNumber", -1);
                            if (txtNotes.Text.Trim() == "")
                            {
                                insertTransactionCmd.Parameters.AddWithValue("@Notes", DBNull.Value);
                            }
                            else
                            {
                                insertTransactionCmd.Parameters.AddWithValue("@Notes", txtNotes.Text.Trim());
                            }
                            if (chkCredit.Checked)
                            {
                                insertTransactionCmd.Parameters.AddWithValue("@PaymentMethod", 1);
                            }
                            else
                            {
                                insertTransactionCmd.Parameters.AddWithValue("@PaymentMethod", 0);
                            }
                            insertTransactionCmd.Parameters.AddWithValue("@CreditCardTransactionNumber", "");
                            insertTransactionCmd.Parameters.AddWithValue("@CreatedTime", DateTime.Now);

                            if (con.State == ConnectionState.Closed) con.Open();
                            SqlDataReader readHid = insertTransactionCmd.ExecuteReader();

                            while (readHid.Read())
                            {
                                hID = readHid.GetInt32(0);
                            }
                            readHid.Close();
                            if (con.State == ConnectionState.Open) con.Close();


                            SqlCommand insertDetailCmd = new SqlCommand();
                            foreach (DataGridViewRow dr in dgvDeliveryOrder.Rows)
                            {
                                if (dr.Cells["Total"].Value != null & dr.Cells["ItemQty"].Value != null)
                                {
                                    insertDetailCmd.Parameters.Clear();
                                    insertDetailCmd.CommandType = CommandType.StoredProcedure;
                                    insertDetailCmd.Connection = con;
                                    insertDetailCmd.CommandText = "uspPurchaseOrderDetailInsert";
                                    insertDetailCmd.Parameters.AddWithValue("@PurchaseOrderHID", hID);
                                    insertDetailCmd.Parameters.AddWithValue("@Cost", dr.Cells["Total"].Value);
                                    insertDetailCmd.Parameters.AddWithValue("@SalePrice", dr.Cells["ItemPrice"].Value);
                                    insertDetailCmd.Parameters.AddWithValue("@ItemID", dr.Cells["ItemID"].Value);
                                    insertDetailCmd.Parameters.AddWithValue("@Quantity", dr.Cells["ItemQty"].Value);

                                    if (con.State == ConnectionState.Closed) con.Open();
                                    insertDetailCmd.ExecuteNonQuery();
                                    if (con.State == ConnectionState.Open) con.Close();
                                }

                            }
                            if (DeliveryObj != null)
                            {
                                insertDetailCmd.Parameters.Clear();
                                insertDetailCmd.CommandType = CommandType.StoredProcedure;
                                insertDetailCmd.Connection = con;
                                insertDetailCmd.CommandText = "uspPurchaseOrderDetailInsert";
                                insertDetailCmd.Parameters.AddWithValue("@PurchaseOrderHID", hID);
                                insertDetailCmd.Parameters.AddWithValue("@Cost", DeliveryObj.UnitPrice);
                                insertDetailCmd.Parameters.AddWithValue("@SalePrice", DeliveryObj.UnitPrice);
                                insertDetailCmd.Parameters.AddWithValue("@ItemID", DeliveryObj.ID);
                                insertDetailCmd.Parameters.AddWithValue("@Quantity", 1);
                                DeliveryObj = null;
                                if (con.State == ConnectionState.Closed) con.Open();
                                insertDetailCmd.ExecuteNonQuery();
                                if (con.State == ConnectionState.Open) con.Close();
                            }
                            printTransaction();
                            printTransaction();
                            clearData();
                        }
                        else
                        {
                            MessageBox.Show("من فضلك اختر مندوب التوصيل");
                        }
                    }
                    else
                    {
                        MessageBox.Show("لا يوجد أصناف في هذا الطلب, من فضلك قم بمراجعة الطلب");
                    }
                }
                else
                {
                    if (dgvDeliveryOrder.Rows.Count > 0)
                    {
                        if (cmbDeliveryEmp.Text != "")
                        {
                            SqlCommand UpdateTransactionCmd = new SqlCommand();
                            UpdateTransactionCmd.Connection = con;
                            UpdateTransactionCmd.CommandType = CommandType.StoredProcedure;
                            UpdateTransactionCmd.CommandText = "uspPurchaseOrderUpdate";
                            UpdateTransactionCmd.Parameters.AddWithValue("@OrderID", hID);
                            UpdateTransactionCmd.Parameters.AddWithValue("@DeliveryID", cmbDeliveryEmp.SelectedValue);
                            if (con.State == ConnectionState.Closed) con.Open();
                            UpdateTransactionCmd.ExecuteNonQuery();
                            printTransaction();
                            printTransaction();
                            clearData();
                            EnableForm();
                        }
                        else
                        {
                            MessageBox.Show("من فضلك اختر مندوب التوصيل");
                        }
                    }
                    else
                    {
                        MessageBox.Show("لا يوجد أصناف في هذا الطلب, من فضلك قم بمراجعة الطلب");
                    }
                }
            }
            catch (Exception ex)
            { }

        }

        private void GetLastTransaction()
        {
            ///// Get Last Order Details ///////////////////////
            SqlCommand LastOrderCmd = new SqlCommand();
            LastOrderCmd.Connection = con;
            LastOrderCmd.CommandType = CommandType.Text;
            LastOrderCmd.CommandText = "SELECT * FROM PurchaseOrderH WHERE ID = @ID";
            LastOrderCmd.Parameters.AddWithValue("@ID", hID);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readOrder = LastOrderCmd.ExecuteReader();
            while(readOrder.Read())
            {
                clearData();
                txtInvTotal.Text = readOrder["Total"].ToString();
                txtDueAmount.Text = readOrder["DueAmount"].ToString();
                txtNotes.Text = readOrder["Notes"].ToString();
                try { paymentMethod = WebOrders.GetPaymentMethod((int)readOrder["PaymentMethod"]); }catch { paymentMethod = WebOrders.GetPaymentMethod(0); }
                customerData = readOrder["CustomerData"].ToString();
                DisableForm();
            }
            readOrder.Close();
            ////////////////////////////////////////////////////
            /////////////// Get Order Details //////////////////
            SqlCommand LastDetialsCmd = new SqlCommand();
            LastDetialsCmd.Connection = con;
            LastDetialsCmd.CommandType = CommandType.Text;
            LastDetialsCmd.CommandText = "SELECT * FROM PurchaseOrderD WHERE PurchaseOrderHID = @ID";
            LastDetialsCmd.Parameters.AddWithValue("@ID", hID);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readDetails = LastDetialsCmd.ExecuteReader();
            List<LastOrderDetails> DetailsLst = new List<LastOrderDetails>();
            while (readDetails.Read())
            {
                LastOrderDetails Obj = new LastOrderDetails();
                Obj.ItemId = readDetails["ItemID"].ToString();
                Obj.SalePrice = decimal.Parse(readDetails["SalePrice"].ToString());
                Obj.QTY = decimal.Parse(readDetails["Quantity"].ToString());
                Obj.Total = Obj.SalePrice * Obj.QTY;
                DetailsLst.Add(Obj);
            }
            readDetails.Close();
            foreach(LastOrderDetails DetailObj in DetailsLst)
            {
                SqlCommand ItemCmd = new SqlCommand();
                ItemCmd.Connection = con;
                ItemCmd.CommandType = CommandType.Text;
                ItemCmd.CommandText = "SELECT * FROM Item WHERE ID = @ID";
                ItemCmd.Parameters.AddWithValue("@ID", DetailObj.ItemId);
                if (con.State == ConnectionState.Closed) con.Open();
                SqlDataReader readItem = ItemCmd.ExecuteReader();
                string Name = "";
                string Barcode = "";
                while (readItem.Read())
                {
                    Name = readItem["Name"].ToString();
                    Barcode = readItem["Barcode"].ToString();
                }
                readItem.Close();
                if (DetailObj.SalePrice >= 0)
                {
                    if (DetailObj.ItemId != "7638732050472537" && Barcode != "7638732050472537")
                    {
                        dgvDeliveryOrder.Rows.Add(DetailObj.ItemId, Barcode, Name, "", DetailObj.SalePrice, DetailObj.QTY, DetailObj.Total);
                    }
                    else
                    {
                        deliveryCharge = DetailObj.SalePrice;
                    }
                }
            }
        }

        private class LastOrderDetails
        {
            public string ItemId;
            public decimal SalePrice;
            public decimal QTY;
            public decimal Total;
        }

        private void printTransaction(bool copy = false)
        {
            SqlCommand phoneCmd = new SqlCommand();
            phoneCmd.Connection = con;
            phoneCmd.CommandType = CommandType.StoredProcedure;
            phoneCmd.CommandText = "GetCustPhoneNumber";
            phoneCmd.Parameters.AddWithValue("@ID", cmbCustomerName.SelectedValue);
            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readPhone = phoneCmd.ExecuteReader();
            while (readPhone.Read())
            {
                custPhone = readPhone[0].ToString();
                custAddress = readPhone[1].ToString();
            }
            readPhone.Close();

            TransactionReceipt tr = new TransactionReceipt();
            tr.seLogo = (Bitmap)Image.FromFile("s1.png", true);
            tr.transactionDate = DateTime.Now;
            tr.transactionTime = DateTime.Now;
            tr.TransactionNo = hID;
            //tr.safeNo = int.Parse(txtSafeNumber.Text);
            tr.cashirName = cmbDeliveryEmp.Text;
            tr.deliveryEmpName = cmbDeliveryEmp.Text;
            tr.custName = cmbCustomerName.Text;
            tr.GridViewRow = dgvDeliveryOrder;
            tr.custPhone = custPhone;
            tr.custAdress = custAddress;
            tr.notes = txtNotes.Text;
            tr.phoneEmp = Program.fristName.ToString() + " " + Program.lastName.ToString();
            tr.paymentMethod = paymentMethod;
            tr.customerData = customerData;
            tr.discount = decimal.Parse(txtWebDiscounts.Text);
            tr.deliveryCharge = deliveryCharge;
            tr.walletvalue = WalletValue;
            tr.copy = copy;
            tr.transactionTot = decimal.Parse(txtInvTotal.Text);
            //tr.discount = decimal.Parse(txtDiscountPercent.Text);
            //tr.dueAmount = decimal.Parse(txtDueAmount.Text);
            //tr.paid = decimal.Parse(txtPaid.Text);
            //tr.change = decimal.Parse(txtChange.Text);
            int rowsCount = dgvDeliveryOrder.RowCount * 40;
            tr.printDelivery(rowsCount + 820);
        }

        private void cmbCustomerName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //SqlCommand accountNumCmd = new SqlCommand();
                //accountNumCmd.Connection = con;
                //accountNumCmd.CommandType = CommandType.StoredProcedure;
                //accountNumCmd.CommandText = "uspGetCustAccountNum";
                //accountNumCmd.Parameters.AddWithValue("@ID", cmbCustomerName.SelectedValue);
                //if (con.State == ConnectionState.Closed) con.Open();
                //SqlDataReader readAccountNum = accountNumCmd.ExecuteReader();
                //string sID = null;
                //while (readAccountNum.Read())
                //{
                //    sID = readAccountNum[0].ToString();
                //}
                //readAccountNum.Close();
                //if (con.State == ConnectionState.Open) con.Close();
                //txtCustomerID.Text = sID;
                txtCustomerID.Text = cmbCustomerName.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("لا يوجد عميل بهذاالرقم", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void cmbCustomerName_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.ActiveControl != btnCancelInv || this.ActiveControl != btnPrint)
                {
                    DataTable tbSP = ((DataTable)cmbCustomerName.DataSource).Copy();
                    tbSP.DefaultView.RowFilter = "Name='" + cmbCustomerName.Text + "'";
                    if (tbSP.DefaultView.Count == 0)
                    {
                        MessageBox.Show("اسم العميل غير صحيح", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        cmbCustomerName.Focus();
                    }
                    tbSP.DefaultView.RowFilter = "";
                }
            }
            catch { }
        }
        private void txtCustomerID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //SqlCommand custIdCmd = new SqlCommand();
                //custIdCmd.Connection = con;
                //custIdCmd.CommandType = CommandType.StoredProcedure;
                //custIdCmd.CommandText = "uspGetCustIdByAccNum";
                //custIdCmd.Parameters.AddWithValue("@AccountNumber", txtCustomerID.Text.Trim());
                //if (con.State == ConnectionState.Closed) con.Open();

                //SqlDataReader readCustID = null;
                //readCustID = custIdCmd.ExecuteReader();
                //int sv = -1;
                //while (readCustID.Read())
                //{
                //    sv = int.Parse(readCustID[0].ToString());
                //}
                //readCustID.Close();
                //if (con.State == ConnectionState.Open) con.Close();
                //cmbCustomerName.SelectedValue = sv;
                cmbCustomerName.SelectedValue = int.Parse(txtCustomerID.Text);

            }
            catch (Exception ex)
            {
                MessageBox.Show("لا يوجد عميل بهذا الرقم", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void clearData()
        {
            LoadWebOrders();
            dgvDeliveryOrder.Rows.Clear();
            txtInvTotal.Text = "0,00";
            // txtDiscountPercent.Text = "0,00";
            //txtCashDiscount.Text = "0,00";
            //txtCredit.Text = "0,00";
            txtDueAmount.Text = "0,00";
            //txtPaid.Text = "0,00";
            //txtChange.Text = "0,00";
            //txtCreditTransactionNum.Text = "";
            itemQty = 0;
            itemTotal = 0;
            txtNotes.Text = "";
            //cashDiscount = 0;
            //discountPercent = 0;
            invTotal = 0;
            //paidCash = 0;
            //dueAmount = 0;
            //change = 0;
            //chkCredit.Checked = false;
            //chkCreditCard.Checked = false;
            //txtCreditTransactionNum.Enabled = false;
            //txtCredit.Enabled = false;
            txtItemBarcode.Focus();
            cmbCustomerName.SelectedIndex = 0;
            cmbDeliveryEmp.Text = "";
            paymentMethod = "";
            deliveryCharge = 0;
            this.deliveryFees.Text = "0,00";
            customerData = "";
            txtWebDiscounts.Text = "0";
            CreditCTransNo = "";
            chkCredit.Checked = false;
        }
        private void txtPaid_TextChanged(object sender, EventArgs e)
        {
            if (txtPaid.Text == "") return;
            try
            {
                paidCash = decimal.Parse(txtPaid.Text);
            }
            catch
            {
                MessageBox.Show("ادخل ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPaid.Focus();
                txtPaid.Clear();
                selectTxt(this, e);
            }
        }
        private void txtDueAmount_TextChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    dueAmount = decimal.Parse(txtDueAmount.Text);
            //}
            //catch(Exception ex)
            //{}
        }
        private void dgvDeliveryOrder_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgvDeliveryOrder.Rows[e.RowIndex].Cells["ItemQty"].Value == null)
                {
                    MessageBox.Show("لا يمكن ترك القيمة فارغة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    dgvDeliveryOrder.Rows[e.RowIndex].Cells["ItemQty"].Value = 1;
                    dgvDeliveryOrder.BeginEdit(true);
                    return;
                }
                else
                {

                    try
                    {
                        decimal itemUnit = decimal.Parse(dgvDeliveryOrder.Rows[e.RowIndex].Cells["ItemQty"].Value.ToString());
                        if (itemUnit <= 0)
                        {
                            MessageBox.Show("ادخل قيمة صحيحة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                            dgvDeliveryOrder.Rows[e.RowIndex].Cells["ItemQty"].Value = 1;
                            dgvDeliveryOrder.BeginEdit(true);
                            return;

                        }
                        else
                        {
                            decimal itemPrice = decimal.Parse(dgvDeliveryOrder.Rows[e.RowIndex].Cells["ItemPrice"].Value.ToString());
                            decimal itemTotal = Math.Round(itemUnit * itemPrice, 2);
                            dgvDeliveryOrder.Rows[e.RowIndex].Cells["Total"].Value = itemTotal.ToString();
                            calculateInvoiceTotal();
                            CalcudalteDueAmount();
                            calculateChange();
                            //dgvTransactions.Rows[e.RowIndex].Cells["ItemQty"].Selected = true;
                            //dgvDeliveryOrder.Columns["ItemQty"].ReadOnly = true;
                            this.txtItemBarcode.Focus();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("يجب ادخال ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                        dgvDeliveryOrder.Rows[e.RowIndex].Cells["ItemQty"].Value = 1;
                        dgvDeliveryOrder.BeginEdit(true);
                        return;
                    }
                    txtPaid.Text = "0.00";
                    txtChange.Text = "0.00";
                }
            }
            catch (Exception ex)
            { }

            
        }
        private void chkCreditCard_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCreditCard.Checked == true)
            {
                txtCreditTransactionNum.Enabled = true;
                txtCreditTransactionNum.Focus();
            }

            if (chkCreditCard.Checked == false)
            {
                txtCreditTransactionNum.Enabled = false;
                txtCreditTransactionNum.Clear();
            }
        }
        private void chkCredit_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCredit.Checked == true)
            {
                txtCredit.Enabled = true;
                txtCredit.Focus();
            }

            if (chkCredit.Checked == false)
            {
                txtCredit.Enabled = false;
                txtCredit.Text = "0,00";
            }
        }
        private void cmbDeliveryEmp_Leave(object sender, EventArgs e)
        {
            //try
            //{
            //    if (this.ActiveControl != btnCancelInv)
            //    {
            //        DataTable tbDP = ((DataTable)cmbDeliveryEmp.DataSource).Copy();
            //        tbDP.DefaultView.RowFilter = "EmpName='" + cmbDeliveryEmp.Text + "'";
            //        if (tbDP.DefaultView.Count == 0)
            //        {
            //            MessageBox.Show("اسم المندوب غير صحيح", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            cmbDeliveryEmp.Focus();
            //        }
            //        tbDP.DefaultView.RowFilter = "";
            //    }
            //}
            //catch { }
        }
        private void dgvDeliveryOrder_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                calculateInvoiceTotal();
                CalcudalteDueAmount();
                calculateChange();
            }
            catch (Exception ex)
            { }

        }

        private void dgvDeliveryOrder_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            txtItemBarcode.Focus();
        }

        private void dgvDeliveryOrder_Validated(object sender, EventArgs e)
        {
            txtItemBarcode.Focus();
        }

        private void DeliveryOrderScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void txtItemBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                SqlDataReader readItem;
                if (e.KeyCode == Keys.Enter)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "uspGetItemsByBarcode";
                    if (con.State == ConnectionState.Closed) con.Open();
                    if (txtItemBarcode.Text.Trim().Substring(0, 2) == "23" & txtItemBarcode.Text.Length >= 13)
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Barcode", txtItemBarcode.Text.Trim().Substring(2, 5));
                        itemQty = Math.Round(decimal.Parse(txtItemBarcode.Text.Trim().Substring(7, 5)) / 1000, 3);
                        if (itemQty <= 0)
                        {
                            itemQty = 1;
                        }

                        readItem = cmd.ExecuteReader();
                        if (readItem.HasRows == false)
                        {
                            readItem.Close();
                            cmd.Parameters.Clear();
                            cmd.Parameters.AddWithValue("@Barcode", txtItemBarcode.Text);
                            itemQty = 1;
                            readItem = cmd.ExecuteReader();
                            if (readItem.HasRows == false)
                            {
                                readItem.Close();
                                cmd.Parameters.Clear();
                                txtItemBarcode.Clear();
                                ItemNotFound itmNFo = new ItemNotFound();
                                itmNFo.ShowDialog(this);
                                txtItemBarcode.Clear();
                                this.txtItemBarcode.Focus();
                                return;
                            }

                        }

                    }
                    else if (txtItemBarcode.Text.Trim().Substring(0, 1) == "D" || txtItemBarcode.Text.Trim().Substring(0, 1) == "d")
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Barcode", txtItemBarcode.Text);
                        readItem = cmd.ExecuteReader();
                        while (readItem.Read())
                        {
                            itemPrice = Math.Round(decimal.Parse(readItem[4].ToString()), 2).ToString();
                            deliveryCharge = decimal.Parse(itemPrice);
                            DeliveryObj = new DeliveryDetails();
                            DeliveryObj.UnitPrice = deliveryCharge;
                            DeliveryObj.ID = txtItemBarcode.Text;
                        }
                        if (con.State == ConnectionState.Open) con.Close();

                        txtItemBarcode.Clear();
                        calculateInvoiceTotal();
                        CalcudalteDueAmount();
                        return;
                    }
                    else
                    {
                        cmd.Parameters.Clear();
                        cmd.Parameters.AddWithValue("@Barcode", txtItemBarcode.Text);
                        itemQty = 1;
                        readItem = cmd.ExecuteReader();
                    }
                    if (readItem.HasRows == false)
                    {
                        readItem.Close();
                        cmd.Parameters.Clear();
                        txtItemBarcode.Clear();
                        ItemNotFound itmNFo = new ItemNotFound();
                        itmNFo.ShowDialog(this);
                        txtItemBarcode.Clear();
                        txtItemBarcode.Focus();
                        return;
                    }
                    while (readItem.Read())
                    {
                        itemID = readItem[0].ToString();
                        itemBarcode = readItem[1].ToString();
                        itemName = readItem[2].ToString();
                        itemUnit = readItem[3].ToString();
                        itemPrice = Math.Round(decimal.Parse(readItem[4].ToString()), 2).ToString();
                    }
                    readItem.Close();
                    if (con.State == ConnectionState.Open) con.Close();
                    itemTotal = Math.Round(itemQty * decimal.Parse(itemPrice), 2);

                    bool IsExist = false;
                    DataGridViewRow Existdgvr = null;
                    foreach (DataGridViewRow dgvr in dgvDeliveryOrder.Rows)
                    {
                        if (dgvr.Cells["ItemID"].Value.ToString() == itemID)
                        {
                            IsExist = true;
                            Existdgvr = dgvr;
                        }
                    }
                    if (!IsExist)
                    {
                        dgvDeliveryOrder.Rows.Add(itemID, itemBarcode, itemName, itemUnit, itemPrice, itemQty, itemTotal);
                    }
                    else
                    {
                        decimal ExistQTY = decimal.Parse(Existdgvr.Cells["ItemQty"].Value.ToString());
                        decimal NewQTY = itemQty + ExistQTY;
                        Existdgvr.Cells["ItemQty"].Value = NewQTY;
                        Existdgvr.Cells["Total"].Value = NewQTY * decimal.Parse(itemPrice);
                    }

                    //SqlCommand tempItemCmd = new SqlCommand();
                    //tempItemCmd.Connection = con;
                    //tempItemCmd.CommandType = CommandType.StoredProcedure;
                    //tempItemCmd.CommandText = "uspTempTransactionInsert";
                    //tempItemCmd.Parameters.AddWithValue("@UserID", Program.empID);
                    //tempItemCmd.Parameters.AddWithValue("@ItemID", itemID);
                    //tempItemCmd.Parameters.AddWithValue("@ItemBarcode", itemBarcode);
                    //tempItemCmd.Parameters.AddWithValue("@ItemName", itemName);
                    //tempItemCmd.Parameters.AddWithValue("@ItemUnit", itemUnit);
                    //tempItemCmd.Parameters.AddWithValue("@ItemPrice", itemPrice);
                    //tempItemCmd.Parameters.AddWithValue("@ItemQty", itemQty);
                    //tempItemCmd.Parameters.AddWithValue("@ItemTotal", itemTotal);
                    //if (con.State == ConnectionState.Closed) con.Open();
                    //tempItemCmd.ExecuteNonQuery();
                    if (con.State == ConnectionState.Open) con.Close();

                    txtItemBarcode.Clear();
                    calculateInvoiceTotal();
                    CalcudalteDueAmount();
                    dgvDeliveryOrder.FirstDisplayedScrollingRowIndex = dgvDeliveryOrder.Rows.Count - 1;
                }
            }
            catch (Exception ex)
            { }
        }

        private void dgvDeliveryOrder_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            itemTotal = 0;
        }

        private void dgvDeliveryOrder_UserDeletedRow_1(object sender, DataGridViewRowEventArgs e)
        {
            try
            {
                calculateInvoiceTotal();
                CalcudalteDueAmount();
                calculateChange();
                Program.delTransactionRow = false;
            }
            catch (Exception ex)
            { }
        }

        private void dgvDeliveryOrder_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (MessageBox.Show("هل انت متأكد", "تأكيد", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.RightAlign) == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    if (this.ActiveControl == dgvDeliveryOrder)
                    {
                        AdminDelItem adlog = new AdminDelItem();
                        adlog.ShowDialog();
                        if (!Program.delTransactionRow)
                        {
                            e.Cancel = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        private void dgvDeliveryOrder_KeyDown(object sender, KeyEventArgs e)
        {
            //if (this.ActiveControl == dgvDeliveryOrder)
            //{
            //    if (e.KeyCode == Keys.Enter)
            //    {
            //        if (dgvDeliveryOrder.CurrentCell == dgvDeliveryOrder.Rows[0].Cells["ItemQty"])
            //        {
            //            txtItemBarcode.Focus();
            //        }
            //    }
            //}
        }

        private void txtCreditTransactionNum_TextChanged(object sender, EventArgs e)
        {

        }

        private void label20_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void txtDueAmount_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void btnPrint_MouseEnter(object sender, EventArgs e)
        {
            btnPrint.BackgroundImage = Properties.Resources.print_gray;
        }

        private void btnPrint_MouseLeave(object sender, EventArgs e)
        {
            btnPrint.BackgroundImage = Properties.Resources.print_green;
        }

        private void btnClose_MouseEnter(object sender, EventArgs e)
        {
            btnClose.BackgroundImage = Properties.Resources.out_gray;
        }

        private void btnClose_MouseLeave(object sender, EventArgs e)
        {
            btnClose.BackgroundImage = Properties.Resources._out;
        }

        private void btnCancelInv_MouseEnter(object sender, EventArgs e)
        {
            btnCancelInv.BackgroundImage = Properties.Resources.cancel_gray;
        }

        private void btnCancelInv_MouseLeave(object sender, EventArgs e)
        {
            btnCancelInv.BackgroundImage = Properties.Resources.cancel;
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void LoadWebOrders()
        {
            List<WebOrders> OrdersLst = WebOrders.GetWebOrders();
            dgvWebOrders.AutoGenerateColumns = false;
            dgvWebOrders.DataSource = OrdersLst.FindAll(c => c.DeliveryID == 0 && c.Collected == false);
            try
            {
                dgvWebOrders.SelectedRows[0].Selected = false;
            }catch { }
        }

        private void btnWebOrders_Click(object sender, EventArgs e)
        {
            WebDeliveryOrders frm = new WebDeliveryOrders();
            frm.ShowDialog();
            WebOrders WebObj = frm.WebObj;
            if (WebObj.ID != 0)
            {
                clearData();
                hID = WebObj.ID;
                //txtCustomerID.Text = WebObj.CustomerID.ToString();
                txtInvTotal.Text = WebObj.Total.ToString();
                txtDueAmount.Text = WebObj.Total.ToString();
                txtNotes.Text = WebObj.Notes;
                paymentMethod = WebObj.strPaymentMethod;
                customerData = WebObj.CustomerData;
                foreach (WebOrderDetails DetailsObj in WebObj.DetailsLst)
                {
                    if (DetailsObj.SalePrice >= 0)
                    {
                        if (DetailsObj.ItemID != "7638732050472537")
                        {
                            dgvDeliveryOrder.Rows.Add(DetailsObj.ItemID, DetailsObj.Barcode, DetailsObj.ItemName, DetailsObj.Unit, DetailsObj.SalePrice, DetailsObj.Quantity, DetailsObj.Total);
                        }
                        else
                        {
                            deliveryCharge = DetailsObj.SalePrice;
                        }
                    }
                    else
                    {
                        WebObj.Discounts += Math.Abs(DetailsObj.SalePrice);
                    }
                }
                txtWebDiscounts.Text = WebObj.Discounts.ToString();
                DisableForm();
            }
        }

        private void DisableForm()
        {
            IsEdit = true;
            btnCancelInv.Enabled = false;
            btnCancelInv.BackgroundImage = Properties.Resources.cancel_gray;
            txtCustomerID.Enabled = false;
            cmbCustomerName.Enabled = false;
            txtCustomerID.Visible = false;
            cmbCustomerName.Visible = false;
            txtItemBarcode.Enabled = false;
            txtNotes.Enabled = false;
            txtWebDiscounts.Visible = true;
            lblWebDiscounts.Visible = true;
            dgvDeliveryOrder.ReadOnly = true;
            dgvDeliveryOrder.AllowUserToDeleteRows = false;
        }

        private void EnableForm()
        {
            IsEdit = false;
            btnCancelInv.Enabled = true;
            btnCancelInv.BackgroundImage = Properties.Resources.cancel;
            txtCustomerID.Enabled = true;
            cmbCustomerName.Enabled = true;
            txtCustomerID.Visible = true;
            cmbCustomerName.Visible = true;
            txtItemBarcode.Enabled = true;
            txtNotes.Enabled = true;
            txtWebDiscounts.Visible = false;
            lblWebDiscounts.Visible = false;
            dgvDeliveryOrder.ReadOnly = false;
            dgvDeliveryOrder.AllowUserToDeleteRows = true;
        }

        private void dgvWebOrders_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                WebOrders WebObj = (WebOrders)dgvWebOrders.Rows[e.RowIndex].Cells["UncollectedWebObj"].Value;
                if (WebObj.ID != 0)
                {
                    clearData();
                    hID = WebObj.ID;
                    //txtCustomerID.Text = WebObj.CustomerID.ToString();
                    txtInvTotal.Text = WebObj.Total.ToString();
                    txtDueAmount.Text = WebObj.Total.ToString();
                    txtNotes.Text = WebObj.Notes;
                    paymentMethod = WebObj.strPaymentMethod;
                    customerData = WebObj.CustomerData;
                    foreach (WebOrderDetails DetailsObj in WebObj.DetailsLst)
                    {
                        if (DetailsObj.SalePrice >= 0)
                        {
                            if (!DetailsObj.Barcode.Contains("d") && !DetailsObj.Barcode.Contains("D") && !DetailsObj.ItemID.Contains("d") && !DetailsObj.ItemID.Contains("D"))
                            {
                                dgvDeliveryOrder.Rows.Add(DetailsObj.ItemID, DetailsObj.Barcode, DetailsObj.ItemName, DetailsObj.Unit, DetailsObj.SalePrice, DetailsObj.Quantity, DetailsObj.Total);
                            }
                            else
                            {
                                deliveryCharge = DetailsObj.SalePrice;
                            }
                        }
                        else
                        {
                            if (DetailsObj.ItemID == "32494")
                            {
                                WalletValue = Math.Abs(DetailsObj.SalePrice);
                            }
                            else
                            {
                                WebObj.Discounts += Math.Abs(DetailsObj.SalePrice);
                            }
                        }
                    }
                    txtWebDiscounts.Text = WebObj.Discounts.ToString();
                    DisableForm();
                }
            }
        }

        private void tWebOrders_Tick(object sender, EventArgs e)
        {
            LoadWebOrders();
        }

        private void DeliveryOrderScreen_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 48 && e.KeyChar <= 57)
            {
                if (!txtItemBarcode.Focused && !txtCustomerID.Focused)
                {
                    txtItemBarcode.Focus();
                    txtItemBarcode.AppendText(e.KeyChar.ToString());
                }
            }
        }

        private void label22_Click(object sender, EventArgs e)
        {

        }
        //private void dgvDeliveryOrder_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        //{
        //    dgvDeliveryOrder.ClearSelection();
        //    dgvDeliveryOrder.CurrentCell = dgvDeliveryOrder.Rows[e.RowIndex].Cells["ItemQty"];
        //    dgvDeliveryOrder.BeginEdit(true);
        //}
    }
}
