﻿namespace SaudiMarketPOS
{
    partial class CustomerSearsh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAddCustToInv = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.dgvCustSearsh = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtPhoneNumber = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCustName = new System.Windows.Forms.TextBox();
            this.txtCustID = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddNewCust = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.pbImsLogo = new System.Windows.Forms.PictureBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.customerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AccountNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustSearsh)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).BeginInit();
            this.panel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddCustToInv
            // 
            this.btnAddCustToInv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnAddCustToInv.FlatAppearance.BorderSize = 0;
            this.btnAddCustToInv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddCustToInv.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.btnAddCustToInv.ForeColor = System.Drawing.Color.White;
            this.btnAddCustToInv.Location = new System.Drawing.Point(449, 392);
            this.btnAddCustToInv.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddCustToInv.Name = "btnAddCustToInv";
            this.btnAddCustToInv.Size = new System.Drawing.Size(156, 36);
            this.btnAddCustToInv.TabIndex = 4;
            this.btnAddCustToInv.Text = "إضافة العميل الى الفاتورة (F5)";
            this.btnAddCustToInv.UseVisualStyleBackColor = false;
            this.btnAddCustToInv.Click += new System.EventHandler(this.btnAddCustToInv_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(124, 392);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(156, 36);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "إغلاق (ESC)";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dgvCustSearsh
            // 
            this.dgvCustSearsh.AllowUserToAddRows = false;
            this.dgvCustSearsh.AllowUserToDeleteRows = false;
            this.dgvCustSearsh.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCustSearsh.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(88)))), ((int)(((byte)(66)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCustSearsh.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCustSearsh.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustSearsh.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.customerID,
            this.AccountNumber,
            this.customerName,
            this.Address,
            this.PhoneNumber});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvCustSearsh.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvCustSearsh.EnableHeadersVisualStyles = false;
            this.dgvCustSearsh.GridColor = System.Drawing.SystemColors.ActiveCaption;
            this.dgvCustSearsh.Location = new System.Drawing.Point(0, 0);
            this.dgvCustSearsh.Margin = new System.Windows.Forms.Padding(2);
            this.dgvCustSearsh.MultiSelect = false;
            this.dgvCustSearsh.Name = "dgvCustSearsh";
            this.dgvCustSearsh.ReadOnly = true;
            this.dgvCustSearsh.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCustSearsh.RowHeadersVisible = false;
            this.dgvCustSearsh.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCustSearsh.Size = new System.Drawing.Size(712, 242);
            this.dgvCustSearsh.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel1.Controls.Add(this.txtPhoneNumber);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtCustName);
            this.panel1.Controls.Add(this.txtCustID);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(9, 70);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(712, 51);
            this.panel1.TabIndex = 7;
            // 
            // txtPhoneNumber
            // 
            this.txtPhoneNumber.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtPhoneNumber.Location = new System.Drawing.Point(493, 14);
            this.txtPhoneNumber.Margin = new System.Windows.Forms.Padding(2);
            this.txtPhoneNumber.Name = "txtPhoneNumber";
            this.txtPhoneNumber.Size = new System.Drawing.Size(127, 26);
            this.txtPhoneNumber.TabIndex = 0;
            this.txtPhoneNumber.TextChanged += new System.EventHandler(this.txtPhoneNumber_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(624, 17);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "رقم التليفون";
            // 
            // txtCustName
            // 
            this.txtCustName.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtCustName.Location = new System.Drawing.Point(260, 14);
            this.txtCustName.Margin = new System.Windows.Forms.Padding(2);
            this.txtCustName.Name = "txtCustName";
            this.txtCustName.Size = new System.Drawing.Size(127, 26);
            this.txtCustName.TabIndex = 1;
            this.txtCustName.TextChanged += new System.EventHandler(this.txtCustName_TextChanged);
            // 
            // txtCustID
            // 
            this.txtCustID.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.txtCustID.Location = new System.Drawing.Point(29, 14);
            this.txtCustID.Margin = new System.Windows.Forms.Padding(2);
            this.txtCustID.Name = "txtCustID";
            this.txtCustID.Size = new System.Drawing.Size(127, 26);
            this.txtCustID.TabIndex = 2;
            this.txtCustID.TextChanged += new System.EventHandler(this.txtCustID_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(161, 17);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 15);
            this.label3.TabIndex = 1;
            this.label3.Text = "كود العميل";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(393, 17);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "اسم العميل";
            // 
            // btnAddNewCust
            // 
            this.btnAddNewCust.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnAddNewCust.FlatAppearance.BorderSize = 0;
            this.btnAddNewCust.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddNewCust.Font = new System.Drawing.Font("STC Bold", 8F, System.Drawing.FontStyle.Bold);
            this.btnAddNewCust.ForeColor = System.Drawing.Color.White;
            this.btnAddNewCust.Location = new System.Drawing.Point(288, 392);
            this.btnAddNewCust.Margin = new System.Windows.Forms.Padding(2);
            this.btnAddNewCust.Name = "btnAddNewCust";
            this.btnAddNewCust.Size = new System.Drawing.Size(156, 36);
            this.btnAddNewCust.TabIndex = 5;
            this.btnAddNewCust.Text = "إضافة عميل جديد";
            this.btnAddNewCust.UseVisualStyleBackColor = false;
            this.btnAddNewCust.Click += new System.EventHandler(this.btnAddNewCust_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.panel2.Controls.Add(this.pbImsLogo);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(731, 52);
            this.panel2.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("STC Bold", 18F, System.Drawing.FontStyle.Bold);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(288, 9);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 32);
            this.label5.TabIndex = 0;
            this.label5.Text = "البحث عن عميل";
            // 
            // pbImsLogo
            // 
            this.pbImsLogo.ErrorImage = null;
            this.pbImsLogo.Location = new System.Drawing.Point(12, 7);
            this.pbImsLogo.Name = "pbImsLogo";
            this.pbImsLogo.Size = new System.Drawing.Size(90, 37);
            this.pbImsLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImsLogo.TabIndex = 15;
            this.pbImsLogo.TabStop = false;
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.dgvCustSearsh);
            this.panel8.Font = new System.Drawing.Font("STC Bold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel8.Location = new System.Drawing.Point(9, 134);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(712, 242);
            this.panel8.TabIndex = 50;
            // 
            // customerID
            // 
            this.customerID.DataPropertyName = "ID";
            this.customerID.HeaderText = "كود العميل";
            this.customerID.Name = "customerID";
            this.customerID.ReadOnly = true;
            this.customerID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // AccountNumber
            // 
            this.AccountNumber.DataPropertyName = "AccountNumber";
            this.AccountNumber.HeaderText = "رقم الحساب";
            this.AccountNumber.Name = "AccountNumber";
            this.AccountNumber.ReadOnly = true;
            this.AccountNumber.Visible = false;
            // 
            // customerName
            // 
            this.customerName.DataPropertyName = "Name";
            this.customerName.HeaderText = "اسم العميل";
            this.customerName.Name = "customerName";
            this.customerName.ReadOnly = true;
            // 
            // Address
            // 
            this.Address.DataPropertyName = "Address1";
            dataGridViewCellStyle2.Format = "N2";
            dataGridViewCellStyle2.NullValue = null;
            this.Address.DefaultCellStyle = dataGridViewCellStyle2;
            this.Address.FillWeight = 300F;
            this.Address.HeaderText = "العنوان";
            this.Address.Name = "Address";
            this.Address.ReadOnly = true;
            // 
            // PhoneNumber
            // 
            this.PhoneNumber.DataPropertyName = "PhoneNumber";
            this.PhoneNumber.HeaderText = "رقم التليفون";
            this.PhoneNumber.Name = "PhoneNumber";
            this.PhoneNumber.ReadOnly = true;
            // 
            // CustomerSearsh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 466);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnAddNewCust);
            this.Controls.Add(this.btnAddCustToInv);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel8);
            this.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CustomerSearsh";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "البحث عن العملاء";
            this.Activated += new System.EventHandler(this.CustomerSearsh_Activated);
            this.Load += new System.EventHandler(this.CustomerSearsh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustSearsh)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImsLogo)).EndInit();
            this.panel8.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnAddCustToInv;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.DataGridView dgvCustSearsh;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtPhoneNumber;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCustName;
        private System.Windows.Forms.TextBox txtCustID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAddNewCust;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pbImsLogo;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn AccountNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn Address;
        private System.Windows.Forms.DataGridViewTextBoxColumn PhoneNumber;
    }
}