﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SaudiMarketPOS.Properties;

namespace SaudiMarketPOS
{
    public partial class CustomerSearsh : Form
    {
        private Form parentForm;// = new TransactionScreen();

        public CustomerSearsh()
        {
            InitializeComponent();
        }
        public CustomerSearsh(Form parentForm)
            : this()
        {
            this.parentForm = parentForm;
        }
        private void SetKeyDownEvent(Control ctlr)
        {
            ctlr.KeyDown += ctl_KeyDown;
            if (ctlr.HasChildren)
                foreach (Control c in ctlr.Controls)
                    this.SetKeyDownEvent(c);
        }
        void ctl_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.F6)
            //{
            //    ItemSearsh itms = new ItemSearsh(this);
            //    itms.ShowDialog(this);
            //    this.dgvTransactions.Focus();
            //}
            //if (e.KeyCode == Keys.F7)
            //{
            //    CustomerSearsh custSearsh = new CustomerSearsh(this);
            //    custSearsh.ShowDialog(this);
            //    this.txtItemBarcode.Focus();
            //}
            if (e.KeyCode == Keys.F5)
            {
                this.btnAddCustToInv_Click(this.btnAddCustToInv, new EventArgs());
            }
            if (e.KeyCode == Keys.Escape)
            {
                this.btnClose_Click(this.btnClose, new EventArgs());
            }
        }
        
        SqlConnection con = new SqlConnection();
        DataTable custDT = new DataTable();

        string CustID;
        private void CustomerSearsh_Load(object sender, EventArgs e)
        {
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            try
            {
                this.SetKeyDownEvent(this);
                con.ConnectionString = Program.conString;
                //con.ConnectionString = Settings.Default.ConnectionString;
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "uspSelectCustomers";
                cmd.Connection = con;
                if (con.State == ConnectionState.Closed) con.Open();
                custDT.Load(cmd.ExecuteReader());
                dgvCustSearsh.DataSource = custDT;
                if (con.State == ConnectionState.Open) con.Close();
                dgvCustSearsh.Columns["customerID"].Width = 100;
                dgvCustSearsh.Columns["customerName"].Width = 160;
                dgvCustSearsh.Columns["Address"].Width = 300;
                dgvCustSearsh.Columns["PhoneNumber"].Width = 100;
                txtPhoneNumber.Focus();
            }
            catch
            { }
        }

        private void txtPhoneNumber_TextChanged(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmdFind = new SqlCommand();
                cmdFind.CommandType = CommandType.StoredProcedure;
                cmdFind.CommandText = "uspFindCustByPhone";
                cmdFind.Parameters.AddWithValue("@PhoneNumber", txtPhoneNumber.Text);
                cmdFind.Connection = con;
                DataTable dtFind = new DataTable();
                if (con.State == ConnectionState.Closed) con.Open();
                dtFind.Load(cmdFind.ExecuteReader());
                dgvCustSearsh.DataSource = dtFind;
                if (con.State == ConnectionState.Open) con.Close();
            }
            catch (Exception ex)
            { }
        }

        private void txtCustID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmdFind = new SqlCommand();
                cmdFind.CommandType = CommandType.StoredProcedure;
                cmdFind.CommandText = "uspFindCustByID";
                cmdFind.Parameters.AddWithValue("@ID", int.Parse(txtCustID.Text));
                cmdFind.Connection = con;
                DataTable dtFind = new DataTable();
                if (con.State == ConnectionState.Closed) con.Open();
                dtFind.Load(cmdFind.ExecuteReader());
                dgvCustSearsh.DataSource = dtFind;
                if (con.State == ConnectionState.Open) con.Close();
            }
            catch (Exception ex)
            { }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCustName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                SqlCommand cmdFind = new SqlCommand();
                cmdFind.CommandType = CommandType.StoredProcedure;
                cmdFind.CommandText = "uspFindCustByName";
                cmdFind.Parameters.AddWithValue("@Name", txtCustName.Text);
                cmdFind.Connection = con;
                DataTable dtFind = new DataTable();
                if (con.State == ConnectionState.Closed) con.Open();
                dtFind.Load(cmdFind.ExecuteReader());
                dgvCustSearsh.DataSource = dtFind;
                if (con.State == ConnectionState.Open) con.Close();
            }
            catch (Exception ex)
            { }

        }

        private void btnAddNewCust_Click(object sender, EventArgs e)
        {
            try
            {
                CustomerAdd custAdd = new CustomerAdd();
                custAdd.ShowDialog();
            }
            catch (Exception ex)
            { }
        }

        private void btnAddCustToInv_Click(object sender, EventArgs e)
        {
            try
            {
            CustID = dgvCustSearsh.CurrentRow.Cells["customerID"].Value.ToString();
                if (CustID != null && CustID != "")
                {
                    if (this.parentForm is TransactionScreen)
                        ((TransactionScreen)this.parentForm).txtCustomerID.Text = CustID;
                    else if (this.parentForm is DeliveryOrderScreen)
                        ((DeliveryOrderScreen)this.parentForm).txtCustomerID.Text = CustID;

                }
                this.Close();
            }
            catch (Exception ex)
            { }
        }

        private void CustomerSearsh_Activated(object sender, EventArgs e)
        {
            txtPhoneNumber.Focus();
        }

        //private void dgvCustSearsh_Leave(object sender, EventArgs e)
        //{
        //    CustID = dgvCustSearsh.CurrentRow.Cells["AccountNumber"].Value.ToString();
        //}
    }
}
