﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace SaudiMarketPOS
{
    public partial class CreditCardTransaction : Form
    {
        public decimal Value = 0;
        public string TransNo = "";
        public decimal VisaValue = 0;
        public decimal CashValue = 0;
        public DialogResult Result = DialogResult.Cancel;

        public CreditCardTransaction()
        {
            InitializeComponent();
        }
        SqlConnection con = new SqlConnection();
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void SetKeyDownEvent(Control ctlr)
        {
            ctlr.KeyDown += ctl_KeyDown;
            if (ctlr.HasChildren)
                foreach (Control c in ctlr.Controls)
                    this.SetKeyDownEvent(c);
        }
        void ctl_KeyDown(object sender, KeyEventArgs e)
        {            
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
            
        }
        private void CreditCardTransaction_Load(object sender, EventArgs e)
        {
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            try
            {
                this.SetKeyDownEvent(this);
                con.ConnectionString = Program.conString;
                lblDateTime.Text = DateTime.Now.ToString();

                lblCashir.Text = Program.fristName.ToString() + " " + Program.lastName.ToString();
                txtInvNumber.Focus();

                txtVisaAmount.Text = Value.ToString();
                txtVisaAmount.Focus();
                //SqlCommand cmdFillCustName = new SqlCommand();
                //cmdFillCustName.CommandType = CommandType.StoredProcedure;
                //cmdFillCustName.CommandText = "uspSelectCustomer";
                //cmdFillCustName.Connection = con;
                //if (con.State == ConnectionState.Closed) con.Open();

                //DataTable dtCustName = new DataTable();
                //dtCustName.Load(cmdFillCustName.ExecuteReader());
                //cmbCustomerName.DataSource = dtCustName;
                //cmbCustomerName.ValueMember = "ID";
                //cmbCustomerName.DisplayMember = "Name";
            }
            catch (Exception ex)
            { }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDateTime.Text = DateTime.Now.ToString();
        }
        private void clearData()
        {
            //cmbCustomerName.SelectedIndex = 0;
            txtVisaAmount.Text = "0.00";
            txtCreditTransNumber.Text = "";
            txtNotes.Text = "";
            //cmbCustomerName.Focus();
        }
        private void selectTxt(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).SelectionStart = 0;
                ((TextBox)sender).SelectionLength = ((TextBox)sender).Text.Length;
            }
            catch { }

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (txtInvNumber.Text.Trim() == "")
                //{
                //    txtInvNumber.Focus();
                //    return; 
                //}

                if (txtVisaAmount.Text == "" || txtCreditTransNumber.Text == "" || decimal.Parse(txtVisaAmount.Text) <= 0)
                {
                    MessageBox.Show("لابد من استكمال جميع البيانات", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    txtCreditTransNumber.Focus();
                    return;
                }
                else
                {
                    decimal TotalPaidValue = decimal.Parse(txtVisaAmount.Text.ToString()) + decimal.Parse(txtCashAmount.Text.ToString());
                    if (Value > TotalPaidValue)
                    {
                        MessageBox.Show("القيمة المدفوعة أقل من قيمة الفاتورة");
                    }
                    else
                    {
                        //SqlCommand cmdSaveTrans = new SqlCommand();
                        //cmdSaveTrans.Connection = con;
                        //cmdSaveTrans.CommandType = CommandType.StoredProcedure;
                        //cmdSaveTrans.CommandText = "uspCreditCardTransactionInsert";
                        //cmdSaveTrans.Parameters.AddWithValue("@TransactionNumber", txtCreditTransNumber.Text.Trim());
                        //cmdSaveTrans.Parameters.AddWithValue("@TransactionAmount", txtAmount.Text.Trim());
                        //cmdSaveTrans.Parameters.AddWithValue("@CreatedTime", DateTime.Now);
                        //cmdSaveTrans.Parameters.AddWithValue("@Notes", txtNotes.Text.Trim());
                        //cmdSaveTrans.Parameters.AddWithValue("@UserID", Program.empID);
                        //cmdSaveTrans.Parameters.AddWithValue("@InvNumber",txtInvNumber.Text.Trim());
                        ////cmdSaveTrans.Parameters.AddWithValue("@CustomerID", cmbCustomerName.SelectedValue);
                        //if (con.State == ConnectionState.Closed) con.Open();
                        //cmdSaveTrans.ExecuteNonQuery();
                        //if (con.State == ConnectionState.Open) con.Close();
                        //clearData();
                        //MessageBox.Show("تم التسجيل بنجاح", "تأكيد", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                        VisaValue = decimal.Parse(txtVisaAmount.Text);
                        CashValue = decimal.Parse(txtCashAmount.Text);
                        TransNo = txtCreditTransNumber.Text;
                        Result = DialogResult.OK;
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            { }

        }

        private void cmbCustomerName_Leave(object sender, EventArgs e)
        {
            //try
            //{
            //    if (cmbCustomerName.Text == "")
            //    { 
            //       return;
            //    }
            //    else
            //    {
            //        DataTable tbSP = ((DataTable)cmbCustomerName.DataSource).Copy();
            //        tbSP.DefaultView.RowFilter = "Name='" + cmbCustomerName.Text + "'";
            //        if (tbSP.DefaultView.Count == 0)
            //        {
            //            MessageBox.Show("اسم العميل غير صحيح", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            cmbCustomerName.Text = "";
            //            cmbCustomerName.Focus();
            //        }
            //        tbSP.DefaultView.RowFilter = "";
            //    }
            //}
            //catch { }
        }

        private void txtCreditTransNumber_TextChanged(object sender, EventArgs e)
        {
            if (txtCreditTransNumber.Text == "") return;
            try
            {
                Math.Round(decimal.Parse(txtCreditTransNumber.Text), 2);
            }
            catch
            {
                MessageBox.Show("ادخل ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error,MessageBoxDefaultButton.Button1,MessageBoxOptions.RightAlign );
                txtCreditTransNumber.Focus();
                txtCreditTransNumber.Clear();
            }
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            if (txtVisaAmount.Text == "") return;
            try
            {
                Math.Round(decimal.Parse(txtVisaAmount.Text), 2);
            }
            catch
            {
                MessageBox.Show("ادخل ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error,MessageBoxDefaultButton.Button1,MessageBoxOptions.RightAlign );
                txtVisaAmount.Focus();
                txtVisaAmount.Clear();
            }
        }

        private void txtAmount_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtVisaAmount.Text == "")
                {
                    txtVisaAmount.Text = "0.00";
                }

                decimal am = decimal.Parse(txtVisaAmount.Text.Trim());
                if (am <= 0)
                {
                    MessageBox.Show("قيمة غير صحيحة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    txtVisaAmount.Text = "0.00";
                }
            }
            catch(Exception ex) 
            {
            }
            
        }

        private void btnGetInvoice_Click(object sender, EventArgs e)
        {
            
        }

        private void txtInvNumber_Leave(object sender, EventArgs e)
        {
            try
            {
                if (this.ActiveControl == btnClose) return;
                if (this.ActiveControl == btnSave && txtInvNumber.Text.Trim() == "") return;

                //check the invoice if exists 
                SqlCommand cmdCheckInvoice = new SqlCommand();
                cmdCheckInvoice.Connection = con;
                cmdCheckInvoice.CommandType = CommandType.StoredProcedure;
                cmdCheckInvoice.CommandText = "usp_CheckInv";
                cmdCheckInvoice.Parameters.AddWithValue("@InvID", int.Parse(txtInvNumber.Text.Trim()));

                if (con.State == ConnectionState.Closed) con.Open();
                SqlDataReader dr = cmdCheckInvoice.ExecuteReader();
                if (dr.HasRows == true)
                {
                    txtInvNumber.Text = "";
                    MessageBox.Show("هذا البون قد تم تسجيل عملية بطاقة ائتمان له من قبل", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    dr.Close();
                    txtInvNumber.Focus();
                    return;
                }
                else
                {
                    dr.Close();
                }

                //Get Invoice Data
                SqlCommand cmdGetInvData = new SqlCommand();
                cmdGetInvData.Connection = con;
                cmdGetInvData.CommandType = CommandType.StoredProcedure;
                cmdGetInvData.CommandText = "usp_GetInvData";
                cmdGetInvData.Parameters.Add("@InvID", int.Parse(txtInvNumber.Text.Trim()));

                if (con.State == ConnectionState.Closed) con.Open();
                SqlDataReader readInvoiceData = cmdGetInvData.ExecuteReader();
                if (readInvoiceData.HasRows == false)
                {
                    txtInvNumber.Text = "";
                    MessageBox.Show("لا يوجد بون بهذا الرقم", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    readInvoiceData.Close();
                    txtInvNumber.Focus();
                    return;
                }
                else
                {
                    while (readInvoiceData.Read())
                    {
                        txtVisaAmount.Text = readInvoiceData[1].ToString();
                    }
                    readInvoiceData.Close();
                    label3.Visible = true;
                    label6.Visible = true;
                    label5.Visible = true;
                    txtInvNumber.Enabled = false;
                    txtVisaAmount.Visible = true;
                    txtCreditTransNumber.Visible = true;
                    txtNotes.Visible = true;
                    txtCreditTransNumber.Focus();
                }
            }
            catch (Exception ex)
            { }
            
        }

        private void txtInvNumber_TextChanged(object sender, EventArgs e)
        {
            if (txtInvNumber.Text == "") return;
            try
            {
                int.Parse(txtInvNumber.Text.Trim());
            }
            catch
            {
                MessageBox.Show("ادخل ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                txtInvNumber.Focus();
                txtInvNumber.Clear();
                selectTxt(this, e);
            }
        }

        private void CreditCardTransaction_Activated(object sender, EventArgs e)
        {
            txtInvNumber.Focus();
        }

        
    }
}
