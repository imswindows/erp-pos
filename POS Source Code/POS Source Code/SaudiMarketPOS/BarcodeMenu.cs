﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SaudiMarketPOS
{
    public partial class BarcodeMenu : Form
    {
        public BarcodeMenu()
        {
            InitializeComponent();
        }

        private void btnManual_Click(object sender, EventArgs e)
        {
            BarcodePrinting frm = new BarcodePrinting();
            frm.ShowDialog();
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            BarcodeAuto frm = new BarcodeAuto();
            frm.ShowDialog();
        }
    }
}
