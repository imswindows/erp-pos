﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using SaudiMarketPOS.Properties;
using System.Collections;
using ReceiptCreator;

namespace SaudiMarketPOS
{
    public partial class ReturnTransactionWindow : Form
    {
        public ReturnTransactionWindow()
        {
            InitializeComponent();
        }
        string itemID = string.Empty;
        string itemBarcode = string.Empty;
        string itemName = string.Empty;
        string itemUnit = string.Empty;
        string itemPrice = string.Empty;
        decimal itemQty;
        decimal itemTotal;
        decimal cashDiscount;
        decimal discountPercent;
        decimal invTotal;
        decimal paidCash;
        decimal dueAmount;
        decimal change;
        int invID;
        int hID = 0;
        decimal retTotal;
        SqlConnection con = new SqlConnection();
        int userID = 0;
        int customerID = 0;
        private void ReturnTransactionWindow_Load(object sender, EventArgs e)
        {
            string apppath = AppDomain.CurrentDomain.BaseDirectory;
            pbImsLogo.Image = Image.FromFile(apppath + "Icon.ico");
            try
            {
                //con.ConnectionString = Settings.Default.ConnectionString;
                con.ConnectionString = Program.conString;
                lblCashir.Text = Program.fristName + " " + Program.lastName;
                //this.txtMainSafe.Text = Settings.Default.SafeNumber.ToString();
                this.txtMainSafe.Text = Program.safeNumber.ToString();
                btnPrint.Enabled = false;
                dgvInvoiceItems.AllowUserToAddRows = false;
                this.SetKeyDownEvent(this);
                //dgvInvoiceItems.Height = 190;
                //dgvReturnItem.Height = 190;
            }
            catch (Exception ex)
            { }
        }

        private void LoadOrderData()
        {
            try
            {
                dgvInvoiceItems.DataSource = null;
                dgvInvoiceItems.Rows.Clear();
                dgvInvoiceItems.Columns.Clear();
                if ((txtTransactionID.Text.Length == 0)) return;
                SqlCommand com = new SqlCommand();
                com.CommandType = CommandType.StoredProcedure;
                com.Connection = con;
                com.CommandText = "uspSelectTransactionH";
                com.Parameters.AddWithValue("@ID", Convert.ToInt32(txtTransactionID.Text));
                if (con.State == ConnectionState.Closed) con.Open();
                SqlDataReader dr = com.ExecuteReader();
                if (dr.HasRows == false)
                {
                    txtTransactionID.Text = "";
                    MessageBox.Show(" لا يوجد بون بهذا الرقم أو قد يكون تم ارجاعه من قبل", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    dr.Close();
                    txtTransactionID.Focus();
                    return;
                }

                while (dr.Read())
                {
                    txtDate.Text = dr[14].ToString();
                    txtSafeNumber.Text = dr[15].ToString();
                    txtInvTotal.Text = Math.Round(decimal.Parse(dr[2].ToString()), 2).ToString();
                    txtDiscountPercent.Text = Math.Round(decimal.Parse(dr[5].ToString()), 2).ToString();
                    txtCashDiscount.Text = Math.Round(decimal.Parse(dr[6].ToString()), 2).ToString();
                    //MessageBox.Show(dr[13].ToString());
                    //bool  Cre = bool.Parse(dr[13].ToString());
                    //if (Cre == true )
                    //{ chkCredit.Checked = true; }
                    //if (Cre == false)
                    //{ chkCredit.Checked = false; }
                    txtDueAmount.Text = Math.Round(decimal.Parse(dr[9].ToString()), 2).ToString();
                    txtReturnTotal.Text = "0.00";// Math.Round(decimal.Parse(dr[9].ToString()), 2).ToString();
                    txtPaid.Text = Math.Round(decimal.Parse(dr[10].ToString()), 2).ToString();
                    txtChange.Text = Math.Round(decimal.Parse(dr[11].ToString()), 2).ToString();
                    userID = int.Parse(dr[4].ToString());
                    customerID = int.Parse(dr[1].ToString());
                }
                dr.Close();
                //---------------------------------------
                dgvInvoiceItemsAddingClo();

                SqlCommand detailsCmd = new SqlCommand();
                detailsCmd.Connection = con;
                detailsCmd.CommandType = CommandType.StoredProcedure;
                detailsCmd.CommandText = "usp_GetTransactionDetails";
                detailsCmd.Parameters.AddWithValue("@TransactionID", txtTransactionID.Text.ToString().Trim());

                if (con.State == ConnectionState.Closed) con.Open();
                DataTable salesD = new DataTable();
                salesD.Load(detailsCmd.ExecuteReader());
                dgvInvoiceItems.AutoGenerateColumns = false;
                dgvInvoiceItems.DataSource = salesD;
                List<int> RemovedRows = new List<int>();
                foreach (DataGridViewRow dgvr in dgvInvoiceItems.Rows)
                {
                    if (dgvr.Cells["ItemID"].Value.ToString().Contains("d0") || dgvr.Cells["ItemID"].Value.ToString().Contains("D0"))
                    {
                        RemovedRows.Add(dgvr.Index);
                    }
                    else if(decimal.Parse(dgvr.Cells["ItemPrice"].Value.ToString()) <= 0)
                    {
                        RemovedRows.Add(dgvr.Index);
                    }
                }
                //foreach(int RowIndex in RemovedRows)
                //{
                //    dgvInvoiceItems.Rows[RowIndex].Visible = false;
                //}
                //-------------------------------------------
                SqlCommand getUserName = new SqlCommand();
                getUserName.Connection = con;
                getUserName.CommandType = CommandType.StoredProcedure;
                getUserName.CommandText = "uspUserSelect";
                getUserName.Parameters.AddWithValue("@ID", userID);
                SqlDataReader readUser;
                readUser = getUserName.ExecuteReader();
                while (readUser.Read())
                {
                    txtCashierName.Text = readUser[0].ToString();
                }
                readUser.Close();

                SqlCommand getCustName = new SqlCommand();
                getCustName.Connection = con;
                getCustName.CommandType = CommandType.StoredProcedure;
                getCustName.CommandText = "uspCustomerGet";
                getCustName.Parameters.AddWithValue("@ID", customerID);
                SqlDataReader readCust;
                readCust = getCustName.ExecuteReader();
                while (readCust.Read())
                {
                    txtCustmoerName.Text = readCust[0].ToString();
                }
                readCust.Close();

                if (con.State == ConnectionState.Open) con.Close();
                dgvInvoiceItems.Columns.Add("ReturnQty", "الكمية المرتجعة");
                dgvInvoiceItems.Columns["ReturnQty"].Width = 80;

                dgvInvoiceItems.Columns.Add("Total", "الإجمالي");
                dgvInvoiceItems.Columns["Total"].ReadOnly = true;
                dgvInvoiceItems.Columns["Total"].Width = 80;
                int rowCount = dgvInvoiceItems.RowCount;
                for (int dgvr = 0; dgvr < rowCount; dgvr++)
                {
                    //decimal itemUnit = decimal.Parse(this.dgvInvoiceItems.Rows[dgvr].Cells["ItemQty"].Value.ToString());
                    //decimal itemPrice = decimal.Parse(this.dgvInvoiceItems.Rows[dgvr].Cells["ItemPrice"].Value.ToString());
                    //decimal itemTotal = itemUnit * itemPrice;
                    //this.dgvInvoiceItems.Rows[dgvr].Cells["Total"].Value = itemTotal.ToString();
                    this.dgvInvoiceItems.Rows[dgvr].Cells["ReturnQty"].Value = "0";
                    this.dgvInvoiceItems.Rows[dgvr].Cells["Total"].Value = "0.00";
                }
                if (con.State == ConnectionState.Open) con.Close();
                //dgvInvoiceItems.Focus();
                //dgvReturnItemAddingCol();

                //SqlCommand retDetailsCmd = new SqlCommand();
                //retDetailsCmd.Connection = con;
                //retDetailsCmd.CommandType = CommandType.StoredProcedure;
                //retDetailsCmd.CommandText = "usp_GetTransactionDetails";
                //retDetailsCmd.Parameters.AddWithValue("@TransactionID", txtTransactionID.Text.ToString().Trim());
                txtTransactionID.Enabled = false;

                btnPrint.Enabled = true;

                //dgvInvoiceItems.Focus();
                //calculateInvoiceTotal();
                //CalcudalteDueAmount();
                //calculateChange();
                foreach(DataGridViewColumn col in dgvInvoiceItems.Columns)
                {
                    col.SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }
            catch (Exception ex)
            { }
        }

        private void txtTransactionID_Leave(object sender, EventArgs e)
        {
            LoadOrderData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                //MessageBox.Show(dgvReturnTransaction.Height.ToString());
                this.Close();
                
                //MainMenu mn = new MainMenu();
                //mn.Show();
            }
            catch (Exception ex)
            { }
        }

        private void selectTxt(object sender, EventArgs e)
        {
            try
            {
                ((TextBox)sender).SelectionStart = 0;
                ((TextBox)sender).SelectionLength = ((TextBox)sender).Text.Length;
            }
            catch { }

        }

        private void calculateReturnTotal()
        {
            retTotal = 0;
            decimal CashDis = decimal.Parse(txtCashDiscount.Text);
            decimal PerDis = decimal.Parse(txtDiscountPercent.Text);
            decimal InvTotal = decimal.Parse(txtInvTotal.Text);

            foreach (DataGridViewRow totDr in dgvInvoiceItems.Rows)
            {
                // حساب الاجمالى
                if (totDr.Cells["Total"].Value != null && totDr.Cells["Total"].Value.ToString() != "")
                {
                    retTotal += Convert.ToDecimal(totDr.Cells["Total"].Value);
                }
            }
            if (CashDis > 0)
            {
                decimal Percentage = CashDis / InvTotal;
                retTotal = retTotal - (retTotal * (Percentage / 100));
            }
            else if (PerDis > 0)
            {
                retTotal = retTotal - (retTotal * (PerDis / 100));
            }
            txtReturnTotal.Text = Math.Round(retTotal, 2).ToString();
        }
       
        //private void CalcudalteDueAmount()
        //{
        //    decimal totalDiscount = 0.0m;
        //    cashDiscount = decimal.Parse(txtCashDiscount.Text.ToString());
        //    totalDiscount = cashDiscount;
        //    discountPercent = decimal.Parse(txtDiscountPercent.Text.Trim());
        //    if (discountPercent != 0)
        //    {
        //        discountPercent = discountPercent / 100.0m;
        //        decimal calcDiscountPercent = invTotal * discountPercent;
        //        totalDiscount += calcDiscountPercent;
        //    }
        //    txtDueAmount.Text = (invTotal - totalDiscount).ToString();
        //}
        //private void calculateChange()
        //{
        //    if (paidCash == 0) return;
        //    change = paidCash - dueAmount;
        //    txtChange.Text = change.ToString();
        //}

        private void txtDiscountPercent_Leave(object sender, EventArgs e)
        {
            //if (txtDiscountPercent.Text.Trim() == "")
            //{
            //    txtDiscountPercent.Text = "0,00";
            //}
            //discountPercent = decimal.Parse(txtDiscountPercent.Text.Trim());
            //if (discountPercent == 0)
            //{ return; }

            //else if (discountPercent < 0 || discountPercent > 100)
            //{
            //    MessageBox.Show("قيمة غير مناسبة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    txtDiscountPercent.Text = "0,00";
            //    txtDiscountPercent.Focus();
            //    selectTxt(this, e);
            //    return;
            //}
            //else
            //{
            //    CalcudalteDueAmount();
            //}
            //calculateChange();
        }

        private void txtDiscountPercent_TextChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    decimal.Parse(txtDiscountPercent.Text.Trim());
            //}
            //catch
            //{
            //    MessageBox.Show("ادخل ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    txtDiscountPercent.Text = "0,00";
            //    txtDiscountPercent.Focus();
            //    selectTxt(this, e);
            //}
        }

        private void txtCashDiscount_Leave(object sender, EventArgs e)
        {
            //if (txtCashDiscount.Text == "")
            //{
            //    txtCashDiscount.Text = "0,00";
            //}
            //cashDiscount = decimal.Parse(txtCashDiscount.Text.Trim());
            //if (cashDiscount < 0 || cashDiscount > invTotal)
            //{
            //    MessageBox.Show("قيمة غير مناسبة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    txtCashDiscount.Text = "0,00";
            //    txtCashDiscount.Focus();
            //    selectTxt(this, e);
            //}
            //else
            //{
            //    CalcudalteDueAmount();
            //}
            //calculateChange();
        }

        private void txtCashDiscount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                cashDiscount = decimal.Parse(txtCashDiscount.Text.ToString());
            }
            catch
            {
                MessageBox.Show("يجب ادخال ارقام فقط");
                txtCashDiscount.Text = "0,00";
                txtCashDiscount.Focus();
                selectTxt(this, e);
            }
        }

        private void txtDueAmount_TextChanged(object sender, EventArgs e)
        {
            //dueAmount = decimal.Parse(txtDueAmount.Text);
        }

        private void txtPaid_TextChanged(object sender, EventArgs e)
        {
            if (txtPaid.Text == "") return;
            try
            {
                paidCash = decimal.Parse(txtPaid.Text);
            }
            catch
            {
                MessageBox.Show("ادخل ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPaid.Focus();
                txtPaid.Clear();
                selectTxt(this, e);
            }
        }

        private void txtPaid_Leave(object sender, EventArgs e)
        {
            //if (txtPaid.Text == "")
            //{
            //    txtPaid.Text = "0.00";
            //    MessageBox.Show("لا يمكن ترك القيمة فارغة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    txtPaid.Focus();
            //    selectTxt(this, e);
            //    return;
            //}

            //if (paidCash < dueAmount)
            //{
            //    MessageBox.Show("المبلغ غير صحيح", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    txtPaid.Clear();
            //    txtPaid.Focus();
            //}
            //else
            //{
            //    calculateChange();
            //}
        }

        private void cancelInv()
        {
            SqlCommand cancelInvCmd = new SqlCommand();
            cancelInvCmd.Connection = con;
            cancelInvCmd.CommandType = CommandType.StoredProcedure;
            cancelInvCmd.CommandText = "uspTransactionTableUpdate";
            cancelInvCmd.Parameters.AddWithValue("@ID", txtTransactionID.Text.Trim());
            if (con.State == ConnectionState.Closed) con.Open();
            cancelInvCmd.ExecuteNonQuery();
            if (con.State == ConnectionState.Open) con.Close();
        }
        private void clearData()
        {
            dgvInvoiceItems.DataSource = new DataTable();
            txtInvTotal.Text = "0,00";
            txtDiscountPercent.Text = "0,00";
            txtCashDiscount.Text = "0,00";
            
            txtDueAmount.Text = "0,00";
            txtPaid.Text = "0,00";
            txtChange.Text = "0,00";
            itemQty = 0;
            itemTotal = 0;
            cashDiscount = 0;
            discountPercent = 0;
            invTotal = 0;
            paidCash = 0;
            dueAmount = 0;
            change = 0;
            
            dgvInvoiceItems.Columns.Clear();
            txtTransactionID.Enabled = true;
            txtTransactionID.Clear();
            txtTransactionID.Focus();
            txtDate.Clear();
            txtReturnTotal.Clear();
            btnPrint.Enabled = false;
        }
        private void SetKeyDownEvent(Control ctlr)
        {
            ctlr.KeyDown += ctl_KeyDown;
            if (ctlr.HasChildren)
                foreach (Control c in ctlr.Controls)
                    this.SetKeyDownEvent(c);
        }
        void ctl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F9)
            {
                if (dgvInvoiceItems.Rows.Count == 0) return;
                dgvInvoiceItems.ClearSelection();
                dgvInvoiceItems.CurrentCell = dgvInvoiceItems.Rows[0].Cells["ItemQty"];
                dgvInvoiceItems.BeginEdit(true);
            }
            
            if (e.KeyCode == Keys.F8)
            {
                calculateReturnTotal();
                this.button2_Click(this.btnPrint, new EventArgs());
            }
            if (e.KeyCode == Keys.D && e.Modifiers == Keys.Alt)
            {
                dgvInvoiceItems .ClearSelection();
                dgvInvoiceItems.CurrentCell = dgvInvoiceItems.Rows[dgvInvoiceItems.Rows.Count - 1].Cells["ItemQty"];
                dgvInvoiceItems.Rows[dgvInvoiceItems.Rows.Count - 1].Selected = true;
                dgvInvoiceItems.Focus();
            }
        }
        private void btnCancelInv_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (decimal.Parse(txtReturnTotal.Text) == 0)
                {
                    MessageBox.Show("يجب تحديد الكميات المرتجعه", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Hand,MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                    return;
                }
                if (MessageBox.Show(string.Format("اجمالي المرتجعات : {1}{0}", Environment.NewLine, txtReturnTotal.Text.ToString()), "تأكيد", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.RightAlign) == DialogResult.No)
                {
                    return;
                }
                else
                {
                    SqlCommand insertRetTransactionCmd = new SqlCommand();

                    insertRetTransactionCmd.Connection = con;
                    insertRetTransactionCmd.CommandType = CommandType.StoredProcedure;
                    insertRetTransactionCmd.CommandText = "uspReturnTransactionHInsert";
                    insertRetTransactionCmd.Parameters.AddWithValue("@TransactionID", txtTransactionID.Text.Trim());
                    insertRetTransactionCmd.Parameters.AddWithValue("@TotalReturn", txtReturnTotal.Text.Trim());
                    insertRetTransactionCmd.Parameters.AddWithValue("@UserID", Program.empID);
                    insertRetTransactionCmd.Parameters.AddWithValue("@DueAmount", DBNull.Value);
                    insertRetTransactionCmd.Parameters.AddWithValue("@DiscountPercent", DBNull.Value);
                    insertRetTransactionCmd.Parameters.AddWithValue("@CashDiscount", DBNull.Value);
                    insertRetTransactionCmd.Parameters.AddWithValue("@CreditAmount", DBNull.Value);
                    insertRetTransactionCmd.Parameters.AddWithValue("@Paid", DBNull.Value);
                    insertRetTransactionCmd.Parameters.AddWithValue("@Notes", DBNull.Value);
                    insertRetTransactionCmd.Parameters.AddWithValue("@CreatedTime", DateTime.Now);
                    insertRetTransactionCmd.Parameters.AddWithValue("@SafeNumber", Program.safeNumber.ToString());
                    //insertRetTransactionCmd.Parameters.AddWithValue("@SupervisorID", Program.supervisorID);
                    if (con.State == ConnectionState.Closed) con.Open();
                    SqlDataReader ssd = insertRetTransactionCmd.ExecuteReader();

                    while (ssd.Read())
                    {
                        hID = ssd.GetInt32(0);
                    }
                    ssd.Close();
                    if (con.State == ConnectionState.Open) con.Close();
                    SqlCommand insertDetailCmd = new SqlCommand();
                    foreach (DataGridViewRow dr in dgvInvoiceItems.Rows)
                    {
                        if (decimal.Parse(dr.Cells["Total"].Value.ToString()) != 0 & decimal.Parse(dr.Cells["ReturnQty"].Value.ToString()) != 0)
                        {
                            insertDetailCmd.Parameters.Clear();
                            insertDetailCmd.CommandType = CommandType.StoredProcedure;
                            insertDetailCmd.Connection = con;
                            insertDetailCmd.CommandText = "uspTransactionRetDetailInsert";
                            insertDetailCmd.Parameters.AddWithValue("@ReturnTransactionID", hID);
                            insertDetailCmd.Parameters.AddWithValue("@ItemID", dr.Cells["ItemID"].Value);
                            insertDetailCmd.Parameters.AddWithValue("@Quantity", dr.Cells["ReturnQty"].Value);
                            insertDetailCmd.Parameters.AddWithValue("@SalePrice", dr.Cells["ItemPrice"].Value);
                            insertDetailCmd.Parameters.AddWithValue("@ReturnPrice", dr.Cells["Total"].Value);

                            if (con.State == ConnectionState.Closed) con.Open();
                            insertDetailCmd.ExecuteNonQuery();
                            if (con.State == ConnectionState.Open) con.Close();
                        }

                    }
                    SqlCommand updateHeader = new SqlCommand();
                    updateHeader.Connection = con;
                    updateHeader.CommandText = "UPDATE ReturnTransactionH SET CanExport = 1 WHERE ID = " + hID;
                    updateHeader.CommandType = CommandType.Text;
                    if (con.State == ConnectionState.Closed) con.Open();
                    updateHeader.ExecuteNonQuery();
                    if (con.State == ConnectionState.Open) con.Close();

                    //cancelInv();
                    printRetTransaction();
                    
                    this.Close();
                }
            }
            catch (Exception ex)
            { }
        }

        private void dgvInvoiceItemsAddingClo()
        {
            dgvInvoiceItems.Columns.Add("TransactionDID", "TransactionDID");
            dgvInvoiceItems.Columns.Add("ItemBarcode", "رقم الصنف");
            dgvInvoiceItems.Columns.Add("ItemName", "اسم الصنف");
            dgvInvoiceItems.Columns.Add("ItemUnit", "الوحدة");
            dgvInvoiceItems.Columns.Add("ItemPrice", "السعر");
            dgvInvoiceItems.Columns.Add("ItemQty", "الكمية");
            //dgvInvoiceItems.Columns.Add("Total", "الإجمالي");
            dgvInvoiceItems.Columns.Add("ItemID", "ItemID");

            dgvInvoiceItems.Columns["TransactionDID"].DataPropertyName = "ID";
            dgvInvoiceItems.Columns["ItemBarcode"].DataPropertyName = "Barcode";
            dgvInvoiceItems.Columns["ItemName"].DataPropertyName = "Name";
            dgvInvoiceItems.Columns["ItemUnit"].DataPropertyName = "unitName";
            dgvInvoiceItems.Columns["ItemPrice"].DataPropertyName = "SalePrice";
            dgvInvoiceItems.Columns["ItemQty"].DataPropertyName = "Remain";
            dgvInvoiceItems.Columns["ItemID"].DataPropertyName = "ItemID";

            dgvInvoiceItems.Columns["TransactionDID"].Visible = false;
            dgvInvoiceItems.Columns["ItemID"].Visible = false;

            dgvInvoiceItems.Columns["ItemBarcode"].ReadOnly = true;
            dgvInvoiceItems.Columns["ItemName"].ReadOnly = true;
            dgvInvoiceItems.Columns["ItemUnit"].ReadOnly = true;
            dgvInvoiceItems.Columns["ItemPrice"].ReadOnly = true;
            dgvInvoiceItems.Columns["ItemQty"].ReadOnly = true;
            //dgvInvoiceItems.Columns["Total"].ReadOnly = true;

            dgvInvoiceItems.Columns["ItemBarcode"].Width = 125;
            dgvInvoiceItems.Columns["ItemName"].Width = 220;
            dgvInvoiceItems.Columns["ItemUnit"].Width = 60;
            dgvInvoiceItems.Columns["ItemPrice"].Width = 80;
            dgvInvoiceItems.Columns["ItemQty"].Width = 80;
            //dgvReturnTransaction.Columns["Total"].Width = 90;
            //dgvInvoiceItems.ReadOnly = true;

            dgvInvoiceItems.AllowUserToDeleteRows = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblDateTime.Text = DateTime.Now.ToString();
        }

        private void dgvInvoiceItems_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //if (dgvInvoiceItems.Columns[e.ColumnIndex].Name == "ReturnQty")
            //{
            //    try
            //    {
            //        decimal itemQty = decimal.Parse(dgvInvoiceItems.Rows[e.RowIndex].Cells["ItemQty"].Value.ToString());
            //        decimal returnQty = decimal.Parse(dgvInvoiceItems.Rows[e.RowIndex].Cells["ReturnQty"].Value.ToString());
            //        if (returnQty > itemQty)
            //        {
            //            MessageBox.Show("ادخل قيمة صحيحة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
            //            dgvInvoiceItems.Rows[e.RowIndex].Cells["ReturnQty"].Value = 0;
            //            dgvInvoiceItems.BeginEdit(true);
            //            return;
            //        }
            //        else
            //        {
            //            returnQty = decimal.Parse(dgvInvoiceItems.Rows[e.RowIndex].Cells["ReturnQty"].Value.ToString());
            //            decimal itemPrice = decimal.Parse(dgvInvoiceItems.Rows[e.RowIndex].Cells["ItemPrice"].Value.ToString());
            //            decimal itemTotal = returnQty * itemPrice;
            //            dgvInvoiceItems.Rows[e.RowIndex].Cells["Total"].Value = itemTotal.ToString();

            //            calculateReturnTotal();
            //            dgvInvoiceItems.ClearSelection();

            //            dgvInvoiceItems.CurrentCell = dgvInvoiceItems.Rows[0].Cells["ReturnQty"];
            //            //dgvInvoiceItems.BeginEdit(true);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show("يجب ادخال ارقام فقط", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
            //        dgvInvoiceItems.Rows[e.RowIndex].Cells["ItemQty"].Value = 1;
            //        dgvInvoiceItems.BeginEdit(true);
            //        return;
            //    }
            //    //CalcudalteDueAmount();
            //    //calculateChange();
            //}
        }

        private void dgvInvoiceItems_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            calculateReturnTotal();
        }

        private void dgvInvoiceItems_Validated(object sender, EventArgs e)
        {
            
        }

        private void printRetTransaction()
        {
            TransactionReceipt tr = new TransactionReceipt();
            tr.transactionDate = DateTime.Now;
            tr.transactionTime = DateTime.Now;
            tr.TransactionNo = hID;
            tr.safeNo = int.Parse(txtMainSafe.Text);
            tr.cashirName = lblCashir.Text;
            tr.referanceTransaction = txtTransactionID.Text;
            tr.custName = txtCustmoerName.Text;
            tr.GridViewRow = dgvInvoiceItems;
            //foreach (DataGridViewRow dr in dgvTransactionsss.Rows)
            //{
            //    tr.itemTot= float.Parse(dr.Cells["Total"].Value.ToString());
            //    tr.qty=float.Parse(dr.Cells["ItemQty"].Value.ToString());
            //    tr.price=float.Parse(dr.Cells["ItemPrice"].Value.ToString());
            //    tr.itemName=dr.Cells["ItemName"].Value.ToString();
            //}
            tr.transactionTot = Math.Round(decimal.Parse(txtReturnTotal.Text), 2);
            tr.seLogo = (Bitmap)Image.FromFile("s1.png");
            int rowsCount = dgvInvoiceItems.RowCount * 40;
            tr.printReturnT(rowsCount + 820);

        }

        private void dgvInvoiceItems_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            try
            {
                if (MessageBox.Show("هل انت متأكد", "تأكيد", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2, MessageBoxOptions.RightAlign) == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            { }
            
        }

        private void txtTransactionID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                LoadOrderData();
            }
        }

        private void dgvInvoiceItems_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == dgvInvoiceItems.Columns["ReturnQty"].Index)
                {
                    decimal ItemQTY = 0;
                    decimal ReturnQTY = 0;
                    ItemQTY = decimal.Parse(dgvInvoiceItems.Rows[e.RowIndex].Cells["ItemQty"].Value.ToString());
                    decimal.TryParse(e.FormattedValue.ToString(), out ReturnQTY);
                    if (ReturnQTY < 0 || ReturnQTY > ItemQTY)
                    {
                        MessageBox.Show("ادخل قيمة صحيحة", "خطأ", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                        e.Cancel = true;
                    }
                    else
                    {
                        decimal itemPrice = decimal.Parse(dgvInvoiceItems.Rows[e.RowIndex].Cells["ItemPrice"].Value.ToString());
                        decimal itemTotal = ReturnQTY * itemPrice;
                        dgvInvoiceItems.Rows[e.RowIndex].Cells["Total"].Value = itemTotal.ToString("0.00");

                        calculateReturnTotal();
                        dgvInvoiceItems.ClearSelection();
                    }
                }
            }
            catch { }
        }

        private void dgvInvoiceItems_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                if (e.ColumnIndex == dgvInvoiceItems.Columns["ReturnQty"].Index)
                {
                    decimal TotalQTY = decimal.Parse(dgvInvoiceItems.Rows[e.RowIndex].Cells["ItemQty"].Value.ToString());
                    dgvInvoiceItems.Rows[e.RowIndex].Cells["ReturnQty"].Value = TotalQTY;
                    decimal itemPrice = decimal.Parse(dgvInvoiceItems.Rows[e.RowIndex].Cells["ItemPrice"].Value.ToString());
                    decimal itemTotal = TotalQTY * itemPrice;
                    dgvInvoiceItems.Rows[e.RowIndex].Cells["Total"].Value = itemTotal.ToString("0.00");

                    calculateReturnTotal();
                    dgvInvoiceItems.ClearSelection();
                }
            }
        }

        //private void dgvReturnItemAddingCol()
        //{
        //    dgvReturnItem.Columns.Add("TransactionDIdD", "TransactionDID");
        //    dgvReturnItem.Columns.Add("ItemBarcodee", "رقم الصنف");
        //    dgvReturnItem.Columns.Add("ItemNamee", "اسم الصنف");
        //    dgvReturnItem.Columns.Add("ItemUnitt", "الوحدة");
        //    dgvReturnItem.Columns.Add("ItemPricee", "السعر");
        //    dgvReturnItem.Columns.Add("ItemQtyy", "الكمية");
        //    //dgvReturnItem.Columns.Add("Total", "الإجمالي");
        //    dgvReturnItem.Columns.Add("ItemIdD", "ItemID");

        //    dgvReturnItem.Columns["TransactionDIdD"].DataPropertyName = "ID";
        //    dgvReturnItem.Columns["ItemBarcodee"].DataPropertyName = "Barcode";
        //    dgvReturnItem.Columns["ItemNamee"].DataPropertyName = "Name";
        //    dgvReturnItem.Columns["ItemUnitt"].DataPropertyName = "unitName";
        //    dgvReturnItem.Columns["ItemPricee"].DataPropertyName = "SalePrice";
        //    dgvReturnItem.Columns["ItemQtyy"].DataPropertyName = "Quantity";
        //    dgvReturnItem.Columns["ItemIdD"].DataPropertyName = "ItemID";

        //    dgvReturnItem.Columns["TransactionDIdD"].Visible = false;
        //    dgvReturnItem.Columns["ItemIdD"].Visible = false;

        //    dgvReturnItem.Columns["ItemBarcodee"].ReadOnly = true;
        //    dgvReturnItem.Columns["ItemNamee"].ReadOnly = true;
        //    dgvReturnItem.Columns["ItemUnitt"].ReadOnly = true;
        //    dgvReturnItem.Columns["ItemPricee"].ReadOnly = true;
        //    //dgvReturnItem.Columns["Total"].ReadOnly = true;

        //    dgvReturnItem.Columns["ItemBarcodee"].Width = 150;
        //    dgvReturnItem.Columns["ItemNamee"].Width = 235;
        //    dgvReturnItem.Columns["ItemUnitt"].Width = 70;
        //    dgvReturnItem.Columns["ItemPricee"].Width = 90;
        //    dgvReturnItem.Columns["ItemQtyy"].Width = 90;
        //    //dgvReturnItem.Columns["Total"].Width = 90;
        //}


    }
}
