﻿namespace SaudiMarketPOS
{
    partial class DeletedOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chkIncludeCloseTime = new System.Windows.Forms.CheckBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpReportDay = new System.Windows.Forms.DateTimePicker();
            this.dgvDeletedTrans = new System.Windows.Forms.DataGridView();
            this.DTransactionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UserName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TransactionID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CreatedTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UpdateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SafeNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Detailscount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrintReport = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeletedTrans)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chkIncludeCloseTime);
            this.splitContainer1.Panel1.Controls.Add(this.btnSearch);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.dtpReportDay);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvDeletedTrans);
            this.splitContainer1.Size = new System.Drawing.Size(946, 323);
            this.splitContainer1.SplitterDistance = 58;
            this.splitContainer1.SplitterWidth = 7;
            this.splitContainer1.TabIndex = 0;
            // 
            // chkIncludeCloseTime
            // 
            this.chkIncludeCloseTime.AutoSize = true;
            this.chkIncludeCloseTime.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkIncludeCloseTime.Location = new System.Drawing.Point(456, 15);
            this.chkIncludeCloseTime.Name = "chkIncludeCloseTime";
            this.chkIncludeCloseTime.Size = new System.Drawing.Size(180, 24);
            this.chkIncludeCloseTime.TabIndex = 44;
            this.chkIncludeCloseTime.Text = "طلبات ملغاه في نفس اليوم";
            this.chkIncludeCloseTime.UseVisualStyleBackColor = true;
            // 
            // btnSearch
            // 
            this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(161)))), ((int)(((byte)(39)))));
            this.btnSearch.FlatAppearance.BorderSize = 0;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.btnSearch.ForeColor = System.Drawing.Color.White;
            this.btnSearch.Location = new System.Drawing.Point(297, 9);
            this.btnSearch.Margin = new System.Windows.Forms.Padding(2);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(124, 35);
            this.btnSearch.TabIndex = 49;
            this.btnSearch.Text = "بحث";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(815, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 20);
            this.label2.TabIndex = 43;
            this.label2.Text = "طباعة تقرير باليوم";
            // 
            // dtpReportDay
            // 
            this.dtpReportDay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpReportDay.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpReportDay.Location = new System.Drawing.Point(659, 13);
            this.dtpReportDay.Margin = new System.Windows.Forms.Padding(4);
            this.dtpReportDay.Name = "dtpReportDay";
            this.dtpReportDay.Size = new System.Drawing.Size(118, 26);
            this.dtpReportDay.TabIndex = 0;
            // 
            // dgvDeletedTrans
            // 
            this.dgvDeletedTrans.AllowUserToAddRows = false;
            this.dgvDeletedTrans.AllowUserToDeleteRows = false;
            this.dgvDeletedTrans.AllowUserToOrderColumns = true;
            this.dgvDeletedTrans.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDeletedTrans.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDeletedTrans.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DTransactionId,
            this.UserName,
            this.TransactionID,
            this.CreatedTime,
            this.UpdateTime,
            this.SafeNumber,
            this.Detailscount,
            this.Total,
            this.PrintReport});
            this.dgvDeletedTrans.Location = new System.Drawing.Point(12, 3);
            this.dgvDeletedTrans.Name = "dgvDeletedTrans";
            this.dgvDeletedTrans.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.dgvDeletedTrans.RowHeadersVisible = false;
            this.dgvDeletedTrans.Size = new System.Drawing.Size(934, 255);
            this.dgvDeletedTrans.TabIndex = 48;
            this.dgvDeletedTrans.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvClosedShifts_CellClick);
            // 
            // DTransactionId
            // 
            this.DTransactionId.DataPropertyName = "Id";
            this.DTransactionId.HeaderText = "DTransactionId";
            this.DTransactionId.Name = "DTransactionId";
            this.DTransactionId.ReadOnly = true;
            this.DTransactionId.Visible = false;
            // 
            // UserName
            // 
            this.UserName.DataPropertyName = "UserName";
            this.UserName.HeaderText = "UserName";
            this.UserName.Name = "UserName";
            this.UserName.ReadOnly = true;
            // 
            // TransactionID
            // 
            this.TransactionID.DataPropertyName = "TransactionID";
            this.TransactionID.HeaderText = "رقم الطلب";
            this.TransactionID.Name = "TransactionID";
            this.TransactionID.ReadOnly = true;
            // 
            // CreatedTime
            // 
            this.CreatedTime.DataPropertyName = "CreatedTime";
            this.CreatedTime.HeaderText = "تاريخ الإنشاء";
            this.CreatedTime.Name = "CreatedTime";
            this.CreatedTime.ReadOnly = true;
            // 
            // UpdateTime
            // 
            this.UpdateTime.DataPropertyName = "UpdateTime";
            this.UpdateTime.HeaderText = "تاريخ التحديث";
            this.UpdateTime.Name = "UpdateTime";
            this.UpdateTime.ReadOnly = true;
            // 
            // SafeNumber
            // 
            this.SafeNumber.DataPropertyName = "SafeNumber";
            this.SafeNumber.FillWeight = 60F;
            this.SafeNumber.HeaderText = "رقم الخزنة";
            this.SafeNumber.Name = "SafeNumber";
            this.SafeNumber.ReadOnly = true;
            // 
            // Detailscount
            // 
            this.Detailscount.DataPropertyName = "Detailscount";
            this.Detailscount.HeaderText = "عدد التفاصيل";
            this.Detailscount.Name = "Detailscount";
            this.Detailscount.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            this.Total.FillWeight = 70F;
            this.Total.HeaderText = "الإجمالي";
            this.Total.Name = "Total";
            // 
            // PrintReport
            // 
            this.PrintReport.FillWeight = 50F;
            this.PrintReport.HeaderText = "طباعة";
            this.PrintReport.Name = "PrintReport";
            this.PrintReport.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.PrintReport.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.PrintReport.ToolTipText = "طباعة";
            this.PrintReport.UseColumnTextForButtonValue = true;
            // 
            // DeletedOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(946, 323);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "DeletedOrders";
            this.Text = "OldShifts";
            this.Load += new System.EventHandler(this.OldShifts_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDeletedTrans)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DateTimePicker dtpReportDay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgvDeletedTrans;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.CheckBox chkIncludeCloseTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTransactionId;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TransactionID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CreatedTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn UpdateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn SafeNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn Detailscount;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewButtonColumn PrintReport;
    }
}