﻿namespace SaudiMarketPOS
{
    partial class BarcodePrinting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvItemBarcodes = new System.Windows.Forms.DataGridView();
            this.cbNames = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.nudPrintsNo = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbBarcodes = new System.Windows.Forms.ComboBox();
            this.btnAddToList = new System.Windows.Forms.Button();
            this.btnBeginPrinting = new System.Windows.Forms.Button();
            this.cbPrinters = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ItemId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ItemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrintNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteRow = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemBarcodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrintsNo)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvItemBarcodes
            // 
            this.dgvItemBarcodes.AllowUserToAddRows = false;
            this.dgvItemBarcodes.AllowUserToDeleteRows = false;
            this.dgvItemBarcodes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvItemBarcodes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvItemBarcodes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ItemId,
            this.ItemName,
            this.PrintNo,
            this.DeleteRow});
            this.dgvItemBarcodes.Location = new System.Drawing.Point(22, 52);
            this.dgvItemBarcodes.Margin = new System.Windows.Forms.Padding(5);
            this.dgvItemBarcodes.Name = "dgvItemBarcodes";
            this.dgvItemBarcodes.RowHeadersVisible = false;
            this.dgvItemBarcodes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvItemBarcodes.Size = new System.Drawing.Size(1016, 264);
            this.dgvItemBarcodes.TabIndex = 0;
            this.dgvItemBarcodes.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvItemBarcodes_CellClick);
            // 
            // cbNames
            // 
            this.cbNames.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbNames.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbNames.FormattingEnabled = true;
            this.cbNames.Location = new System.Drawing.Point(97, 13);
            this.cbNames.Margin = new System.Windows.Forms.Padding(5);
            this.cbNames.Name = "cbNames";
            this.cbNames.Size = new System.Drawing.Size(252, 28);
            this.cbNames.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "اسم الصنف";
            // 
            // nudPrintsNo
            // 
            this.nudPrintsNo.Location = new System.Drawing.Point(787, 15);
            this.nudPrintsNo.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudPrintsNo.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPrintsNo.Name = "nudPrintsNo";
            this.nudPrintsNo.Size = new System.Drawing.Size(120, 26);
            this.nudPrintsNo.TabIndex = 3;
            this.nudPrintsNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudPrintsNo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudPrintsNo.Enter += new System.EventHandler(this.nudPrintsNo_Enter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(681, 18);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 20);
            this.label2.TabIndex = 4;
            this.label2.Tag = "";
            this.label2.Text = "عدد مرات الطباعة";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(359, 17);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "الباركود";
            // 
            // cbBarcodes
            // 
            this.cbBarcodes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbBarcodes.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbBarcodes.FormattingEnabled = true;
            this.cbBarcodes.Location = new System.Drawing.Point(419, 14);
            this.cbBarcodes.Margin = new System.Windows.Forms.Padding(5);
            this.cbBarcodes.Name = "cbBarcodes";
            this.cbBarcodes.Size = new System.Drawing.Size(252, 28);
            this.cbBarcodes.TabIndex = 2;
            // 
            // btnAddToList
            // 
            this.btnAddToList.Location = new System.Drawing.Point(913, 12);
            this.btnAddToList.Name = "btnAddToList";
            this.btnAddToList.Size = new System.Drawing.Size(125, 28);
            this.btnAddToList.TabIndex = 4;
            this.btnAddToList.Text = "اضافة";
            this.btnAddToList.UseVisualStyleBackColor = true;
            this.btnAddToList.Click += new System.EventHandler(this.btnAddToList_Click);
            // 
            // btnBeginPrinting
            // 
            this.btnBeginPrinting.Location = new System.Drawing.Point(408, 326);
            this.btnBeginPrinting.Name = "btnBeginPrinting";
            this.btnBeginPrinting.Size = new System.Drawing.Size(125, 28);
            this.btnBeginPrinting.TabIndex = 6;
            this.btnBeginPrinting.Text = "بدء الطباعة";
            this.btnBeginPrinting.UseVisualStyleBackColor = true;
            this.btnBeginPrinting.Click += new System.EventHandler(this.btnBeginPrinting_Click);
            // 
            // cbPrinters
            // 
            this.cbPrinters.FormattingEnabled = true;
            this.cbPrinters.Location = new System.Drawing.Point(74, 326);
            this.cbPrinters.Margin = new System.Windows.Forms.Padding(5);
            this.cbPrinters.Name = "cbPrinters";
            this.cbPrinters.Size = new System.Drawing.Size(326, 28);
            this.cbPrinters.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 329);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 20);
            this.label4.TabIndex = 10;
            this.label4.Text = "الطابعة";
            // 
            // ItemId
            // 
            this.ItemId.HeaderText = "الباركود";
            this.ItemId.Name = "ItemId";
            this.ItemId.ReadOnly = true;
            // 
            // ItemName
            // 
            this.ItemName.FillWeight = 200F;
            this.ItemName.HeaderText = "الاسم";
            this.ItemName.Name = "ItemName";
            this.ItemName.ReadOnly = true;
            // 
            // PrintNo
            // 
            this.PrintNo.HeaderText = "عدد مرات الطباعة";
            this.PrintNo.Name = "PrintNo";
            // 
            // DeleteRow
            // 
            this.DeleteRow.FillWeight = 50F;
            this.DeleteRow.HeaderText = "حذف";
            this.DeleteRow.Name = "DeleteRow";
            this.DeleteRow.ReadOnly = true;
            this.DeleteRow.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.DeleteRow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.DeleteRow.Text = "حذف";
            this.DeleteRow.UseColumnTextForButtonValue = true;
            // 
            // BarcodePrinting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1053, 368);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cbPrinters);
            this.Controls.Add(this.btnBeginPrinting);
            this.Controls.Add(this.btnAddToList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbBarcodes);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nudPrintsNo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbNames);
            this.Controls.Add(this.dgvItemBarcodes);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "BarcodePrinting";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RightToLeftLayout = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "طباعة الباركود";
            this.Load += new System.EventHandler(this.BarcodePrinting_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemBarcodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrintsNo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvItemBarcodes;
        private System.Windows.Forms.ComboBox cbNames;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudPrintsNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbBarcodes;
        private System.Windows.Forms.Button btnAddToList;
        private System.Windows.Forms.Button btnBeginPrinting;
        private System.Windows.Forms.ComboBox cbPrinters;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ItemName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrintNo;
        private System.Windows.Forms.DataGridViewButtonColumn DeleteRow;
    }
}