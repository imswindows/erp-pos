﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaudiMarketPOS
{
    public class WebOrders
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int CustomerID { get; set; }
        public string CustomerName { get; set; }
        public decimal Total { get; set; }
        public string Notes { get; set; }
        public bool Collected { get; set; }
        public string CustomerData { get; set; }
        public int PaymentMethod { get; set; }
        public string strPaymentMethod { get; set; }
        public decimal Discounts { get; set; }
        public int DeliveryID { get; set; }
        public decimal WalletValue { get; set; }
        //public decimal DeliveryCharge { get; set; }
        public WebOrders WebObj { get; set; }
        public List<WebOrderDetails> DetailsLst { get; set; }

        public string District { get; set; }
        public string Zone { get; set; }
        public string Street { get; set; }
        public string Building { get; set; }
        public string Floor { get; set; }
        public string Flat { get; set; }
        public string Landmark { get; set; }
        public string PhoneNumber { get; set; }

        public static string GetPaymentMethod(int ID)
        {
            switch (ID)
            {
                default:
                case 0:
                    return "Cash On Delivery";
                case 1:
                    return "Credit Card";
                case 2:
                    return "Online Paid";
                case 3:
                    return "Cash Payment";
            }
        }

        public static DataTable SelectWebOrders()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = Program.conString;
            SqlCommand cmdWebOrders = new SqlCommand();
            cmdWebOrders.CommandType = CommandType.StoredProcedure;
            cmdWebOrders.CommandText = "uspPurchaseOrderHSelectWeb";
            cmdWebOrders.Connection = con;
            DataTable dtWebOrders = new DataTable();
            if (con.State == ConnectionState.Closed) con.Open();
            dtWebOrders.Load(cmdWebOrders.ExecuteReader());
            return dtWebOrders;
        }

        public static WebOrders ConvertToObj(DataRow dr)
        {
            WebOrders Obj = new WebOrders();

            Obj.ID = int.Parse(dr["ID"].ToString());
            //Obj.CustomerID = int.Parse(dr["CustomerID"].ToString());
            //Obj.CustomerName = dr["CustomerName"].ToString();
            Obj.UserID = int.Parse(dr["UserID"].ToString());
            Obj.Total = decimal.Parse(dr["Total"].ToString());
            Obj.Notes = dr["Notes"].ToString();
            try
            { Obj.Collected = bool.Parse(dr["Collected"].ToString()); }
            catch { Obj.Collected = false; }
            Obj.CustomerData = dr["CustomerData"].ToString();
            string[] CustomerData = Obj.CustomerData.Split('^');
            Obj.CustomerName = CustomerData[1];
            try { Obj.PaymentMethod = int.Parse(dr["PaymentMethod"].ToString()); }
            catch { Obj.PaymentMethod = 0; }
            Obj.strPaymentMethod = GetPaymentMethod(Obj.PaymentMethod);
            Obj.WebObj = Obj;
            Obj.DetailsLst = WebOrderDetails.GetOrderDetails(Obj.ID);
            try { Obj.DeliveryID = int.Parse(dr["DeliveryEmloyeeID"].ToString()); }
            catch { Obj.DeliveryID = 0; }
            if (Obj.CustomerData == null || Obj.CustomerData.Trim() == "")
            {
                Obj.CustomerID = int.Parse(dr["CustomerID"].ToString());
                Obj.CustomerName = dr["CustomerName"].ToString();
            }
            else
            {
                
            }

            return Obj;
        }

        public static List<WebOrders> GetWebOrders()
        {
            List<WebOrders> ObjLst = new List<WebOrders>();
            DataTable dtWebOrders = new DataTable();
            dtWebOrders = SelectWebOrders();
            foreach (DataRow dr in dtWebOrders.Rows)
            {
                try
                {
                    WebOrders Obj = ConvertToObj(dr);
                    ObjLst.Add(Obj);
                }
                catch { }
            }
            return ObjLst;
        }
    }

    public class WebOrderDetails
    {
        public int ID { get; set; }
        public int PurchaseOrderHID { get; set; }
        public decimal Cost { get; set; }
        public decimal SalePrice { get; set; }
        public string ItemID { get; set; }
        public string ItemName { get; set; }
        public string Barcode { get; set; }
        public string Unit { get; set; }
        public decimal Quantity { get; set; }
        public decimal Total { get; set; }

        public static DataTable SelectOrderDetails(int OrderID)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString = Program.conString;
            SqlCommand cmdOrderDetails = new SqlCommand();
            cmdOrderDetails.Connection = con;
            cmdOrderDetails.CommandType = CommandType.StoredProcedure;
            cmdOrderDetails.CommandText = "uspPurchaseOrderDSelectWeb";
            cmdOrderDetails.Parameters.AddWithValue("@OrderID", OrderID);
            DataTable dtOrderDetails = new DataTable();
            if (con.State == ConnectionState.Closed) con.Open();
            dtOrderDetails.Load(cmdOrderDetails.ExecuteReader());
            return dtOrderDetails;
        }

        public static WebOrderDetails ConvertToObj(DataRow dr)
        {
            WebOrderDetails Obj = new WebOrderDetails();

            Obj.ID = int.Parse(dr["ID"].ToString());
            Obj.PurchaseOrderHID = int.Parse(dr["PurchaseOrderHID"].ToString());
            Obj.Cost = decimal.Parse(dr["Cost"].ToString());
            Obj.SalePrice = decimal.Parse(dr["SalePrice"].ToString());
            Obj.ItemID = dr["ItemID"].ToString();
            Obj.ItemName = dr["Name"].ToString();
            Obj.Barcode = dr["Barcode"].ToString();
            Obj.Unit = dr["UnitName"].ToString();
            Obj.Quantity = decimal.Parse(dr["Quantity"].ToString());
            Obj.Total = Obj.Quantity * Obj.SalePrice;

            return Obj;
        }

        public static List<WebOrderDetails> GetOrderDetails(int OrderID)
        {
            List<WebOrderDetails> ObjLst = new List<WebOrderDetails>();
            DataTable dtOrderDetails = new DataTable();
            dtOrderDetails = SelectOrderDetails(OrderID);
            foreach (DataRow dr in dtOrderDetails.Rows)
            {
                WebOrderDetails Obj = ConvertToObj(dr);
                ObjLst.Add(Obj);
            }
            return ObjLst;
        }
    }
}
