﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SaudiMarketPOS
{
    public partial class WebDeliveryOrders : Form
    {
        SqlConnection con = new SqlConnection();
        public WebOrders WebObj = new WebOrders();

        public WebDeliveryOrders()
        {
            InitializeComponent();
        }

        private void WebDeliveryOrders_Load(object sender, EventArgs e)
        {
            //con.ConnectionString = Program.conString;
            //SqlCommand cmdWebOrders = new SqlCommand();
            //cmdWebOrders.CommandType = CommandType.StoredProcedure;
            //cmdWebOrders.CommandText = "uspPurchaseOrderHSelect";
            //cmdWebOrders.Connection = con;
            //DataTable dtWebOrders = new DataTable();
            //if (con.State == ConnectionState.Closed) con.Open();
            //dtWebOrders.Load(cmdWebOrders.ExecuteReader());
            //dgvWebOrders.DataSource = dtWebOrders;
            List<WebOrders> OrdersLst = WebOrders.GetWebOrders();
            dgvWebOrders.AutoGenerateColumns = false;
            dgvWebOrders.DataSource = OrdersLst.FindAll(c => c.DeliveryID == 0 && c.Collected == false);
            try
            {
                dgvWebOrders.SelectedRows[0].Selected = false;
            }catch { }
        }

        private void dgvWebOrders_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                WebObj = (WebOrders)dgvWebOrders.Rows[e.RowIndex].Cells["UncollectedWebObj"].Value;
                Close();
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                WebObj = (WebOrders)dgvWebOrders.SelectedRows[0].Cells["UncollectedWebObj"].Value;
                Close();
            }
            catch
            {
                MessageBox.Show("لم يتم اختيار فاتوره");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
