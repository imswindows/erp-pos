﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Users
    {
        public SqlConnection con;
        public int ID;
        public int? EmployeeID;
        public string FirstName;
        public string LastName;
        public string UserName;
        public string uPassword;
        public DateTime? LastLogin;
        public bool Active;
        public string EmailAdress;
        public string City;
        public string State;
        public string Telephone;
        public string PasswordReset;
        public string Address1;
        public string Address2;
        public int? RoleID;

        public void Login()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@UserName", UserName);
            cmd.Parameters.AddWithValue("@uPassword", uPassword);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "usp_UsrLogin";
            cmd.Connection = con;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                ID = int.Parse(dr["ID"].ToString());
                RoleID = int.Parse(dr["RoleID"].ToString());
            }
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            dr.Close();
        }

        public DataTable SelectEmployeeNames()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "uspSelectEmpName";
            cmd.Connection = con;
            DataTable dt = new DataTable();
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            dt.Load(cmd.ExecuteReader());
            return dt;
        }

        public DataTable SelectCashierNames()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "uspSelectCashirName";
            cmd.Connection = con;
            DataTable dt = new DataTable();
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            dt.Load(cmd.ExecuteReader());
            return dt;
        }

        public string[] CheckCurrentPassword(int ID)
        {
            string[] StrArray = new string[2];

            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@ID", ID);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "uspUsersChkCurrentPass";
            cmd.Connection = con;

            if (con.State == ConnectionState.Closed) con.Open();
            SqlDataReader readPass = cmd.ExecuteReader();

            if (readPass.Read())
            {
                StrArray[0] = readPass["UserName"].ToString();
                StrArray[1] = readPass["uPassword"].ToString();
            }
            readPass.Close();

            return StrArray;
        }

        public void ChangePassword(int ID, string Username, string Password)
        {
            SqlCommand passChangeCmd = new SqlCommand();
            passChangeCmd.Connection = con;
            passChangeCmd.CommandType = CommandType.StoredProcedure;
            passChangeCmd.CommandText = "uspUsersChangePassword";
            passChangeCmd.Parameters.AddWithValue("@UserID", ID);
            passChangeCmd.Parameters.AddWithValue("@UserName", Username);
            passChangeCmd.Parameters.AddWithValue("@uPassword", Password);
            if (con.State == ConnectionState.Closed) con.Open();
            passChangeCmd.ExecuteNonQuery();
            if (con.State == ConnectionState.Open) con.Close();
        }
    }
}
