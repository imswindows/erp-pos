﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class OpenningBalance
    {
        public SqlConnection con;
        public int ID;
        public int UserID;
        public decimal Balance;
        public DateTime CreatedTime;
        public DateTime? ClosedTime;
        public int? SafeNumberID;
        public bool Closed;

        public bool CheckOpenShift(int UserID)
        {
            bool IsOpen = false;
            SqlCommand cmd = new SqlCommand();
            cmd.Parameters.AddWithValue("@userID", UserID);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "uspCheckUserHasOpendShift";
            cmd.Connection = con;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataReader readStatus = cmd.ExecuteReader();
            IsOpen = readStatus.HasRows;
            readStatus.Close();
            return IsOpen;
        }
    }
}
