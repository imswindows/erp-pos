USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[FindItemsByCode]    Script Date: 03/10/2016 11:10:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[FindItemsByCode] @Code varchar(16)
as
select Barcode , Name ,SellPrice from Item where [ID] like @Code+'%'
and Active = 1 