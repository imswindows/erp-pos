USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[FindItemsByCode]    Script Date: 15/11/2016 12:32:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[FindItemsByCode] @Code varchar(16)
as
select ID, Barcode , Name , SellPrice from Item where [ID] like @Code+'%'
and Active = 1 