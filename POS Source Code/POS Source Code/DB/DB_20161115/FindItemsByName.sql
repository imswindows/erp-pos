USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[FindItemsByName]    Script Date: 15/11/2016 12:32:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[FindItemsByName] @Name nvarchar(50)
as
select ID, Barcode , Name , SellPrice from Item where Name like '%'+@Name+'%'
and Active = 1

