USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[selectItems]    Script Date: 23/10/2016 04:54:34 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[selectItems]
as select
ID, Barcode , Name , SellPrice
from item where Active = 1
Order by Name asc
