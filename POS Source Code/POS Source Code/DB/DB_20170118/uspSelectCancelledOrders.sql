USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspSelectCollectedOrders]    Script Date: 18/01/2017 11:37:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspSelectCancelledOrders] 
@DateFrom datetime,
@DateTo datetime
as
SELECT     TransactionTableH_Main.ID, TransactionTableH_Main.UserID, (Users.FirstName+' '+Users.LastName) EmpName
, TransactionTableH_Main.DueAmount, TransactionTableH_Main.Collected, TransactionTableH_Main.CreatedTime
FROM         TransactionTableH_Main INNER JOIN
                      Users ON TransactionTableH_Main.USerID = Users.ID
                      where 
                      TransactionTableH_Main.CreatedTime >=  @DateFrom and
					  TransactionTableH_Main.CreatedTime <= @DateTo and
                      TransactionTableH_Main.Canceled=1
