alter proc [dbo].[uspGetCollectedCreditCZ] @StartingShift datetime , @EndingShift datetime
as
SELECT SUM(CreditTotal) FROM
(
select SUM(CreditAmount) as CreditTotal from dbo.TransactionTableH_Main
where CashingTime between @StartingShift and @EndingShift and Collected = 1
 and (PaymentMethod = 1)

 UNION ALL

 select SUM(CreditAmount) as CreditTotal from dbo.TransactionTableH_Main
where CashingTime between @StartingShift and @EndingShift and Collected = 1
 and (IsOnCredit = 1)
 ) as T