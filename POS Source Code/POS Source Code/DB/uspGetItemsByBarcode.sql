USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspGetItemsByBarcode]    Script Date: 08/09/2016 03:18:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspGetItemsByBarcode] @Barcode varchar(16)
as 
SELECT
Item.ID ,
item.Barcode ,
Item.Name, 
SaleUnit.UnitName, 
Item.SellPrice
FROM  Item 
left outer JOIN
SaleUnit ON Item.SaleUnitID = SaleUnit.ID
WHERE Item.[ID] = @Barcode
