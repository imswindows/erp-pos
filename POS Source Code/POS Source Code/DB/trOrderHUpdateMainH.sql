USE [POS]
GO
/****** Object:  Trigger [dbo].[trOrderHUpdateMainH]    Script Date: 06/11/2016 12:21:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER trigger [dbo].[trOrderHUpdateMainH] On [dbo].[PurchaseOrderH]  after Update 
as
begin

UPDATE [dbo].[TransactionTableH_Main]
   SET 
      [CustomerID] = i.CustomerID
      ,[Total] = i.Total
      ,[SalesTax] = i.SalesTax
      ,[UserID] = i.UserID
      ,[DiscountPercent] = i.DiscountPercent
      ,[CashDiscount] = i.CashDiscount
      ,[CreditAmount] = i.CreditAmount
      ,[DeliveryEmloyeeID] = i.DeliveryEmloyeeID
      ,[DueAmount] = i.DueAmount
      ,[Paid] = i.Paid
      ,[Change] = i.Change
      ,[Notes] = i.Notes
      ,[IsOnCredit] = 0
	  
      ,[SafeNumber] = i.SafeNumber
      ,[Collected] = i.Collected
      --,[Canceled] = i.Canceled
      ,[IsDeliveryOrder] = 1
	  ,[PaymentMethod] = i.PaymentMethod
	  ,[CreditCTransNo] = i.CreditCardTransactionNumber
From [dbo].[TransactionTableH_Main] Join inserted i
on TransactionTableH_Main.[ID] = i.ID

end