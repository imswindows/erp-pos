USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspSelectTransactionH]    Script Date: 05/11/2016 01:49:32 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspSelectTransactionH] @ID int 
as 
SELECT 
ID, 
CustomerID, 
Total, 
SalesTax, 
UserID, 
DiscountPercent, 
CashDiscount, 
CreditAmount, 
DeliveryEmloyeeID, 
DueAmount, 
Paid, 
change, 
Notes, 
IsOnCredit, 
CreatedTime, 
SafeNumber
from TransactionTableH_Main where ID = @ID and Canceled = 0 and Collected = 1
