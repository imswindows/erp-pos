USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspGetReturnTotalDuringShift]    Script Date: 02/11/2016 10:37:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspGetReturnTotalDuringShift] @UserID int , @StartingShift datetime , @EndingShift datetime
as
select SUM(TotalReturn)as ReturnTotal from dbo.ReturnTransactionH_Main
where UserID = @UserID and CreatedTime between @StartingShift and @EndingShift 