USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspTransactionInsert]    Script Date: 06/11/2016 11:37:04 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspTransactionInsert]
@CustomerID int ,
@Total money ,
@UserID int , 
@DiscountPercent decimal(18,2), 
@CashDiscount money, 
@CreditAmount money, 
@DueAmount money,
@Paid money,
@Change money,
@DeliveryEmloyeeID int,
@Notes nvarchar(max),  
@IsOnCredit bit, 
@CreatedTime datetime,
@SafeNumber int,
@collected bit,
@IsDeliveryOrder bit,
@CreditCTransNo nvarchar(25)
as
insert into TransactionTable
(CustomerID  ,
Total ,
UserID , 
DiscountPercent, 
CashDiscount , 
CreditAmount , 
DueAmount,
Paid,
Change,
DeliveryEmloyeeID , 
Notes ,
IsOnCredit, 
CreatedTime,
SafeNumber,
Canceled ,
Collected,
IsDeliveryOrder,
CreditCTransNo)
values
(@CustomerID  ,
@Total ,
@UserID , 
@DiscountPercent, 
@CashDiscount , 
@CreditAmount , 
@DueAmount,
@Paid,
@Change,
@DeliveryEmloyeeID , 
@Notes ,
@IsOnCredit, 
@CreatedTime ,
@SafeNumber,
0 ,
@collected,
@IsDeliveryOrder,
@CreditCTransNo)
declare @Hid int
set @Hid=  SCOPE_IDENTITY()
select @Hid
