-- ================================================
-- Template generated from Template Explorer using:
-- Create Trigger (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	fill in main details from order D
-- =============================================
CREATE TRIGGER trOrderDToTransactionD_Main
   ON    [dbo].[PurchaseOrderD]
   AFTER  INSERT 
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for trigger here

	insert into dbo.TansactionDetailMain (ID, Cost, SalePrice, TransactionID, ItemID, Quantity, Taxable, SalesTax)
    select ID, Cost, SalePrice, [PurchaseOrderHID] , ItemID, Quantity, Taxable, SalesTax from inserted


END
GO
