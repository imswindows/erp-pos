USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspSelectCollectedOrders]    Script Date: 11/7/2016 7:51:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspSelectCollectedOrders] 
@DateFrom datetime,
@DateTo datetime
as
SELECT     TransactionTableH_Main.ID, TransactionTableH_Main.UserID, (Users.FirstName+' '+Users.LastName) EmpName
, TransactionTableH_Main.DueAmount, TransactionTableH_Main.Collected, TransactionTableH_Main.CreatedTime
FROM         TransactionTableH_Main INNER JOIN
                      Users ON TransactionTableH_Main.USerID = Users.ID
                      where 
                      TransactionTableH_Main.CashingTime >=  @DateFrom and
					  TransactionTableH_Main.CashingTime <= @DateTo and
                      TransactionTableH_Main.Collected=1
