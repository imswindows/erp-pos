USE [POS]
GO
/****** Object:  Trigger [dbo].[trOrderHtoMainH]    Script Date: 06/11/2016 12:20:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER trigger [dbo].[trOrderHtoMainH] On [dbo].[PurchaseOrderH]  after insert 
as
begin

insert into dbo.TransactionTableH_Main 
      (ID, CustomerID, Total, SalesTax, UserID, DiscountPercent, CashDiscount, CreditAmount, DeliveryEmloyeeID, DueAmount, Paid, Change, Notes, IsOnCredit, CreatedTime, SafeNumber, Collected, Canceled, IsDeliveryOrder , PaymentMethod, CreditCTransNo)
select ID, CustomerID, Total, SalesTax, UserID, DiscountPercent, CashDiscount, CreditAmount, DeliveryEmloyeeID, DueAmount, Paid, Change, Notes, 0	      , CreatedTime, SafeNumber, 0, 0, 1 , PaymentMethod, CreditCardTransactionNumber FROM INSERTED ;
end