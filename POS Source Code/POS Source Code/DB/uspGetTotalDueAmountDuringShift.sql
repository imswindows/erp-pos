USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspGetTotalDueAmountDuringShift]    Script Date: 27/09/2016 10:14:45 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspGetTotalDueAmountDuringShift] @UserID int , @StartingShift datetime , @EndingShift datetime
as
select SUM(DueAmount)as SalesDueAmount from dbo.TransactionTableH_Main
where UserID = @UserID and CashingTime between @StartingShift and @EndingShift
