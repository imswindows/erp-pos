USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspGetTotalSalesDuringShift]    Script Date: 12/01/2017 03:58:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspGetTotalSalesZ] @StartingShift datetime , @EndingShift datetime
as
select SUM(Total)as SalesTotal from dbo.TransactionTableH_Main
where CashingTime between @StartingShift and @EndingShift

GO
/****** Object:  StoredProcedure [dbo].[uspGetTotalDueAmountDuringShift]    Script Date: 12/01/2017 03:59:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspGetTotalDueAmountZ] @StartingShift datetime , @EndingShift datetime
as
select SUM(DueAmount)as SalesDueAmount from dbo.TransactionTableH_Main
where CashingTime between @StartingShift and @EndingShift

GO
/****** Object:  StoredProcedure [dbo].[uspGetCreditTotalDuringShift]    Script Date: 12/01/2017 04:00:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspGetCreditTotalZ] @StartingShift datetime , @EndingShift datetime
as
select SUM(TransactionAmount) as CreditTotal from dbo.CreditCardTransaction
where CreatedTime between @StartingShift and @EndingShift

GO
/****** Object:  StoredProcedure [dbo].[uspGetunCollectedDOTotalDuringShift]    Script Date: 12/01/2017 04:01:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspGetunCollectedDOTotalZ] @StartingShift datetime , @EndingShift datetime
as
select SUM(Total)as UncollectedDOTotal from dbo.TransactionTableH_Main
where CashingTime between @StartingShift and @EndingShift and IsDeliveryOrder=1 and Collected=0

GO
/****** Object:  StoredProcedure [dbo].[uspGetReturnTotalDuringShift]    Script Date: 12/01/2017 04:02:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspGetReturnTotalZ] @StartingShift datetime , @EndingShift datetime
as
select SUM(TotalReturn)as ReturnTotal from dbo.ReturnTransactionH_Main
where CreatedTime between @StartingShift and @EndingShift

GO
/****** Object:  StoredProcedure [dbo].[uspGetCollectedCreditCDuringShift]    Script Date: 12/01/2017 04:03:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter proc [dbo].[uspGetCollectedCreditCZ] @StartingShift datetime , @EndingShift datetime
as
SELECT SUM(CreditTotal) FROM
(
select SUM(CreditAmount) as CreditTotal from dbo.TransactionTableH_Main
where CashingTime between @StartingShift and @EndingShift and Collected = 1
 and (PaymentMethod = 1)

 UNION ALL

 select SUM(CreditAmount) as CreditTotal from dbo.TransactionTableH_Main
where CashingTime between @StartingShift and @EndingShift and Collected = 1
 and (IsOnCredit = 1)
 ) as T

 GO
/****** Object:  StoredProcedure [dbo].[uspGetPaidOnLineTotalDuringShift]    Script Date: 12/01/2017 04:04:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[uspGetPaidOnLineTotalZ] @StartingShift datetime , @EndingShift datetime
as
select SUM(Total)as PaidOnLine from dbo.TransactionTableH_Main
where CashingTime between @StartingShift and @EndingShift and PaymentMethod = 2
and Collected = 1
