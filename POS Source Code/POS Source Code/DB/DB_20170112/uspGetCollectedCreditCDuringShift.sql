USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspGetCollectedCreditCDuringShift]    Script Date: 12/01/2017 02:57:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspGetCollectedCreditCDuringShift] @UserID int , @StartingShift datetime , @EndingShift datetime
as
SELECT SUM(CreditTotal) FROM
(
select SUM(Total) as CreditTotal from dbo.TransactionTableH_Main
where UserID = @UserID and CashingTime between @StartingShift and @EndingShift and Collected = 1
 and (PaymentMethod = 1)

 UNION ALL

 select SUM(CreditAmount) as CreditTotal from dbo.TransactionTableH_Main
where UserID = @UserID and CashingTime between @StartingShift and @EndingShift and Collected = 1
 and (IsOnCredit = 1)
 ) as T