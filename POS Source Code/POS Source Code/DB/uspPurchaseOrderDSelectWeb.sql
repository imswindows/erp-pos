-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE uspPurchaseOrderDSelectWeb
@OrderID int

AS
BEGIN

SELECT [dbo].[PurchaseOrderD].*, [dbo].[Item].Name, [dbo].[Item].Barcode, [dbo].[SaleUnit].[UnitName] FROM [dbo].[PurchaseOrderD]
LEFT OUTER JOIN [dbo].[Item] ON [dbo].[Item].ID = [dbo].[PurchaseOrderD].ItemID
LEFT OUTER JOIN [dbo].[SaleUnit] ON [dbo].[SaleUnit].ID = [dbo].[Item].SaleUnitID
WHERE PurchaseOrderHID = @OrderID

END
GO
