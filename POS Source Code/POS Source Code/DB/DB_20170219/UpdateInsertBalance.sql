USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspOpenningBalanceInsert]    Script Date: 19-Feb-17 2:13:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspOpenningBalanceInsert]
@ID int,
@userID int , 
@OpenningBalance money ,
@CreatedTime datetime,
@SafeNumberID int

as 
insert into OpenningBalance (ID, userID , OpenningBalance ,CreatedTime,ClosedTime ,SafeNumberID, Exported)
values (@ID, @userID , @OpenningBalance ,@CreatedTime,null,@SafeNumberID, 0 )