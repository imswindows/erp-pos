USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspPurchaseOrderHSelectWeb]    Script Date: 11/8/2016 9:11:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER proc [dbo].[uspPurchaseOrderHSelectWeb]

as
SELECT PurchaseOrderH.*, ([dbo].[Customer].FirstName + ' ' + [dbo].[Customer].LastName) CustomerName

FROM PurchaseOrderH
left outer JOIN [dbo].[Customer] ON [dbo].[Customer].ID = PurchaseOrderH.CustomerID
WHERE  [DeliveryEmloyeeID] is null and customerid <> 1

