USE [POS]
GO
/****** Object:  Trigger [dbo].[InsertTransactionD]    Script Date: 06/11/2016 05:50:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER trigger [dbo].[InsertTransactionD] On [dbo].[TransactionTable] after insert 
as
begin

insert into dbo.TransactionTableH_Main (ID, CustomerID, Total, SalesTax, UserID, DiscountPercent, CashDiscount, CreditAmount, DeliveryEmloyeeID, DueAmount, Paid, Change, Notes, IsOnCredit, CreatedTime, SafeNumber, Collected, Canceled, IsDeliveryOrder, CreditCTransNo, CashingTime)
select ID, CustomerID, Total, SalesTax, UserID, DiscountPercent, CashDiscount, CreditAmount, DeliveryEmloyeeID, DueAmount, Paid, Change, Notes, IsOnCredit, CreatedTime, SafeNumber, Collected, Canceled, IsDeliveryOrder, CreditCTransNo, GETDATE() FROM INSERTED ;
end