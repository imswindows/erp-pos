USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspGetunCollectedDOTotalDuringShift]    Script Date: 27/09/2016 10:16:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspGetunCollectedDOTotalDuringShift] @UserID int , @StartingShift datetime , @EndingShift datetime
as
select SUM(Total)as UncollectedDOTotal from dbo.TransactionTableH_Main
where UserID = @UserID and CashingTime between @StartingShift and @EndingShift and IsDeliveryOrder=1 and Collected=0
