USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspGetPaidOnLineTotalDuringShift]    Script Date: 06/11/2016 03:04:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspGetPaidOnLineTotalDuringShift] @UserID int , @StartingShift datetime , @EndingShift datetime
as
select SUM(Total)as PaidOnLine from dbo.TransactionTableH_Main
where UserID = @UserID and CashingTime between @StartingShift and @EndingShift and PaymentMethod = 2
and Collected = 1
