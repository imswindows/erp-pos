USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetTransactionDetails]    Script Date: 08/12/2016 12:19:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[usp_GetTransactionDetails]
 @TransactionID int
as
SELECT     Item.Barcode, Item.Name, SaleUnit.UnitName, TansactionDetailMain.SalePrice, TansactionDetailMain.Quantity
, TansactionDetailMain.ID, TansactionDetailMain.ItemID, SUM(ISNULL(ReturnD.Quantity, 0)) ReturnQTY
, (TansactionDetailMain.Quantity - SUM(ISNULL(ReturnD.Quantity, 0))) Remain
FROM         TansactionDetailMain 
INNER JOIN Item ON TansactionDetailMain.ItemID = Item.ID
INNER JOIN SaleUnit ON Item.SaleUnitID = SaleUnit.ID
LEFT JOIN ReturnTransactionH_Main ReturnH ON ReturnH.TransactionID = TansactionDetailMain.TransactionID
LEFT JOIN ReturnTransactionD_Main ReturnD ON ReturnD.ReturnTransactionID = ReturnH.ID
AND ReturnD.ItemID = TansactionDetailMain.ItemID
WHERE TansactionDetailMain.TransactionID = @TransactionID
GROUP BY Item.Barcode, Item.Name, SaleUnit.UnitName, TansactionDetailMain.SalePrice, TansactionDetailMain.Quantity
, TansactionDetailMain.ID, TansactionDetailMain.ItemID
