USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspTransactionTableUpdate]    Script Date: 27/09/2016 12:28:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspTransactionTableUpdate] @ID int 
as
update dbo.TransactionTableH_Main set Canceled = 1
where ID = @ID
