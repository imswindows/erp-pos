USE [POS]
GO

/****** Object:  Table [dbo].[ItemPriceLog]    Script Date: 22/12/2016 06:52:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ItemPriceLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [nvarchar](50) NOT NULL,
	[ChangeDate] [datetime] NOT NULL,
 CONSTRAINT [PK_ItemPriceLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


