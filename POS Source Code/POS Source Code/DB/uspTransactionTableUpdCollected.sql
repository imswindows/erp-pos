USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspTransactionTableUpdCollected]    Script Date: 06/11/2016 01:02:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspTransactionTableUpdCollected] 
@ID int,
@InvStatus bit,
@UserID int,
@PaymentMethod int,
@CreditCTransNo nvarchar(50) = null
as
update PurchaseOrderH
set
Collected = @InvStatus,
UserID = @UserID,
PaymentMethod = @PaymentMethod,
CreditCardTransactionNumber = @CreditCTransNo
where ID = @ID

update [TransactionTableH_Main]
set
[CashingTime] = GETDATE()
where ID = @ID
