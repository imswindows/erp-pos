USE [test]
GO
/****** Object:  StoredProcedure [dbo].[usparSalesInvHImport]    Script Date: 02/10/2016 05:00:13 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usparSalesInvHImport]
 @ID int = 0  Output 
, @InvoiceNo nvarchar(10)  = Null 
, @CustomerID int   = Null 
, @CustomerData nvarchar(40)  = Null 
, @SalesOrderID int  = Null 
, @CustomerPO nvarchar(10)  = Null 
, @arSalesRep smallint   = Null 
, @HDate smalldatetime   = Null 
, @PaymentMethod smallint   = Null 
, @CalcSalesTax bit   = Null 
, @Discount money   = Null 
, @TaxPercent money   = Null 
, @Remarks nvarchar(80)  = Null 
, @BusinessLine tinyint   = Null 
, @CurrencyID tinyint   = Null 
, @ivInvGRNDoc nvarchar(10)  = Null 
, @IsPrinted bit   = Null 
, @Branch tinyint = null
--logging data   
  ,@UserName nvarchar(10) 
  ,@Source nvarchar (50) = Null  
  ,@Subsys nvarchar (50) = Null  
--With Encryption   
AS 
Begin 
  Declare @Action nvarchar (50)  
  Declare @FullData Xml 

Begin try 

Set @Action = 'ImportPOS' 

if @SalesOrderID = 0
 Set @SalesOrderID = null

if (not exists (Select * From  arSalesInvH Where InvoiceNo = @InvoiceNo ))
  
Insert into dbo.arSalesInvH
(
  InvoiceNo
,  CustomerID
,  CustomerData
,  SalesOrderID
,  CustomerPO
,  arSalesRep
,  HDate
,  PaymentMethod
,  CalcSalesTax
,  Total
,  SalesTax
,  Discount
,  TaxPercent
,  Remarks
,  Due
,  BusinessLine
,  DetailsDiscount
,  CurrencyID
,  ivInvGRNDoc
,  DueDate
,  IsPrinted
, Branch
)
Values 
(
  @InvoiceNo
,  @CustomerID
,  @CustomerData
,  @SalesOrderID
,  @CustomerPO
,  @arSalesRep
,  IsNull( @HDate , 0)
,  @PaymentMethod
,  IsNull( @CalcSalesTax , 0)
,  0 --IsNull( @Total , 0)
,  0 --IsNull( @SalesTax , 0)
,  IsNull( @Discount , 0)
,  IsNull( @TaxPercent , 0)
,  @Remarks
,  0--@Due
,  @BusinessLine
,  0--@DetailsDiscount
,  @CurrencyID
,  @ivInvGRNDoc
,  Null --@DueDate
,  IsNull( @IsPrinted , 0)
, ISNULL(@Branch , dbo.fnGetBranch()) 
)
select @ID = SCOPE_IDENTITY() 
 
 
-- Error handle
END TRY
BEGIN CATCH
  -- Log Error 
  
  Declare @Msg nvarchar(1000)
  set @Msg = 'Couldnt '+@Action+' Data Please review Error '+(ERROR_MESSAGE())
  RaisError (@Msg ,16,1)
END CATCH

  	RETURN  
  End



