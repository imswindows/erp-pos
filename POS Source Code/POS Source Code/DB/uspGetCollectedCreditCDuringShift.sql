USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspGetCollectedCreditCDuringShift]    Script Date: 06/11/2016 03:03:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspGetCollectedCreditCDuringShift] @UserID int , @StartingShift datetime , @EndingShift datetime
as
select SUM(Total) as CreditTotal from dbo.TransactionTableH_Main
where UserID = @UserID and CashingTime between @StartingShift and @EndingShift and Collected = 1
 and (PaymentMethod = 1 OR IsOnCredit = 1)