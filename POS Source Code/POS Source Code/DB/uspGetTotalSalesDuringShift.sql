USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspGetTotalSalesDuringShift]    Script Date: 27/09/2016 10:13:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspGetTotalSalesDuringShift] @UserID int , @StartingShift datetime , @EndingShift datetime
as
select SUM(Total)as SalesTotal from dbo.TransactionTableH_Main
where UserID = @UserID and CashingTime between @StartingShift and @EndingShift
