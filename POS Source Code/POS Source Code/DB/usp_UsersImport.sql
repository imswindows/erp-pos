USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[usp_UsersImport]    Script Date: 18/09/2016 07:18:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_UsersImport]
(@ID smallint = Null,
			@FirstName nvarchar(50)= null
           ,@LastName nvarchar(50)= null
           ,@UserName nvarchar(50)= null
		   ,@Password nvarchar(50)= null
           ,@Active bit = 1
		   ,@RoleID int=null
)
AS
Begin
  Declare @Action nvarchar (50) 
--Begin try
if Exists  (Select * from [dbo].[Users] where [ID] =  @ID )
begin
  Set @Action = 'Update'
UPDATE [dbo].[Users]
   SET [UserName] = @UserName,
   [uPassword] = @Password,
   [RoleID] = @RoleID
    
 WHERE ID= @ID
end
else
Begin
  Set @Action = 'Insert'
SET IDENTITY_INSERT dbo.Users ON
 
INSERT INTO  [dbo].[Users]
           ([ID]
		   ,[FirstName]
		   ,[LastName]
           ,[UserName]
		   ,[uPassword]
		   ,Active
		   ,RoleID)
     VALUES
           (@ID
		   ,@FirstName 
		   ,@LastName
		   ,@UserName
		   ,@Password
		   ,@Active
		   ,@RoleID)
 
 
SET IDENTITY_INSERT dbo.Users OFF
select @ID = SCOPE_IDENTITY()
End
-- Error handle
--END TRY
--BEGIN CATCH
--  -- Log Error
--  Declare @Msg nvarchar(1000)
--  set @Msg = 'Couldnt '+@Action+' Data Please review Error ="'+Replace(ERROR_MESSAGE(),'<',',')+'"'
--  RaisError (@Msg ,10,1)
--END CATCH
 
      RETURN 
  End
 
 