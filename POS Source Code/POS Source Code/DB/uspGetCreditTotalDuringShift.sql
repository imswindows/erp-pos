USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspGetCreditTotalDuringShift]    Script Date: 06/11/2016 03:01:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspGetCreditTotalDuringShift] @UserID int , @StartingShift datetime , @EndingShift datetime
as
select SUM(TransactionAmount) as CreditTotal from dbo.CreditCardTransaction
where UserID = @UserID and CreatedTime between @StartingShift and @EndingShift 
