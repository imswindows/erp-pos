ALTER procedure [dbo].[uspSelectUncollctedOrders]
@userID int ,
@TransactionDate date ,
@EndTime date
as
SELECT     TransactionTableH_Main.ID, TransactionTableH_Main.DeliveryEmloyeeID
, TransactionTableH_Main.UserID,TransactionTableH_Main.Paid, TransactionTableH_Main.DueAmount
,TransactionTableH_Main.Collected, (Users.FirstName+' '+Users.LastName) as UserName, 
(Users_1.FirstName+' '+Users_1.LastName) AS EmpName
FROM TransactionTableH_Main INNER JOIN
Users ON TransactionTableH_Main.UserID = Users.ID INNER JOIN
Users AS Users_1 ON TransactionTableH_Main.DeliveryEmloyeeID = Users_1.ID AND TransactionTableH_Main.DeliveryEmloyeeID = Users_1.ID
where --Users.ID = @userID and 
TransactionTableH_Main.CreatedTime >= @TransactionDate and
TransactionTableH_Main.CreatedTime <= @EndTime and  
--IsDeliveryOrder=1 and 
(Collected=0 or Collected is Null) 
--TransactionTable.DeliveryEmloyeeID = Users.ID AND
