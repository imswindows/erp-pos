USE [POS]
GO
/****** Object:  StoredProcedure [dbo].[uspPurchaseOrderHInsert]    Script Date: 06/11/2016 12:26:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER proc [dbo].[uspPurchaseOrderHInsert]
@UserID int,
@CustomerID int ,
@DeliveryEmloyeeID int,
@Total money , 
@DiscountPercent decimal(18,2), 
@CashDiscount money, 
@CreditAmount money, 
@DueAmount money,
@Paid money,
@Change money,
@SafeNumber int,
@Notes nvarchar(max),  
@CreditCardTransactionNumber nvarchar(50), 
@CreatedTime datetime,
@PaymentMethod int = 0

as
insert into PurchaseOrderH
(
UserID ,
CustomerID  ,
DeliveryEmloyeeID ,
Total  , 
DiscountPercent , 
CashDiscount , 
CreditAmount , 
DueAmount ,
Paid ,
Change ,
SafeNumber ,
Notes ,  
CreditCardTransactionNumber , 
CreatedTime,
PaymentMethod
)
values
(
@UserID ,
@CustomerID  ,
@DeliveryEmloyeeID ,
@Total  , 
@DiscountPercent , 
@CashDiscount , 
@CreditAmount , 
@DueAmount ,
@Paid ,
@Change ,
@SafeNumber ,
@Notes ,  
@CreditCardTransactionNumber , 
@CreatedTime ,
@PaymentMethod
)
declare @Hid int
set @Hid=  SCOPE_IDENTITY()
select @Hid
