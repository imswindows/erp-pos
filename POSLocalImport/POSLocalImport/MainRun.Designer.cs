﻿namespace POSImportExport
{
    partial class MainRun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainRun));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnGetInvoice = new System.Windows.Forms.Button();
            this.btnSendOrder = new System.Windows.Forms.Button();
            this.btnDeliveryWeb = new System.Windows.Forms.Button();
            this.btnGetUsers = new System.Windows.Forms.Button();
            this.btnSendInvoiceReturn = new System.Windows.Forms.Button();
            this.btnGetItems = new System.Windows.Forms.Button();
            this.btnSendCust = new System.Windows.Forms.Button();
            this.btnSendInvoice = new System.Windows.Forms.Button();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.pnlSettings = new System.Windows.Forms.Panel();
            this.cboSubBranch = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboBranch = new System.Windows.Forms.ComboBox();
            this.pnlServer2 = new System.Windows.Forms.Panel();
            this.cmdRefreshServer2 = new System.Windows.Forms.Button();
            this.lblDatabase2 = new System.Windows.Forms.Label();
            this.cboPOSDB = new System.Windows.Forms.ComboBox();
            this.lblPassword2 = new System.Windows.Forms.Label();
            this.lblUserName2 = new System.Windows.Forms.Label();
            this.txtPassword2 = new System.Windows.Forms.TextBox();
            this.txtUser2 = new System.Windows.Forms.TextBox();
            this.rbSQLServerAuthentication2 = new System.Windows.Forms.RadioButton();
            this.rbWindowsAuthentication2 = new System.Windows.Forms.RadioButton();
            this.cboPOS = new System.Windows.Forms.ComboBox();
            this.lblServer2 = new System.Windows.Forms.Label();
            this.lblcboWareHouse = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cboWareHouse = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdRefreshServer1 = new System.Windows.Forms.Button();
            this.lblDatabase1 = new System.Windows.Forms.Label();
            this.cboFinanceDB = new System.Windows.Forms.ComboBox();
            this.lblPassword1 = new System.Windows.Forms.Label();
            this.lblUserName1 = new System.Windows.Forms.Label();
            this.txtPassword1 = new System.Windows.Forms.TextBox();
            this.txtUser1 = new System.Windows.Forms.TextBox();
            this.rbSQLServerAuthentication1 = new System.Windows.Forms.RadioButton();
            this.rbWindowsAuthentication1 = new System.Windows.Forms.RadioButton();
            this.cboFinance = new System.Windows.Forms.ComboBox();
            this.lblServer1 = new System.Windows.Forms.Label();
            this.pnlServer1 = new System.Windows.Forms.Panel();
            this.cboCentral = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.rbWindowsAuthentication3 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.rbSQLServerAuthentication3 = new System.Windows.Forms.RadioButton();
            this.txtUser3 = new System.Windows.Forms.TextBox();
            this.cboCentralDB = new System.Windows.Forms.ComboBox();
            this.txtPassword3 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLogin = new System.Windows.Forms.Button();
            this.chkProgress = new System.Windows.Forms.CheckedListBox();
            this.ilIcons = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.pnlSettings.SuspendLayout();
            this.pnlServer2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlServer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.btnGetInvoice);
            this.splitContainer1.Panel1.Controls.Add(this.btnSendOrder);
            this.splitContainer1.Panel1.Controls.Add(this.btnDeliveryWeb);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetUsers);
            this.splitContainer1.Panel1.Controls.Add(this.btnSendInvoiceReturn);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetItems);
            this.splitContainer1.Panel1.Controls.Add(this.btnSendCust);
            this.splitContainer1.Panel1.Controls.Add(this.btnSendInvoice);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(927, 592);
            this.splitContainer1.SplitterDistance = 232;
            this.splitContainer1.TabIndex = 0;
            // 
            // btnGetInvoice
            // 
            this.btnGetInvoice.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnGetInvoice.Location = new System.Drawing.Point(18, 211);
            this.btnGetInvoice.Name = "btnGetInvoice";
            this.btnGetInvoice.Size = new System.Drawing.Size(203, 23);
            this.btnGetInvoice.TabIndex = 19;
            this.btnGetInvoice.Text = "8.Get Invoices from Central";
            this.btnGetInvoice.UseVisualStyleBackColor = true;
            this.btnGetInvoice.Visible = false;
            this.btnGetInvoice.Click += new System.EventHandler(this.btnGetInvoice_Click);
            // 
            // btnSendOrder
            // 
            this.btnSendOrder.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSendOrder.Location = new System.Drawing.Point(18, 182);
            this.btnSendOrder.Name = "btnSendOrder";
            this.btnSendOrder.Size = new System.Drawing.Size(203, 23);
            this.btnSendOrder.TabIndex = 18;
            this.btnSendOrder.Text = "7.Send Delivery To Web";
            this.btnSendOrder.UseVisualStyleBackColor = true;
            this.btnSendOrder.Visible = false;
            this.btnSendOrder.Click += new System.EventHandler(this.btnSendOrder_Click);
            // 
            // btnDeliveryWeb
            // 
            this.btnDeliveryWeb.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnDeliveryWeb.Location = new System.Drawing.Point(18, 153);
            this.btnDeliveryWeb.Name = "btnDeliveryWeb";
            this.btnDeliveryWeb.Size = new System.Drawing.Size(203, 23);
            this.btnDeliveryWeb.TabIndex = 17;
            this.btnDeliveryWeb.Text = "6.Get Delivery From Web";
            this.btnDeliveryWeb.UseVisualStyleBackColor = true;
            this.btnDeliveryWeb.Visible = false;
            this.btnDeliveryWeb.Click += new System.EventHandler(this.btnDeliveryWeb_Click);
            // 
            // btnGetUsers
            // 
            this.btnGetUsers.Location = new System.Drawing.Point(18, 37);
            this.btnGetUsers.Name = "btnGetUsers";
            this.btnGetUsers.Size = new System.Drawing.Size(203, 23);
            this.btnGetUsers.TabIndex = 9;
            this.btnGetUsers.Text = "2.Get Users to POS";
            this.btnGetUsers.UseVisualStyleBackColor = true;
            this.btnGetUsers.Click += new System.EventHandler(this.btnGetUsers_Click);
            // 
            // btnSendInvoiceReturn
            // 
            this.btnSendInvoiceReturn.Location = new System.Drawing.Point(18, 95);
            this.btnSendInvoiceReturn.Name = "btnSendInvoiceReturn";
            this.btnSendInvoiceReturn.Size = new System.Drawing.Size(203, 23);
            this.btnSendInvoiceReturn.TabIndex = 5;
            this.btnSendInvoiceReturn.Text = "4.Send Invoice Return to Central";
            this.btnSendInvoiceReturn.UseVisualStyleBackColor = true;
            this.btnSendInvoiceReturn.Click += new System.EventHandler(this.btnSendInvoiceReturn_Click);
            // 
            // btnGetItems
            // 
            this.btnGetItems.Location = new System.Drawing.Point(18, 124);
            this.btnGetItems.Name = "btnGetItems";
            this.btnGetItems.Size = new System.Drawing.Size(203, 23);
            this.btnGetItems.TabIndex = 3;
            this.btnGetItems.Text = "5.Get Items";
            this.btnGetItems.UseVisualStyleBackColor = true;
            this.btnGetItems.Click += new System.EventHandler(this.btnGetItems_Click);
            // 
            // btnSendCust
            // 
            this.btnSendCust.Location = new System.Drawing.Point(18, 8);
            this.btnSendCust.Name = "btnSendCust";
            this.btnSendCust.Size = new System.Drawing.Size(203, 23);
            this.btnSendCust.TabIndex = 1;
            this.btnSendCust.Text = "1.Send Customers to Central";
            this.btnSendCust.UseVisualStyleBackColor = true;
            this.btnSendCust.Click += new System.EventHandler(this.btnSendCust_Click);
            // 
            // btnSendInvoice
            // 
            this.btnSendInvoice.Location = new System.Drawing.Point(18, 66);
            this.btnSendInvoice.Name = "btnSendInvoice";
            this.btnSendInvoice.Size = new System.Drawing.Size(203, 23);
            this.btnSendInvoice.TabIndex = 0;
            this.btnSendInvoice.Text = "3.Send Invoice to Central";
            this.btnSendInvoice.UseVisualStyleBackColor = true;
            this.btnSendInvoice.Click += new System.EventHandler(this.btnSendInvoice_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.pnlSettings);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.chkProgress);
            this.splitContainer2.Size = new System.Drawing.Size(687, 588);
            this.splitContainer2.SplitterDistance = 366;
            this.splitContainer2.TabIndex = 0;
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.cboSubBranch);
            this.pnlSettings.Controls.Add(this.label2);
            this.pnlSettings.Controls.Add(this.label4);
            this.pnlSettings.Controls.Add(this.label3);
            this.pnlSettings.Controls.Add(this.label1);
            this.pnlSettings.Controls.Add(this.cboBranch);
            this.pnlSettings.Controls.Add(this.pnlServer2);
            this.pnlSettings.Controls.Add(this.lblcboWareHouse);
            this.pnlSettings.Controls.Add(this.label5);
            this.pnlSettings.Controls.Add(this.cboWareHouse);
            this.pnlSettings.Controls.Add(this.panel1);
            this.pnlSettings.Controls.Add(this.pnlServer1);
            this.pnlSettings.Controls.Add(this.btnSave);
            this.pnlSettings.Controls.Add(this.btnLogin);
            this.pnlSettings.Location = new System.Drawing.Point(3, 3);
            this.pnlSettings.Name = "pnlSettings";
            this.pnlSettings.Size = new System.Drawing.Size(671, 379);
            this.pnlSettings.TabIndex = 1;
            // 
            // cboSubBranch
            // 
            this.cboSubBranch.FormattingEnabled = true;
            this.cboSubBranch.Location = new System.Drawing.Point(445, 299);
            this.cboSubBranch.Name = "cboSubBranch";
            this.cboSubBranch.Size = new System.Drawing.Size(199, 21);
            this.cboSubBranch.TabIndex = 11;
            this.cboSubBranch.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(335, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "POS Server  Data";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(343, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Market WareHouse";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(343, 302);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Sub Branch";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Central Server  Data";
            // 
            // cboBranch
            // 
            this.cboBranch.FormattingEnabled = true;
            this.cboBranch.Location = new System.Drawing.Point(445, 272);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(199, 21);
            this.cboBranch.TabIndex = 7;
            this.cboBranch.Visible = false;
            this.cboBranch.SelectedIndexChanged += new System.EventHandler(this.cboBranch_SelectedIndexChanged);
            // 
            // pnlServer2
            // 
            this.pnlServer2.Controls.Add(this.cmdRefreshServer2);
            this.pnlServer2.Controls.Add(this.lblDatabase2);
            this.pnlServer2.Controls.Add(this.cboPOSDB);
            this.pnlServer2.Controls.Add(this.lblPassword2);
            this.pnlServer2.Controls.Add(this.lblUserName2);
            this.pnlServer2.Controls.Add(this.txtPassword2);
            this.pnlServer2.Controls.Add(this.txtUser2);
            this.pnlServer2.Controls.Add(this.rbSQLServerAuthentication2);
            this.pnlServer2.Controls.Add(this.rbWindowsAuthentication2);
            this.pnlServer2.Controls.Add(this.cboPOS);
            this.pnlServer2.Controls.Add(this.lblServer2);
            this.pnlServer2.Location = new System.Drawing.Point(338, 20);
            this.pnlServer2.Name = "pnlServer2";
            this.pnlServer2.Size = new System.Drawing.Size(317, 164);
            this.pnlServer2.TabIndex = 2;
            // 
            // cmdRefreshServer2
            // 
            this.cmdRefreshServer2.Location = new System.Drawing.Point(285, 3);
            this.cmdRefreshServer2.Name = "cmdRefreshServer2";
            this.cmdRefreshServer2.Size = new System.Drawing.Size(21, 23);
            this.cmdRefreshServer2.TabIndex = 11;
            this.cmdRefreshServer2.UseVisualStyleBackColor = true;
            // 
            // lblDatabase2
            // 
            this.lblDatabase2.AutoSize = true;
            this.lblDatabase2.Location = new System.Drawing.Point(5, 133);
            this.lblDatabase2.Name = "lblDatabase2";
            this.lblDatabase2.Size = new System.Drawing.Size(57, 13);
            this.lblDatabase2.TabIndex = 9;
            this.lblDatabase2.Text = "Database:";
            // 
            // cboPOSDB
            // 
            this.cboPOSDB.FormattingEnabled = true;
            this.cboPOSDB.Location = new System.Drawing.Point(63, 130);
            this.cboPOSDB.Name = "cboPOSDB";
            this.cboPOSDB.Size = new System.Drawing.Size(243, 21);
            this.cboPOSDB.TabIndex = 8;
            this.cboPOSDB.Click += new System.EventHandler(this.cboPOSDB_Click);
            // 
            // lblPassword2
            // 
            this.lblPassword2.AutoSize = true;
            this.lblPassword2.Location = new System.Drawing.Point(96, 107);
            this.lblPassword2.Name = "lblPassword2";
            this.lblPassword2.Size = new System.Drawing.Size(57, 13);
            this.lblPassword2.TabIndex = 7;
            this.lblPassword2.Text = "Password:";
            // 
            // lblUserName2
            // 
            this.lblUserName2.AutoSize = true;
            this.lblUserName2.Location = new System.Drawing.Point(89, 81);
            this.lblUserName2.Name = "lblUserName2";
            this.lblUserName2.Size = new System.Drawing.Size(63, 13);
            this.lblUserName2.TabIndex = 6;
            this.lblUserName2.Text = "User Name:";
            // 
            // txtPassword2
            // 
            this.txtPassword2.Location = new System.Drawing.Point(158, 104);
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.PasswordChar = '*';
            this.txtPassword2.Size = new System.Drawing.Size(148, 20);
            this.txtPassword2.TabIndex = 5;
            // 
            // txtUser2
            // 
            this.txtUser2.Location = new System.Drawing.Point(158, 78);
            this.txtUser2.Name = "txtUser2";
            this.txtUser2.Size = new System.Drawing.Size(148, 20);
            this.txtUser2.TabIndex = 4;
            // 
            // rbSQLServerAuthentication2
            // 
            this.rbSQLServerAuthentication2.AutoSize = true;
            this.rbSQLServerAuthentication2.Location = new System.Drawing.Point(63, 55);
            this.rbSQLServerAuthentication2.Name = "rbSQLServerAuthentication2";
            this.rbSQLServerAuthentication2.Size = new System.Drawing.Size(151, 17);
            this.rbSQLServerAuthentication2.TabIndex = 3;
            this.rbSQLServerAuthentication2.Text = "SQL Server authentication";
            this.rbSQLServerAuthentication2.UseVisualStyleBackColor = true;
            // 
            // rbWindowsAuthentication2
            // 
            this.rbWindowsAuthentication2.AutoSize = true;
            this.rbWindowsAuthentication2.Checked = true;
            this.rbWindowsAuthentication2.Location = new System.Drawing.Point(63, 32);
            this.rbWindowsAuthentication2.Name = "rbWindowsAuthentication2";
            this.rbWindowsAuthentication2.Size = new System.Drawing.Size(140, 17);
            this.rbWindowsAuthentication2.TabIndex = 2;
            this.rbWindowsAuthentication2.TabStop = true;
            this.rbWindowsAuthentication2.Text = "Windows authentication";
            this.rbWindowsAuthentication2.UseVisualStyleBackColor = true;
            this.rbWindowsAuthentication2.Visible = false;
            // 
            // cboPOS
            // 
            this.cboPOS.FormattingEnabled = true;
            this.cboPOS.Location = new System.Drawing.Point(63, 4);
            this.cboPOS.Name = "cboPOS";
            this.cboPOS.Size = new System.Drawing.Size(216, 21);
            this.cboPOS.TabIndex = 1;
            this.cboPOS.Click += new System.EventHandler(this.cboPOSServer_SelectedIndexChanged);
            // 
            // lblServer2
            // 
            this.lblServer2.AutoSize = true;
            this.lblServer2.Location = new System.Drawing.Point(20, 7);
            this.lblServer2.Name = "lblServer2";
            this.lblServer2.Size = new System.Drawing.Size(43, 13);
            this.lblServer2.TabIndex = 0;
            this.lblServer2.Text = "Server:";
            // 
            // lblcboWareHouse
            // 
            this.lblcboWareHouse.AutoSize = true;
            this.lblcboWareHouse.Location = new System.Drawing.Point(343, 275);
            this.lblcboWareHouse.Name = "lblcboWareHouse";
            this.lblcboWareHouse.Size = new System.Drawing.Size(76, 13);
            this.lblcboWareHouse.TabIndex = 6;
            this.lblcboWareHouse.Text = "Market Branch";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 189);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "ERP Server  Data";
            // 
            // cboWareHouse
            // 
            this.cboWareHouse.FormattingEnabled = true;
            this.cboWareHouse.Location = new System.Drawing.Point(445, 245);
            this.cboWareHouse.Name = "cboWareHouse";
            this.cboWareHouse.Size = new System.Drawing.Size(199, 21);
            this.cboWareHouse.TabIndex = 5;
            this.cboWareHouse.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdRefreshServer1);
            this.panel1.Controls.Add(this.lblDatabase1);
            this.panel1.Controls.Add(this.cboFinanceDB);
            this.panel1.Controls.Add(this.lblPassword1);
            this.panel1.Controls.Add(this.lblUserName1);
            this.panel1.Controls.Add(this.txtPassword1);
            this.panel1.Controls.Add(this.txtUser1);
            this.panel1.Controls.Add(this.rbSQLServerAuthentication1);
            this.panel1.Controls.Add(this.rbWindowsAuthentication1);
            this.panel1.Controls.Add(this.cboFinance);
            this.panel1.Controls.Add(this.lblServer1);
            this.panel1.Location = new System.Drawing.Point(15, 205);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(317, 164);
            this.panel1.TabIndex = 7;
            // 
            // cmdRefreshServer1
            // 
            this.cmdRefreshServer1.Location = new System.Drawing.Point(285, 9);
            this.cmdRefreshServer1.Name = "cmdRefreshServer1";
            this.cmdRefreshServer1.Size = new System.Drawing.Size(21, 23);
            this.cmdRefreshServer1.TabIndex = 10;
            this.cmdRefreshServer1.UseVisualStyleBackColor = true;
            // 
            // lblDatabase1
            // 
            this.lblDatabase1.AutoSize = true;
            this.lblDatabase1.Location = new System.Drawing.Point(6, 134);
            this.lblDatabase1.Name = "lblDatabase1";
            this.lblDatabase1.Size = new System.Drawing.Size(57, 13);
            this.lblDatabase1.TabIndex = 9;
            this.lblDatabase1.Text = "Database:";
            // 
            // cboFinanceDB
            // 
            this.cboFinanceDB.FormattingEnabled = true;
            this.cboFinanceDB.Location = new System.Drawing.Point(64, 131);
            this.cboFinanceDB.Name = "cboFinanceDB";
            this.cboFinanceDB.Size = new System.Drawing.Size(243, 21);
            this.cboFinanceDB.TabIndex = 8;
            this.cboFinanceDB.Click += new System.EventHandler(this.cboFinanceDB_Click);
            // 
            // lblPassword1
            // 
            this.lblPassword1.AutoSize = true;
            this.lblPassword1.Location = new System.Drawing.Point(82, 109);
            this.lblPassword1.Name = "lblPassword1";
            this.lblPassword1.Size = new System.Drawing.Size(57, 13);
            this.lblPassword1.TabIndex = 7;
            this.lblPassword1.Text = "Password:";
            // 
            // lblUserName1
            // 
            this.lblUserName1.AutoSize = true;
            this.lblUserName1.Location = new System.Drawing.Point(75, 83);
            this.lblUserName1.Name = "lblUserName1";
            this.lblUserName1.Size = new System.Drawing.Size(63, 13);
            this.lblUserName1.TabIndex = 6;
            this.lblUserName1.Text = "User Name:";
            // 
            // txtPassword1
            // 
            this.txtPassword1.Location = new System.Drawing.Point(144, 106);
            this.txtPassword1.Name = "txtPassword1";
            this.txtPassword1.PasswordChar = '*';
            this.txtPassword1.Size = new System.Drawing.Size(162, 20);
            this.txtPassword1.TabIndex = 5;
            // 
            // txtUser1
            // 
            this.txtUser1.Location = new System.Drawing.Point(144, 80);
            this.txtUser1.Name = "txtUser1";
            this.txtUser1.Size = new System.Drawing.Size(162, 20);
            this.txtUser1.TabIndex = 4;
            // 
            // rbSQLServerAuthentication1
            // 
            this.rbSQLServerAuthentication1.AutoSize = true;
            this.rbSQLServerAuthentication1.Location = new System.Drawing.Point(49, 57);
            this.rbSQLServerAuthentication1.Name = "rbSQLServerAuthentication1";
            this.rbSQLServerAuthentication1.Size = new System.Drawing.Size(151, 17);
            this.rbSQLServerAuthentication1.TabIndex = 3;
            this.rbSQLServerAuthentication1.Text = "SQL Server authentication";
            this.rbSQLServerAuthentication1.UseVisualStyleBackColor = true;
            // 
            // rbWindowsAuthentication1
            // 
            this.rbWindowsAuthentication1.AutoSize = true;
            this.rbWindowsAuthentication1.Checked = true;
            this.rbWindowsAuthentication1.Location = new System.Drawing.Point(49, 34);
            this.rbWindowsAuthentication1.Name = "rbWindowsAuthentication1";
            this.rbWindowsAuthentication1.Size = new System.Drawing.Size(140, 17);
            this.rbWindowsAuthentication1.TabIndex = 2;
            this.rbWindowsAuthentication1.TabStop = true;
            this.rbWindowsAuthentication1.Text = "Windows authentication";
            this.rbWindowsAuthentication1.UseVisualStyleBackColor = true;
            this.rbWindowsAuthentication1.Visible = false;
            // 
            // cboFinance
            // 
            this.cboFinance.FormattingEnabled = true;
            this.cboFinance.Location = new System.Drawing.Point(64, 9);
            this.cboFinance.Name = "cboFinance";
            this.cboFinance.Size = new System.Drawing.Size(215, 21);
            this.cboFinance.TabIndex = 1;
            this.cboFinance.Click += new System.EventHandler(this.cboFinanceServer_SelectedIndexChanged);
            // 
            // lblServer1
            // 
            this.lblServer1.AutoSize = true;
            this.lblServer1.Location = new System.Drawing.Point(6, 12);
            this.lblServer1.Name = "lblServer1";
            this.lblServer1.Size = new System.Drawing.Size(43, 13);
            this.lblServer1.TabIndex = 0;
            this.lblServer1.Text = "Server:";
            // 
            // pnlServer1
            // 
            this.pnlServer1.Controls.Add(this.cboCentral);
            this.pnlServer1.Controls.Add(this.button1);
            this.pnlServer1.Controls.Add(this.label9);
            this.pnlServer1.Controls.Add(this.rbWindowsAuthentication3);
            this.pnlServer1.Controls.Add(this.label6);
            this.pnlServer1.Controls.Add(this.rbSQLServerAuthentication3);
            this.pnlServer1.Controls.Add(this.txtUser3);
            this.pnlServer1.Controls.Add(this.cboCentralDB);
            this.pnlServer1.Controls.Add(this.txtPassword3);
            this.pnlServer1.Controls.Add(this.label8);
            this.pnlServer1.Controls.Add(this.label7);
            this.pnlServer1.Location = new System.Drawing.Point(15, 20);
            this.pnlServer1.Name = "pnlServer1";
            this.pnlServer1.Size = new System.Drawing.Size(317, 164);
            this.pnlServer1.TabIndex = 1;
            // 
            // cboCentral
            // 
            this.cboCentral.FormattingEnabled = true;
            this.cboCentral.Location = new System.Drawing.Point(64, 5);
            this.cboCentral.Name = "cboCentral";
            this.cboCentral.Size = new System.Drawing.Size(216, 21);
            this.cboCentral.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(286, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(21, 23);
            this.button1.TabIndex = 10;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Server:";
            // 
            // rbWindowsAuthentication3
            // 
            this.rbWindowsAuthentication3.AutoSize = true;
            this.rbWindowsAuthentication3.Checked = true;
            this.rbWindowsAuthentication3.Location = new System.Drawing.Point(64, 33);
            this.rbWindowsAuthentication3.Name = "rbWindowsAuthentication3";
            this.rbWindowsAuthentication3.Size = new System.Drawing.Size(140, 17);
            this.rbWindowsAuthentication3.TabIndex = 2;
            this.rbWindowsAuthentication3.TabStop = true;
            this.rbWindowsAuthentication3.Text = "Windows authentication";
            this.rbWindowsAuthentication3.UseVisualStyleBackColor = true;
            this.rbWindowsAuthentication3.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Database:";
            // 
            // rbSQLServerAuthentication3
            // 
            this.rbSQLServerAuthentication3.AutoSize = true;
            this.rbSQLServerAuthentication3.Location = new System.Drawing.Point(64, 56);
            this.rbSQLServerAuthentication3.Name = "rbSQLServerAuthentication3";
            this.rbSQLServerAuthentication3.Size = new System.Drawing.Size(151, 17);
            this.rbSQLServerAuthentication3.TabIndex = 3;
            this.rbSQLServerAuthentication3.Text = "SQL Server authentication";
            this.rbSQLServerAuthentication3.UseVisualStyleBackColor = true;
            // 
            // txtUser3
            // 
            this.txtUser3.Location = new System.Drawing.Point(159, 79);
            this.txtUser3.Name = "txtUser3";
            this.txtUser3.Size = new System.Drawing.Size(148, 20);
            this.txtUser3.TabIndex = 4;
            // 
            // cboCentralDB
            // 
            this.cboCentralDB.FormattingEnabled = true;
            this.cboCentralDB.Location = new System.Drawing.Point(64, 131);
            this.cboCentralDB.Name = "cboCentralDB";
            this.cboCentralDB.Size = new System.Drawing.Size(243, 21);
            this.cboCentralDB.TabIndex = 8;
            this.cboCentralDB.Click += new System.EventHandler(this.cboCentralDB_Click);
            // 
            // txtPassword3
            // 
            this.txtPassword3.Location = new System.Drawing.Point(159, 105);
            this.txtPassword3.Name = "txtPassword3";
            this.txtPassword3.PasswordChar = '*';
            this.txtPassword3.Size = new System.Drawing.Size(148, 20);
            this.txtPassword3.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(90, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "User Name:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(97, 108);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Password:";
            // 
            // btnSave
            // 
            this.btnSave.AutoSize = true;
            this.btnSave.Location = new System.Drawing.Point(339, 334);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(83, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save Settings";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(339, 214);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(82, 23);
            this.btnLogin.TabIndex = 3;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // chkProgress
            // 
            this.chkProgress.FormattingEnabled = true;
            this.chkProgress.Location = new System.Drawing.Point(3, 8);
            this.chkProgress.Name = "chkProgress";
            this.chkProgress.Size = new System.Drawing.Size(668, 154);
            this.chkProgress.TabIndex = 0;
            // 
            // ilIcons
            // 
            this.ilIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilIcons.ImageStream")));
            this.ilIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ilIcons.Images.SetKeyName(0, "Microsoft.SqlServer.Management.SqlManagerUI.Images.storedproc.ico");
            this.ilIcons.Images.SetKeyName(1, "Microsoft.SqlServer.Management.SqlManagerUI.Images.table.ico");
            this.ilIcons.Images.SetKeyName(2, "Microsoft.SqlServer.Management.SqlManagerUI.Images.udf.ico");
            this.ilIcons.Images.SetKeyName(3, "Microsoft.SqlServer.Management.SqlManagerUI.Images.view.ico");
            this.ilIcons.Images.SetKeyName(4, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.application_role.i" +
        "co");
            this.ilIcons.Images.SetKeyName(5, "Microsoft.SqlServer.Management.SqlMgmt.Images.database_roles_16x.ico");
            this.ilIcons.Images.SetKeyName(6, "Microsoft.SqlServer.Management.SqlManagerUI.Images.default.ico");
            this.ilIcons.Images.SetKeyName(7, "Microsoft.SqlServer.Management.SqlMgmt.Images.full_text_catalog.ico");
            this.ilIcons.Images.SetKeyName(8, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.message_type.ico");
            this.ilIcons.Images.SetKeyName(9, "Microsoft.SqlServer.Management.SqlManagerUI.Images.partition_function.ico");
            this.ilIcons.Images.SetKeyName(10, "Microsoft.SqlServer.Management.SqlManagerUI.Images.partition_scheme.ico");
            this.ilIcons.Images.SetKeyName(11, "Microsoft.VisualStudio.DataTools.Default.ico");
            this.ilIcons.Images.SetKeyName(12, "Microsoft.SqlServer.Management.SqlManagerUI.Images.plan_guide_enabled.ico");
            this.ilIcons.Images.SetKeyName(13, "Microsoft.SqlServer.Management.SqlManagerUI.Images.rule.ico");
            this.ilIcons.Images.SetKeyName(14, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.schema.ico");
            this.ilIcons.Images.SetKeyName(15, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.remote_service.ico" +
        "");
            this.ilIcons.Images.SetKeyName(16, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.contract.ico");
            this.ilIcons.Images.SetKeyName(17, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.queue.ico");
            this.ilIcons.Images.SetKeyName(18, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.route.ico");
            this.ilIcons.Images.SetKeyName(19, "Microsoft.VisualStudio.DataTools.Assembly.ico");
            this.ilIcons.Images.SetKeyName(20, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.database_trigger.i" +
        "co");
            this.ilIcons.Images.SetKeyName(21, "Microsoft.SqlServer.Management.SqlManagerUI.Images.synonym.ico");
            this.ilIcons.Images.SetKeyName(22, "Microsoft.SqlServer.Management.SqlMgmt.Images.user_16x.ico");
            this.ilIcons.Images.SetKeyName(23, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.aggregate_valued_f" +
        "unction.ico");
            this.ilIcons.Images.SetKeyName(24, "Microsoft.SqlServer.Management.SqlMgmt.Images.user_defined_data_type.ico");
            this.ilIcons.Images.SetKeyName(25, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.user_defined_objec" +
        "t_type.ico");
            this.ilIcons.Images.SetKeyName(26, "Microsoft.SqlServer.Management.SqlManagerUI.Images.XML_schemas_16x.ico");
            this.ilIcons.Images.SetKeyName(27, "Microsoft.VisualStudio.DataTools.Trigger.ico");
            this.ilIcons.Images.SetKeyName(28, "Microsoft.VisualStudio.DataTools.Index.ico");
            this.ilIcons.Images.SetKeyName(29, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.broker_service.ico" +
        "");
            this.ilIcons.Images.SetKeyName(30, "Microsoft.SqlServer.Management.SqlManagerUI.Images.table_valued_function.ico");
            this.ilIcons.Images.SetKeyName(31, "Microsoft.SqlServer.Management.SqlManagerUI.Images.scalar_valued_function.ico");
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(927, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // MainRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(927, 617);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainRun";
            this.Text = "POS Local Exchange";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainRun_FormClosed);
            this.Load += new System.EventHandler(this.ObjectCompare_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            this.pnlServer2.ResumeLayout(false);
            this.pnlServer2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlServer1.ResumeLayout(false);
            this.pnlServer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ImageList ilIcons;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Button btnSendInvoice;
        private System.Windows.Forms.Panel pnlSettings;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Panel pnlServer2;
        private System.Windows.Forms.Button cmdRefreshServer2;
        private System.Windows.Forms.Label lblDatabase2;
        private System.Windows.Forms.ComboBox cboPOSDB;
        private System.Windows.Forms.Label lblPassword2;
        private System.Windows.Forms.Label lblUserName2;
        private System.Windows.Forms.TextBox txtPassword2;
        private System.Windows.Forms.TextBox txtUser2;
        private System.Windows.Forms.RadioButton rbSQLServerAuthentication2;
        private System.Windows.Forms.RadioButton rbWindowsAuthentication2;
        private System.Windows.Forms.ComboBox cboPOS;
        private System.Windows.Forms.Label lblServer2;
        private System.Windows.Forms.Panel pnlServer1;
        private System.Windows.Forms.Button cmdRefreshServer1;
        private System.Windows.Forms.Label lblDatabase1;
        private System.Windows.Forms.ComboBox cboFinanceDB;
        private System.Windows.Forms.Label lblPassword1;
        private System.Windows.Forms.Label lblUserName1;
        private System.Windows.Forms.TextBox txtPassword1;
        private System.Windows.Forms.TextBox txtUser1;
        private System.Windows.Forms.RadioButton rbSQLServerAuthentication1;
        private System.Windows.Forms.RadioButton rbWindowsAuthentication1;
        private System.Windows.Forms.ComboBox cboFinance;
        private System.Windows.Forms.Label lblServer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox chkProgress;
        private System.Windows.Forms.Button btnSendCust;
        private System.Windows.Forms.Button btnSendInvoiceReturn;
        private System.Windows.Forms.Button btnGetItems;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.Label lblcboWareHouse;
        private System.Windows.Forms.ComboBox cboWareHouse;
        private System.Windows.Forms.Button btnGetUsers;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboBranch;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDeliveryWeb;
        private System.Windows.Forms.Button btnSendOrder;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboCentralDB;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPassword3;
        private System.Windows.Forms.TextBox txtUser3;
        private System.Windows.Forms.RadioButton rbSQLServerAuthentication3;
        private System.Windows.Forms.RadioButton rbWindowsAuthentication3;
        private System.Windows.Forms.ComboBox cboCentral;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboSubBranch;
        private System.Windows.Forms.Button btnGetInvoice;
    }
}