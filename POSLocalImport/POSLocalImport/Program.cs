﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace POSImportExport
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            if (args.Length == 0)
                Application.Run(new MainRun());
            else
            {
                MainRun MM = new MainRun();
                MM.LoadSavedDate();
                MM.btnLogin_Click(null, null);
                for (int i = 0; i < args.Length; i++)
                {
                    switch (args[i])
                    {
                        case "1":
                            MM.btnSendCust_Click(null, null);
                            break;
                        case "2":
                            MM.btnGetUsers_Click(null, null);
                            break;
                        case "3":
                            MM.btnSendInvoice_Click(null, null);
                            break;
                        case "4":
                            MM.btnSendInvoiceReturn_Click(null, null);
                            break;
                        case "5":
                            MM.btnGetItems_Click(null, null);
                            break;
                        case "6":
                            MM.btnDeliveryWeb_Click(null, null);
                            break;
                        case "7":
                            MM.btnSendOrder_Click(null, null);
                            break;
                        case "8":
                            MM.btnGetInvoice_Click(null, null);
                            break;
                        case "100":
                            MM.btnSendCust_Click(null, null);
                            MM.btnSendInvoice_Click(null, null);
                            MM.btnSendInvoiceReturn_Click(null, null);
                            break;
                        case "101":
                            MM.btnDeliveryWeb_Click(null, null);
                            MM.btnGetUsers_Click(null, null);
                            MM.btnSendOrder_Click(null, null);
                            break;
                            //case "1" :                                              
                            //     MM.btnBackupFinance_Click(null ,null);
                            //    break ;
                            //case "2" :
                            //    MM.BtnBackupPOS_Click(null, null);
                            //    break ;
                            //case "4":
                            //    MM.btnGetSupplier_Click(null, null);
                            //    break;
                            //case "9" :
                            //    MM.btnGetItemPrice_Click(null, null);
                            //    break;
                            //case "10":
                            //    MM.btnDelInvoices_Click(null, null);
                            //    break;
                            //case "11":
                            //    MM.btnDelReturnInv_Click(null, null);
                            //    break;
                            //case "12" :
                            //    MM.btnSalesResymc_Click(null, null);
                            //    break ;
                            //case "13" :
                            //    MM.btnSalesReturnSync_Click(null, null);
                            //    break;
                            //case "14" :
                            //    MM.btnPurchInvSync_Click(null, null);
                            //    break;
                            //case "15" :
                            //    MM.btnPurchInvRsync_Click(null, null);
                            //    break;
                            //case "61":
                            //    MM.btnSendInvHistory_Click(null , null);
                            //    break; 
                            //case "71" :
                            //    MM.btnSendInvRetHistory_Click(null, null);
                            //    break;
                            //case "411":
                            //    MM.SQLRunFinance(args[i]);
                            //    break;
                            //case "511":
                            //    MM.SQLRunPOS(args[i]);
                            //    break;
                    }

                }
            }
        }
    }
}
