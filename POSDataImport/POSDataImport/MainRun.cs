﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Runtime.InteropServices;
using System.Data.SqlClient;


namespace POSImportExport
{
    public partial class MainRun : Form
    {
        string FinanceDB = null;
        string POSDB = null;
        string WareHouseDefault = null;
        string BranchDefault = null;

        Server FinanceServer = null;
        Server POSServer = null;
        bool showLoginScreen;

        string filename = "POSDataExchange.xml";

        public MainRun()
        {
            showLoginScreen = true;
            InitializeComponent();
        }

        public MainRun(bool _showLoginScreen)
        {
            showLoginScreen = _showLoginScreen;
            InitializeComponent();
        }

        #region connection and server realted
        public void SetFinanceServer(Server srv1)
        {
            FinanceServer = srv1;
        }

        public void SetPOSServer(Server srv2)
        {
            POSServer = srv2;
        }

        public void SetFinanceDB(string db1)
        {
            FinanceDB = db1;
        }

        public void SetPOSDB(string db2)
        {
            POSDB = db2;
        }

        public void cmdRefreshFinanceServer_Click(object sender, EventArgs e)
        {
            RefreshServerList(cboFinance);
        }

        public void cmdRefreshPOSServer_Click(object sender, EventArgs e)
        {
            RefreshServerList(cboFinance);
        }

        public void RefreshServerList(ComboBox cbo)
        {
            cbo.Items.Clear();
            DataTable dt = SmoApplication.EnumAvailableSqlServers(false);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    cbo.Items.Add(dr["Name"].ToString());
                }
            }
        }

        public void RefreshDatabaseList(ComboBox cbo, string server)
        {
            try
            {
                cbo.Items.Clear();
                ServerConnection conn = new ServerConnection();
                conn.ServerInstance = server;
                Server srv = new Server(conn);

                foreach (Database db in srv.Databases)
                {
                    cbo.Items.Add(db.Name);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Authentication Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void RefreshDatabaseList(ComboBox cbo, string server, string login, string password)
        {
            try
            {
                cbo.Items.Clear();
                ServerConnection conn = new ServerConnection();
                conn.ServerInstance = server;
                conn.LoginSecure = false;
                conn.Login = login;
                conn.Password = password;
                Server srv = new Server(conn);

                foreach (Database db in srv.Databases)
                {
                    cbo.Items.Add(db.Name);
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Authentication Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void cboPOSDB_Click(object sender, EventArgs e)
        {
            if (cboPOSDB.Items.Count == 0)
            {
                if (rbWindowsAuthentication2.Checked)
                {
                    RefreshDatabaseList(cboPOSDB, cboPOS.Text);
                }
                else
                {
                    RefreshDatabaseList(cboPOSDB, cboPOS.Text, txtUser2.Text, txtPassword2.Text);
                }
            }
        }

        public void cboFinanceDB_Click(object sender, EventArgs e)
        {
            if (cboFinanceDB.Items.Count == 0)
            {
                if (rbWindowsAuthentication1.Checked)
                {
                    RefreshDatabaseList(cboFinanceDB, cboFinance.Text);
                }
                else
                {
                    RefreshDatabaseList(cboFinanceDB, cboFinance.Text, txtUser1.Text, txtPassword1.Text);
                }
            }
        }

        public void cboFinanceServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboFinanceDB.Items.Clear();
        }

        public void cboPOSServer_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboPOSDB.Items.Clear();
        }

        public void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                POSDataActions.DoLogging("Connecting to ERP Server", "-900", "Connecting..", false, "ERP Connection");
                if (rbSQLServerAuthentication1.Checked == true)
                {
                    ServerConnection conn = new ServerConnection();
                    conn.ServerInstance = cboFinance.Text;
                    conn.LoginSecure = false;
                    conn.Login = txtUser1.Text;
                    conn.Password = txtPassword1.Text;
                    FinanceServer = new Server(conn);
                }
                else
                {
                    ServerConnection conn = new ServerConnection();
                    conn.ServerInstance = cboFinance.Text;
                    FinanceServer = new Server(conn);
                }
                POSDataActions.DoLogging("Connecting to ERP Server", "-900", "Connected", false, "ERP Connection");
            }
            catch (Exception ex)
            {
                POSDataActions.DoLogging("Connecting to ERP Server", "-900", ex.Message, false, "ERP Connection");
            }


            try
            {
                POSDataActions.DoLogging("Connecting to Central Server", "-900", "Connecting..", false, "Central Connection");
                if (rbSQLServerAuthentication2.Checked == true)
                {
                    ServerConnection conn = new ServerConnection();
                    conn.ServerInstance = cboPOS.Text;
                    conn.LoginSecure = false;
                    conn.Login = txtUser2.Text;
                    conn.Password = txtPassword2.Text;
                    POSServer = new Server(conn);
                }
                else
                {
                    ServerConnection conn = new ServerConnection();
                    conn.ServerInstance = cboPOS.Text;
                    POSServer = new Server(conn);
                }
                POSDataActions.DoLogging("Connecting to Central Server", "-900", "Connected", false, "Central Connection");
            }
            catch (Exception ex)
            {
                POSDataActions.DoLogging("Connecting to Central Server", "-900", ex.Message, false, "Central Connection");
            }
            

            btnSendCust.Visible = true;
            btnDelReturnInv.Visible = true;
            btnBackupFinance.Visible = true;
            BtnBackupPOS.Visible = true;
            btnSendInvoiceReturn.Visible = true;
            btnDelInvoices.Visible = true;
            btnGetItems.Visible = true;
            btnGetItemPrice.Visible = true;
            btnGetSupplier.Visible = true;
            btnSendInvoice.Visible = true;
            btnGetUsers.Visible = true;
            btnSalesResymc.Visible = true;
            btnSalesReturnSync.Visible = true;
            btnPurchInvSync.Visible = true;
            btnPurchInvRsync.Visible = true;
            btnSendInvHistory.Visible = true;
            btnSendInvRetHistory.Visible = true;
            btnSave.Visible = true;
            btnDeliveryWeb.Visible = true; 
            btnSendOrder.Visible = true;

            #region Fill in ware house combo
            cboWareHouse.Visible = true;
            DataTable table = new DataTable();
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(sourceConnectionString)) //use your conn. string here
                {
                    using (SqlDataAdapter da = new SqlDataAdapter(@"SELECT Id, Name FROM ivWareHouses", sqlConn))
                        da.Fill(table);
                }
            }
            catch { }
            cboWareHouse.DataSource = new BindingSource(table, null);
            cboWareHouse.DisplayMember = "Name"; //colum you want to show in comboBox
            cboWareHouse.ValueMember = "Id"; //column you want to use in the background (not necessary)!
            if (WareHouseDefault != null)
                cboWareHouse.SelectedValue  = WareHouseDefault;
            #endregion


            #region Fill in branch combo
            cboBranch.Visible = true;
            DataTable table2 = new DataTable();
            string sourceConnectionString2 = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            using (SqlConnection sqlConn = new SqlConnection(sourceConnectionString)) //use your conn. string here
            {
                using (SqlDataAdapter da = new SqlDataAdapter(@"SELECT Id, Name FROM [hrBranch]", sqlConn))
                    da.Fill(table2);
            }
            cboBranch.DataSource = new BindingSource(table2, null);
            cboBranch.DisplayMember = "Name"; //colum you want to show in comboBox
            cboBranch.ValueMember = "Id"; //column you want to use in the background (not necessary)!
            if (BranchDefault != null)
                cboBranch.SelectedValue = BranchDefault;
            #endregion

            try
            {
                string srv1 = FinanceServer.Information.Version.ToString();
                string srv2 = POSServer.Information.Version.ToString();
                this.DialogResult = DialogResult.OK;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion

        #region utils
        public void ObjectCompare_Load(object sender, EventArgs e)
        {

            LoadSavedDate();
            btnSendCust.Visible = false;
            btnDelReturnInv.Visible = false;
            btnBackupFinance.Visible = false;
            BtnBackupPOS.Visible = false;
            btnSendInvoiceReturn.Visible = false;
            btnDelInvoices.Visible = false;
            btnGetItems.Visible = false;
            btnGetItemPrice.Visible = false;
            btnGetSupplier.Visible = false;
            btnSendInvoice.Visible = false;
            btnGetUsers.Visible = false;
            //DataTable dt = SmoApplication.EnumAvailableSqlServers(false);

            //if (dt.Rows.Count > 0)
            //{
            //    foreach (DataRow dr in dt.Rows)
            //    {

            //        cboFinanceServer.Items.Add(dr["Name"].ToString());
            //        cboPOSServer.Items.Add(dr["Name"].ToString());

            //    }
            //}
        }

        public string Filename
        {
            get { return System.IO.Path.GetDirectoryName(Application.ExecutablePath) + "\\" + filename; }
            set { filename = value; }
        }

        public void LoadSavedDate()
        {

            try
            {
                using (XmlReader reader = XmlReader.Create(Filename))
                {
                    // Moves the reader to the root element.
                    reader.MoveToContent();
                    //reader.ReadToFollowing("FinanceServer");
                    reader.Read();

                    if (reader.ReadToFollowing("FinanceServer"))
                    {
                        reader.ReadStartElement("FinanceServer");
                        reader.ReadStartElement("Server");
                        cboFinance.Text = reader.ReadString(); reader.ReadEndElement();
                        rbSQLServerAuthentication1.Checked = true;
                        reader.ReadStartElement("Login");
                        txtUser1.Text = reader.ReadString(); reader.ReadEndElement();
                        reader.ReadStartElement("Password");
                        txtPassword1.Text = reader.ReadString(); reader.ReadEndElement();
                        reader.ReadStartElement("Database");
                        cboFinanceDB.Text = reader.ReadString(); reader.ReadEndElement();
                        reader.ReadStartElement("MarketWareHouse");
                        WareHouseDefault = reader.ReadString(); reader.ReadEndElement();
                        reader.ReadStartElement("BranchDefault");
                        BranchDefault = reader.ReadString(); reader.ReadEndElement();

                        reader.ReadStartElement("SwitchItems");
                        chkItemDesc.Checked = Convert.ToBoolean( reader.ReadString()); reader.ReadEndElement();

                    }

                    if (reader.ReadToFollowing("POSServer"))
                    {
                        reader.ReadStartElement("POSServer");
                        reader.ReadStartElement("Server");
                        cboPOS.Text = reader.ReadString(); reader.ReadEndElement();
                        rbSQLServerAuthentication2.Checked = true;
                        reader.ReadStartElement("Login");
                        txtUser2.Text = reader.ReadString(); reader.ReadEndElement();
                        reader.ReadStartElement("Password");
                        txtPassword2.Text = reader.ReadString(); reader.ReadEndElement();
                        reader.ReadStartElement("Database");
                        cboPOSDB.Text = reader.ReadString(); reader.ReadEndElement();
                    }
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Save()
        {
            if (filename == null)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "XML document|*.xml";
                saveFileDialog1.Title = "Save an Project File";
                saveFileDialog1.ShowDialog();
                if (saveFileDialog1.FileName != "")
                {
                    this.filename = saveFileDialog1.FileName;
                }
            }

            if (filename != null)
            {
                Save(filename);
            }
        }

        public void Save(string filename)
        {

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;

            using (XmlWriter writer = XmlWriter.Create(Filename, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("POSImportExport");

                writer.WriteStartElement("Settings");

                writer.WriteStartElement("FinanceServer");
                writer.WriteElementString("Server", FinanceServer.Name);
                writer.WriteElementString("Login", FinanceServer.ConnectionContext.Login);
                writer.WriteElementString("Password", FinanceServer.ConnectionContext.Password);
                writer.WriteElementString("Database", cboFinanceDB.Text);
                writer.WriteElementString("MarketWareHouse", cboWareHouse.SelectedValue.ToString());
                writer.WriteElementString("BranchDefault", cboBranch.SelectedValue.ToString());
                writer.WriteElementString("SwitchItems", chkItemDesc.Checked.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement("POSServer");
                writer.WriteElementString("Server", POSServer.Name);
                writer.WriteElementString("Login", POSServer.ConnectionContext.Login);
                writer.WriteElementString("Password", POSServer.ConnectionContext.Password);
                writer.WriteElementString("Database", cboPOSDB.Text);
                writer.WriteEndElement();

                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndDocument();

            }
        }

        #endregion

        public void btnSendCust_Click(object sender, EventArgs e)
        {
            chkProgress.Items.Add("Uploading customers data to Server", true);
            string sourceConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string destConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            POSDataActions.SendCustomerData(sourceConnectionString, destConnectionString);
        }

        public void btnGetSupplier_Click(object sender, EventArgs e)
        {
            chkProgress.Items.Add("DownLoading  Supplier data from Server", true);
            string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.GetSupplierData(sourceConnectionString, destConnectionString))
            {
                //MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void MainRun_FormClosed(object sender, FormClosedEventArgs e)
        {
           // Save();
        }

        public void btnBackupFinance_Click(object sender, EventArgs e)
        {
            chkProgress.Items.Add("Backup finance System to Temp folder", true);
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.BackupDB(sourceConnectionString))
                MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void BtnBackupPOS_Click(object sender, EventArgs e)
        {
            chkProgress.Items.Add("Backup POS DB to temp folder", true);
            string sourceConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            if (POSDataActions.BackupDB(sourceConnectionString))
                MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        public void btnSendInvoice_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("Uploading invoices data to Server", true);
            string sourceConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string destConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.SendInvoiceToFinance(sourceConnectionString, destConnectionString, cboWareHouse.SelectedValue.ToString(), cboBranch.SelectedValue.ToString()))
            {//MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close();
        }

        public void btnSendInvoiceReturn_Click(object sender, EventArgs e)
        {
            chkProgress.Items.Add("Uploading return invoices data to Server", true);
            string sourceConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string destConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.SendInvoiceReturnToFinance(sourceConnectionString, destConnectionString, cboWareHouse.SelectedValue.ToString(), cboBranch.SelectedValue.ToString()))
            {//MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void btnGetItems_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("DownLoading  items data from Server", true);
            string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.GetItemsData(sourceConnectionString, destConnectionString, cboWareHouse.SelectedValue.ToString(), cboBranch.SelectedValue.ToString(), chkItemDesc.Checked))
            {
                MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close();
        }

        public void btnDelInvoices_Click(object sender, EventArgs e)
        {
            chkProgress.Items.Add("Uploading return invoices data to Server", true);
            string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            //string destConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.CleanInvoiceData(destConnectionString))
            {//MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void btnDelReturnInv_Click(object sender, EventArgs e)
        {
            chkProgress.Items.Add("Uploading return invoices data to Server", true);
            string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            //string destConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.CleanReturnInvoiceData(destConnectionString))
            {//MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void btnGetUsers_Click(object sender, EventArgs e)
        {
            chkProgress.Items.Add("DownLoading  Users data from Server", true);
            string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.GetSalesRepsData(sourceConnectionString, destConnectionString, cboBranch.SelectedValue.ToString()))
            { //MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public void btnGetItemPrice_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("DownLoading  items data from Server", true);
            string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.GetItemsDataWithPrice(sourceConnectionString, destConnectionString, cboWareHouse.SelectedValue.ToString(), null, chkItemDesc.Checked))
            {
                MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close();
            //backgroundWorker1.RunWorkerAsync(17);
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;

            //frmWait pleaseWait = new frmWait();
           // pleaseWait.Show();
           // chkProgress.Items.Add("DownLoading  items data from Server", true);
            string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.GetItemsDataWithPrice(sourceConnectionString, destConnectionString, cboWareHouse.SelectedValue.ToString(),null, chkItemDesc.Checked))
                MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
           // pleaseWait.Close();
        }

        public bool SQLRunFinance(string filename)
        {
            string sql = System.IO.File.ReadAllText(filename);
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            using (SqlConnection tconn = new SqlConnection(sourceConnectionString))
            {
                SqlCommand Totalscmd = new SqlCommand(sql, tconn);
                tconn.Open();
                return Totalscmd.ExecuteNonQuery() > 0; 
            }

        }

        public bool SQLRunPOS(string filename)
        {
            string sql = System.IO.File.ReadAllText(filename);
            string sourceConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            using (SqlConnection tconn = new SqlConnection(sourceConnectionString))
            {
                SqlCommand Totalscmd = new SqlCommand(sql, tconn);
                tconn.Open();
                return Totalscmd.ExecuteNonQuery() > 0;
            }

        }

        public void btnSalesResymc_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("Resync Sales invoice to Multi Inv Out", true);
            //string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.ResyncSalesInvoice(sourceConnectionString))
            {//MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close(); 
        }

        public void btnSalesReturnSync_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("Resync Sales return invoice to Multi Inv in", true);
            //string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.ResyncSalesRInvoice(sourceConnectionString))
            {  //MessageBox.Show("Data was succefully Synced", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close(); 
        }

        public void btnPurchInvRsync_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("Resync purchase invoice to Multi Inv in", true);
            //string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.ResyncPurchaseRInvoice(sourceConnectionString))
            {//MessageBox.Show("Data was succefully Synced", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close(); 
        }

        public void btnPurchInvSync_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("Resync purchase return invoice to Multi Inv in", true);
            //string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string sourceConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.ResyncPurchaseInvoice(sourceConnectionString))
            {//MessageBox.Show("Data was succefully Synced", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close(); 
        }

        public void btnSendInvHistory_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("Uploading invoices data to Server", true);
            string sourceConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string destConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.SendInvoiceHistoryToFinance(sourceConnectionString, destConnectionString, cboWareHouse.SelectedValue.ToString(), cboBranch.SelectedValue.ToString()))
            {//MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close();
        }

        public void btnSendInvRetHistory_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("Uploading return invoices data to Server", true);
            string sourceConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string destConnectionString = FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.SendInvoiceReturnHistoryToFinance(sourceConnectionString, destConnectionString, cboWareHouse.SelectedValue.ToString(), cboBranch.SelectedValue.ToString()))
            { //MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        public void btnDeliveryWeb_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("Downloading Sales Orders From Server", true);
            string destConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string sourceConnectionString= FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.GetDeliveryFromFinance(sourceConnectionString, destConnectionString, cboWareHouse.SelectedValue.ToString(), cboBranch.SelectedValue.ToString()))
            {//MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close();
        }

        public void btnSendOrder_Click(object sender, EventArgs e)
        {
            frmWait pleaseWait = new frmWait();
            pleaseWait.Show();
            chkProgress.Items.Add("Downloading Sales Orders From Server", true);
            string sourceConnectionString = POSServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboPOSDB.Text + "'";
            //string sourceConnectionString = "Server="+POSServer.text+;Database=Club2011p5;Persist Security Info=True;user id=program;password=Alir;";
            string destConnectionString= FinanceServer.ConnectionContext.ConnectionString + "; Initial Catalog='" + cboFinanceDB.Text + "'";
            if (POSDataActions.SendOrdersToFinance(sourceConnectionString, destConnectionString, cboWareHouse.SelectedValue.ToString(), cboBranch.SelectedValue.ToString()))
            {//MessageBox.Show("Data was succefully uploaded", "Succefully done", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            pleaseWait.Close();
        }
    }

}


