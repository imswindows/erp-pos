﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using WTEG.Utils;
using WTEG;
using Microsoft.ApplicationBlocks.Data;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Reflection;

namespace POSImportExport
{
    class POSDataActions
    {
        static string LogFilePath = System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + "\\log\\";

        public static void DoLogging(string strMsg, string strMsgID, string strSource, bool isHours = true, string FileName = "")
        {
            string strLogFilePath = LogFilePath;

            if (isHours)
                strLogFilePath = strLogFilePath + FileName + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + DateTime.Now.Hour.ToString() + ".txt";

            else
                strLogFilePath = strLogFilePath + FileName + DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString() + ".txt";
            try
            {
                StreamWriter objStreamWriter;
                objStreamWriter = File.AppendText(strLogFilePath);
                objStreamWriter.WriteLine(DateTime.Now.ToString(@"dd/MM/yyyy hh:mm:ss tt") + "\t" + "\t" + strMsgID + "\t" + strMsg + " \t " + strSource);
                objStreamWriter.Flush();
                objStreamWriter.Close();
            }
            catch
            {
            }
            finally
            {
                //AppsettingsReader = null;
            }
        }
        private void Form1_Load(System.Object sender, System.EventArgs e)
        {
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection("Server=(local);" + "DataBase=Northwind; Integrated Security=SSPI");
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand("Select Count(*) from Employees", conn);
            conn.Open();

            cmd.CommandText = "SELECT * FROM Employees";
            System.Data.SqlClient.SqlDataReader rdr = cmd.ExecuteReader();
            DataSet ds = new DataSet();
            DataTable dt = null;

            //Normally is this "not done" in the load event, 
            //you will see a strange datagrid
            while (rdr.Read())
            {
                if (dt == null)
                {
                    dt = new DataTable();
                    DataTable dtschema = null;
                    dtschema = rdr.GetSchemaTable();
                    foreach (DataRow drschema in dtschema.Rows)
                    {
                        dt.Columns.Add(drschema["ColumnName"].ToString(), Type.GetType(drschema["DataType"].ToString()));
                    }
                }

                DataRow dr = dt.NewRow();
                object[] tempObject = new object[dt.Columns.Count];
                rdr.GetValues(tempObject);
                //did not go in one time
                dr.ItemArray = tempObject;
                dt.Rows.Add(dr);
                System.Threading.Thread.Sleep(500);
                //only for showing
            }
            ds.Tables.Add(dt);

            rdr.Close();
            conn.Dispose();
        }

        public void CopyFullTables(string sourceConnectionString, string destConnectionString, string SourceTable, string DestinationTable)
        {
            string sql = "Select * From " + SourceTable;
            using (SqlConnection sourceConn = new SqlConnection(sourceConnectionString))
            using (SqlCommand sourceCmd = new SqlCommand(sql, sourceConn))
            {
                sourceConn.Open();

                using (SqlDataReader reader = sourceCmd.ExecuteReader())
                using (SqlBulkCopy copier = new SqlBulkCopy(destConnectionString))
                {
                    copier.DestinationTableName = DestinationTable;
                    copier.BulkCopyTimeout = 300;

                    DataTable schema = reader.GetSchemaTable();
                    copier.ColumnMappings.Clear();
                    foreach (DataRow row in schema.Rows)
                    {
                        copier.ColumnMappings.Add(row["ColumnName"].ToString(), row["ColumnName"].ToString());
                    }

                    copier.WriteToServer(reader);
                }
            }
        }

        public static bool SendCustomerData(string sourceConnectionString, string destConnectionString)
        {
            string SQL = "SELECT  [ID] , [FirstName]+' '+ IsNull([LastName],'') as Name ,IsNull([EmailAddress] ,'unknown@aa.b') as EmailAddress,IsNull([PhoneNumber] ,'00') as PhoneNumber" +
                          ",IsNull([FaxNumber] ,'00') as FaxNumber ,IsnULL([Address1],'cAIRO') AS Address1 ,iSnuLL([Address2],'cAIRO') AS Address2 " +
                          " ,iSnULL([City],'cAIREO') AS City ,iSnULL([ZipCode],'11111') ZipCode ,iSnULL([Country] ,'eGYPT') AS Country ,iSnULL([CreditLimit] ,0) AS CreditLimit " +
                         "  FROM [dbo].[Customer]";
            //SqlConnection conn = new SqlConnection(sourceConnectionString);


            //SqlConnection DestConn = new SqlConnection(destConnectionString);
            try
            {
                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(SQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            int ID = (int)rdr["ID"];

                            string Name = (string)rdr["Name"];

                            string EmailAddress = (string)rdr["EmailAddress"];
                            string PhoneNumber = (string)rdr["PhoneNumber"];
                            string FaxNumber = (string)rdr["FaxNumber"];
                            string Address1 = (string)rdr["Address1"];
                            string Address2 = (string)rdr["Address2"];
                            string City = (string)rdr["City"];
                            decimal CreditLimit = (decimal)rdr["CreditLimit"];
                            string ZipCode = (string)rdr["ZipCode"];
                            //string Country = (string)rdr["Country"];
                            DoLogging(ID.ToString(), "100", "Send Customer" + ID.ToString(), false, "CustomerData");
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();
                                using (SqlCommand destcmd = new SqlCommand())
                                {
                                    destcmd.CommandText = "usp_arCustomerImport";
                                    destcmd.Connection = DestConn;
                                    destcmd.CommandType = CommandType.StoredProcedure;

                                    destcmd.Parameters.AddWithValue("ID", ID);
                                    destcmd.Parameters.AddWithValue("Name", Name);
                                    destcmd.Parameters.AddWithValue("Contact", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Address1", Address1);
                                    destcmd.Parameters.AddWithValue("Address2", Address2);
                                    destcmd.Parameters.AddWithValue("City", City);
                                    destcmd.Parameters.AddWithValue("Country", 1); //always Egypt
                                    destcmd.Parameters.AddWithValue("Email", EmailAddress);
                                    destcmd.Parameters.AddWithValue("PostalCode", ZipCode);
                                    destcmd.Parameters.AddWithValue("SalesTax", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("CustomerType", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Telephone1", PhoneNumber);
                                    destcmd.Parameters.AddWithValue("Fax", FaxNumber);
                                    destcmd.Parameters.AddWithValue("Telephone2", DBNull.Value);

                                    destcmd.Parameters.AddWithValue("arSalesRep", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Terms", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("CreditLemit", CreditLimit);
                                    destcmd.Parameters.AddWithValue("SupplierID", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Remarks", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Category", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("PaymentMethod", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("CustAcc", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Tel1type", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("tel2type", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("tel3type", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("IsActive", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("ConsolidationCustomer", DBNull.Value);


                                    destcmd.Parameters.AddWithValue("UserName", "System");
                                    destcmd.Parameters.AddWithValue("Source", "Export");
                                    destcmd.Parameters.AddWithValue("Subsys", "POS");
                                    destcmd.Parameters.AddWithValue("LanguageID", "ar-EG");


                                    destcmd.ExecuteNonQuery();
                                }
                                // DestConn.Close();

                            }
                        }
                        // conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("customer Data", "-900", err.Message, false, "CustomerData");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool GetSupplierData(string sourceConnectionString, string destConnectionString)
        {
            string SQL = "SELECT [ID] ,[Name],IsNull([City],'Cairo') City ,IsNull([Address1] ,'Cairo') Address1 ,IsNull(Fax ,'NA') Fax" +
                        ",IsNull([Address2],'Cairo') Address2,IsNull([PostalCode] ,'11111') PostalCode ,IsNull(Contact,Name) Contact" +
                        ",IsNull([Email],'u@c.b') Email ,'Egypt' [Country], 1 as IsActive ,IsNull([Telephone1],'00') Telephone1 " +
                        ",IsNull([SpecialText],'nothing') SpecialText " +
                         " FROM [dbo].[apSuppliers]";

            string SQLInsert = "SET IDENTITY_INSERT Supplier ON " +
                " INSERT INTO [dbo].[Supplier] (ID,[FirstName],[LastName] ,[City] ,[Address1],[Address2] ,[ZipCode] ,[EmailAddress] ,[Country],[Active],PhoneNumber ,[Notes] ,MinimumOrderAmmount ,CreatedDate ,TaxNumber ,FaxNumber)" +
             " VALUES (@ID ,@Name ,@LastName,@City ,@Address1 ,@Address2 ,@ZipCode ,@EmailAddress ,@Country , 1 ,@PhoneNumber  ,@Notes ,@MinimumOrderAmmount ,GetDate() ,'Infile' ,@FaxNumber)" +
             "SET IDENTITY_INSERT Supplier Off ";


            try
            {

                #region empty old data first
                string SQLEmpty = "Delete From Supplier";
                using (SqlConnection DelConn = new SqlConnection(destConnectionString))
                {
                    DelConn.Open();
                    using (SqlCommand destcmd = new SqlCommand())
                    {
                        destcmd.CommandText = SQLEmpty;
                        destcmd.Connection = DelConn;
                        destcmd.ExecuteNonQuery();
                    }
                    DelConn.Close();
                }

                #endregion

                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(SQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            Int16 ID = (Int16)rdr["ID"];

                            string Name = (string)rdr["Name"];

                            string EmailAddress = (string)rdr["Email"];
                            string PhoneNumber = (string)rdr["Telephone1"];
                            string FaxNumber = (string)rdr["Fax"];
                            string Address1 = (string)rdr["Address1"];
                            string Address2 = (string)rdr["Address2"];
                            string City = (string)rdr["City"];
                            string Contact = (string)rdr["Contact"];
                            string ZipCode = (string)rdr["PostalCode"];
                            string Country = (string)rdr["Country"];
                            string SpecialText = (string)rdr["SpecialText"];
                            DoLogging(ID.ToString(), "100", "Get supplier" + ID.ToString(), false, "SupplierData");
                            #endregion
                            #region apply to destination
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();
                                using (SqlCommand destcmd = new SqlCommand())
                                {
                                    destcmd.CommandText = SQLInsert;
                                    destcmd.Connection = DestConn;
                                    // destcmd.CommandType = CommandType.StoredProcedure;

                                    destcmd.Parameters.AddWithValue("ID", ID);
                                    destcmd.Parameters.AddWithValue("Name", Name);

                                    destcmd.Parameters.AddWithValue("Address1", Address1);
                                    destcmd.Parameters.AddWithValue("Address2", Address2);
                                    destcmd.Parameters.AddWithValue("City", City);
                                    destcmd.Parameters.AddWithValue("Country", Country); //always Egypt
                                    destcmd.Parameters.AddWithValue("EmailAddress", EmailAddress);
                                    destcmd.Parameters.AddWithValue("ZipCode", ZipCode);
                                    destcmd.Parameters.AddWithValue("FaxNumber", FaxNumber);
                                    destcmd.Parameters.AddWithValue("PhoneNumber", PhoneNumber);
                                    destcmd.Parameters.AddWithValue("MinimumOrderAmmount", 1);

                                    destcmd.Parameters.AddWithValue("LastName", Contact);

                                    destcmd.Parameters.AddWithValue("Notes", SpecialText);

                                    destcmd.ExecuteNonQuery();
                                }
                                // DestConn.Close();

                            }
                            #endregion
                        }
                        // conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("supplier Data", "-800", err.Message, false, "SupplierData");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool BackupDB(string sourceConnectionString)
        {
            bool Success = false;
            string directory = @"c:\temp";
            try
            {
                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    if (!Directory.Exists(directory))
                    {
                        // Try to create the directory.
                        DirectoryInfo di = Directory.CreateDirectory(directory);
                    }

                    conn.Open();
                    string DatabaseName = conn.Database;
                    string FilePath = directory + "/" + DatabaseName + DateTime.Today.ToString("yyyy-MM-dd-HH-mm") + ".bak";
                    string backup = "backup Database " + DatabaseName + " to disk = '" + FilePath + "'";
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, backup);

                    Filer.AddFileToZip(FilePath.Replace(".bak", ".zip"), FilePath);

                    System.IO.File.Delete(FilePath);

                    conn.Close();

                    Success = false;
                }
            }
            catch (Exception EX)
            {
                DoLogging("backup Data", "-700", EX.Message, false, "BackupDB");
                MessageBox.Show("Backupo Filed with eerror " + EX.Message);

            }
            return Success;
        }

        private class Headers
        {
            public int SourceID { get; set; }
            public int HeaderID { get; set; }
        }

        public static bool SendInvoiceToFinance(string sourceConnectionString, string destConnectionString, string MarketWarehouse, string BranchDefault)
        {
            string HeaderSQL = "SELECT  [ID] ,[CustomerID] ,IsNUll([DiscountPercent] ,0) DiscountPercent,IsNull([Notes] ,'') Notes ,UserID , CreatedTime, CashingTime, Total, DetailCount  FROM [dbo].[TransactionTableH_Main] where [Collected] = 1 and [Exported] = 0";

            string UpdateExported = "UPDATE [dbo].[TransactionTableH_Main] SET [Exported] = 1 WHERE [ID] = @ID";

            string CancelHeader = "SELECT  [ID] ,UserID FROM [dbo].[TransactionTableH_Main] WHERE [Canceled] = 1 AND ([Collected] is NULL OR [Collected] = 0) and [Exported] = 0";

            string DestCancelHeader = "UPDATE arSalesOrderH SET Status = 6, arSalesRep = @UserID WHERE [ID] = @ID";

            string DetailsSQL = "SELECT [ItemID] , [Quantity] ,[SalePrice] FROM [dbo].[TansactionDetailMain] where [TransactionID] = @ID";


            string CalsHeaderDataSQL = " update sh2 " +
                                        " set total= (Select IsNull (Sum(sd.Qty*sd.LinePrice) ,0) " +
                                        " from arSalesInvD sd  " +
                                        " Where sd.SalesInvHID = @Col1)  " +
                                        " , DetailsDiscount = (Select isNull(Sum(sd.Qty*sd.Price*(sd.Discount/100)) ,0) " +
                                        " from arSalesInvD sd " +
                                        " Where sd.SalesInvHID = @Col1  ) " +
                                        " , SalesTax = IsNull( (Select  " +
                                        "    CASE  " +
                                        "     WHEN sh2.CalcSalesTax = 0 THEN  0 " +
                                       "      WHEN sh2.CalcSalesTax = 1 THEN  " +
                                       "        Sum(sd.Qty*sd.LinePrice*(sd.STaxPercent/100)) " +
                                       "     else 0 " +
                                       "    END  " +
                                       "  from arSalesInvD sd Where sd.SalesInvHID =  @Col1 ),0) " +
                                      " from arSalesInvH sh2  " +
                                       " where sh2.ID = @Col1 ";

            string balanceSQL = "SELECT * FROM [dbo].[OpenningBalance] where [Closed] = 1 and [Exported] = 0";
            string balanceInsertSQL = @"INSERT INTO [dbo].[POSOpenningBalance] (UserID, OpenningBalance, CreatedTime, ClosedTime, SafeNumberID, Closed, ShiftId)
            VALUES (@userId, @openningBalance, @createdTime, @closedTime, @safeNumberID, @closed, @ID)";
            string UpdateBalanceExported = "UPDATE [dbo].[OpenningBalance] SET [Exported] = 1 WHERE [ID] = @ID";
            string updateHeader = @"UPDATE [dbo].[arSalesInvH] SET FullyImported = 1, POSTotalAmount = @TotalAmount, POSCentralTotalAmount = @CentralTotal, POSRecNo = @DetailCount, POSCentralRecNo = @CenDetailCount WHERE ID = @ID";

            string deleteHeader = @"DELETE FROM arSalesInvH WHERE InvoiceNo = @ID and FullyImported = 0";
            string deleteDetails = @"DELETE arSalesInvD FROM arSalesInvD D INNER JOIN arSalesInvH H ON H.InvoiceNo = D.SalesInvHID WHERE H.InvoiceNo = @ID and FullyImported = 0";
            int ID = 0;
            List<int> Ids = new List<int>();
            try
            {
                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(HeaderSQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Headers HeaderObj = new Headers();
                            #region get data from one row
                            ID = int.Parse(rdr["ID"].ToString());
                            HeaderObj.SourceID = ID;
                            Int32 CustomerID = (Int32)rdr["CustomerID"];
                            var invoice = "";
                            if (CustomerID <= 1)
                                invoice = "99" + ID.ToString();
                            else
                                invoice = ID.ToString();
                            Int32 UserID = (Int32)rdr["UserID"];
                            Int32 detailCount = 0;
                            try { detailCount = (Int32)rdr["DetailCount"];  } catch { }
                            //Int32? PilotID = (Int32?)rdr["DeliveryEmloyeeID"];
                            Decimal DiscountPercent = (Decimal)rdr["DiscountPercent"];
                            Decimal total = (Decimal)rdr["Total"];
                            string Notes = (string)rdr["Notes"];
                            DateTime CreatedTime = (DateTime)rdr["CreatedTime"];
                            DateTime CashingTime = (DateTime)rdr["CashingTime"];
                            var count = 0;
                            DoLogging(ID.ToString(), "100", "Read Invoice Header" + ID.ToString(), false, "InvoiceToFinance");
                            #endregion
                            #region apply to destination header
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                int NewHeaderID = 0;
                                DestConn.Open();

                                #region send data t ocreate header in finaance system
                                try
                                {
                                    using (SqlCommand destcmd = new SqlCommand())
                                    {
                                        NewHeaderID = 0;
                                        destcmd.CommandText = "usparSalesInvHImport";
                                        destcmd.Connection = DestConn;
                                        destcmd.CommandType = CommandType.StoredProcedure;

                                        destcmd.Parameters.AddWithValue("ID", NewHeaderID).Direction = ParameterDirection.Output;
                                        
                                        destcmd.Parameters.AddWithValue("InvoiceNo", invoice);
                                        destcmd.Parameters.AddWithValue("CustomerID", CustomerID.ToString());
                                        destcmd.Parameters.AddWithValue("CustomerData", "POS Import");
                                        destcmd.Parameters.AddWithValue("SalesOrderID", DBNull.Value);
                                        destcmd.Parameters.AddWithValue("CustomerPO", DBNull.Value);
                                        destcmd.Parameters.AddWithValue("arSalesRep", UserID.ToString());
                                        destcmd.Parameters.AddWithValue("HDate", CashingTime);//CreatedTime.ToShortDateString()
                                        destcmd.Parameters.AddWithValue("PaymentMethod", 1);
                                        destcmd.Parameters.AddWithValue("CalcSalesTax", false);
                                        destcmd.Parameters.AddWithValue("Discount", DiscountPercent);
                                        destcmd.Parameters.AddWithValue("TaxPercent", DBNull.Value);
                                        destcmd.Parameters.AddWithValue("Remarks", Notes);
                                        destcmd.Parameters.AddWithValue("BusinessLine", DBNull.Value);
                                        destcmd.Parameters.AddWithValue("CurrencyID", DBNull.Value);
                                        destcmd.Parameters.AddWithValue("ivInvGRNDoc", DBNull.Value);
                                        destcmd.Parameters.AddWithValue("IsPrinted", false);
                                        destcmd.Parameters.AddWithValue("Branch", BranchDefault);
                                        destcmd.Parameters.AddWithValue("UserName", "System");
                                        destcmd.Parameters.AddWithValue("Source", "Import");
                                        destcmd.Parameters.AddWithValue("Subsys", "POS");

                                        destcmd.ExecuteNonQuery();
                                        var salesId = 0;
                                        using (SqlCommand getId = new SqlCommand())
                                        {
                                            destcmd.CommandText = "SELECT ID FROM arSalesInvH WHERE InvoiceNo = '" + invoice + "'";
                                            destcmd.Connection = DestConn;
                                            destcmd.CommandType = CommandType.Text;
                                            DestConn.Close(); DestConn.Open();
                                            using (SqlDataReader grdr = destcmd.ExecuteReader())
                                            {
                                                while (grdr.Read())
                                                {
                                                    salesId = (Int32)grdr["ID"];
                                                }
                                            }
                                        }
                                            
                                        #region Apply destination details
                                        using (SqlConnection Dconn = new SqlConnection(sourceConnectionString))
                                        {
                                            SqlCommand Detailscmd = new SqlCommand(DetailsSQL, Dconn);
                                            Dconn.Open();
                                            Detailscmd.Parameters.AddWithValue("ID", ID);
                                            using (SqlDataReader Drdr = Detailscmd.ExecuteReader())
                                            {
                                                while (Drdr.Read())
                                                {
                                                    string ItemID = (string)Drdr["ItemID"];
                                                    decimal Quantity = (decimal)Drdr["Quantity"];
                                                    decimal SalePrice = (decimal)Drdr["SalePrice"];
                                                    DoLogging(ItemID.ToString(), "100", "Read Invoice Details for Item" + ItemID, false, "InvoiceToFinance");
                                                    DestConn.Close(); DestConn.Open();
                                                    #region send data t ocreate details in finaance system
                                                    using (SqlCommand detailcmd = new SqlCommand())
                                                    {

                                                        detailcmd.CommandText = "usparSalesInvDEdit";
                                                        detailcmd.Connection = DestConn;
                                                        detailcmd.CommandType = CommandType.StoredProcedure;
                                                        detailcmd.Parameters.AddWithValue("ID", 0);
                                                        detailcmd.Parameters.AddWithValue("SalesInvHID", salesId);
                                                        detailcmd.Parameters.AddWithValue("ItemID", ItemID);
                                                        detailcmd.Parameters.AddWithValue("WareHouseID", MarketWarehouse);
                                                        detailcmd.Parameters.AddWithValue("Qty", Quantity.ToString());
                                                        detailcmd.Parameters.AddWithValue("Price", SalePrice.ToString());
                                                        detailcmd.Parameters.AddWithValue("Discount", DBNull.Value);
                                                        detailcmd.Parameters.AddWithValue("STaxPercent", DBNull.Value);
                                                        detailcmd.Parameters.AddWithValue("Description", DBNull.Value);
                                                        detailcmd.Parameters.AddWithValue("JobID", DBNull.Value);
                                                        detailcmd.Parameters.AddWithValue("Account", DBNull.Value);
                                                        detailcmd.Parameters.AddWithValue("ivInvDeliveryNote", DBNull.Value);
                                                        detailcmd.Parameters.AddWithValue("CostID", DBNull.Value);
                                                        detailcmd.Parameters.AddWithValue("BatchNo", DBNull.Value);
                                                        detailcmd.Parameters.AddWithValue("UserName", "System");
                                                        detailcmd.Parameters.AddWithValue("Source", "Import");
                                                        detailcmd.Parameters.AddWithValue("Subsys", "POS");
                                                        detailcmd.ExecuteNonQuery();
                                                        count++;
                                                        DoLogging(ItemID.ToString(), "100", "Inserted Invoice Details for Item" + ItemID, false, "InvoiceToFinance");

                                                    }
                                                    #endregion
                                                }
                                                
                                            }
                                            if (Dconn.State == ConnectionState.Open) Dconn.Close();
                                            //Update the header tp be exported
                                            using (SqlCommand exportHeadercmd = new SqlCommand(UpdateExported, Dconn))
                                            {

                                                exportHeadercmd.Parameters.AddWithValue("ID", ID);
                                                if (Dconn.State == ConnectionState.Closed) Dconn.Open();
                                                exportHeadercmd.ExecuteNonQuery();
                                                if (Dconn.State == ConnectionState.Open) Dconn.Close();
                                            }

                                            //  Dconn.Close();
                                        }
                                        #endregion
                                        
                                        DoLogging(ID.ToString(), "100", "Inserted Invoice Header" + salesId.ToString(), false, "InvoiceToFinance");
                                        //Ids.Add(ID);
                                        
                                        using (SqlConnection sconn = new SqlConnection(destConnectionString))
                                        {
                                            sconn.Open();
                                            using (SqlCommand DoneHeadercmd = new SqlCommand(updateHeader, sconn))
                                            {
                                                DoneHeadercmd.Parameters.AddWithValue("ID", salesId);
                                                DoneHeadercmd.Parameters.AddWithValue("TotalAmount", total);
                                                DoneHeadercmd.Parameters.AddWithValue("CentralTotal", total);
                                                DoneHeadercmd.Parameters.AddWithValue("DetailCount", count);
                                                DoneHeadercmd.Parameters.AddWithValue("CenDetailCount", detailCount);
                                                DoneHeadercmd.ExecuteNonQuery();
                                            }
                                            sconn.Close();
                                            SqlCommand Totalscmd = new SqlCommand(CalsHeaderDataSQL, sconn);
                                            sconn.Open();
                                            Totalscmd.Parameters.AddWithValue("Col1", salesId);
                                            Totalscmd.ExecuteNonQuery();
                                            sconn.Close();
                                        }

                                    }
                                }
                                catch (Exception ex)
                                {
                                    #region Delete Messy Details
                                    using (SqlConnection Dconn = new SqlConnection(destConnectionString))
                                    {
                                        SqlCommand DeleteDcmd = new SqlCommand(deleteDetails, Dconn);
                                        Dconn.Open();
                                        DeleteDcmd.Parameters.AddWithValue("ID", ID);
                                        DeleteDcmd.ExecuteNonQuery();
                                        Dconn.Close();
                                    }

                                    #endregion

                                    #region Delete Messy Headers
                                    using (SqlConnection Dconn = new SqlConnection(destConnectionString))
                                    {
                                        SqlCommand DeleteDcmd = new SqlCommand(deleteHeader, Dconn);
                                        Dconn.Open();
                                        DeleteDcmd.Parameters.AddWithValue("ID", ID);
                                        DeleteDcmd.ExecuteNonQuery();
                                        Dconn.Close();
                                    }

                                    #endregion
                                    DoLogging(ID.ToString(), "100", "Invoice is not inserted" + ID.ToString(), false, "InvoiceToFinance");
                                }
                                #endregion

                                //// Deleye header after copy 
                                //DoLogging("100", ID.ToString(), "Delete Invoce header From POS");
                                //using (SqlConnection Delconn = new SqlConnection(sourceConnectionString))
                                //{
                                //    SqlCommand Totalscmd = new SqlCommand(DeleteDoneHeaderSQL, Delconn);
                                //    Delconn.Open();
                                //    Totalscmd.Parameters.AddWithValue("ID", ID);
                                //    Totalscmd.ExecuteNonQuery();
                                //}
                                //  DestConn.Close();

                            }
                            #endregion
                        }
                        //  conn.Close();
                    }

                }


                
                try
                {
                    List<int> CancelIDLst = new List<int>();
                    using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                    {
                        SqlCommand Headercmd = new SqlCommand(CancelHeader, conn);
                        if (conn.State == ConnectionState.Closed)
                        {
                            conn.Open();
                        }
                        using (SqlDataReader rdr = Headercmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                int CID = (int)rdr["ID"];
                                int UserID = (int)rdr["UserID"];
                                CancelIDLst.Add(CID);
                                DoLogging(CID.ToString(), "100", "Read Canceled Invoice" + CID.ToString(), false, "InvoiceToFinance");

                                using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                                {
                                    if (DestConn.State == ConnectionState.Closed)
                                    {
                                        DestConn.Open();
                                    }
                                    using (SqlCommand destcmd = new SqlCommand())
                                    {
                                        destcmd.CommandText = DestCancelHeader;
                                        destcmd.Connection = DestConn;
                                        destcmd.CommandType = CommandType.Text;
                                        destcmd.Parameters.AddWithValue("ID", CID.ToString());
                                        destcmd.Parameters.AddWithValue("UserID", UserID.ToString());

                                        destcmd.ExecuteReader();

                                        DoLogging(CID.ToString(), "100", "Canceled Invoice Header" + CID.ToString(), false, "InvoiceToFinance");
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    DoLogging("100", "100", "Failed to update Canceled Orders " + "Exception: " + ex.Message, false, "InvoiceToFinance");
                }
            }
            catch (Exception err)
            {
                #region Delete Messy Details
                using (SqlConnection Dconn = new SqlConnection(destConnectionString))
                {
                    SqlCommand DeleteDcmd = new SqlCommand(deleteDetails, Dconn);
                    Dconn.Open();
                    DeleteDcmd.Parameters.AddWithValue("ID", ID);
                    DeleteDcmd.ExecuteNonQuery();
                    Dconn.Close();
                }

                #endregion

                #region Delete Messy Headers
                using (SqlConnection Dconn = new SqlConnection(destConnectionString))
                {
                    SqlCommand DeleteDcmd = new SqlCommand(deleteHeader, Dconn);
                    Dconn.Open();
                    DeleteDcmd.Parameters.AddWithValue("ID", ID);
                    DeleteDcmd.ExecuteNonQuery();
                    Dconn.Close();
                }

                #endregion
                DoLogging(ID.ToString(), "100", "Invoice is not inserted" + ID.ToString(), false, "InvoiceToFinance");
                DoLogging("Error", "-600", err.Message, false, "InvoiceToFinance");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            try
            {
                #region Export the Opening Balance Start
                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(balanceSQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            Headers HeaderObj = new Headers();
                            #region get data from one row
                            Int32 BID = (Int32)rdr["ID"];
                            Int32 UserID = (Int32)rdr["UserID"];
                            decimal openningBalance = (decimal)rdr["OpenningBalance"];
                            DateTime CreatedTime = (DateTime)rdr["CreatedTime"];
                            DateTime closedTime = (DateTime)rdr["ClosedTime"];
                            int safenumber = (int)rdr["SafeNumberID"];
                            bool closed = (bool)rdr["Closed"];

                            DoLogging(BID.ToString(), "100", "Read Invoice Header" + BID.ToString(), false, "InvoiceToFinance");
                            #endregion
                            #region apply to destination header
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();

                                #region send data t ocreate header in finaance system
                                try
                                {
                                    using (SqlCommand destcmd = new SqlCommand())
                                    {
                                        //HeaderObj.HeaderID = NewHeaderID;
                                        destcmd.CommandText = balanceInsertSQL;
                                        destcmd.Connection = DestConn;
                                        destcmd.CommandType = CommandType.Text;

                                        destcmd.Parameters.AddWithValue("ID", BID);
                                        destcmd.Parameters.AddWithValue("userId", UserID);
                                        destcmd.Parameters.AddWithValue("createdTime", CreatedTime);
                                        destcmd.Parameters.AddWithValue("closedTime", closedTime);
                                        destcmd.Parameters.AddWithValue("closed", closed);
                                        destcmd.Parameters.AddWithValue("safeNumberID", safenumber);
                                        destcmd.Parameters.AddWithValue("openningBalance", openningBalance);

                                        destcmd.ExecuteNonQuery();

                                        using (SqlConnection sconn = new SqlConnection(sourceConnectionString))
                                        {
                                            sconn.Open();
                                            using (SqlCommand DoneHeadercmd = new SqlCommand(UpdateBalanceExported, sconn))
                                            {
                                                DoneHeadercmd.Parameters.AddWithValue("ID", BID);
                                                DoneHeadercmd.ExecuteNonQuery();
                                            }
                                        }
                                        DoLogging(BID.ToString(), "100", "Inserted Invoice Header" + BID.ToString(), false, "InvoiceToFinance");

                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                                #endregion
                            }
                            #endregion
                        }
                        //  conn.Close();
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                DoLogging("Error", "-600", ex.Message, false, "Balance not imported");
            }

            return true;
        }

        public static bool SendInvoiceHistoryToFinance(string sourceConnectionString, string destConnectionString, string MarketWarehouse, string BranchDefault)
        {
            string lastInvoiceNo;
            string GetLastInvoice = "Select top 1 InvoiceNo from arSalesInvH where ISNUMERIC(InvoiceNo ) = 1 Order by CAST( InvoiceNo as Int) desc";
            string HeaderSQL = "SELECT  [ID] ,[CustomerID] ,IsNUll([DiscountPercent] ,0) DiscountPercent,IsNull([Notes] ,'') Notes ,UserID , CreatedTime  FROM [dbo].[TransactionTableH_Main] Where ID > @ID";

            string DetailsSQL = "SELECT [ItemID] , [Quantity] ,[SalePrice] FROM [dbo].[TansactionDetailMain] where [TransactionID] = @ID";

            string CalsHeaderDataSQL = " update sh2 " +
                                        " set total= (Select IsNull (Sum(sd.Qty*sd.LinePrice) ,0) " +
                                        " from arSalesInvD sd  " +
                                        " Where sd.SalesInvHID = @Col1)  " +
                                        " , DetailsDiscount = (Select isNull(Sum(sd.Qty*sd.Price*(sd.Discount/100)) ,0) " +
                                        " from arSalesInvD sd " +
                                        " Where sd.SalesInvHID = @Col1  ) " +
                                        " , SalesTax = IsNull( (Select  " +
                                        "    CASE  " +
                                        "     WHEN sh2.CalcSalesTax = 0 THEN  0 " +
                                       "      WHEN sh2.CalcSalesTax = 1 THEN  " +
                                       "        Sum(sd.Qty*sd.LinePrice*(sd.STaxPercent/100)) " +
                                       "     else 0 " +
                                       "    END  " +
                                       "  from arSalesInvD sd Where sd.SalesInvHID =  @Col1 ),0) " +
                                      " from arSalesInvH sh2  " +
                                       " where sh2.ID = @Col1 ";

            string DeleteDoneHeaderSQL = " Delete From TransactionTable Where ID = @ID";
            try
            {
                using (SqlConnection conni = new SqlConnection(destConnectionString))
                {
                    SqlCommand Invoicecmd = new SqlCommand(GetLastInvoice, conni);
                    conni.Open();
                    lastInvoiceNo = (string)Invoicecmd.ExecuteScalar();
                    conni.Close();
                }

                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(HeaderSQL, conn);
                    Headercmd.Parameters.AddWithValue("ID", lastInvoiceNo).Direction = ParameterDirection.Input;

                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            Int32 ID = (Int32)rdr["ID"];
                            Int32 CustomerID = (Int32)rdr["CustomerID"];
                            Int32 UserID = (Int32)rdr["UserID"];
                            Decimal DiscountPercent = (Decimal)rdr["DiscountPercent"];
                            string Notes = (string)rdr["Notes"];
                            DateTime CreatedTime = (DateTime)rdr["CreatedTime"];

                            DoLogging(ID.ToString(), "100", "Read Invoice Header" + ID.ToString(), false, "InvoiceHistoryToFinance");
                            #endregion
                            #region apply to destination header
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                int NewHeaderID = 0;
                                DestConn.Open();

                                #region send data t ocreate header in finaance system
                                using (SqlCommand destcmd = new SqlCommand())
                                {
                                    NewHeaderID = 0;
                                    destcmd.CommandText = "usparSalesInvHEdit";
                                    destcmd.Connection = DestConn;
                                    destcmd.CommandType = CommandType.StoredProcedure;

                                    destcmd.Parameters.AddWithValue("ID", NewHeaderID).Direction = ParameterDirection.Output;

                                    destcmd.Parameters.AddWithValue("InvoiceNo", ID.ToString());
                                    destcmd.Parameters.AddWithValue("CustomerID", CustomerID.ToString());
                                    destcmd.Parameters.AddWithValue("CustomerData", "POS Import");
                                    destcmd.Parameters.AddWithValue("SalesOrderID", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("CustomerPO", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("arSalesRep", UserID.ToString());
                                    destcmd.Parameters.AddWithValue("HDate", DateTime.Today.ToString("MM-dd-yyyy"));//CreatedTime.ToShortDateString()
                                    destcmd.Parameters.AddWithValue("PaymentMethod", 1);
                                    destcmd.Parameters.AddWithValue("CalcSalesTax", false);
                                    destcmd.Parameters.AddWithValue("Discount", DiscountPercent);
                                    destcmd.Parameters.AddWithValue("TaxPercent", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Remarks", Notes);
                                    destcmd.Parameters.AddWithValue("BusinessLine", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("CurrencyID", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("ivInvGRNDoc", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("IsPrinted", false);
                                    destcmd.Parameters.AddWithValue("Branch", BranchDefault);
                                    destcmd.Parameters.AddWithValue("UserName", "System");
                                    destcmd.Parameters.AddWithValue("Source", "Import");
                                    destcmd.Parameters.AddWithValue("Subsys", "POS");

                                    destcmd.ExecuteNonQuery();
                                    NewHeaderID = (int)destcmd.Parameters["ID"].Value;
                                    DoLogging(ID.ToString(), "100", "Inserted Invoice Header" + NewHeaderID.ToString(), false, "InvoiceHistoryToFinance");
                                }
                                #endregion

                                #region Apply destination details
                                using (SqlConnection Dconn = new SqlConnection(sourceConnectionString))
                                {
                                    SqlCommand Detailscmd = new SqlCommand(DetailsSQL, Dconn);
                                    Dconn.Open();
                                    Detailscmd.Parameters.AddWithValue("ID", ID);
                                    using (SqlDataReader Drdr = Detailscmd.ExecuteReader())
                                    {
                                        while (Drdr.Read())
                                        {
                                            string ItemID = (string)Drdr["ItemID"];
                                            decimal Quantity = (decimal)Drdr["Quantity"];
                                            decimal SalePrice = (decimal)Drdr["SalePrice"];
                                            DoLogging(ItemID.ToString(), "100", "Read Invoice Details for Item" + ItemID, false, "InvoiceHistoryToFinance");
                                            #region send data t ocreate details in finaance system
                                            using (SqlCommand destcmd = new SqlCommand())
                                            {
                                                destcmd.CommandText = "usparSalesInvDEdit";
                                                destcmd.Connection = DestConn;
                                                destcmd.CommandType = CommandType.StoredProcedure;

                                                destcmd.Parameters.AddWithValue("ID", 0);

                                                destcmd.Parameters.AddWithValue("SalesInvHID", NewHeaderID.ToString());
                                                destcmd.Parameters.AddWithValue("ItemID", ItemID);
                                                destcmd.Parameters.AddWithValue("WareHouseID", MarketWarehouse);
                                                destcmd.Parameters.AddWithValue("Qty", Quantity.ToString());
                                                destcmd.Parameters.AddWithValue("Price", SalePrice.ToString());
                                                destcmd.Parameters.AddWithValue("Discount", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("STaxPercent", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("Description", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("JobID", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("Account", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("ivInvDeliveryNote", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("CostID", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("BatchNo", DBNull.Value);

                                                destcmd.Parameters.AddWithValue("UserName", "System");
                                                destcmd.Parameters.AddWithValue("Source", "Import");
                                                destcmd.Parameters.AddWithValue("Subsys", "POS");

                                                destcmd.ExecuteNonQuery();

                                                DoLogging(ItemID.ToString(), "100", "Inserted Invoice Details for Item" + ItemID, false, "InvoiceHistoryToFinance");

                                            }
                                            #endregion

                                        }
                                    }
                                    //  Dconn.Close();
                                }
                                #endregion

                                #region apply total for header
                                // update header data wit htotals and taxes
                                DoLogging("100", "100", "Update Invoce header total value", false, "InvoiceHistoryToFinance");
                                using (SqlConnection tconn = new SqlConnection(destConnectionString))
                                {
                                    SqlCommand Totalscmd = new SqlCommand(CalsHeaderDataSQL, tconn);
                                    tconn.Open();
                                    Totalscmd.Parameters.AddWithValue("Col1", NewHeaderID);
                                    Totalscmd.ExecuteNonQuery();
                                }

                                #endregion

                                //// Deleye header after copy 
                                //DoLogging("100", ID.ToString(), "Delete Invoce header From POS");
                                //using (SqlConnection Delconn = new SqlConnection(sourceConnectionString))
                                //{
                                //    SqlCommand Totalscmd = new SqlCommand(DeleteDoneHeaderSQL, Delconn);
                                //    Delconn.Open();
                                //    Totalscmd.Parameters.AddWithValue("ID", ID);
                                //    Totalscmd.ExecuteNonQuery();
                                //}
                                //  DestConn.Close();

                            }
                            #endregion
                        }
                        //  conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("Invocie Data", "-600", err.Message, false, "InvoiceHistoryToFinance");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool SendInvoiceReturnToFinance(string sourceConnectionString, string destConnectionString, string MarketWarehouse, string BranchDefault)
        {

            List<int> CopiedTrans = new List<int>();

            string HeaderSQL = "SELECT ReturnTransactionH.ID, ISNULL(ReturnTransactionH.DiscountPercent, 0) AS DiscountPercent, ISNULL(ReturnTransactionH.Note, '') AS Notes, " +
                               " ReturnTransactionH.TransactionID, TransactionTable.CustomerID ,IsNull(ReturnTransactionH.UserID ,1) UserID, ReturnTransactionH.TotalReturn as TotalReturn, ReturnTransactionH.CreatedTime as CreatedTime" +
                               ", ReturnTransactionH.DetailCount as DetailCount" +
                               " FROM  ReturnTransactionH INNER JOIN " +
                               " TransactionTableH_Main TransactionTable ON ReturnTransactionH.TransactionID = TransactionTable.ID WHERE CanExport = 1";

            string DetailsSQL = "SELECT [ItemID] , [Quantity] ,[SalePrice] FROM [dbo].[ReturnTransactionD] where [ReturnTransactionID] = @ID";

            string CalsHeaderDataSQL = "update sh2 set total = (Select IsNull(Sum(sd.Qty * sd.LinePrice), 0) from arSalesRInvD sd Where sd.SalesInvRHID = @Col1), " +
                "DetailsDiscount = (Select isNull(Sum(sd.Qty * sd.Price * (sd.Discount / 100)), 0) from arSalesRInvD sd Where sd.SalesInvRHID = @Col1  ), " +
                "SalesTax = IsNull((Select CASE WHEN sh2.CalcSalesTax = 0 THEN  0 WHEN sh2.CalcSalesTax = 1 THEN Sum(sd.Qty * sd.LinePrice * (sd.STaxPercent / 100)) " +
                "else 0 END from arSalesRInvD sd Where sd.SalesInvRHID = @Col1),0) from arSalesRInvH sh2 where sh2.ID = @Col1";

            string updateheader = @"Update arSalesRInvH set FullyImported = 1, POSRecNo = @detailCount, POSCentralRecNo = @detailCentralCount, POSTotalAmount = @total, POSCentralTotalAmount = @total WHERE ID = @ID";

            string DeleteDoneHeaderSQL = " Delete From ReturnTransactionH Where ID = @ID";
            try
            {
                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(HeaderSQL, conn);

                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {
                            #region get data from one row
                            Int32 ID = (Int32)rdr["ID"];
                            Int32 detailCount = (Int32)rdr["DetailCount"];
                            Int32 CustomerID = (Int32)rdr["CustomerID"];
                            Int32 UserID = (Int32)rdr["UserID"];
                            Int32 CentralCount = 0;
                            try
                            { CentralCount = (Int32)rdr["DetailCount"]; } catch { }
                            Decimal DiscountPercent = (Decimal)rdr["DiscountPercent"];
                            string Notes = (string)rdr["Notes"];
                            int TransactionID = (int)rdr["TransactionID"];
                            DateTime CreatedTime = (DateTime)rdr["CreatedTime"];
                            decimal totalRetrurn = (decimal)rdr["TotalReturn"];
                            //preapre dat to be deleted
                            CopiedTrans.Add(ID);

                            DoLogging(ID.ToString(), "100", "Read Return Invoice Header=" + ID.ToString(), false, "InvoiceReturnToFinance");
                            #endregion
                            #region apply to destination header
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();

                                #region send data t ocreate header in finaance system
                                using (SqlCommand destcmd = new SqlCommand())
                                {
                                    destcmd.CommandText = "usparSalesRInvHEdit";
                                    destcmd.Connection = DestConn;
                                    destcmd.CommandType = CommandType.StoredProcedure;
                                    destcmd.Parameters.AddWithValue("ID", 0).Direction = ParameterDirection.Output;
                                    
                                    destcmd.Parameters.AddWithValue("InvoiceNo", ID.ToString());
                                    destcmd.Parameters.AddWithValue("CustomerID", CustomerID.ToString());
                                    destcmd.Parameters.AddWithValue("OriginalInvoice", TransactionID);
                                    destcmd.Parameters.AddWithValue("Total", totalRetrurn);
                                    destcmd.Parameters.AddWithValue("CustomerPO", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("arSalesRepID", UserID.ToString());
                                    destcmd.Parameters.AddWithValue("HDate", CreatedTime);
                                    destcmd.Parameters.AddWithValue("PaymentMethod", 1);
                                    destcmd.Parameters.AddWithValue("CalcSalesTax", true);
                                    destcmd.Parameters.AddWithValue("Discount", DiscountPercent);
                                    destcmd.Parameters.AddWithValue("Remarks", Notes);
                                    destcmd.Parameters.AddWithValue("SalesTax", 0);
                                    destcmd.Parameters.AddWithValue("TaxPercent", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("BusinessLine", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("CurrencyID", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Branch", BranchDefault);
                                    destcmd.Parameters.AddWithValue("DetailsDiscount", BranchDefault);
                                    //destcmd.Parameters.AddWithValue("ivInvGRNDoc", DBNull.Value);

                                    destcmd.Parameters.AddWithValue("UserName", "System");
                                    destcmd.Parameters.AddWithValue("Source", "Import");
                                    destcmd.Parameters.AddWithValue("Subsys", "POS");

                                    destcmd.ExecuteNonQuery();
                                    var salesId = destcmd.Parameters["ID"].Value;
                                    //NewHeaderID = (int)destcmd.Parameters["ID"].Value;
                                    var count = 0;
                                    DoLogging(ID.ToString(), "100", "Read Return Invoice Added=" + ID.ToString(), false, "InvoiceReturnToFinance");
                                    #region Apply destination details
                                    using (SqlConnection Dconn = new SqlConnection(sourceConnectionString))
                                    {
                                        SqlCommand Detailscmd = new SqlCommand(DetailsSQL, Dconn);
                                        Detailscmd.Parameters.AddWithValue("ID", ID);
                                        Dconn.Open();
                                        using (SqlDataReader Drdr = Detailscmd.ExecuteReader())
                                        {
                                            while (Drdr.Read())
                                            {
                                                string ItemID = (string)Drdr["ItemID"];
                                                decimal Quantity = (decimal)Drdr["Quantity"];
                                                decimal SalePrice = (decimal)Drdr["SalePrice"];

                                                DoLogging(ItemID.ToString(), "100", "Read Return Invoice Details=" + ItemID.ToString(), false, "InvoiceReturnToFinance");
                                                #region send data t ocreate details in finaance system
                                                using (SqlCommand detailcmd = new SqlCommand())
                                                {
                                                    detailcmd.CommandText = "usparSalesRInvDFullEdit";
                                                    detailcmd.Connection = DestConn;
                                                    detailcmd.CommandType = CommandType.StoredProcedure;
                                                    detailcmd.Parameters.AddWithValue("ID", 0);
                                                    detailcmd.Parameters.AddWithValue("SalesInvRHID", salesId);
                                                    detailcmd.Parameters.AddWithValue("ItemID", ItemID);
                                                    detailcmd.Parameters.AddWithValue("WareHouseID", MarketWarehouse);
                                                    detailcmd.Parameters.AddWithValue("Qty", Quantity.ToString());
                                                    detailcmd.Parameters.AddWithValue("Price", SalePrice.ToString());
                                                    detailcmd.Parameters.AddWithValue("Discount", DBNull.Value);
                                                    detailcmd.Parameters.AddWithValue("STaxPercent", DBNull.Value);
                                                    detailcmd.Parameters.AddWithValue("Description", DBNull.Value);
                                                    detailcmd.Parameters.AddWithValue("JobID", DBNull.Value);
                                                    detailcmd.Parameters.AddWithValue("Account", DBNull.Value);
                                                    detailcmd.Parameters.AddWithValue("InvoiceDetailID", DBNull.Value);
                                                    detailcmd.Parameters.AddWithValue("CostID", DBNull.Value);
                                                    detailcmd.Parameters.AddWithValue("BatchNo", DBNull.Value);
                                                    detailcmd.Parameters.AddWithValue("UserName", "System");
                                                    detailcmd.Parameters.AddWithValue("Source", "Import");
                                                    detailcmd.Parameters.AddWithValue("Subsys", "POS");
                                                    detailcmd.ExecuteNonQuery();
                                                    count++;
                                                    DoLogging(ItemID.ToString(), "100", "Save Return Invoice Details=" + ItemID.ToString(), false, "InvoiceReturnToFinance");
                                                }
                                                #endregion
                                            }
                                        }
                                    }
                                    #endregion
                                    #region  update header data wit htotals and taxes

                                    using (SqlConnection tconn = new SqlConnection(destConnectionString))
                                    {
                                        DoLogging("Calc header", "100", "Read Return Invoice Header", false, "InvoiceReturnToFinance");
                                        SqlCommand Totalscmd = new SqlCommand(CalsHeaderDataSQL, tconn);
                                        tconn.Open();
                                        Totalscmd.Parameters.AddWithValue("Col1", salesId);
                                        Totalscmd.ExecuteNonQuery();
                                    }

                                    using (SqlConnection tconn = new SqlConnection(destConnectionString))
                                    {
                                        DoLogging("All imported header", "100", "Update Return Invoice With Data", false, "InvoiceReturnToFinance");
                                        SqlCommand Totalscmd = new SqlCommand(updateheader, tconn);
                                        tconn.Open();
                                        Totalscmd.Parameters.AddWithValue("ID", salesId);
                                        Totalscmd.Parameters.AddWithValue("detailCentralCount", count);
                                        Totalscmd.Parameters.AddWithValue("detailCount", CentralCount);
                                        Totalscmd.Parameters.AddWithValue("total", totalRetrurn);
                                        Totalscmd.ExecuteNonQuery();
                                    }
                                    #endregion

                                    // Deleye header after copy 
                                    DoLogging("100", ID.ToString(), "Delete Invoce header From POS");
                                    using (SqlConnection Delconn = new SqlConnection(sourceConnectionString))
                                    {
                                        SqlCommand Totalscmd = new SqlCommand(DeleteDoneHeaderSQL, Delconn);
                                        Delconn.Open();
                                        Totalscmd.Parameters.AddWithValue("ID", ID);
                                        Totalscmd.ExecuteNonQuery();
                                        Delconn.Close();
                                    }
                                }
                                #endregion


                                // DestConn.Close();

                            }
                            #endregion
                        }

                        //foreach (int Index in CopiedTrans)
                        //{
                        //    //// Deleye header after copy 
                        //    DoLogging("100", Index.ToString(), "Delete Invoce header From POS");
                        //    using (SqlConnection Delconn = new SqlConnection(sourceConnectionString))
                        //    {
                        //        SqlCommand Totalscmd = new SqlCommand(DeleteDoneHeaderSQL, Delconn);
                        //        Delconn.Open();
                        //        Totalscmd.Parameters.AddWithValue("ID", Index);
                        //        Totalscmd.ExecuteNonQuery();
                        //    }
                        //}
                        // conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("return Invoice Data", "-500", err.Message, false, "InvoiceReturnToFinance");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool SendInvoiceReturnHistoryToFinance(string sourceConnectionString, string destConnectionString, string MarketWarehouse, string BranchDefault)
        {
            string lastInvoiceNo;
            string GetLastInvoice = "Select top 1 InvoiceNo from dbo.arSalesRInvH where ISNUMERIC(InvoiceNo ) = 1 Order by CAST( InvoiceNo as Int) desc";

            List<int> CopiedTrans = new List<int>();

            string HeaderSQL = "SELECT ReturnTransactionH.ID, ISNULL(ReturnTransactionH.DiscountPercent, 0) AS DiscountPercent, ISNULL(ReturnTransactionH.Note, '') AS Notes, " +
                               " ReturnTransactionH.TransactionID, TransactionTable.CustomerID ,IsNull(ReturnTransactionH.UserID ,1) UserID " +
                               " FROM  ReturnTransactionH_Main ReturnTransactionH INNER JOIN " +
                               " TransactionTableH_Main TransactionTable ON ReturnTransactionH.TransactionID = TransactionTable.ID " +
                               " Where ReturnTransactionH.ID > @ID";

            string DetailsSQL = "SELECT [ItemID] , [Quantity] ,[SalePrice] FROM ReturnTransactionD_Main [ReturnTransactionD] where [ReturnTransactionID] = @ID";

            string CalsHeaderDataSQL = " update sh2 " +
                                        " set total= (Select IsNull (Sum(sd.Qty*sd.LinePrice) ,0) " +
                                        " from arSalesInvD sd  " +
                                        " Where sd.SalesInvHID = @Col1)  " +
                                        " , DetailsDiscount = (Select isNull(Sum(sd.Qty*sd.Price*(sd.Discount/100)) ,0) " +
                                        " from arSalesInvD sd " +
                                        " Where sd.SalesInvHID = @Col1  ) " +
                                        " , SalesTax = IsNull( (Select  " +
                                        "    CASE  " +
                                        "     WHEN sh2.CalcSalesTax = 0 THEN  0 " +
                                       "      WHEN sh2.CalcSalesTax = 1 THEN  " +
                                       "        Sum(sd.Qty*sd.LinePrice*(sd.STaxPercent/100)) " +
                                       "     else 0 " +
                                       "    END  " +
                                       "  from arSalesInvD sd Where sd.SalesInvHID =  @Col1 ),0) " +
                                      " from arSalesInvH sh2  " +
                                       " where sh2.ID = @Col1 ";
            string DeleteDoneHeaderSQL = " Delete From ReturnTransactionH Where ID = @ID";
            try
            {
                using (SqlConnection conni = new SqlConnection(destConnectionString))
                {
                    SqlCommand Invoicecmd = new SqlCommand(GetLastInvoice, conni);
                    conni.Open();
                    lastInvoiceNo = (string)Invoicecmd.ExecuteScalar();
                    conni.Close();
                }

                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(HeaderSQL, conn);
                    Headercmd.Parameters.AddWithValue("ID", lastInvoiceNo).Direction = ParameterDirection.Input;
                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {
                            #region get data from one row
                            Int32 ID = (Int32)rdr["ID"];
                            Int32 CustomerID = (Int32)rdr["CustomerID"];
                            Int32 UserID = (Int32)rdr["UserID"];
                            Decimal DiscountPercent = (Decimal)rdr["DiscountPercent"];
                            string Notes = (string)rdr["Notes"];
                            int TransactionID = (int)rdr["TransactionID"];
                            //preapre dat to be deleted
                            CopiedTrans.Add(ID);

                            DoLogging(ID.ToString(), "100", "Read Return Invoice Header=" + ID.ToString(), false, "InvoiceReturnHToFinance");
                            #endregion
                            #region apply to destination header
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                int NewHeaderID = 0;
                                DestConn.Open();

                                #region send data t ocreate header in finaance system
                                using (SqlCommand destcmd = new SqlCommand())
                                {
                                    destcmd.CommandText = "usparSalesRInvHEdit";
                                    destcmd.Connection = DestConn;
                                    destcmd.CommandType = CommandType.StoredProcedure;

                                    destcmd.Parameters.AddWithValue("ID", NewHeaderID).Direction = ParameterDirection.InputOutput;

                                    destcmd.Parameters.AddWithValue("InvoiceNo", ID.ToString());
                                    destcmd.Parameters.AddWithValue("CustomerID", CustomerID.ToString());
                                    destcmd.Parameters.AddWithValue("OriginalInvoice", TransactionID);

                                    destcmd.Parameters.AddWithValue("CustomerPO", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("arSalesRepID", UserID.ToString());
                                    destcmd.Parameters.AddWithValue("HDate", DateTime.Today.ToString("MM-dd-yyyy"));
                                    destcmd.Parameters.AddWithValue("PaymentMethod", 1);
                                    destcmd.Parameters.AddWithValue("CalcSalesTax", true);
                                    destcmd.Parameters.AddWithValue("Discount", DiscountPercent);
                                    destcmd.Parameters.AddWithValue("TaxPercent", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Remarks", Notes);
                                    destcmd.Parameters.AddWithValue("BusinessLine", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("CurrencyID", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Branch", BranchDefault);
                                    //destcmd.Parameters.AddWithValue("ivInvGRNDoc", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("SalesTax", 0);
                                    destcmd.Parameters.AddWithValue("UserName", "System");
                                    destcmd.Parameters.AddWithValue("Source", "Import");
                                    destcmd.Parameters.AddWithValue("Subsys", "POS");

                                    destcmd.ExecuteNonQuery();

                                    NewHeaderID = (int)destcmd.Parameters["ID"].Value;

                                    DoLogging(ID.ToString(), "100", "Read Return Invoice Added=" + NewHeaderID.ToString(), false, "InvoiceReturnHToFinance");
                                }
                                #endregion

                                #region Apply destination details
                                using (SqlConnection Dconn = new SqlConnection(sourceConnectionString))
                                {
                                    SqlCommand Detailscmd = new SqlCommand(DetailsSQL, Dconn);
                                    Detailscmd.Parameters.AddWithValue("ID", ID);
                                    Dconn.Open();
                                    using (SqlDataReader Drdr = Detailscmd.ExecuteReader())
                                    {
                                        while (Drdr.Read())
                                        {
                                            string ItemID = (string)Drdr["ItemID"];
                                            decimal Quantity = (decimal)Drdr["Quantity"];
                                            decimal SalePrice = (decimal)Drdr["SalePrice"];

                                            DoLogging(ItemID.ToString(), "100", "Read Return Invoice Details=" + ItemID.ToString(), false, "InvoiceReturnHToFinance");
                                            #region send data t ocreate details in finaance system
                                            using (SqlCommand destcmd = new SqlCommand())
                                            {
                                                destcmd.CommandText = "usparSalesRInvDFullEdit";
                                                destcmd.Connection = DestConn;
                                                destcmd.CommandType = CommandType.StoredProcedure;

                                                destcmd.Parameters.AddWithValue("ID", 0);

                                                destcmd.Parameters.AddWithValue("SalesInvRHID", NewHeaderID.ToString());
                                                destcmd.Parameters.AddWithValue("ItemID", ItemID);
                                                destcmd.Parameters.AddWithValue("WareHouseID", MarketWarehouse);
                                                destcmd.Parameters.AddWithValue("Qty", Quantity.ToString());
                                                destcmd.Parameters.AddWithValue("Price", SalePrice.ToString());
                                                destcmd.Parameters.AddWithValue("Discount", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("STaxPercent", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("Description", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("JobID", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("Account", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("InvoiceDetailID", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("CostID", DBNull.Value);
                                                destcmd.Parameters.AddWithValue("BatchNo", DBNull.Value);

                                                destcmd.Parameters.AddWithValue("UserName", "System");
                                                destcmd.Parameters.AddWithValue("Source", "Import");
                                                destcmd.Parameters.AddWithValue("Subsys", "POS");

                                                destcmd.ExecuteNonQuery();

                                                DoLogging(ItemID.ToString(), "100", "Save Return Invoice Details=" + ItemID.ToString(), false, "InvoiceReturnHToFinance");
                                            }
                                            #endregion
                                        }
                                    }
                                }
                                #endregion
                                // DestConn.Close();

                            }
                            #endregion

                            #region  update header data wit htotals and taxes

                            using (SqlConnection tconn = new SqlConnection(destConnectionString))
                            {
                                DoLogging("Calc header", "100", "Read Return Invoice Header", false, "InvoiceReturnHToFinance");
                                SqlCommand Totalscmd = new SqlCommand(CalsHeaderDataSQL, tconn);
                                tconn.Open();
                                Totalscmd.Parameters.AddWithValue("Col1", ID);
                                Totalscmd.ExecuteNonQuery();
                            }
                            #endregion

                            //// Deleye header after copy 
                            //DoLogging("100", ID.ToString(), "Delete Invoce header From POS");
                            //using (SqlConnection Delconn = new SqlConnection(sourceConnectionString))
                            //{
                            //    SqlCommand Totalscmd = new SqlCommand(DeleteDoneHeaderSQL, Delconn);
                            //    Delconn.Open();
                            //    Totalscmd.Parameters.AddWithValue("ID", ID);
                            //    Totalscmd.ExecuteNonQuery();
                            //}
                            //  DestConn.Close();
                        }

                        //foreach (int Index in CopiedTrans)
                        //{
                        //    //// Deleye header after copy 
                        //    DoLogging("100", Index.ToString(), "Delete Invoce header From POS");
                        //    using (SqlConnection Delconn = new SqlConnection(sourceConnectionString))
                        //    {
                        //        SqlCommand Totalscmd = new SqlCommand(DeleteDoneHeaderSQL, Delconn);
                        //        Delconn.Open();
                        //        Totalscmd.Parameters.AddWithValue("ID", Index);
                        //        Totalscmd.ExecuteNonQuery();
                        //    }
                        //}
                        // conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("return Invoice history Data", "-500", err.Message, false, "InvoiceReturnHToFinance");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private class ItemData
        {
            public string Barcode { get; set; }
            public decimal Price { get; set; }
        }

        public static void CompareItems(string sourceConnectionString, string destConnectionString, string MarketWarehouse)
        {
            List<ItemData> LocalItemLst = new List<ItemData>();
            List<ItemData> ServerItemLst = new List<ItemData>();
            using (SqlConnection conn = new SqlConnection(destConnectionString))
            {
                string LocalSQL = "SELECT * FROM Item";
                SqlCommand cmd = new SqlCommand(LocalSQL, conn);
                conn.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        ItemData ItemObj = new ItemData();
                        ItemObj.Barcode = rdr["ID"].ToString();
                        ItemObj.Price = decimal.Parse(rdr["SellPrice"].ToString());
                        LocalItemLst.Add(ItemObj);
                    }
                }
            }
            using (SqlConnection conn = new SqlConnection(sourceConnectionString))
            {
                string SQL = "SELECT  Items.ID, Items.Name, IsNull(ivItemsSubCategory.MainId,1) as MainGategoryID, IsNull(Items.SubCategoryID,1) SubCategoryID, IsNull(Items.Salesprice,0) As Salesprice, IsNull(Items.OldSalesPrice, 0) As OldSalesPrice, Items.SaleUnit, Items.ItemType, Items.StockType, Items.PurchUnit, " +
                                    " Items.UnitCost, Items.SalesTax, Items.DiscountP, IsNull(Items.Remaks,'') as Remarks, " +
                                   " Items.Description, IsNull(Items.AlternateCode ,Items.ID) AlternateCode, Items.Measurment, Items.Tax1, Items.Tax2, IsNull(IsPromotion, 'false') As IsPromotion, IsNull(ivItemBatchbal.Qty ,0) as Balance " +
                                   " FROM  dbo.Items left JOIN " +
                                   " ivItemsSubCategory ON Items.SubCategoryID = ivItemsSubCategory.ID left JOIN" +
                                   " ivItemBatchbal ON Items.ID = ivItemBatchbal.ItemID  and ivItemBatchbal.WareHouseID =  " + MarketWarehouse.ToString() +
                                " where Items.IsActive = 1 and Items.IsSellable = 1 " + " Order by Items.ID";
                SqlCommand cmd = new SqlCommand(SQL, conn);
                conn.Open();
                using (SqlDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        ItemData ItemObj = new ItemData();
                        ItemObj.Barcode = rdr["ID"].ToString();
                        ItemObj.Price = decimal.Parse(rdr["Salesprice"].ToString());
                        ServerItemLst.Add(ItemObj);
                    }
                }
            }
            foreach (ItemData ItemObj in ServerItemLst)
            {
                if (LocalItemLst.Find(c => c.Barcode == ItemObj.Barcode) != null)
                {
                    ItemData LocalItemObj = LocalItemLst.Find(c => c.Barcode == ItemObj.Barcode);
                    if (LocalItemObj.Price != ItemObj.Price)
                    {
                        using (SqlConnection conn = new SqlConnection(destConnectionString))
                        {
                            string InsertLog = "INSERT INTO ItemPriceLog (ItemId, ChangeDate) VALUES (@ItemId, GETDATE())";
                            SqlCommand cmd = new SqlCommand(InsertLog, conn);
                            cmd.Parameters.AddWithValue("ItemId", ItemObj.Barcode);
                            conn.Open();
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(destConnectionString))
                    {
                        string InsertLog = "INSERT INTO ItemPriceLog (ItemId, ChangeDate) VALUES (@ItemId, GETDATE())";
                        SqlCommand cmd = new SqlCommand(InsertLog, conn);
                        cmd.Parameters.AddWithValue("ItemId", ItemObj.Barcode);
                        conn.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public static bool GetItemsData(string sourceConnectionString, string destConnectionString, string MarketWarehouse, string MarketBranch, bool SwithcDescription = false)
        {
            var itemId = "";
            try
            {
                CompareItems(sourceConnectionString, destConnectionString, MarketWarehouse);
                #region empty old data first
                //string SQLEmpty = "Delete From Item";
                //using (SqlConnection DelConn = new SqlConnection(destConnectionString))
                //{
                //    DelConn.Open();
                //    using (SqlCommand destcmd = new SqlCommand(SQLEmpty, DelConn))
                //    {
                //        DoLogging("Empty Items", "100", "Empty Items", false, "Items");
                //        destcmd.ExecuteNonQuery();
                //    }

                //    SQLEmpty = "Delete From Category";
                //    using (SqlCommand destcmd = new SqlCommand(SQLEmpty, DelConn))
                //    {
                //        DoLogging("Empty Category", "100", "Empty Category", false, "Items");
                //        destcmd.ExecuteNonQuery();
                //    }

                //    SQLEmpty = "Delete From Department";
                //    using (SqlCommand destcmd = new SqlCommand(SQLEmpty, DelConn))
                //    {
                //        DoLogging("Empty Main Category", "100", "Empty Main Category", false, "Items");
                //        destcmd.ExecuteNonQuery();
                //    }
                //    //  DelConn.Close();
                //}

                #endregion

                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    #region fill in main category data
                    string SQL = "SELECT   [ID] ,[Name] ,IsNull([Description],'None') as Description FROM  [dbo].[ivItemsMainCategory]";

                    string SQLInsert = "INSERT INTO  [dbo].[Department](ID ,[Name],[Description],[Active]) VALUES (@ID ,@Name  ,@Description ,@Active )";
                    string SQLUpdate = "UPDATE  [dbo].[Department] SET [Name] = @Name  , [Description] = @Description , [Active] = @Active WHERE ID = @ID ";
                    string SQLSelect = "SELECT ID FROM Department WHERE ID = @ID";

                    SqlCommand cmd = new SqlCommand(SQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            string ID = rdr["ID"].ToString();

                            string Name = (string)rdr["Name"];

                            string Description = (string)rdr["Description"];

                            #endregion
                            #region apply to destination
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();
                                object hasData;
                                using (SqlCommand destcmd = new SqlCommand(SQLSelect, DestConn))
                                {

                                    destcmd.Parameters.AddWithValue("ID", ID);
                                    hasData = destcmd.ExecuteScalar();
                                }
                                if(hasData != null)
                                {
                                    using (SqlCommand destcmd = new SqlCommand(SQLUpdate, DestConn))
                                    {

                                        destcmd.Parameters.AddWithValue("ID", ID);
                                        destcmd.Parameters.AddWithValue("Name", Name);

                                        destcmd.Parameters.AddWithValue("Description", Description);
                                        destcmd.Parameters.AddWithValue("Active", true);

                                        destcmd.ExecuteNonQuery();
                                    }
                                }
                                else
                                {
                                    using (SqlCommand destcmd = new SqlCommand(SQLInsert, DestConn))
                                    {

                                        destcmd.Parameters.AddWithValue("ID", ID);
                                        destcmd.Parameters.AddWithValue("Name", Name);

                                        destcmd.Parameters.AddWithValue("Description", Description);
                                        destcmd.Parameters.AddWithValue("Active", true);

                                        destcmd.ExecuteNonQuery();
                                    }
                                }
                                DestConn.Close();

                            }
                            #endregion
                        }
                        conn.Close();
                    }
                    #endregion

                    #region fill in sub category data
                    SQL = "SELECT  [ID] ,IsNull([MainId],1) MainId ,[Name]  FROM [dbo].[ivItemsSubCategory]";

                    SQLInsert = "INSERT INTO [dbo].[Category] (ID,[Name]  ,[DepartmentID],[Active]) VALUES (@ID ,@Name  ,@DepartmentID ,@Active)";
                    SQLUpdate = "UPDATE  [dbo].[Category] SET [Name] = @Name ,[DepartmentID] = @DepartmentID , [Active] = @Active WHERE ID = @ID ";
                    SQLSelect = "SELECT ID FROM [dbo].[Category] WHERE ID = @ID";
                    cmd = new SqlCommand(SQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            String ID = rdr["ID"].ToString();

                            string Name = (string)rdr["Name"];

                            string MainId = rdr["MainId"].ToString();
                            DoLogging(ID.ToString(), "100", "Insert Category", false, "Items");
                            #endregion
                            #region apply to destination
                            object hasCat;
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();
                                using (SqlCommand destcmd = new SqlCommand(SQLSelect, DestConn))
                                {
                                    destcmd.Parameters.AddWithValue("ID", ID);
                                    hasCat = destcmd.ExecuteScalar();
                                }
                                if(hasCat != null)
                                {
                                    using (SqlCommand destcmd = new SqlCommand(SQLUpdate, DestConn))
                                    {
                                        destcmd.Parameters.AddWithValue("ID", ID);
                                        destcmd.Parameters.AddWithValue("Name", Name);
                                        destcmd.Parameters.AddWithValue("DepartmentID", MainId);
                                        destcmd.Parameters.AddWithValue("Active", true);
                                        destcmd.ExecuteNonQuery();
                                    }
                                }
                                else
                                {
                                    using (SqlCommand destcmd = new SqlCommand(SQLInsert, DestConn))
                                    {
                                        destcmd.Parameters.AddWithValue("ID", ID);
                                        destcmd.Parameters.AddWithValue("Name", Name);
                                        destcmd.Parameters.AddWithValue("DepartmentID", MainId);
                                        destcmd.Parameters.AddWithValue("Active", true);
                                        destcmd.ExecuteNonQuery();
                                    }
                                }
                                DestConn.Close();
                            }
                            #endregion
                        }
                        conn.Close();
                    }
                    #endregion

                    #region fill in items data
                    SQL = @"SELECT  Items.ID, Items.Name,[dbo].[ivItemsBranch].[SalesPrice] as BranchSalesPrice, IsNull(ivItemsSubCategory.MainId,1) as MainGategoryID, IsNull(Items.SubCategoryID,1) SubCategoryID
                    , IsNull(Items.Salesprice, 0) As Salesprice, IsNull(Items.OldSalesPrice, 0) As OldSalesPrice, Items.SaleUnit, Items.ItemType, Items.StockType, Items.PurchUnit,Items.UnitCost, Items.SalesTax, Items.DiscountP
                    , IsNull(Items.Remaks, '') as Remarks,Items.Description, IsNull(Items.AlternateCode, Items.ID) AlternateCode, Items.Measurment, Items.Tax1, Items.Tax2
                    , IsNull(IsPromotion, 'false') As IsPromotion, IsNull(ivItemBatchbal.Qty , 0) as Balance FROM dbo.Items left JOIN ivItemsSubCategory ON Items.SubCategoryID = ivItemsSubCategory.ID
                    left JOIN ivItemBatchbal ON Items.ID = ivItemBatchbal.ItemID  and ivItemBatchbal.WareHouseID = " + MarketWarehouse.ToString() +
                    @"left join ivItemsBranch ON Items.ID = ivItemsBranch.ItemID and ivItemsBranch.BranchID = " + MarketBranch.ToString() +
                    "where Items.IsActive = 1 and Items.IsSellable = 1 Order by Items.ID";

                    SQLInsert = "INSERT INTO [dbo].[Item]([ID],[Name],[SellPrice],[Description],[Active],[Barcode] ,[CategoryID],[DepartmentID] ,[Balance], Weighted ,SaleUnitID, OnSale, SalePrice) " +
                                " VALUES(@ID ,@Name ,@SellPrice,@Description,@Active,@Barcode,@CategoryID,@DepartmentID,@Balance,0,1,@OnSale,@SalePrice)";
                    SQLUpdate = "UPDATE [dbo].[Item] SET [Name] = @Name,[SellPrice] = @SellPrice,[Description] = @Description,[Active] = @Active,[Barcode] = @Barcode,[CategoryID] = @CategoryID," +
                        "[DepartmentID] = @DepartmentID ,[Balance] = @Balance, Weighted = 0,SaleUnitID = 1, OnSale = @OnSale, SalePrice = @SalePrice " +
                                " WHERE ID = @ID ";
                    SQLSelect = "SELECT ID FROM [dbo].[Item] WHERE ID = @ID";

                    #region get row count
                    //SqlCommand countCmd = new SqlCommand(SQL, conn);
                    cmd = new SqlCommand(SQL, conn);
                    //conn.Open();
                    //using (SqlDataReader crdr = countCmd.ExecuteReader())
                    //{
                    //    // get record count for progress bar
                    //    using (DataTable dt = new DataTable())
                    //    {
                    //        dt.Load(crdr);
                    //        pBar.Minimum = 0;
                    //        pBar.Maximum =  dt.Rows.Count;
                    //    }
                    //    conn.Close();
                    //}
                    #endregion
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {

                            // pBar.Value += pBar.Value; 
                            #region get data from one row
                            String ItemID = (String)rdr["ID"];
                            itemId = ItemID;
                            string Name = (string)rdr["Name"];
                            string MainGategoryID = rdr["MainGategoryID"].ToString();
                            string SubCategoryID = rdr["SubCategoryID"].ToString();
                            decimal Salesprice = 0;
                            try { Salesprice = (decimal)rdr["BranchSalesPrice"]; } catch { Salesprice = (decimal)rdr["Salesprice"]; }
                            string AlternateCode = (string)rdr["AlternateCode"];
                            string Description = (string)rdr["Remarks"];
                            decimal Balance = (decimal)rdr["Balance"];
                            bool IsPromotion = bool.Parse(rdr["IsPromotion"].ToString());
                            decimal OldPrice = decimal.Parse(rdr["OldSalesPrice"].ToString());

                            DoLogging(ItemID.ToString(), "100", "Insert Item", false, "Items");
                            #endregion
                            #region apply to destination
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();
                                object hasItem;
                                using (SqlCommand destcmd = new SqlCommand(SQLSelect, DestConn))
                                {
                                    destcmd.Parameters.AddWithValue("ID", ItemID);
                                    hasItem = destcmd.ExecuteScalar();
                                }
                                if (hasItem != null)
                                {
                                    using (SqlCommand destcmd = new SqlCommand(SQLUpdate, DestConn))
                                    {

                                        destcmd.Parameters.AddWithValue("ID", ItemID);
                                        destcmd.Parameters.AddWithValue("Name", SwithcDescription ? Description : Name);
                                        destcmd.Parameters.AddWithValue("Active", true);

                                        destcmd.Parameters.AddWithValue("SellPrice", Salesprice);
                                        destcmd.Parameters.AddWithValue("Description", SwithcDescription ? Name : Description);
                                        destcmd.Parameters.AddWithValue("Barcode", AlternateCode);

                                        destcmd.Parameters.AddWithValue("CategoryID", SubCategoryID);
                                        destcmd.Parameters.AddWithValue("DepartmentID", MainGategoryID);

                                        destcmd.Parameters.AddWithValue("Balance", Balance);
                                        destcmd.Parameters.AddWithValue("OnSale", IsPromotion);
                                        destcmd.Parameters.AddWithValue("SalePrice", OldPrice);

                                        destcmd.ExecuteNonQuery();
                                        DoLogging(ItemID.ToString(), "100", "Done Item", false, "Items");
                                    }
                                }
                                else
                                {
                                    using (SqlCommand destcmd = new SqlCommand(SQLInsert, DestConn))
                                    {

                                        destcmd.Parameters.AddWithValue("ID", ItemID);
                                        destcmd.Parameters.AddWithValue("Name", SwithcDescription ? Description : Name);
                                        destcmd.Parameters.AddWithValue("Active", true);

                                        destcmd.Parameters.AddWithValue("SellPrice", Salesprice);
                                        destcmd.Parameters.AddWithValue("Description", SwithcDescription ? Name : Description);
                                        destcmd.Parameters.AddWithValue("Barcode", AlternateCode);

                                        destcmd.Parameters.AddWithValue("CategoryID", SubCategoryID);
                                        destcmd.Parameters.AddWithValue("DepartmentID", MainGategoryID);

                                        destcmd.Parameters.AddWithValue("Balance", Balance);
                                        destcmd.Parameters.AddWithValue("OnSale", IsPromotion);
                                        destcmd.Parameters.AddWithValue("SalePrice", OldPrice);

                                        destcmd.ExecuteNonQuery();
                                        DoLogging(ItemID.ToString(), "100", "Done Item", false, "Items");
                                    }
                                }
                                
                                //  DestConn.Close();
                            }
                            #endregion
                        }
                        // conn.Close();
                    }
                    #endregion
                }
            }
            catch (Exception err)
            {
                DoLogging("Item Data", "-400", err.Message + " ItemID: " + itemId, false, "Items");
                MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool GetItemsDataWithPrice(string sourceConnectionString, string destConnectionString, string MarketWarehouse, BackgroundWorker worker = null, bool SwithcDescription = false)
        {
            try
            {
                #region empty old data first
                string SQLEmpty = "Delete From Item";
                using (SqlConnection DelConn = new SqlConnection(destConnectionString))
                {
                    DelConn.Open();
                    using (SqlCommand destcmd = new SqlCommand(SQLEmpty, DelConn))
                    {
                        DoLogging("Empty Items", "100", "Empty Items", false, "ItemsPrice");
                        destcmd.ExecuteNonQuery();
                    }

                    SQLEmpty = "Delete From Category";
                    using (SqlCommand destcmd = new SqlCommand(SQLEmpty, DelConn))
                    {
                        DoLogging("Empty Category", "100", "Empty Category", false, "ItemsPrice");
                        destcmd.ExecuteNonQuery();
                    }

                    SQLEmpty = "Delete From Department";
                    using (SqlCommand destcmd = new SqlCommand(SQLEmpty, DelConn))
                    {
                        DoLogging("Empty Main Category", "100", "Empty Main Category", false, "ItemsPrice");
                        destcmd.ExecuteNonQuery();
                    }
                    //  DelConn.Close();
                }

                #endregion

                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    #region fill in main category data
                    string SQL = "SELECT   [ID] ,[Name] ,IsNull([Description],'None') as Description FROM  [dbo].[ivItemsMainCategory]";

                    string SQLInsert = "INSERT INTO  [dbo].[Department](ID ,[Name],[Description],[Active]) VALUES (@ID ,@Name  ,@Description ,@Active )";

                    SqlCommand cmd = new SqlCommand(SQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            string ID = rdr["ID"].ToString();

                            string Name = (string)rdr["Name"];

                            string Description = (string)rdr["Description"];

                            #endregion
                            #region apply to destination
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();
                                using (SqlCommand destcmd = new SqlCommand(SQLInsert, DestConn))
                                {

                                    destcmd.Parameters.AddWithValue("ID", ID);
                                    destcmd.Parameters.AddWithValue("Name", Name);

                                    destcmd.Parameters.AddWithValue("Description", Description);
                                    destcmd.Parameters.AddWithValue("Active", true);

                                    destcmd.ExecuteNonQuery();
                                }
                                DestConn.Close();

                            }
                            #endregion
                        }
                        conn.Close();
                    }
                    #endregion

                    #region fill in sub category data
                    SQL = "SELECT  [ID] ,[MainId] ,[Name]  FROM [dbo].[ivItemsSubCategory]";

                    SQLInsert = "INSERT INTO [dbo].[Category] (ID,[Name]  ,[DepartmentID],[Active]) VALUES (@ID ,@Name  ,@DepartmentID ,@Active)";

                    cmd = new SqlCommand(SQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            String ID = rdr["ID"].ToString();

                            string Name = (string)rdr["Name"];

                            string MainId = rdr["MainId"].ToString();
                            DoLogging(ID.ToString(), "100", "Insert Category", false, "ItemsPrice");
                            #endregion
                            #region apply to destination
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();
                                using (SqlCommand destcmd = new SqlCommand(SQLInsert, DestConn))
                                {
                                    destcmd.Parameters.AddWithValue("ID", ID);
                                    destcmd.Parameters.AddWithValue("Name", Name);
                                    destcmd.Parameters.AddWithValue("DepartmentID", MainId);
                                    destcmd.Parameters.AddWithValue("Active", true);
                                    destcmd.ExecuteNonQuery();
                                }
                                DestConn.Close();
                            }
                            #endregion
                        }
                        conn.Close();
                    }
                    #endregion

                    #region fill in item category data
                    SQL = "SELECT distinct Items.ID, Items.Name, IsNull(ivItemsSubCategory.MainId,1) as MainGategoryID, IsNull(Items.SubCategoryID,1) SubCategoryID , Items.SaleUnit, Items.ItemType, Items.StockType, Items.PurchUnit, " +
                                  " Items.UnitCost, Items.SalesTax, Items.DiscountP, IsNull(Items.Remaks,'') as Remarks, " +
                                 " Items.Description, IsNull(Items.AlternateCode ,Items.ID) AlternateCode, Items.Measurment, Items.Tax1, Items.Tax2,  IsNull(ivItemBatchbal.Qty ,0) as Balance " +
                                 " ,IsNull( " +
                                 "IsNUll(( (Items.SalesPrice * (((1-isNull(Items.DiscountP,0)/100)) ) ) * " +
                                 "(1-	IsNull((Select IsNull(ivItemPrice.Discount ,0) " +
                                 "	 From ivItemPrice   Where  ivItemPrice.ItemID = Items.ID " +
                                 "	and CONVERT (date, GETDATE()) between ivItemPrice.FromDate and  ivItemPrice.ToDate ) ,0)/100)),Salesprice) ,0) as Salesprice " +
                                 " FROM  dbo.Items left JOIN " +
                                 " ivItemsSubCategory ON Items.SubCategoryID = ivItemsSubCategory.ID left JOIN" +
                                 " ivItemPrice   ON ivItemPrice.ItemID = Items.ID  left JOIN " +
                                 " ivItemBatchbal ON Items.ID = ivItemBatchbal.ItemID  and ivItemBatchbal.WareHouseID =  " + MarketWarehouse.ToString() +
                              " where Items.IsActive = 1  --and ivItemBatchbal.WareHouseID = " + MarketWarehouse.ToString() +
                              " Order by Items.ID";

                    SQLInsert = "INSERT INTO [dbo].[Item]([ID],[Name],[SellPrice],[Description],[Active],[Barcode] ,[CategoryID],[DepartmentID] ,[Balance], Weighted ,SaleUnitID) " +
                                " VALUES(@ID ,@Name ,@SellPrice,@Description,@Active,@Barcode,@CategoryID,@DepartmentID,@Balance , 0,1)";

                    #region get row count
                    int RowsCount = 0;
                    SqlCommand countCmd = new SqlCommand(SQL, conn);

                    conn.Open();
                    using (SqlDataReader crdr = countCmd.ExecuteReader())
                    {
                        // get record count for progress bar
                        using (DataTable dt = new DataTable())
                        {
                            dt.Load(crdr);
                            RowsCount = dt.Rows.Count;
                        }
                        conn.Close();
                    }
                    #endregion
                    cmd = new SqlCommand(SQL, conn);
                    conn.Open();
                    int index = 1;
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {

                        while (rdr.Read())
                        {
                            index += 1;
                            int percentComplete = (int)((float)index / (float)RowsCount * 100);
                            // worker.ReportProgress(percentComplete);
                            #region get data from one row
                            String ItemID = rdr["ID"].ToString();
                            string Name = (string)rdr["Name"];
                            string MainGategoryID = rdr["MainGategoryID"].ToString();
                            string SubCategoryID = rdr["SubCategoryID"].ToString();
                            decimal Salesprice = (decimal)rdr["Salesprice"];
                            string AlternateCode = (string)rdr["AlternateCode"];
                            string Description = (string)rdr["Remarks"];
                            decimal Balance = (decimal)rdr["Balance"];

                            DoLogging(ItemID.ToString(), "100", "Insert Item", false, "ItemsPrice");
                            #endregion
                            #region apply to destination
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();
                                using (SqlCommand destcmd = new SqlCommand(SQLInsert, DestConn))
                                {

                                    destcmd.Parameters.AddWithValue("ID", ItemID);
                                    destcmd.Parameters.AddWithValue("Name", SwithcDescription ? Description : Name);

                                    destcmd.Parameters.AddWithValue("Description", SwithcDescription ? Name : Description);
                                    destcmd.Parameters.AddWithValue("Active", true);

                                    destcmd.Parameters.AddWithValue("SellPrice", Salesprice);

                                    destcmd.Parameters.AddWithValue("Barcode", AlternateCode);

                                    destcmd.Parameters.AddWithValue("CategoryID", SubCategoryID);
                                    destcmd.Parameters.AddWithValue("DepartmentID", MainGategoryID);

                                    destcmd.Parameters.AddWithValue("Balance", Balance);

                                    destcmd.ExecuteNonQuery();
                                }
                                //  DestConn.Close();
                            }
                            #endregion
                        }
                        // conn.Close();
                    }
                    #endregion
                }
            }
            catch (Exception err)
            {
                DoLogging("Error", "-400", err.Message, false, "ItemsPrice");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool SendOrdersToFinance(string sourceConnectionString, string destConnectionString, string MarketWarehouse, string BranchDefault)
        {

            string HeaderSQL = "SELECT [ID]  ,[UserID]  ,[CustomerID]  ,[DeliveryEmloyeeID]  ,[Total]  ,[SalesTax] ,[DiscountPercent],[CashDiscount] ,[CreditAmount] ,[DueAmount] ,[Paid],[Change],[SafeNumber] ,IsNull([Notes],' ') [Notes] ,[CreditCardTransactionNumber]  ,[CreatedTime] ,[Collected] ,[OrderSource]   ,[CustomerData]  ,[PaymentMethod] FROM [dbo].[PurchaseOrderH] Where Status is null and UserId <> 0 and Collected = 1 ";

            string DetailsSQL = "SELECT [ID] ,[PurchaseOrderHID]  ,[Cost] ,[SalePrice] ,[ItemID] ,[Quantity] ,[Taxable] ,[SalesTax]  FROM [dbo].[PurchaseOrderD] where [PurchaseOrderHID] = @ID";

            string CalsHeaderDataSQL = " UPDATE [dbo].[PurchaseOrderH] " +
                           " SET [Status] = 5 " +
                           " WHERE ID = @ID  ";

            string DeleteDoneHeaderSQL = " Delete From TransactionTable Where ID = @ID";
            try
            {
                Int32 ID = 0;

                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(HeaderSQL, conn);


                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            ID = (Int32)rdr["ID"];
                            Int32 CustomerID = (Int32)rdr["CustomerID"];
                            Int32 UserID = (Int32)rdr["UserID"];
                            Decimal DiscountPercent = (Decimal)rdr["DiscountPercent"];
                            string Notes = (string)rdr["Notes"];
                            DateTime CreatedTime = (DateTime)rdr["CreatedTime"];

                            DoLogging(ID.ToString(), "100", "Read Invoice Header" + ID.ToString(), false, "InvoiceToFinance");
                            #endregion
                            #region apply to destination header
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                int NewHeaderID = 0;
                                DestConn.Open();

                                #region send data t ocreate header in finaance system
                                using (SqlCommand destcmd = new SqlCommand())

                                {
                                    NewHeaderID = 0;
                                    destcmd.CommandText = "usparSalesOrderHEdit";
                                    destcmd.Connection = DestConn;
                                    destcmd.CommandType = CommandType.StoredProcedure;

                                    destcmd.Parameters.AddWithValue("ID", NewHeaderID).Direction = ParameterDirection.Output;

                                    destcmd.Parameters.AddWithValue("OrderNo", ID.ToString());
                                    //destcmd.Parameters.AddWithValue("CustomerID", CustomerID);
                                    // destcmd.Parameters.AddWithValue("CustomerData", "POS Import");
                                    destcmd.Parameters.AddWithValue("Offer", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("CustomerPO", "POS Import");
                                    destcmd.Parameters.AddWithValue("arSalesRep", UserID.ToString());
                                    destcmd.Parameters.AddWithValue("HDate", DateTime.Today.ToString("MM-dd-yyyy"));//CreatedTime.ToShortDateString()


                                    destcmd.Parameters.AddWithValue("Remarks", Notes);
                                    destcmd.Parameters.AddWithValue("DeliveryDate", CreatedTime.ToString("MM-dd-yyyy"));
                                    destcmd.Parameters.AddWithValue("Finished", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Delivery", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Notes", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Total", DBNull.Value);
                                    destcmd.Parameters.AddWithValue("Status", "1");

                                    destcmd.Parameters.AddWithValue("UserName", "System");
                                    destcmd.Parameters.AddWithValue("Source", "Import");
                                    destcmd.Parameters.AddWithValue("Subsys", "POS");

                                    destcmd.ExecuteNonQuery();
                                    NewHeaderID = (int)destcmd.Parameters["ID"].Value;
                                    DoLogging(ID.ToString(), "100", "Inserted Sales Order Header Header" + NewHeaderID.ToString(), false, "InvoiceToFinance");
                                }
                                #endregion

                                #region Apply destination details
                                using (SqlConnection Dconn = new SqlConnection(sourceConnectionString))
                                {
                                    SqlCommand Detailscmd = new SqlCommand(DetailsSQL, Dconn);
                                    Dconn.Open();
                                    Detailscmd.Parameters.AddWithValue("ID", ID);
                                    using (SqlDataReader Drdr = Detailscmd.ExecuteReader())
                                    {
                                        while (Drdr.Read())
                                        {
                                            string ItemID = (string)Drdr["ItemID"];
                                            decimal Quantity = (decimal)Drdr["Quantity"];
                                            decimal SalePrice = (decimal)Drdr["SalePrice"];
                                            DoLogging(ItemID.ToString(), "100", "Read order Details for Item" + ItemID, false, "OrderToFinance");
                                            #region send data t ocreate details in finaance system
                                            using (SqlCommand destcmd = new SqlCommand())
                                            {
                                                destcmd.CommandText = "usparSalesOrderDEdit";
                                                destcmd.Connection = DestConn;
                                                destcmd.CommandType = CommandType.StoredProcedure;

                                                destcmd.Parameters.AddWithValue("ID", 0);

                                                destcmd.Parameters.AddWithValue("SalesPOHID", NewHeaderID.ToString());
                                                destcmd.Parameters.AddWithValue("ItemID", ItemID);
                                                destcmd.Parameters.AddWithValue("WareHouseID", MarketWarehouse);
                                                destcmd.Parameters.AddWithValue("Qty", Quantity.ToString());
                                                destcmd.Parameters.AddWithValue("Price", SalePrice.ToString());


                                                destcmd.Parameters.AddWithValue("UserName", "System");
                                                destcmd.Parameters.AddWithValue("Source", "Import");
                                                destcmd.Parameters.AddWithValue("Subsys", "POS");

                                                destcmd.ExecuteNonQuery();

                                                DoLogging(ItemID.ToString(), "100", "Inserted sales order Details for Item" + ItemID, false, "OrderToFinance");

                                            }
                                            #endregion

                                        }
                                    }
                                    //  Dconn.Close();
                                }
                                #endregion

                                #region apply total for header
                                // update header data wit htotals and taxes
                                DoLogging("100", "100", "Update Invoce header total value", false, "InvoiceToFinance");
                                using (SqlConnection tconn = new SqlConnection(sourceConnectionString))
                                {
                                    SqlCommand Totalscmd = new SqlCommand(CalsHeaderDataSQL, tconn);
                                    tconn.Open();
                                    Totalscmd.Parameters.AddWithValue("ID", ID);
                                    Totalscmd.ExecuteNonQuery();
                                }

                                #endregion

                                //// Deleye header after copy 
                                //DoLogging("100", ID.ToString(), "Delete Invoce header From POS");
                                //using (SqlConnection Delconn = new SqlConnection(sourceConnectionString))
                                //{
                                //    SqlCommand Totalscmd = new SqlCommand(DeleteDoneHeaderSQL, Delconn);
                                //    Delconn.Open();
                                //    Totalscmd.Parameters.AddWithValue("ID", ID);
                                //    Totalscmd.ExecuteNonQuery();
                                //}
                                //  DestConn.Close();

                            }
                            #endregion
                        }
                        //  conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("Error", "-600", err.Message, false, "OrderToFinance");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        public static bool GetDeliveryFromFinance(string sourceConnectionString, string destConnectionString, string MarketWarehouse, string BranchDefault)
        {

            string HeaderSQL = "SELECT [ID] ,[OrderNo] ,[CustomerID] ,[arSalesRep]  ,[HDate]  ,[Finished]  ,IsNull([Remarks] ,' ') Remarks ,IsNull([Delivery] ,'0')[Delivery]  ,IsNull([Notes] ,' ') Notes ,IsNull(CustomerData,' ') CustomerData , IsNull(Total, '0') [Total] ,[Status] FROM [dbo].[arSalesOrderH] Where Status<>5 " +
                " and WareHouseID=" + MarketWarehouse;

            string FinishGetOrder = "UPDATE PurchaseOrderH SET STATUS = NULL WHERE ID = @ID";

            string DetailsSQL = "SELECT  [ID] ,[SalesPOHID]  ,[ItemID]  ,[WareHouseID]   ,[Qty]   ,[Price] FROM [dbo].[arSalesOrderD] Where [SalesPOHID] = @ID";

            string CalsHeaderDataSQL = " UPDATE [dbo].[arSalesOrderH] " +
                                       " SET [Status] = 5 " +
                                       " WHERE ID = @ID  ";

            string DeleteDoneHeaderSQL = " Delete From TransactionTable Where ID = @ID";

            string DestinationHeader = "SET IDENTITY_INSERT PurchaseOrderH ON " +
            "insert into PurchaseOrderH(ID, UserID, CustomerID, Total, DiscountPercent, CashDiscount, CreditAmount, DueAmount, SafeNumber, Notes, CreatedTime, OrderSource, CustomerData, PaymentMethod, Status) " +
            "Values(@ID,@UserID, @CustomerID, @Total, @DiscountPercent, @CashDiscount, @CreditAmount, @DueAmount, @SafeNumber, @Notes, @CreatedTime, @OrderSource, @CustomerData, @PaymentMethod, @Status) " +
            "SET IDENTITY_INSERT PurchaseOrderH OFF";
            //" Select Scope_Identity() "+
            //" select @finalid";


            string GetHeaderID = "Select Max(ID) From PurchaseOrderH Where UserID=0";

            string DestinationDetails = "insert into dbo.PurchaseOrderD(PurchaseOrderHID,Cost,SalePrice,ItemID,Quantity)" +
                " Values(@PurchaseOrderHID,@Cost,@SalePrice,@ItemID,@Quantity)";

            string DeleteDetails = "Delete From dbo.PurchaseOrderD where PurchaseOrderHID = @ID " + "Delete from dbo.TansactionDetailMain where TransactionID = @ID";

            try
            {

                Int64 ID = 0;
                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(HeaderSQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            ID = (Int64)rdr["ID"];
                            string OrderNo = (string)rdr["OrderNo"];
                            int CustomerID = (int)rdr["CustomerID"];

                            string CustomerData = (string)rdr["CustomerData"];

                            // avoid erro if not numeric
                            int PaymentMethod = 0;

                            if (!int.TryParse(rdr["Delivery"].ToString(), out PaymentMethod))
                            {
                                PaymentMethod = 0;
                            }
                            //  ,[HDate]  ,[Finished]  ,[Remarks]   ,[Delivery]  ,[Notes]

                            //Int32 UserID = (Int32)rdr["UserID"];
                            //Decimal DiscountPercent = (Decimal)rdr["DiscountPercent"];
                            decimal Total = decimal.Parse(rdr["Total"].ToString());
                            string Notes = null;
                            // if (!rdr.IsDBNull(8))
                            Notes = (string)rdr["Notes"];
                            string Remarks = null;
                            // if (!rdr.IsDBNull(9))
                            Remarks = (string)rdr["Remarks"];
                            //DateTime CreatedTime = (DateTime)rdr["CreatedTime"];

                            DoLogging(ID.ToString(), "100", "Read order Header" + ID.ToString(), false, "GetDeliveryFromFinance");
                            #endregion
                            #region apply to destination header
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                int? NewHeaderID = 0;
                                DestConn.Open();

                                #region send data to create header in POS system
                                using (SqlCommand destcmd = new SqlCommand())
                                {
                                    NewHeaderID = 0;
                                    destcmd.CommandText = DestinationHeader;
                                    destcmd.Connection = DestConn;
                                    destcmd.CommandType = CommandType.Text;
                                    //@UserID,@CustomerID,@Total,@DiscountPercent,@CashDiscount
                                    //,@CreditAmount,@DueAmount,@SafeNumber,@Notes,@CreatedTime,@OrderSource
                                    // destcmd.Parameters.AddWithValue("ID", NewHeaderID).Direction = ParameterDirection.Output;
                                    destcmd.Parameters.AddWithValue("ID", ID);
                                    destcmd.Parameters.AddWithValue("UserID", "0".ToString());
                                    destcmd.Parameters.AddWithValue("CustomerID", CustomerID.ToString());
                                    destcmd.Parameters.AddWithValue("Total", Total);
                                    destcmd.Parameters.AddWithValue("DiscountPercent", "0");
                                    destcmd.Parameters.AddWithValue("CashDiscount", "0");
                                    destcmd.Parameters.AddWithValue("CreditAmount", "0");
                                    destcmd.Parameters.AddWithValue("CustomerData", CustomerData);
                                    destcmd.Parameters.AddWithValue("DueAmount", "0");
                                    destcmd.Parameters.AddWithValue("SafeNumber", "9999");
                                    destcmd.Parameters.AddWithValue("Notes", OrderNo);
                                    destcmd.Parameters.AddWithValue("CreatedTime", DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss"));//CreatedTime.ToShortDateString()
                                    destcmd.Parameters.AddWithValue("OrderSource", "5");
                                    destcmd.Parameters.AddWithValue("PaymentMethod", PaymentMethod);
                                    destcmd.Parameters.AddWithValue("Status", 10);
                                    //destcmd.Parameters.AddWithValue("Remarks", Notes);
                                    try
                                    {
                                        destcmd.ExecuteNonQuery();
                                        //destcmd.CommandText = GetHeaderID;
                                        //NewHeaderID = (int?)destcmd.ExecuteScalar();
                                        //destcmd.Parameters["ID"].Value;
                                        DoLogging(ID.ToString(), "100", "Inserted Invoice Header" + NewHeaderID.ToString(), false, "InvoiceToFinance");
                                    }
                                    catch (Exception ex)
                                    {
                                        #region Delete Details For Header
                                        // update header data wit htotals and taxes
                                        //DoLogging("100", "100", "Update Invoce header total value", false, "InvoiceToFinance");
                                        //using (SqlConnection tconn = new SqlConnection(sourceConnectionString))
                                        //{
                                        //    SqlCommand Totalscmd = new SqlCommand(CalsHeaderDataSQL, tconn);
                                        //    tconn.Open();
                                        //    Totalscmd.Parameters.AddWithValue("ID", ID);
                                        //    Totalscmd.ExecuteNonQuery();
                                        //}
                                        DoLogging(ID.ToString(), "100", "Delete Invocie Details For Messy Order", false, "GetDeliveryFromFinance");
                                        using (SqlConnection dconn = new SqlConnection(destConnectionString))
                                        {
                                            SqlCommand detailscmd = new SqlCommand(DeleteDetails, dconn);
                                            dconn.Open();
                                            detailscmd.Parameters.AddWithValue("ID", ID);
                                            detailscmd.ExecuteNonQuery();
                                        }
                                        #endregion
                                    }
                                }
                                #endregion

                                #region Apply destination details
                                using (SqlConnection Dconn = new SqlConnection(sourceConnectionString))
                                {
                                    SqlCommand Detailscmd = new SqlCommand(DetailsSQL, Dconn);
                                    Dconn.Open();
                                    Detailscmd.Parameters.AddWithValue("ID", ID);
                                    using (SqlDataReader Drdr = Detailscmd.ExecuteReader())
                                    {
                                        while (Drdr.Read())
                                        {
                                            string ItemID = (string)Drdr["ItemID"];
                                            decimal Quantity = (decimal)Drdr["Qty"];
                                            decimal SalePrice = (decimal)Drdr["Price"];
                                            DoLogging(ItemID.ToString(), "100", "Read Invoice Details for Item" + ItemID, false, "InvoiceToFinance");
                                            #region send data t ocreate details in finaance system
                                            using (SqlCommand destcmd = new SqlCommand())
                                            {
                                                destcmd.CommandText = DestinationDetails;
                                                destcmd.Connection = DestConn;
                                                destcmd.CommandType = CommandType.Text;


                                                destcmd.Parameters.AddWithValue("PurchaseOrderHID", ID.ToString());
                                                destcmd.Parameters.AddWithValue("ItemID", ItemID);
                                                destcmd.Parameters.AddWithValue("WareHouseID", MarketWarehouse);
                                                destcmd.Parameters.AddWithValue("Quantity", Quantity.ToString());
                                                destcmd.Parameters.AddWithValue("SalePrice", SalePrice.ToString());
                                                destcmd.Parameters.AddWithValue("Cost", "0");
                                                destcmd.ExecuteNonQuery();

                                                DoLogging(ItemID.ToString(), "100", "Inserted Invoice Details for Item" + ItemID, false, "InvoiceToFinance");

                                            }
                                            #endregion

                                        }
                                    }
                                    //  Dconn.Close();
                                }
                                #endregion

                                #region apply total for header
                                // update header data wit htotals and taxes
                                DoLogging("100", "100", "Update Invoce header total value", false, "InvoiceToFinance");
                                using (SqlConnection tconn = new SqlConnection(sourceConnectionString))
                                {
                                    SqlCommand Totalscmd = new SqlCommand(CalsHeaderDataSQL, tconn);
                                    tconn.Open();
                                    Totalscmd.Parameters.AddWithValue("ID", ID);
                                    Totalscmd.ExecuteNonQuery();
                                }
                                #endregion

                                using (SqlConnection dconn = new SqlConnection(destConnectionString))
                                {
                                    SqlCommand Header2cmd = new SqlCommand(FinishGetOrder, dconn);
                                    if (dconn.State == ConnectionState.Closed)
                                    {
                                        dconn.Open();
                                    }
                                    Header2cmd.Parameters.AddWithValue("ID", ID);
                                    Header2cmd.ExecuteNonQuery();
                                }
                                //// Deleye header after copy 
                                //DoLogging("100", ID.ToString(), "Delete Invoce header From POS");
                                //using (SqlConnection Delconn = new SqlConnection(sourceConnectionString))
                                //{
                                //    SqlCommand Totalscmd = new SqlCommand(DeleteDoneHeaderSQL, Delconn);
                                //    Delconn.Open();
                                //    Totalscmd.Parameters.AddWithValue("ID", ID);
                                //    Totalscmd.ExecuteNonQuery();
                                //}
                                //  DestConn.Close();

                            }
                            #endregion
                        }
                        //  conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("Error", "-600", err.Message, false, "GetDeliveryFromFinance");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }
        public static bool CleanInvoiceData(string destConnectionString)
        {


            try
            {

                #region empty old data first
                string SQLEmpty = "Delete From TansactionDetail";
                using (SqlConnection DelConn = new SqlConnection(destConnectionString))
                {
                    DelConn.Open();
                    using (SqlCommand destcmd = new SqlCommand(SQLEmpty, DelConn))
                    {
                        DoLogging("empty Invoice header", "100", "empty Invoice ", false, "CleanInvoice");
                        destcmd.ExecuteNonQuery();
                    }
                    DelConn.Close();
                    DelConn.Open();
                    SQLEmpty = "Delete From TransactionTable";
                    using (SqlCommand destcmd = new SqlCommand(SQLEmpty, DelConn))
                    {
                        DoLogging("empty Invoice header", "100", "empty Invoice ", false, "CleanInvoice");
                        destcmd.ExecuteNonQuery();
                    }


                    //  DelConn.Close();
                }

                #endregion
            }
            catch (Exception err)
            {
                DoLogging("Error", "-300", err.Message, false, "CleanInvoice");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool CleanReturnInvoiceData(string destConnectionString)
        {
            try
            {

                #region empty old data first
                string SQLEmpty = "Delete From ReturnTransactionD";
                using (SqlConnection DelConn = new SqlConnection(destConnectionString))
                {
                    DelConn.Open();
                    using (SqlCommand destcmd = new SqlCommand(SQLEmpty, DelConn))
                    {
                        destcmd.ExecuteNonQuery();
                    }
                    DelConn.Close();
                    DelConn.Open();
                    SQLEmpty = "Delete From ReturnTransactionH";
                    using (SqlCommand destcmd = new SqlCommand(SQLEmpty, DelConn))
                    {
                        destcmd.ExecuteNonQuery();
                    }


                    //  DelConn.Close();
                }

                #endregion
            }
            catch (Exception err)
            {
                DoLogging("clear Return", "-200", err.Message, false, "CleanInvoice");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool GetSalesRepsDataWOverwrite(string sourceConnectionString, string destConnectionString, string BranchDefault)
        {
            string SQL = "SELECT ID ,IsNull( Name, '') AS   Name  ,IsNull( Address1, '') AS  Address1   ,IsNull( Address2, '') AS Address2 ,IsNull( City, '') AS City " +
             " ,IsNull( Email, '') AS Email ,IsNull( Telephone1, '') AS Telephone1 , IsNull(PostalCode , 'P@ssw0rd') PostalCode , IsNull(Fax ,'') Fax" +
             " FROM  dbo.arSalesRep Where City=" + BranchDefault;

            //string SQLInsert = "INSERT INTO [Users] (ID ,[FirstName] ,[UserName],[uPassword] ,[LastLogin],[Active] ,[EmailAdress] ,[City] ,[Telephone] ,[Address1] ,[Address2] )" +
            // " Values (@ID ,@FirstName ,@UserName,@uPassword ,@LastLogin,@Active ,@EmailAdress ,@City ,@Telephone ,@Address1 ,@Address2 )";


            try
            {

                #region empty old data first
                //string SQLEmpty = "Delete From Users";
                //using (SqlConnection DelConn = new SqlConnection(destConnectionString))
                //{
                //    DelConn.Open();
                //    using (SqlCommand destcmd = new SqlCommand())
                //    {
                //        destcmd.CommandText = SQLEmpty;
                //        destcmd.Connection = DelConn;
                //        destcmd.ExecuteNonQuery();
                //    }
                //    DelConn.Close();
                //}

                #endregion

                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(SQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row

                            Int16 ID = (Int16)rdr["ID"];

                            string Name = (string)rdr["Name"];

                            string EmailAddress = (string)rdr["Email"];
                            string PhoneNumber = (string)rdr["Telephone1"];

                            string Address1 = (string)rdr["Address1"];
                            string Address2 = (string)rdr["Address2"];
                            string City = (string)rdr["City"];
                            string Fax = (string)rdr["Fax"];
                            string PostalCode = (string)rdr["PostalCode"];

                            DoLogging("Get User Data", "100", ID.ToString(), false, "Users");
                            #endregion
                            #region apply to destination
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();
                                using (SqlCommand destcmd = new SqlCommand())
                                {
                                    destcmd.CommandText = "usp_UsersImport";
                                    destcmd.Connection = DestConn;
                                    destcmd.CommandType = CommandType.StoredProcedure;


                                    destcmd.Parameters.AddWithValue("ID", ID);
                                    destcmd.Parameters.AddWithValue("FirstName", Name);
                                    destcmd.Parameters.AddWithValue("LastName", " ");
                                    destcmd.Parameters.AddWithValue("UserName", Address2);
                                    destcmd.Parameters.AddWithValue("Password", PostalCode);
                                    destcmd.Parameters.AddWithValue("Active", true);
                                    destcmd.Parameters.AddWithValue("RoleID", Fax);
                                    //destcmd.Parameters.AddWithValue("PostalCode", PostalCode);
                                    //destcmd.Parameters.AddWithValue("Fax", Fax);

                                    DoLogging("Add User Data", "100", ID.ToString(), false, "Users");
                                    destcmd.ExecuteNonQuery();
                                }
                                // DestConn.Close();

                            }
                            #endregion
                        }
                        // conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("Error", "-100", err.Message, false, "Users");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool GetSalesRepsData(string sourceConnectionString, string destConnectionString, string BranchDefault)
        {
            string SQL = "SELECT ID ,IsNull( Name, '') AS   Name  ,IsNull( Address1, '') AS  Address1   ,IsNull( Address2, '') AS Address2 ,IsNull( City, '') AS City " +
             " ,IsNull( Email, '') AS Email ,IsNull( Telephone1, '') AS Telephone1 , IsNull(PostalCode , 'P@ssw0rd') PostalCode , IsNull(Fax ,'') Fax" +
             " FROM  dbo.arSalesRep Where City=" + BranchDefault;

            //string SQLInsert = "INSERT INTO [Users] (ID ,[FirstName] ,[UserName],[uPassword] ,[LastLogin],[Active] ,[EmailAdress] ,[City] ,[Telephone] ,[Address1] ,[Address2] )" +
            // " Values (@ID ,@FirstName ,@UserName,@uPassword ,@LastLogin,@Active ,@EmailAdress ,@City ,@Telephone ,@Address1 ,@Address2 )";


            try
            {

                #region empty old data first
                //string SQLEmpty = "Delete From Users";
                //using (SqlConnection DelConn = new SqlConnection(destConnectionString))
                //{
                //    DelConn.Open();
                //    using (SqlCommand destcmd = new SqlCommand())
                //    {
                //        destcmd.CommandText = SQLEmpty;
                //        destcmd.Connection = DelConn;
                //        destcmd.ExecuteNonQuery();
                //    }
                //    DelConn.Close();
                //}

                #endregion

                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand cmd = new SqlCommand(SQL, conn);
                    conn.Open();
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row

                            Int16 ID = (Int16)rdr["ID"];

                            string Name = (string)rdr["Name"];

                            string EmailAddress = (string)rdr["Email"];
                            string PhoneNumber = (string)rdr["Telephone1"];

                            string Address1 = (string)rdr["Address1"];
                            string Address2 = (string)rdr["Address2"];
                            string City = (string)rdr["City"];
                            string Fax = (string)rdr["Fax"];
                            string PostalCode = (string)rdr["PostalCode"];

                            DoLogging("Get User Data", "100", ID.ToString(), false, "Users");
                            #endregion
                            #region apply to destination
                            using (SqlConnection DestConn = new SqlConnection(destConnectionString))
                            {
                                DestConn.Open();
                                using (SqlCommand destucmd = new SqlCommand())
                                {
                                    destucmd.CommandText = "Select * from [dbo].[Users] where [ID] =  @ID";
                                    destucmd.Connection = DestConn;
                                    destucmd.CommandType = CommandType.Text;
                                    destucmd.Parameters.AddWithValue("ID", ID);
                                    SqlDataReader Erdr = destucmd.ExecuteReader();
                                    if (!Erdr.HasRows)
                                    {
                                        Erdr.Close();
                                        using (SqlCommand destcmd = new SqlCommand())
                                        {
                                            destcmd.CommandText = "usp_UsersImport";
                                            destcmd.Connection = DestConn;
                                            destcmd.CommandType = CommandType.StoredProcedure;


                                            destcmd.Parameters.AddWithValue("ID", ID);
                                            destcmd.Parameters.AddWithValue("FirstName", Name);
                                            destcmd.Parameters.AddWithValue("LastName", " ");
                                            destcmd.Parameters.AddWithValue("UserName", Address2);
                                            destcmd.Parameters.AddWithValue("Password", PostalCode);
                                            destcmd.Parameters.AddWithValue("Active", true);
                                            destcmd.Parameters.AddWithValue("RoleID", Fax);
                                            //destcmd.Parameters.AddWithValue("PostalCode", PostalCode);
                                            //destcmd.Parameters.AddWithValue("Fax", Fax);

                                            DoLogging("Add User Data", "100", ID.ToString(), false, "Users");
                                            destcmd.ExecuteNonQuery();
                                        }
                                    }
                                }
                                // DestConn.Close();

                            }
                            #endregion
                        }
                        // conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("Error", "-100", err.Message, false, "Users");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool ResyncSalesInvoice(string sourceConnectionString)
        {
            string HeaderSQL = " SELECT  arSalesInvH.ID FROM   arSalesInvH  left Join ivInvOutH on arSalesInvH.ID = ivInvOutH.InvoiceSerial  where ivInvOutH.InvoiceSerial is null ORDER BY  1";

            try
            {
                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(HeaderSQL, conn);

                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            Int32 ID = (Int32)rdr["ID"];


                            DoLogging(ID.ToString(), "100", "Read Sales Invoice Header" + ID.ToString(), false, "SyncSalesinv");
                            #endregion
                            #region apply to destination header

                            {

                                using (SqlConnection DestConn = new SqlConnection(sourceConnectionString))
                                {
                                    DestConn.Open();

                                    #region send data t ocreate header in finaance system
                                    using (SqlCommand destcmd = new SqlCommand())
                                    {

                                        destcmd.CommandText = "usp_arSalesInvToMultiInvOut";
                                        destcmd.Connection = DestConn;
                                        destcmd.CommandType = CommandType.StoredProcedure;

                                        destcmd.Parameters.AddWithValue("InvoiceID", ID.ToString());
                                        destcmd.Parameters.AddWithValue("UserName", "System");
                                        destcmd.Parameters.AddWithValue("Source", "Import");
                                        destcmd.Parameters.AddWithValue("Subsys", "POS");

                                        destcmd.ExecuteNonQuery();

                                        DoLogging(ID.ToString(), "100", "Multi Inv In for" + ID.ToString(), false, "SyncSalesinv");
                                    }

                                    #endregion
                                }
                            }
                            #endregion
                        }
                        //  conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("Error", "-600", err.Message, false, "SyncSalesinv");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool ResyncSalesRInvoice(string sourceConnectionString)
        {
            string HeaderSQL = " SELECT  arSalesRInvH.ID FROM   arSalesRInvH  left Join ivInvInH on arSalesRInvH.ID = ivInvInH.InvoiceSerial  where ivInvInH.InvoiceSerial is null ORDER BY  1";

            try
            {
                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(HeaderSQL, conn);

                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            Int32 ID = (Int32)rdr["ID"];


                            DoLogging(ID.ToString(), "100", "Read Sales return Invoice Header" + ID.ToString(), false, "SyncSalesRinv");
                            #endregion
                            #region apply to destination header

                            {

                                using (SqlConnection DestConn = new SqlConnection(sourceConnectionString))
                                {
                                    DestConn.Open();

                                    #region send data t ocreate header in finaance system
                                    using (SqlCommand destcmd = new SqlCommand())
                                    {

                                        destcmd.CommandText = "usp_arSalesInvRToMultiInvIn";
                                        destcmd.Connection = DestConn;
                                        destcmd.CommandType = CommandType.StoredProcedure;

                                        destcmd.Parameters.AddWithValue("InvoiceID", ID.ToString());
                                        destcmd.Parameters.AddWithValue("UserName", "System");
                                        destcmd.Parameters.AddWithValue("Source", "Import");
                                        destcmd.Parameters.AddWithValue("Subsys", "POS");

                                        destcmd.ExecuteNonQuery();

                                        DoLogging(ID.ToString(), "100", "Multi InvR In for" + ID.ToString(), false, "SyncSalesRinv");
                                    }

                                    #endregion
                                }
                            }
                            #endregion
                        }
                        //  conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("Error", "-600", err.Message, false, "SyncSalesRinv");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool ResyncPurchaseInvoice(string sourceConnectionString)
        {
            string HeaderSQL = " SELECT  apPurchInvH.ID FROM   apPurchInvH  left Join ivInvInH on apPurchInvH.ID = ivInvInH.InvoiceSerial  where ivInvInH.InvoiceSerial is null ORDER BY  1";

            try
            {
                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(HeaderSQL, conn);

                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            Int32 ID = (Int32)rdr["ID"];


                            DoLogging(ID.ToString(), "100", "Read Purchase Invoice Header" + ID.ToString(), false, "SyncPurchinv");
                            #endregion
                            #region apply to destination header

                            {

                                using (SqlConnection DestConn = new SqlConnection(sourceConnectionString))
                                {
                                    DestConn.Open();

                                    #region send data t ocreate header in finaance system
                                    using (SqlCommand destcmd = new SqlCommand())
                                    {

                                        destcmd.CommandText = "usp_apPurchInvToMultiInvIN";
                                        destcmd.Connection = DestConn;
                                        destcmd.CommandType = CommandType.StoredProcedure;

                                        destcmd.Parameters.AddWithValue("InvoiceID", ID.ToString());
                                        destcmd.Parameters.AddWithValue("UserName", "System");
                                        destcmd.Parameters.AddWithValue("Source", "Import");
                                        destcmd.Parameters.AddWithValue("Subsys", "POS");

                                        destcmd.ExecuteNonQuery();

                                        DoLogging(ID.ToString(), "100", "Multi Purch In for" + ID.ToString(), false, "SyncPurchinv");
                                    }

                                    #endregion
                                }
                            }
                            #endregion
                        }
                        //  conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("Error", "-700", err.Message, false, "SyncPurchinv");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public static bool ResyncPurchaseRInvoice(string sourceConnectionString)
        {
            string HeaderSQL = " SELECT  apPurchRInvH.ID FROM   apPurchRInvH  left Join ivInvInH on apPurchRInvH.ID = ivInvInH.InvoiceSerial  where ivInvInH.InvoiceSerial is null ORDER BY  1";

            try
            {
                using (SqlConnection conn = new SqlConnection(sourceConnectionString))
                {
                    SqlCommand Headercmd = new SqlCommand(HeaderSQL, conn);

                    conn.Open();
                    using (SqlDataReader rdr = Headercmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            #region get data from one row
                            Int32 ID = (Int32)rdr["ID"];


                            DoLogging(ID.ToString(), "100", "Read Purchase ret Invoice Header" + ID.ToString(), false, "SyncPurchRinv");
                            #endregion
                            #region apply to destination header

                            {

                                using (SqlConnection DestConn = new SqlConnection(sourceConnectionString))
                                {
                                    DestConn.Open();

                                    #region send data t ocreate header in finaance system
                                    using (SqlCommand destcmd = new SqlCommand())
                                    {

                                        destcmd.CommandText = "usp_apPurchInvRToMultiInvOut";
                                        destcmd.Connection = DestConn;
                                        destcmd.CommandType = CommandType.StoredProcedure;

                                        destcmd.Parameters.AddWithValue("InvoiceID", ID.ToString());
                                        destcmd.Parameters.AddWithValue("UserName", "System");
                                        destcmd.Parameters.AddWithValue("Source", "Import");
                                        destcmd.Parameters.AddWithValue("Subsys", "POS");

                                        destcmd.ExecuteNonQuery();

                                        DoLogging(ID.ToString(), "100", "Multi Purch ret In for" + ID.ToString(), false, "SyncPurchRinv");
                                    }

                                    #endregion
                                }
                            }
                            #endregion
                        }
                        //  conn.Close();

                    }
                }
            }
            catch (Exception err)
            {
                DoLogging("Error", "-700", err.Message, false, "SyncPurchRinv");
                //MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

    }
}

