﻿namespace POSImportExport
{
    partial class MainRun
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainRun));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnSendOrder = new System.Windows.Forms.Button();
            this.btnDeliveryWeb = new System.Windows.Forms.Button();
            this.btnSendInvRetHistory = new System.Windows.Forms.Button();
            this.btnSendInvHistory = new System.Windows.Forms.Button();
            this.btnPurchInvRsync = new System.Windows.Forms.Button();
            this.btnPurchInvSync = new System.Windows.Forms.Button();
            this.btnSalesReturnSync = new System.Windows.Forms.Button();
            this.btnSalesResymc = new System.Windows.Forms.Button();
            this.btnGetItemPrice = new System.Windows.Forms.Button();
            this.btnGetUsers = new System.Windows.Forms.Button();
            this.btnDelReturnInv = new System.Windows.Forms.Button();
            this.btnBackupFinance = new System.Windows.Forms.Button();
            this.BtnBackupPOS = new System.Windows.Forms.Button();
            this.btnSendInvoiceReturn = new System.Windows.Forms.Button();
            this.btnDelInvoices = new System.Windows.Forms.Button();
            this.btnGetItems = new System.Windows.Forms.Button();
            this.btnGetSupplier = new System.Windows.Forms.Button();
            this.btnSendCust = new System.Windows.Forms.Button();
            this.btnSendInvoice = new System.Windows.Forms.Button();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.chkItemDesc = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.pnlSettings = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.cboBranch = new System.Windows.Forms.ComboBox();
            this.lblcboWareHouse = new System.Windows.Forms.Label();
            this.cboWareHouse = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlServer2 = new System.Windows.Forms.Panel();
            this.cmdRefreshServer2 = new System.Windows.Forms.Button();
            this.lblDatabase2 = new System.Windows.Forms.Label();
            this.cboPOSDB = new System.Windows.Forms.ComboBox();
            this.lblPassword2 = new System.Windows.Forms.Label();
            this.lblUserName2 = new System.Windows.Forms.Label();
            this.txtPassword2 = new System.Windows.Forms.TextBox();
            this.txtUser2 = new System.Windows.Forms.TextBox();
            this.rbSQLServerAuthentication2 = new System.Windows.Forms.RadioButton();
            this.rbWindowsAuthentication2 = new System.Windows.Forms.RadioButton();
            this.cboPOS = new System.Windows.Forms.ComboBox();
            this.lblServer2 = new System.Windows.Forms.Label();
            this.pnlServer1 = new System.Windows.Forms.Panel();
            this.cmdRefreshServer1 = new System.Windows.Forms.Button();
            this.lblDatabase1 = new System.Windows.Forms.Label();
            this.cboFinanceDB = new System.Windows.Forms.ComboBox();
            this.lblPassword1 = new System.Windows.Forms.Label();
            this.lblUserName1 = new System.Windows.Forms.Label();
            this.txtPassword1 = new System.Windows.Forms.TextBox();
            this.txtUser1 = new System.Windows.Forms.TextBox();
            this.rbSQLServerAuthentication1 = new System.Windows.Forms.RadioButton();
            this.rbWindowsAuthentication1 = new System.Windows.Forms.RadioButton();
            this.cboFinance = new System.Windows.Forms.ComboBox();
            this.lblServer1 = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.chkProgress = new System.Windows.Forms.CheckedListBox();
            this.ilIcons = new System.Windows.Forms.ImageList(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.pnlSettings.SuspendLayout();
            this.pnlServer2.SuspendLayout();
            this.pnlServer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.btnSendOrder);
            this.splitContainer1.Panel1.Controls.Add(this.btnDeliveryWeb);
            this.splitContainer1.Panel1.Controls.Add(this.btnSendInvRetHistory);
            this.splitContainer1.Panel1.Controls.Add(this.btnSendInvHistory);
            this.splitContainer1.Panel1.Controls.Add(this.btnPurchInvRsync);
            this.splitContainer1.Panel1.Controls.Add(this.btnPurchInvSync);
            this.splitContainer1.Panel1.Controls.Add(this.btnSalesReturnSync);
            this.splitContainer1.Panel1.Controls.Add(this.btnSalesResymc);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetItemPrice);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetUsers);
            this.splitContainer1.Panel1.Controls.Add(this.btnDelReturnInv);
            this.splitContainer1.Panel1.Controls.Add(this.btnBackupFinance);
            this.splitContainer1.Panel1.Controls.Add(this.BtnBackupPOS);
            this.splitContainer1.Panel1.Controls.Add(this.btnSendInvoiceReturn);
            this.splitContainer1.Panel1.Controls.Add(this.btnDelInvoices);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetItems);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetSupplier);
            this.splitContainer1.Panel1.Controls.Add(this.btnSendCust);
            this.splitContainer1.Panel1.Controls.Add(this.btnSendInvoice);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(1030, 485);
            this.splitContainer1.SplitterDistance = 258;
            this.splitContainer1.TabIndex = 0;
            // 
            // btnSendOrder
            // 
            this.btnSendOrder.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnSendOrder.Location = new System.Drawing.Point(10, 511);
            this.btnSendOrder.Name = "btnSendOrder";
            this.btnSendOrder.Size = new System.Drawing.Size(203, 23);
            this.btnSendOrder.TabIndex = 18;
            this.btnSendOrder.Text = "17.  Send Delivery To Web";
            this.btnSendOrder.UseVisualStyleBackColor = true;
            this.btnSendOrder.Visible = false;
            this.btnSendOrder.Click += new System.EventHandler(this.btnSendOrder_Click);
            // 
            // btnDeliveryWeb
            // 
            this.btnDeliveryWeb.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnDeliveryWeb.Location = new System.Drawing.Point(10, 482);
            this.btnDeliveryWeb.Name = "btnDeliveryWeb";
            this.btnDeliveryWeb.Size = new System.Drawing.Size(203, 23);
            this.btnDeliveryWeb.TabIndex = 17;
            this.btnDeliveryWeb.Text = "16.  Get Delivery From Web";
            this.btnDeliveryWeb.UseVisualStyleBackColor = true;
            this.btnDeliveryWeb.Visible = false;
            this.btnDeliveryWeb.Click += new System.EventHandler(this.btnDeliveryWeb_Click);
            // 
            // btnSendInvRetHistory
            // 
            this.btnSendInvRetHistory.Location = new System.Drawing.Point(12, 238);
            this.btnSendInvRetHistory.Name = "btnSendInvRetHistory";
            this.btnSendInvRetHistory.Size = new System.Drawing.Size(203, 23);
            this.btnSendInvRetHistory.TabIndex = 16;
            this.btnSendInvRetHistory.Text = "71.Send History Return to back office";
            this.btnSendInvRetHistory.UseVisualStyleBackColor = true;
            this.btnSendInvRetHistory.Visible = false;
            this.btnSendInvRetHistory.Click += new System.EventHandler(this.btnSendInvRetHistory_Click);
            // 
            // btnSendInvHistory
            // 
            this.btnSendInvHistory.Location = new System.Drawing.Point(12, 180);
            this.btnSendInvHistory.Name = "btnSendInvHistory";
            this.btnSendInvHistory.Size = new System.Drawing.Size(204, 23);
            this.btnSendInvHistory.TabIndex = 15;
            this.btnSendInvHistory.Text = "61.Send Invoice History to Back office";
            this.btnSendInvHistory.UseVisualStyleBackColor = true;
            this.btnSendInvHistory.Visible = false;
            this.btnSendInvHistory.Click += new System.EventHandler(this.btnSendInvHistory_Click);
            // 
            // btnPurchInvRsync
            // 
            this.btnPurchInvRsync.Location = new System.Drawing.Point(10, 455);
            this.btnPurchInvRsync.Name = "btnPurchInvRsync";
            this.btnPurchInvRsync.Size = new System.Drawing.Size(203, 23);
            this.btnPurchInvRsync.TabIndex = 14;
            this.btnPurchInvRsync.Text = "15.  Purchase Ret. Stock Out";
            this.btnPurchInvRsync.UseVisualStyleBackColor = true;
            this.btnPurchInvRsync.Visible = false;
            this.btnPurchInvRsync.Click += new System.EventHandler(this.btnPurchInvRsync_Click);
            // 
            // btnPurchInvSync
            // 
            this.btnPurchInvSync.Location = new System.Drawing.Point(10, 426);
            this.btnPurchInvSync.Name = "btnPurchInvSync";
            this.btnPurchInvSync.Size = new System.Drawing.Size(203, 23);
            this.btnPurchInvSync.TabIndex = 13;
            this.btnPurchInvSync.Text = "14.Purch Inv to Stock In";
            this.btnPurchInvSync.UseVisualStyleBackColor = true;
            this.btnPurchInvSync.Visible = false;
            this.btnPurchInvSync.Click += new System.EventHandler(this.btnPurchInvSync_Click);
            // 
            // btnSalesReturnSync
            // 
            this.btnSalesReturnSync.Location = new System.Drawing.Point(10, 402);
            this.btnSalesReturnSync.Name = "btnSalesReturnSync";
            this.btnSalesReturnSync.Size = new System.Drawing.Size(203, 23);
            this.btnSalesReturnSync.TabIndex = 12;
            this.btnSalesReturnSync.Text = "13.Send Sales Ret. Stock In";
            this.btnSalesReturnSync.UseVisualStyleBackColor = true;
            this.btnSalesReturnSync.Visible = false;
            this.btnSalesReturnSync.Click += new System.EventHandler(this.btnSalesReturnSync_Click);
            // 
            // btnSalesResymc
            // 
            this.btnSalesResymc.Location = new System.Drawing.Point(10, 373);
            this.btnSalesResymc.Name = "btnSalesResymc";
            this.btnSalesResymc.Size = new System.Drawing.Size(203, 23);
            this.btnSalesResymc.TabIndex = 11;
            this.btnSalesResymc.Text = "12.Send Sales to Stock Out";
            this.btnSalesResymc.UseVisualStyleBackColor = true;
            this.btnSalesResymc.Visible = false;
            this.btnSalesResymc.Click += new System.EventHandler(this.btnSalesResymc_Click);
            // 
            // btnGetItemPrice
            // 
            this.btnGetItemPrice.Location = new System.Drawing.Point(12, 293);
            this.btnGetItemPrice.Name = "btnGetItemPrice";
            this.btnGetItemPrice.Size = new System.Drawing.Size(203, 23);
            this.btnGetItemPrice.TabIndex = 10;
            this.btnGetItemPrice.Text = "9.Get Items with price";
            this.btnGetItemPrice.UseVisualStyleBackColor = true;
            this.btnGetItemPrice.Click += new System.EventHandler(this.btnGetItemPrice_Click);
            // 
            // btnGetUsers
            // 
            this.btnGetUsers.Location = new System.Drawing.Point(11, 125);
            this.btnGetUsers.Name = "btnGetUsers";
            this.btnGetUsers.Size = new System.Drawing.Size(203, 23);
            this.btnGetUsers.TabIndex = 9;
            this.btnGetUsers.Text = "5.Get Users to POS";
            this.btnGetUsers.UseVisualStyleBackColor = true;
            this.btnGetUsers.Click += new System.EventHandler(this.btnGetUsers_Click);
            // 
            // btnDelReturnInv
            // 
            this.btnDelReturnInv.Location = new System.Drawing.Point(12, 344);
            this.btnDelReturnInv.Name = "btnDelReturnInv";
            this.btnDelReturnInv.Size = new System.Drawing.Size(203, 23);
            this.btnDelReturnInv.TabIndex = 8;
            this.btnDelReturnInv.Text = "11.Delete POS Return Invoice";
            this.btnDelReturnInv.UseVisualStyleBackColor = true;
            this.btnDelReturnInv.Click += new System.EventHandler(this.btnDelReturnInv_Click);
            // 
            // btnBackupFinance
            // 
            this.btnBackupFinance.Location = new System.Drawing.Point(11, 3);
            this.btnBackupFinance.Name = "btnBackupFinance";
            this.btnBackupFinance.Size = new System.Drawing.Size(203, 23);
            this.btnBackupFinance.TabIndex = 7;
            this.btnBackupFinance.Text = "1.Backup Back office";
            this.btnBackupFinance.UseVisualStyleBackColor = true;
            this.btnBackupFinance.Click += new System.EventHandler(this.btnBackupFinance_Click);
            // 
            // BtnBackupPOS
            // 
            this.BtnBackupPOS.Location = new System.Drawing.Point(11, 32);
            this.BtnBackupPOS.Name = "BtnBackupPOS";
            this.BtnBackupPOS.Size = new System.Drawing.Size(203, 23);
            this.BtnBackupPOS.TabIndex = 6;
            this.BtnBackupPOS.Text = "2.Backup POS";
            this.BtnBackupPOS.UseVisualStyleBackColor = true;
            this.BtnBackupPOS.Click += new System.EventHandler(this.BtnBackupPOS_Click);
            // 
            // btnSendInvoiceReturn
            // 
            this.btnSendInvoiceReturn.Location = new System.Drawing.Point(12, 209);
            this.btnSendInvoiceReturn.Name = "btnSendInvoiceReturn";
            this.btnSendInvoiceReturn.Size = new System.Drawing.Size(203, 23);
            this.btnSendInvoiceReturn.TabIndex = 5;
            this.btnSendInvoiceReturn.Text = "7.Send Invoice Return to back office";
            this.btnSendInvoiceReturn.UseVisualStyleBackColor = true;
            this.btnSendInvoiceReturn.Click += new System.EventHandler(this.btnSendInvoiceReturn_Click);
            // 
            // btnDelInvoices
            // 
            this.btnDelInvoices.Location = new System.Drawing.Point(12, 320);
            this.btnDelInvoices.Name = "btnDelInvoices";
            this.btnDelInvoices.Size = new System.Drawing.Size(203, 23);
            this.btnDelInvoices.TabIndex = 4;
            this.btnDelInvoices.Text = "10.Delete POS Invoices";
            this.btnDelInvoices.UseVisualStyleBackColor = true;
            this.btnDelInvoices.Click += new System.EventHandler(this.btnDelInvoices_Click);
            // 
            // btnGetItems
            // 
            this.btnGetItems.Location = new System.Drawing.Point(12, 269);
            this.btnGetItems.Name = "btnGetItems";
            this.btnGetItems.Size = new System.Drawing.Size(203, 23);
            this.btnGetItems.TabIndex = 3;
            this.btnGetItems.Text = "8.Get Items";
            this.btnGetItems.UseVisualStyleBackColor = true;
            this.btnGetItems.Click += new System.EventHandler(this.btnGetItems_Click);
            // 
            // btnGetSupplier
            // 
            this.btnGetSupplier.Location = new System.Drawing.Point(10, 94);
            this.btnGetSupplier.Name = "btnGetSupplier";
            this.btnGetSupplier.Size = new System.Drawing.Size(203, 23);
            this.btnGetSupplier.TabIndex = 2;
            this.btnGetSupplier.Text = "4.Get Supplier to POS";
            this.btnGetSupplier.UseVisualStyleBackColor = true;
            this.btnGetSupplier.Click += new System.EventHandler(this.btnGetSupplier_Click);
            // 
            // btnSendCust
            // 
            this.btnSendCust.Location = new System.Drawing.Point(12, 61);
            this.btnSendCust.Name = "btnSendCust";
            this.btnSendCust.Size = new System.Drawing.Size(204, 23);
            this.btnSendCust.TabIndex = 1;
            this.btnSendCust.Text = "3.Send Customers to Back office";
            this.btnSendCust.UseVisualStyleBackColor = true;
            this.btnSendCust.Click += new System.EventHandler(this.btnSendCust_Click);
            // 
            // btnSendInvoice
            // 
            this.btnSendInvoice.Location = new System.Drawing.Point(12, 151);
            this.btnSendInvoice.Name = "btnSendInvoice";
            this.btnSendInvoice.Size = new System.Drawing.Size(204, 23);
            this.btnSendInvoice.TabIndex = 0;
            this.btnSendInvoice.Text = "6.Send Invoice to Back office";
            this.btnSendInvoice.UseVisualStyleBackColor = true;
            this.btnSendInvoice.Click += new System.EventHandler(this.btnSendInvoice_Click);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.chkItemDesc);
            this.splitContainer2.Panel1.Controls.Add(this.btnSave);
            this.splitContainer2.Panel1.Controls.Add(this.pnlSettings);
            this.splitContainer2.Panel1.Controls.Add(this.btnLogin);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.progressBar1);
            this.splitContainer2.Panel2.Controls.Add(this.chkProgress);
            this.splitContainer2.Size = new System.Drawing.Size(764, 481);
            this.splitContainer2.SplitterDistance = 241;
            this.splitContainer2.TabIndex = 0;
            // 
            // chkItemDesc
            // 
            this.chkItemDesc.AutoSize = true;
            this.chkItemDesc.Location = new System.Drawing.Point(18, 221);
            this.chkItemDesc.Name = "chkItemDesc";
            this.chkItemDesc.Size = new System.Drawing.Size(187, 17);
            this.chkItemDesc.TabIndex = 5;
            this.chkItemDesc.Text = "Switch Item name and Description";
            this.chkItemDesc.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.AutoSize = true;
            this.btnSave.Location = new System.Drawing.Point(675, 193);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(83, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "Save Settings";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pnlSettings
            // 
            this.pnlSettings.Controls.Add(this.label3);
            this.pnlSettings.Controls.Add(this.cboBranch);
            this.pnlSettings.Controls.Add(this.lblcboWareHouse);
            this.pnlSettings.Controls.Add(this.cboWareHouse);
            this.pnlSettings.Controls.Add(this.label2);
            this.pnlSettings.Controls.Add(this.label1);
            this.pnlSettings.Controls.Add(this.pnlServer2);
            this.pnlSettings.Controls.Add(this.pnlServer1);
            this.pnlSettings.Location = new System.Drawing.Point(3, 3);
            this.pnlSettings.Name = "pnlSettings";
            this.pnlSettings.Size = new System.Drawing.Size(671, 216);
            this.pnlSettings.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(335, 198);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Market Branch";
            // 
            // cboBranch
            // 
            this.cboBranch.FormattingEnabled = true;
            this.cboBranch.Location = new System.Drawing.Point(437, 190);
            this.cboBranch.Name = "cboBranch";
            this.cboBranch.Size = new System.Drawing.Size(229, 21);
            this.cboBranch.TabIndex = 7;
            this.cboBranch.Visible = false;
            // 
            // lblcboWareHouse
            // 
            this.lblcboWareHouse.AutoSize = true;
            this.lblcboWareHouse.Location = new System.Drawing.Point(12, 198);
            this.lblcboWareHouse.Name = "lblcboWareHouse";
            this.lblcboWareHouse.Size = new System.Drawing.Size(100, 13);
            this.lblcboWareHouse.TabIndex = 6;
            this.lblcboWareHouse.Text = "Market WareHouse";
            // 
            // cboWareHouse
            // 
            this.cboWareHouse.FormattingEnabled = true;
            this.cboWareHouse.Location = new System.Drawing.Point(114, 190);
            this.cboWareHouse.Name = "cboWareHouse";
            this.cboWareHouse.Size = new System.Drawing.Size(215, 21);
            this.cboWareHouse.TabIndex = 5;
            this.cboWareHouse.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(335, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "POS Server  Data";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Back office Server  Data";
            // 
            // pnlServer2
            // 
            this.pnlServer2.Controls.Add(this.cmdRefreshServer2);
            this.pnlServer2.Controls.Add(this.lblDatabase2);
            this.pnlServer2.Controls.Add(this.cboPOSDB);
            this.pnlServer2.Controls.Add(this.lblPassword2);
            this.pnlServer2.Controls.Add(this.lblUserName2);
            this.pnlServer2.Controls.Add(this.txtPassword2);
            this.pnlServer2.Controls.Add(this.txtUser2);
            this.pnlServer2.Controls.Add(this.rbSQLServerAuthentication2);
            this.pnlServer2.Controls.Add(this.rbWindowsAuthentication2);
            this.pnlServer2.Controls.Add(this.cboPOS);
            this.pnlServer2.Controls.Add(this.lblServer2);
            this.pnlServer2.Location = new System.Drawing.Point(338, 20);
            this.pnlServer2.Name = "pnlServer2";
            this.pnlServer2.Size = new System.Drawing.Size(317, 171);
            this.pnlServer2.TabIndex = 2;
            // 
            // cmdRefreshServer2
            // 
            this.cmdRefreshServer2.Location = new System.Drawing.Point(285, 3);
            this.cmdRefreshServer2.Name = "cmdRefreshServer2";
            this.cmdRefreshServer2.Size = new System.Drawing.Size(21, 23);
            this.cmdRefreshServer2.TabIndex = 11;
            this.cmdRefreshServer2.UseVisualStyleBackColor = true;
            // 
            // lblDatabase2
            // 
            this.lblDatabase2.AutoSize = true;
            this.lblDatabase2.Location = new System.Drawing.Point(5, 133);
            this.lblDatabase2.Name = "lblDatabase2";
            this.lblDatabase2.Size = new System.Drawing.Size(56, 13);
            this.lblDatabase2.TabIndex = 9;
            this.lblDatabase2.Text = "Database:";
            // 
            // cboPOSDB
            // 
            this.cboPOSDB.FormattingEnabled = true;
            this.cboPOSDB.Location = new System.Drawing.Point(63, 130);
            this.cboPOSDB.Name = "cboPOSDB";
            this.cboPOSDB.Size = new System.Drawing.Size(243, 21);
            this.cboPOSDB.TabIndex = 8;
            this.cboPOSDB.Click += new System.EventHandler(this.cboPOSDB_Click);
            // 
            // lblPassword2
            // 
            this.lblPassword2.AutoSize = true;
            this.lblPassword2.Location = new System.Drawing.Point(96, 107);
            this.lblPassword2.Name = "lblPassword2";
            this.lblPassword2.Size = new System.Drawing.Size(56, 13);
            this.lblPassword2.TabIndex = 7;
            this.lblPassword2.Text = "Password:";
            // 
            // lblUserName2
            // 
            this.lblUserName2.AutoSize = true;
            this.lblUserName2.Location = new System.Drawing.Point(89, 81);
            this.lblUserName2.Name = "lblUserName2";
            this.lblUserName2.Size = new System.Drawing.Size(63, 13);
            this.lblUserName2.TabIndex = 6;
            this.lblUserName2.Text = "User Name:";
            // 
            // txtPassword2
            // 
            this.txtPassword2.Location = new System.Drawing.Point(158, 104);
            this.txtPassword2.Name = "txtPassword2";
            this.txtPassword2.PasswordChar = '*';
            this.txtPassword2.Size = new System.Drawing.Size(148, 20);
            this.txtPassword2.TabIndex = 5;
            // 
            // txtUser2
            // 
            this.txtUser2.Location = new System.Drawing.Point(158, 78);
            this.txtUser2.Name = "txtUser2";
            this.txtUser2.Size = new System.Drawing.Size(148, 20);
            this.txtUser2.TabIndex = 4;
            // 
            // rbSQLServerAuthentication2
            // 
            this.rbSQLServerAuthentication2.AutoSize = true;
            this.rbSQLServerAuthentication2.Location = new System.Drawing.Point(63, 55);
            this.rbSQLServerAuthentication2.Name = "rbSQLServerAuthentication2";
            this.rbSQLServerAuthentication2.Size = new System.Drawing.Size(150, 17);
            this.rbSQLServerAuthentication2.TabIndex = 3;
            this.rbSQLServerAuthentication2.Text = "SQL Server authentication";
            this.rbSQLServerAuthentication2.UseVisualStyleBackColor = true;
            // 
            // rbWindowsAuthentication2
            // 
            this.rbWindowsAuthentication2.AutoSize = true;
            this.rbWindowsAuthentication2.Checked = true;
            this.rbWindowsAuthentication2.Location = new System.Drawing.Point(63, 32);
            this.rbWindowsAuthentication2.Name = "rbWindowsAuthentication2";
            this.rbWindowsAuthentication2.Size = new System.Drawing.Size(139, 17);
            this.rbWindowsAuthentication2.TabIndex = 2;
            this.rbWindowsAuthentication2.TabStop = true;
            this.rbWindowsAuthentication2.Text = "Windows authentication";
            this.rbWindowsAuthentication2.UseVisualStyleBackColor = true;
            this.rbWindowsAuthentication2.Visible = false;
            // 
            // cboPOS
            // 
            this.cboPOS.FormattingEnabled = true;
            this.cboPOS.Location = new System.Drawing.Point(63, 4);
            this.cboPOS.Name = "cboPOS";
            this.cboPOS.Size = new System.Drawing.Size(216, 21);
            this.cboPOS.TabIndex = 1;
            this.cboPOS.Click += new System.EventHandler(this.cboPOSServer_SelectedIndexChanged);
            // 
            // lblServer2
            // 
            this.lblServer2.AutoSize = true;
            this.lblServer2.Location = new System.Drawing.Point(20, 7);
            this.lblServer2.Name = "lblServer2";
            this.lblServer2.Size = new System.Drawing.Size(41, 13);
            this.lblServer2.TabIndex = 0;
            this.lblServer2.Text = "Server:";
            // 
            // pnlServer1
            // 
            this.pnlServer1.Controls.Add(this.cmdRefreshServer1);
            this.pnlServer1.Controls.Add(this.lblDatabase1);
            this.pnlServer1.Controls.Add(this.cboFinanceDB);
            this.pnlServer1.Controls.Add(this.lblPassword1);
            this.pnlServer1.Controls.Add(this.lblUserName1);
            this.pnlServer1.Controls.Add(this.txtPassword1);
            this.pnlServer1.Controls.Add(this.txtUser1);
            this.pnlServer1.Controls.Add(this.rbSQLServerAuthentication1);
            this.pnlServer1.Controls.Add(this.rbWindowsAuthentication1);
            this.pnlServer1.Controls.Add(this.cboFinance);
            this.pnlServer1.Controls.Add(this.lblServer1);
            this.pnlServer1.Location = new System.Drawing.Point(15, 20);
            this.pnlServer1.Name = "pnlServer1";
            this.pnlServer1.Size = new System.Drawing.Size(317, 171);
            this.pnlServer1.TabIndex = 1;
            // 
            // cmdRefreshServer1
            // 
            this.cmdRefreshServer1.Location = new System.Drawing.Point(285, 4);
            this.cmdRefreshServer1.Name = "cmdRefreshServer1";
            this.cmdRefreshServer1.Size = new System.Drawing.Size(21, 23);
            this.cmdRefreshServer1.TabIndex = 10;
            this.cmdRefreshServer1.UseVisualStyleBackColor = true;
            // 
            // lblDatabase1
            // 
            this.lblDatabase1.AutoSize = true;
            this.lblDatabase1.Location = new System.Drawing.Point(5, 133);
            this.lblDatabase1.Name = "lblDatabase1";
            this.lblDatabase1.Size = new System.Drawing.Size(56, 13);
            this.lblDatabase1.TabIndex = 9;
            this.lblDatabase1.Text = "Database:";
            // 
            // cboFinanceDB
            // 
            this.cboFinanceDB.FormattingEnabled = true;
            this.cboFinanceDB.Location = new System.Drawing.Point(63, 130);
            this.cboFinanceDB.Name = "cboFinanceDB";
            this.cboFinanceDB.Size = new System.Drawing.Size(243, 21);
            this.cboFinanceDB.TabIndex = 8;
            this.cboFinanceDB.Click += new System.EventHandler(this.cboFinanceDB_Click);
            // 
            // lblPassword1
            // 
            this.lblPassword1.AutoSize = true;
            this.lblPassword1.Location = new System.Drawing.Point(96, 107);
            this.lblPassword1.Name = "lblPassword1";
            this.lblPassword1.Size = new System.Drawing.Size(56, 13);
            this.lblPassword1.TabIndex = 7;
            this.lblPassword1.Text = "Password:";
            // 
            // lblUserName1
            // 
            this.lblUserName1.AutoSize = true;
            this.lblUserName1.Location = new System.Drawing.Point(89, 81);
            this.lblUserName1.Name = "lblUserName1";
            this.lblUserName1.Size = new System.Drawing.Size(63, 13);
            this.lblUserName1.TabIndex = 6;
            this.lblUserName1.Text = "User Name:";
            // 
            // txtPassword1
            // 
            this.txtPassword1.Location = new System.Drawing.Point(158, 104);
            this.txtPassword1.Name = "txtPassword1";
            this.txtPassword1.PasswordChar = '*';
            this.txtPassword1.Size = new System.Drawing.Size(148, 20);
            this.txtPassword1.TabIndex = 5;
            // 
            // txtUser1
            // 
            this.txtUser1.Location = new System.Drawing.Point(158, 78);
            this.txtUser1.Name = "txtUser1";
            this.txtUser1.Size = new System.Drawing.Size(148, 20);
            this.txtUser1.TabIndex = 4;
            // 
            // rbSQLServerAuthentication1
            // 
            this.rbSQLServerAuthentication1.AutoSize = true;
            this.rbSQLServerAuthentication1.Location = new System.Drawing.Point(63, 55);
            this.rbSQLServerAuthentication1.Name = "rbSQLServerAuthentication1";
            this.rbSQLServerAuthentication1.Size = new System.Drawing.Size(150, 17);
            this.rbSQLServerAuthentication1.TabIndex = 3;
            this.rbSQLServerAuthentication1.Text = "SQL Server authentication";
            this.rbSQLServerAuthentication1.UseVisualStyleBackColor = true;
            // 
            // rbWindowsAuthentication1
            // 
            this.rbWindowsAuthentication1.AutoSize = true;
            this.rbWindowsAuthentication1.Checked = true;
            this.rbWindowsAuthentication1.Location = new System.Drawing.Point(63, 32);
            this.rbWindowsAuthentication1.Name = "rbWindowsAuthentication1";
            this.rbWindowsAuthentication1.Size = new System.Drawing.Size(139, 17);
            this.rbWindowsAuthentication1.TabIndex = 2;
            this.rbWindowsAuthentication1.TabStop = true;
            this.rbWindowsAuthentication1.Text = "Windows authentication";
            this.rbWindowsAuthentication1.UseVisualStyleBackColor = true;
            this.rbWindowsAuthentication1.Visible = false;
            // 
            // cboFinance
            // 
            this.cboFinance.FormattingEnabled = true;
            this.cboFinance.Location = new System.Drawing.Point(63, 4);
            this.cboFinance.Name = "cboFinance";
            this.cboFinance.Size = new System.Drawing.Size(216, 21);
            this.cboFinance.TabIndex = 1;
            this.cboFinance.Click += new System.EventHandler(this.cboFinanceServer_SelectedIndexChanged);
            // 
            // lblServer1
            // 
            this.lblServer1.AutoSize = true;
            this.lblServer1.Location = new System.Drawing.Point(20, 7);
            this.lblServer1.Name = "lblServer1";
            this.lblServer1.Size = new System.Drawing.Size(41, 13);
            this.lblServer1.TabIndex = 0;
            this.lblServer1.Text = "Server:";
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(675, 156);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(82, 23);
            this.btnLogin.TabIndex = 3;
            this.btnLogin.Text = "Login";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(81, 22);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(577, 23);
            this.progressBar1.TabIndex = 1;
            // 
            // chkProgress
            // 
            this.chkProgress.FormattingEnabled = true;
            this.chkProgress.Location = new System.Drawing.Point(3, 64);
            this.chkProgress.Name = "chkProgress";
            this.chkProgress.Size = new System.Drawing.Size(715, 154);
            this.chkProgress.TabIndex = 0;
            // 
            // ilIcons
            // 
            this.ilIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilIcons.ImageStream")));
            this.ilIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.ilIcons.Images.SetKeyName(0, "Microsoft.SqlServer.Management.SqlManagerUI.Images.storedproc.ico");
            this.ilIcons.Images.SetKeyName(1, "Microsoft.SqlServer.Management.SqlManagerUI.Images.table.ico");
            this.ilIcons.Images.SetKeyName(2, "Microsoft.SqlServer.Management.SqlManagerUI.Images.udf.ico");
            this.ilIcons.Images.SetKeyName(3, "Microsoft.SqlServer.Management.SqlManagerUI.Images.view.ico");
            this.ilIcons.Images.SetKeyName(4, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.application_role.i" +
        "co");
            this.ilIcons.Images.SetKeyName(5, "Microsoft.SqlServer.Management.SqlMgmt.Images.database_roles_16x.ico");
            this.ilIcons.Images.SetKeyName(6, "Microsoft.SqlServer.Management.SqlManagerUI.Images.default.ico");
            this.ilIcons.Images.SetKeyName(7, "Microsoft.SqlServer.Management.SqlMgmt.Images.full_text_catalog.ico");
            this.ilIcons.Images.SetKeyName(8, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.message_type.ico");
            this.ilIcons.Images.SetKeyName(9, "Microsoft.SqlServer.Management.SqlManagerUI.Images.partition_function.ico");
            this.ilIcons.Images.SetKeyName(10, "Microsoft.SqlServer.Management.SqlManagerUI.Images.partition_scheme.ico");
            this.ilIcons.Images.SetKeyName(11, "Microsoft.VisualStudio.DataTools.Default.ico");
            this.ilIcons.Images.SetKeyName(12, "Microsoft.SqlServer.Management.SqlManagerUI.Images.plan_guide_enabled.ico");
            this.ilIcons.Images.SetKeyName(13, "Microsoft.SqlServer.Management.SqlManagerUI.Images.rule.ico");
            this.ilIcons.Images.SetKeyName(14, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.schema.ico");
            this.ilIcons.Images.SetKeyName(15, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.remote_service.ico" +
        "");
            this.ilIcons.Images.SetKeyName(16, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.contract.ico");
            this.ilIcons.Images.SetKeyName(17, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.queue.ico");
            this.ilIcons.Images.SetKeyName(18, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.route.ico");
            this.ilIcons.Images.SetKeyName(19, "Microsoft.VisualStudio.DataTools.Assembly.ico");
            this.ilIcons.Images.SetKeyName(20, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.database_trigger.i" +
        "co");
            this.ilIcons.Images.SetKeyName(21, "Microsoft.SqlServer.Management.SqlManagerUI.Images.synonym.ico");
            this.ilIcons.Images.SetKeyName(22, "Microsoft.SqlServer.Management.SqlMgmt.Images.user_16x.ico");
            this.ilIcons.Images.SetKeyName(23, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.aggregate_valued_f" +
        "unction.ico");
            this.ilIcons.Images.SetKeyName(24, "Microsoft.SqlServer.Management.SqlMgmt.Images.user_defined_data_type.ico");
            this.ilIcons.Images.SetKeyName(25, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.user_defined_objec" +
        "t_type.ico");
            this.ilIcons.Images.SetKeyName(26, "Microsoft.SqlServer.Management.SqlManagerUI.Images.XML_schemas_16x.ico");
            this.ilIcons.Images.SetKeyName(27, "Microsoft.VisualStudio.DataTools.Trigger.ico");
            this.ilIcons.Images.SetKeyName(28, "Microsoft.VisualStudio.DataTools.Index.ico");
            this.ilIcons.Images.SetKeyName(29, "Microsoft.SqlServer.Management.UI.VSIntegration.ObjectExplorer.broker_service.ico" +
        "");
            this.ilIcons.Images.SetKeyName(30, "Microsoft.SqlServer.Management.SqlManagerUI.Images.table_valued_function.ico");
            this.ilIcons.Images.SetKeyName(31, "Microsoft.SqlServer.Management.SqlManagerUI.Images.scalar_valued_function.ico");
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1030, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            // 
            // MainRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 510);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainRun";
            this.Text = "POS Data Exchange";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainRun_FormClosed);
            this.Load += new System.EventHandler(this.ObjectCompare_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.pnlSettings.ResumeLayout(false);
            this.pnlSettings.PerformLayout();
            this.pnlServer2.ResumeLayout(false);
            this.pnlServer2.PerformLayout();
            this.pnlServer1.ResumeLayout(false);
            this.pnlServer1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.ImageList ilIcons;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.Button btnSendInvoice;
        private System.Windows.Forms.Panel pnlSettings;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Panel pnlServer2;
        private System.Windows.Forms.Button cmdRefreshServer2;
        private System.Windows.Forms.Label lblDatabase2;
        private System.Windows.Forms.ComboBox cboPOSDB;
        private System.Windows.Forms.Label lblPassword2;
        private System.Windows.Forms.Label lblUserName2;
        private System.Windows.Forms.TextBox txtPassword2;
        private System.Windows.Forms.TextBox txtUser2;
        private System.Windows.Forms.RadioButton rbSQLServerAuthentication2;
        private System.Windows.Forms.RadioButton rbWindowsAuthentication2;
        private System.Windows.Forms.ComboBox cboPOS;
        private System.Windows.Forms.Label lblServer2;
        private System.Windows.Forms.Panel pnlServer1;
        private System.Windows.Forms.Button cmdRefreshServer1;
        private System.Windows.Forms.Label lblDatabase1;
        private System.Windows.Forms.ComboBox cboFinanceDB;
        private System.Windows.Forms.Label lblPassword1;
        private System.Windows.Forms.Label lblUserName1;
        private System.Windows.Forms.TextBox txtPassword1;
        private System.Windows.Forms.TextBox txtUser1;
        private System.Windows.Forms.RadioButton rbSQLServerAuthentication1;
        private System.Windows.Forms.RadioButton rbWindowsAuthentication1;
        private System.Windows.Forms.ComboBox cboFinance;
        private System.Windows.Forms.Label lblServer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox chkProgress;
        private System.Windows.Forms.Button btnSendCust;
        private System.Windows.Forms.Button btnDelReturnInv;
        private System.Windows.Forms.Button btnBackupFinance;
        private System.Windows.Forms.Button BtnBackupPOS;
        private System.Windows.Forms.Button btnSendInvoiceReturn;
        private System.Windows.Forms.Button btnDelInvoices;
        private System.Windows.Forms.Button btnGetItems;
        private System.Windows.Forms.Button btnGetSupplier;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label lblcboWareHouse;
        private System.Windows.Forms.ComboBox cboWareHouse;
        private System.Windows.Forms.Button btnGetUsers;
        private System.Windows.Forms.Button btnGetItemPrice;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboBranch;
        private System.Windows.Forms.Button btnSalesResymc;
        private System.Windows.Forms.Button btnSalesReturnSync;
        private System.Windows.Forms.Button btnPurchInvRsync;
        private System.Windows.Forms.Button btnPurchInvSync;
        private System.Windows.Forms.Button btnSendInvHistory;
        private System.Windows.Forms.Button btnSendInvRetHistory;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDeliveryWeb;
        private System.Windows.Forms.CheckBox chkItemDesc;
        private System.Windows.Forms.Button btnSendOrder;
    }
}