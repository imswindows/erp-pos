﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace POS_Registration
{
    class Program
    {
        static void Main(string[] args)
        {
            var macAddr = (from nic in NetworkInterface.GetAllNetworkInterfaces()
                           where nic.OperationalStatus == OperationalStatus.Up
                           select nic.GetPhysicalAddress().ToString()).FirstOrDefault();
            const string userRoot = "HKEY_CURRENT_USER";
            const string subkey = "Microsfot.Calendar";
            const string keyName = userRoot + "\\" + subkey;
            Console.WriteLine("Registration Started...");
            for (int i = 0; i <= 100; i++)
            {
                //Console.WriteLine(i.ToString() + " %");
                string Status = i.ToString() + " %";
                Console.WriteLine(Status);
                Console.CursorTop = Console.CursorTop - 1;
                Thread.Sleep(50);
            }
            Registry.SetValue(keyName, "Format", macAddr);
            Registry.SetValue(keyName, "Sample", DateTime.Today.AddDays(45));
            Console.WriteLine("Registration Done. Press Any Key To Exit");
            Console.ReadKey(true);
        }
    }
}
